﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Common.Data.SqlServer
{
    class SQLGenerator : SQLGeneratorBase
    {
        public SQLGenerator(Type objType)
            : base(objType)
        { }

        public override string GeneratorCheckSQL()
        {
            return FormatSqlServerSQL(base.GeneratorCheckSQL());
        }

        private string FormatSqlServerSQL(string sql)
        {
            return sql.Replace("(SELECTNOLOCK)", "WITH(NOLOCK)");
        }

        public override string GeneratorCountSQL(WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorCountSQL(critical));
        }

        public override string GeneratorQuerySQL(WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorQuerySQL(critical));
        }

        public override string GeneratorSelectSQL()
        {
            return FormatSqlServerSQL(base.GeneratorSelectSQL());
        }

        public override string GeneratorSelectSQL(string fieldName, WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorSelectSQL(fieldName, critical));
        }

        public override string GeneratorTopSQL(int count, string orderName, OrderDirection direction, WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorTopSQL(count, orderName, direction, critical));
        }

        protected override string ParamPrefix
        {
            get { return "@"; }
        }
    }
}
