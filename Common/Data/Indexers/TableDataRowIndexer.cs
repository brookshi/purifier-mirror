﻿using System;
using System.Data;

namespace HOYI.Common.Data.Indexers
{
    public class TableDataRowIndexer : DataIndexer
    {
        DataRow row;

        internal TableDataRowIndexer(DataRow row)
        {
            if (row == null)
                throw new ArgumentNullException("row");
            if (row.Table == null)
                throw new ArgumentException("row must belong to a table");
            this.row = row;
        }

        protected override object GetValue(string name)
        {
            if (row.Table.Columns.Contains(name))
                return row[name];
            else
                throw new ArgumentException("Can't find '" + name + "' in table.");
        }

        public override bool Contains(string name)
        {
            return row.Table.Columns.Contains(name);
        }
    }
}
