﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HOYI.Common.Data
{
    public class DataObjVersionException : Exception
    {
        internal DataObjVersionException(string message, Exception innerExp)
            : base(message, innerExp)
        { }
    }
}
