﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;
namespace HOYI.Common.Data.Indexers
{
    public class JObjectIndexer : DataIndexer
    {
        IDictionary<string, JToken> m_JObj;
        public JObjectIndexer(JObject jObj)
        {
            m_JObj = (IDictionary<string, JToken>)jObj;
        }

        protected override object GetValue(string name)
        {
            return m_JObj[name].GetValueObj();
        }

        public override bool Contains(string name)
        {

            return m_JObj.ContainsKey(name);
        }
    }
}
