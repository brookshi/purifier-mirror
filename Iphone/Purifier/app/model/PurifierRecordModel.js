/*
 * File: app/model/PurifierRecordModel.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.3.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.3.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Purifier.model.PurifierRecordModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    config: {
        fields: [
            {
                name: 'Id'
            },
            {
                name: 'PurifierID'
            },
            {
                name: 'Ip'
            },
            {
                name: 'Pm25'
            },
            {
                name: 'Temperature'
            },
            {
                name: 'Humidity'
            },
            {
                dateFormat: 'Y-m-d',
                name: 'CreateDate'
            },
            {
                name: 'Flag'
            },
            {
                name: 'MAC'
            },
            {
                name: 'SNCode'
            },
            {
                name: 'Name'
            }
        ]
    }
});