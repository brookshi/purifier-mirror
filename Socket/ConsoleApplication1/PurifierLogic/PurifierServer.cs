﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOYI.Common.Data;
using SuperSocket.Facility.Protocol;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Config;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.Common;

namespace PurifierLogic
{
    //[CommandFilter]class MyReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    public class ReceiveFilter : FixedHeaderReceiveFilter<BinaryRequestInfo>
    {
        public ReceiveFilter() : base(1)
        {

        }

        protected override int GetBodyLengthFromHeader(byte[] header, int offset, int length)
        {
            return 27;
        }

        protected override BinaryRequestInfo ResolveRequestInfo(ArraySegment<byte> header, byte[] bodyBuffer, int offset, int length)
        {
            return new BinaryRequestInfo(Encoding.ASCII.GetString(header.Array, header.Offset, 1), bodyBuffer.CloneRange(offset, length));
        }
    }
    public class PurifierServer : AppServer<PurifierSession, BinaryRequestInfo>
    {
        public PurifierServer()
            : base(new DefaultReceiveFilterFactory<ReceiveFilter, BinaryRequestInfo>())// base(new CountSpliterReceiveFilterFactory(0x2b, 17))//
        {
        }

        protected override bool Setup(IRootConfig rootConfig, IServerConfig config)
        {
            PersistenceHelper.InitDefault(new DBSetting("HOYI"));
            return base.Setup(rootConfig, config);
        }

        protected override void OnStarted()
        {
            Logger.InfoFormat("===server is started in {0}.", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            base.OnStarted();
        }

        protected override void OnStopped()
        {
            Logger.InfoFormat("===server is stopped in {0}.", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            base.OnStopped();
        }
    }
}
