﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "UserCity")]
    public abstract class UserCity : BaseDataObj
    {
        public Guid Id { get; private set; }
        public Guid UserId { get; private set; }
        public string cityname { get; private set; }
        public bool isqm25in { get; private set; }
        public int sortid { get; set; }

        public DateTime CreatedDate
        {
            get;
            protected set;
        }

        protected UserCity()
        {
 
        }

        public static List<UserCity> GetBindsForUser(Guid userId)
        {
            WhereCriterion wc = new WhereCriterion("UserId", OperatorFlag.Equal, userId);
            return PersistenceHelper.Default.Query<UserCity>(wc);
        }

        public static void DeleteBindsForUser(Guid userId)
        {
            WhereCriterion wc = new WhereCriterion("UserId", OperatorFlag.Equal, userId);
            PersistenceHelper.Default.Delete<UserCity>(wc);
        }

        public static void DeleteBindsForUserName(String userName)
        {
            Account account = Account.SearchUser(userName);
            DeleteBindsForUser(account.Id);
        }

        public static int BindingCityForUserName(String userName, List<TargetCity> targetCitys)
        {
            Account account = Account.SearchUser(userName);
            if (account == null)
                return -1;
            BindingCityForUser(account.Id, targetCitys);
            return 1;
        }

        public static void BindingCityForUser(Guid userId, List<TargetCity> targetCitys)
        {
            DeleteBindsForUser(userId);
            Account user = Account.Read(userId);
            IList<UserCity> cityList = new List<UserCity>();
            for (int i = 0; i < targetCitys.Count; i++)
            {
                var up = CreateDataObj<UserCity>();
                up.UserId = user.Id;
                up.cityname = targetCitys[i].cityname;
                up.isqm25in = targetCitys[i].isqm25in;
                up.sortid = targetCitys[i].sortid;
                up.CreatedDate = DateTime.Now;
                cityList.Add(up);
            }
            PersistenceHelper.Default.Save<UserCity>(cityList);
        }
    }

    public class TargetCity
    {
        public int id;
        public string cityname;
        public bool isqm25in;
	    public int sortid;
    }
}
