﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;
using HOYI.Common;

namespace HOYI.Data
{
    public abstract class PurifierBase : BaseDataObj
    {
        private static Regex s_RegEx = new Regex("[0-9A-F]{12}", RegexOptions.IgnoreCase | RegexOptions.Singleline);
        protected PurifierBase()
        {
            CreatedTime = PersistenceHelper.MinDateTime;
        }

        public DateTime CreatedTime
        {
            get;
            protected set;
        }

        public string MAC
        {
            get;
            protected set;
        }

        protected abstract void SetPurifierId(long purifierId);

        protected override void OnBeforeSave()
        {
            this.InitPurifierId();
            if (!Tools.IsValid(CreatedTime)) CreatedTime = DateTime.Now;
        }

        internal protected void InitPurifierId()
        {
            this.SetPurifierId(this.ParseId());
        }

        private long ParseId()
        {
            return CalcPurifierId(MAC);
        }

        private static void VerifyMAC(string mac)
        {
            if (string.IsNullOrEmpty(mac))
                throw new InvalidOperationException("缺失MAC");
            if (!Regex.IsMatch(mac.Replace("-", ""), "[0-9A-F]{12}", RegexOptions.IgnoreCase | RegexOptions.Singleline))
                throw new InvalidOperationException("无效的MAC地址：" + mac);
        }

        public static long CalcPurifierId(string mac)
        {
            VerifyMAC(mac);
            return long.Parse(mac.Replace("-", ""), NumberStyles.HexNumber);
        }
    }
}
