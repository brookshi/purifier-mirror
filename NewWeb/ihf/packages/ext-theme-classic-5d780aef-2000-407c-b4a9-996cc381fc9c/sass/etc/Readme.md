# ext-theme-classic-5d780aef-2000-407c-b4a9-996cc381fc9c/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-classic-5d780aef-2000-407c-b4a9-996cc381fc9c/sass/etc"`, these files
need to be used explicitly.
