﻿using System;
using System.Data;
using System.Data.Common;
using Oracle.DataAccess.Client;

namespace HOYI.Common.Data.Oracle
{

    /// <summary>
    /// Install ORACLE 'ODP.NET'
    /// Guid Map Oracle Type: CHAR(36) OR NVARCHAR2(36)
    /// Bool Map Oracle Type: NUMBER(1)
    /// </summary>
    public class OracleDataObjPersister : BaseDataObjPersister
    {
        protected override DbProviderFactory DbFactory { get { return OracleClientFactory.Instance; } }

        protected override void MappingTypeToDbType(DbParameter dbParam, Type type)
        {
            dbParam.DbType = GetDbTypeMapping(type);
            if (dbParam.DbType == DbType.Boolean)
            {
                var oraPm = (dbParam as OracleParameter);
                oraPm.OracleDbType = OracleDbType.Decimal;
                oraPm.Size = 1;
                oraPm.Precision = 1;
                oraPm.Scale = 0;
            }
        }

        protected override void SetDbParameterValue(DbParameter dbParam, object value)
        {
            if (dbParam.DbType == DbType.String || dbParam.DbType == DbType.StringFixedLength)
                dbParam.Value = (value != null ? value.ToString() : "");
            else
                base.SetDbParameterValue(dbParam, value);
        }

        protected override void SetCommandTimeout(IDbCommand command)
        {
            if (command.CommandTimeout < 60)
                command.CommandTimeout = 60;
        }

        protected override DbType GetDbTypeMapping(Type type)
        {
            if (type == typeof(string) || type == typeof(char))
                return DbType.String;
            else if (type == typeof(int))
                return DbType.Int32;
            else if (type == typeof(long))
                return DbType.Int64;
            else if (type == typeof(short))
                return DbType.Int16;
            else if (type == typeof(double))
                return DbType.Decimal;
            else if (type == typeof(DateTime))
                return DbType.DateTime;
            else if (type == typeof(float))
                return DbType.Single;
            else if (type == typeof(byte))
                return DbType.Byte;
            else if (type == typeof(bool))
                return DbType.Boolean;
            else if (type == typeof(UInt32))
                return DbType.UInt32;
            else if (type == typeof(UInt16))
                return DbType.UInt16;
            else if (type == typeof(UInt64))
                return DbType.UInt64;
            else if (type == typeof(sbyte))
                return DbType.SByte;
            else if (type == typeof(Guid))
                return DbType.String;
            else if (type == typeof(byte[]))
                return DbType.Binary;
            else if (typeof(Enum).IsAssignableFrom(type))
                return DbType.Int32;
            else
                return DbType.Object;
        }

        protected override int HandleInsertException(Exception ex, BaseDataObj dataObj)
        {
            if (ex is OracleException)
                return base.HandleInsertException(ex, dataObj);
            else
                return 0;
        }

        protected override SQLGeneratorBase CreateSQLGenerator(Type dataObjType)
        {
            return new SQLGenerator(dataObjType);
        }
    }
}
