﻿using HOYI.Common.Data;
using HOYI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestOracle
{

    [DataObj(DBName = "", PrimaryKeyName = "ObjId", TableName = "[OBJKV]")]
    public class ObjKV : BaseDataObj
    {
        public Guid ObjId { get; private set; }
        public string ObjValue { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Age { get; set; }
        public double Length { get; set; }
        public bool IsAdmin
        {
            get;
            set;
        }
        public void Save()
        {
            if (CreatedDate.Year < 2000)
                CreatedDate = PersistenceHelper.MinDateTime;

            PersistenceHelper.Default.Save(this);
        }
    }


    class Program
    {

        static void Main(string[] args)
        {
            PersistenceHelper.InitDefault(new DBSetting("ORA"));
            try
            {

                ObjKV kv = new ObjKV() { ObjValue = DateTime.Now.ToString(), IsAdmin = true, Age = 28, Length = 178.91 };
                kv.Save();

               var na = Account.Create("Chunfeng", "Chunfeng524@163.com");
                na.SetPassword("jscab223");
                na.Phone = "18924602595";
                na.City = "SZ";
                na.Address = "Qianhai SZ GD CN";
                na.Name = "jsc";
                na.Province = "GD";
                na.Save(true);
                var a = Account.Read(na.Id);
                var isSameName = a.Name == na.Name;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            Console.Read();
        }
    }
}
