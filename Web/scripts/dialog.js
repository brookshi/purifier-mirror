﻿/// <reference path="jquery-vsdoc.js" />
if (!window.PSWeb)
    window.PSWeb = {};
HOYI.uiDialog = function (content, pps) {
    var overlay, dialog, jcontent, capLbc, jCap = null, that = this;
    function init(own) {
        overlay = HOYI.overlay;

        var jDoc = overlay ? overlay.jDoc : $(window.document);
        if (!overlay || overlay == null || overlay.length == 0) {
            HOYI.overlay = overlay = $("<div class=\"uiwidgetoverlay\" id=\"uioverlay\"></div>").appendTo(document.forms[0] || document.body);
            var resizeOverlay = null;
            if (HOYI.isIE6())
                resizeOverlay = function () { overlay.css({ "width": jDoc.width(), "height": window.screen.availHeight }); };
            else if (HOYI.isIEx(10))
                resizeOverlay = function () { overlay.css({ "width": jDoc.width(), "height": jDoc[0].documentElement.scrollHeight || jDoc.height() }); };
            else if (HOYI.isIEx(8))
                resizeOverlay = function () { overlay.css({ "height": jDoc.height() - 4, "width": jDoc.width() - 4 }); };
            else
                resizeOverlay = function () { overlay.css({ "width": jDoc.width(), "height": jDoc.height() }); };
            overlay.jDoc = jDoc;
            overlay.resize = resizeOverlay;
            $(window).bind("resize", resizeOverlay);
            overlay.click(function () { var adg = HOYI.uiDialog.activeDialog; if (adg) adg.dialog.focus(); });
        }
        if (!HOYI.uiDialog.dialogs)
            HOYI.uiDialog.dialogs = [];
        if (!HOYI.uiDialog.count)
            HOYI.uiDialog.count = 1;
        else
            HOYI.uiDialog.count = HOYI.uiDialog.count + 1;
        dialog = $("<div class=\"uidialog\" tabindex=\"-1\" id=\"uidialog" + HOYI.uiDialog.count + "\"><div class=\"uidialogtitlebar\"><div class=\"udttbc\"><a href=\"javascript:void(0);\"><span></span></a></div><span class='udttl'>&nbsp;</span></div><div class=\"uidialogcontent\"></div></div>").appendTo(document.body);
        dialog.css("display", "none");
        dialog.keydown(own, function (e) {
            if (e.keyCode == 27) {
                if (e.data.escAlloways || HOYI.uiDialog.activeDialog && HOYI.uiDialog.activeDialog == e.data)
                    e.data.close();
            }
        });
        var cpb = getCapBar();
        var mvObj = {
            mvStart: HOYI.touch.support() ? "touchstart" : "mousedown",
            mvMove: HOYI.touch.support() ? "touchmove" : "mousemove",
            mvEnd: HOYI.touch.support() ? "touchend" : "mouseup",
            down: function (e) {
                if (e.target.tagName == "A" || (e.target.tagName == "SPAN" && e.target.parentNode.tagName == "A")) return;
                if (cpb[0].setCapture)
                    cpb[0].setCapture();
                else if (window.captureEvents)
                    window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                cpb[0].msIsDwn = true;
                cpb[0].dgPos = dialog.offset();
                cpb[0].msPos = { "x": e.pageX, "y": e.pageY };
                jDoc.bind("selectstart", $cevent);
                ($.browser.webkit ? $(document.body) : cpb).bind(mvObj.mvMove, mvObj.move);
                $(document.body).bind(mvObj.mvEnd, mvObj.up).trigger("click");
                return $cevent(e);
            }, up: function (e) {
                jDoc.unbind("selectstart", $cevent);
                cpb[0].msIsDwn = false;
                cpb.css("cursor", "");
                if (cpb[0].releaseCapture)
                    cpb[0].releaseCapture();
                else if (window.captureEvents)
                    window.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
                ($.browser.webkit ? $(document.body) : cpb).unbind(mvObj.mvMove, mvObj.move);
                $(document.body).unbind(mvObj.mvEnd, mvObj.up);
            }, move: function (e) {
                if (cpb[0].msIsDwn) {
                    var x = e.pageX - cpb[0].msPos.x;
                    var y = e.pageY - cpb[0].msPos.y;
                    cpb.css("cursor", "move");
                    setPosition({ "left": cpb[0].dgPos.left + x, "top": cpb[0].dgPos.top + y }, true);
                    return $cevent(e);
                }
            }
        };
        cpb.bind(mvObj.mvStart, mvObj.down);
        cpb.click($cevent);
        var container = $(".uidialogcontent", dialog);
        $("a", cpb).click(function (e) { own.close(); });
        jcontent = $(content).appendTo(container).css("display", "inline-block");
        HOYI.initInputBtn(dialog, true);
        if (pps) {
            if (pps.width && pps.width > 20) {
                dialog.width(pps.width);
                container.css({ "margin": "0.5em auto", width: pps.width - 20 });
            }
            if (pps.height) {
                dialog.height(pps.height);
            }
            var caption = pps.caption;
            caption && setCaptionInner(caption);
        }
    };
    function getCapBar() {
        if (!jCap) {
            jCap = $(".uidialogtitlebar", dialog);
            if (!jCap || !jCap.length)
                jCap = null;
        }
        return jCap;
    };
    function getCapLabel() {
        if (!capLbc) {
            var cpb = getCapBar();
            if (cpb) {
                capLbc = $("span.udttl", cpb);
                if (!capLbc || !capLbc.length)
                    capLbc = null;
            }
        }
        return capLbc;
    };
    function setCaptionInner(caption) {
        var cplb = getCapLabel();
        cplb && ((caption && caption.length) ? cplb.text(caption) : cplb.html("&nbsp;"));
    };
    this.setCapLabPos = function (p) {
        var cplb = getCapLabel();
        cplb && cplb.css("marginLeft", p);
    };
    this.setCloseBarPos = function (p) {
        var cpb = getCapBar();
        cpb && $("div.udttbc", cpb).css("marginRight", p);
    };
    this.setDialog = function (width, height, color) {
        var cn = $(".uidialogcontent", this.dialog);
        if (width) {
            this.dialog.css({ "width": width });
            cn.css({ margin: "0.5em auto", textAlign: "center", "width": width - 30 });
        }
        if (color) {
            this.dialog.css("backgroundColor", color);
        }
        if (height) {
            cn.css("height", height);
        }
    };
    function show() {
        if (this.dialog.css("display") != "none")
            return;
        if (!HOYI.uiDialog.baseZindex)
            HOYI.uiDialog.baseZindex = 1002;
        else
            HOYI.uiDialog.baseZindex = HOYI.uiDialog.baseZindex + 2;
        var jDoc = overlay.jDoc || $(window.document);
        overlay.attr("zIndex", HOYI.uiDialog.baseZindex - 1);
       
        if (this.overlay.attr("overall") != "y")
            overlay.css({ "display": "block", "zIndex": HOYI.uiDialog.baseZindex - 1 });
      
        dialog.css("zIndex", HOYI.uiDialog.baseZindex);
        if (HOYI.uiDialog.activeDialog) {
            if (HOYI.isIE6())
                HOYI.uiDialog.activeDialog.dialog.find("select").css("visibility", "hidden");
            HOYI.uiDialog.dialogs.push(HOYI.uiDialog.activeDialog);
        }
        var rszOvFlag = !(HOYI.progressBar == this && HOYI.progressBar.isFirst);
        HOYI.uiDialog.activeDialog = this;
        dialog.css({ left: 0, top: 0 });
        dialog.show();
        var pose = arguments.length > 0 && arguments[0] ? arguments[0] : null;
        this.rePosDefault(pose);
        if (!pose && !HOYI.touch.support()) {
            $(window).scroll(onDocScroll);
            $(window).bind("resize", onDocScroll);
        }
        rszOvFlag && overlay.resize && overlay.resize();
        dialog.focus();
    };
    function getDefaultPosition() {
        var isTD = HOYI.isTouchDevice(), wH = 0, wW = 0;
        var jWin = $(window);
        if (isTD) {
            var isVer = Math.abs(window.orientation) == 90;
            wH = isVer ? window.screen.availWidth : window.screen.availHeight;
            wW = isVer ? window.screen.availHeight : window.screen.availWidth;
        }
        else {
            wH = Math.min(jWin.height(), window.screen.availHeight);
            wW = Math.min(jWin.width(), window.screen.availWidth);
        }
        var dH = Math.max(dialog.height(), 30);
        var dW = Math.max(dialog.width(), 100);
        var sL = jWin.scrollLeft();
        var sT = jWin.scrollTop();
        var top = (dH > wH) ? sT : (wH - dH) / 2;
        var left = (dW > wW) ? sL : (wW - dW) / 2;
        top += sT;
        left += sL;
        top = top * 0.62;
        left = Math.max(0, left);
        top = Math.max(0, top);
        return { "left": left, "top": top };
    };
    function limitPosition(pos) {
        var dH = dialog.outerHeight();
        var dW = dialog.outerWidth();
        var oW = overlay.width();
        var oH = overlay.height()
        pos.left = Math.min(pos.left, oW - dW);
        pos.top = Math.min(pos.top, oH - dH);
        if ((dW + pos.left) >= oW)
            pos.left = oW - dW - 5;
        if ((dH + pos.top) >= oH)
            pos.top = oH - dH - 5;
    };
    function setPosition(pos, ise) {
        limitPosition(pos);
        dialog.css({ left: pos.left || undefined, top: pos.top || undefined });
        that.onPosDlg && that.onPosDlg(pos, !!ise);
    };
    function onDocScroll(evt) {
        if (dialog.css("display") != "none") {
            function setToDefaultPos() {
                setPosition(getDefaultPosition(), true);
            };
            window.setTimeout(setToDefaultPos, 0);
            window.setTimeout(setToDefaultPos, 100);
        }
    };
    function close() {
        if (dialog.css("display") == "none")
            return;
        this.onClosing && $.type(this.onClosing) == "function" && this.onClosing();
        $(window).unbind("scroll", onDocScroll);
        $(window).unbind("resize", onDocScroll);
        dialog.toggle(false);
        var fsObj = null;
        if (HOYI.uiDialog.dialogs.length == 0) {
            overlay.toggle(false);
            HOYI.uiDialog.activeDialog = null;
            fsObj = window;
        }
        else {
            var dlgs = HOYI.uiDialog.dialogs, popd = null;
            for (var d = 0; d < HOYI.uiDialog.dialogs.length; d++) {
                if (dlgs[d] == this) {
                    popd = this;
                    dlgs.splice(d, 1);
                }
            }
            if (!popd) popd = dlgs.pop();
            if (popd != this) {
                HOYI.uiDialog.activeDialog = popd;
                HOYI.uiDialog.activeDialog.dialog.css("zIndex", HOYI.uiDialog.baseZindex);
            }
            else {
                var adg = HOYI.uiDialog.activeDialog;
                adg.dialog.css("zIndex", HOYI.uiDialog.baseZindex);
            }
            if (HOYI.isIE6())
                popd.dialog.find("select").css("visibility", "visible");
            fsObj = popd.dialog;
        }
        if (this.onClosed && $.type(this.onClosed) == "function")
            this.onClosed();
        $(document).trigger("click");
        fsObj.focus();
        return false;
    };
    function bindData(data) {
        window.HOYI.initDataContainer(data, dialog[0]);
    };
    function clearData() {
        window.HOYI.initDataContainer(null, dialog[0]);
    };
    function findInnerObj(q) {
        return $(q, jcontent);
    };
    init(this);
    this.setCaption = setCaptionInner;
    this.show = show;
    this.close = close;
    this.dialog = dialog;
    this.overlay = overlay;
    this.bindData = bindData;
    this.clearData = clearData;
    this.escAlloways = false;
    this.rePosDefault = function (rfe) {
        var pos = rfe ? null : getDefaultPosition();
        if (rfe) {
            var anchor = $(rfe);
            pos = anchor.offset();
            pos.top += anchor.outerHeight();
        }
        setPosition(pos);
    };
    this.findInnerObj = findInnerObj;
    this.onPosDlg = function (pos, isEvent) { };
};

HOYI.messager = function (opt) {
    var btnHtml = '<input type="button" class="button" style="margin-right: 0.5em;" />';
    var topctn = $("<div id=\"AL_" + (new Date()).getTime() + "\" class=\"alertdialog\"></div>");
    var msgctn = $("<div class='alertmsg'></div>").css("width", "auto");
    var btnctn = $('<div style="text-align:center;">' + btnHtml + '</div>');
    var me = this;

    var yesBtn = btnctn.find("input").val($msg("Yes"));
    yesBtn.click(me, function (e) {
        var ed = e.data;
        e.data.result = false;
        e.data.close();
        ed.OPT && ed.OPT.YF && ed.OPT.YF();
    });

    var noBtn = $(btnHtml).css("display", "none").val($msg("No"));
    noBtn.appendTo(btnctn);
    noBtn.click(me, function (e) {
        var ed = e.data;
        e.data.result = true;
        e.data.close();
        ed.OPT && ed.OPT.NF && ed.OPT.NF();
    });

    var cancelBtn = $(btnHtml).css("display", "none").val($msg("Cancel"));
    cancelBtn.appendTo(btnctn);
    cancelBtn.click(me, function (e) {
        var ed = e.data;
        e.data.result = null;
        e.data.close();
        ed.OPT && ed.OPT.CF && ed.OPT.CF();
    });

    this.OPT = opt;

    msgctn.appendTo(topctn);
    btnctn.appendTo(topctn);
    HOYI.uiDialog.call(this, topctn, $msg("ProductLogoName"));
    this.dialog.css({ "height": "auto", "maxWidth": HOYI.totalWidth || (window.screen.availWidth - 60), "width": "auto" });
    msgctn.css("maxWidth", (HOYI.totalWidth || 1000) - 10);
    $(".uidialogcontent", this.dialog).css({ "minHeight": "40px" });
    function reset(opt) {
        me.OPT = opt;
        if (me.OPT && !$.isEmptyObject(me.OPT)) {
            noBtn.css("display", "");
            cancelBtn.css("display", me.OPT.CF ? "" : "none");
            yesBtn.val($msg("Yes"));
        } else {
            noBtn.css("display", "none");
            cancelBtn.css("display", "none");
            yesBtn.val($msg("OK"));
        }
    };
    this.onClosed = null;
    this.showMsg = function (msg, opt) {
        reset(opt);
        if (msg && msg.length > 0) {
            if (msg.charAt(0) == '\r' || msg.charAt(0) == '\n') msg = msg.substring(1);
            if (msg.charAt(0) == '\r' || msg.charAt(0) == '\n') msg = msg.substring(1);
        }
        else
            msg = "";
        msgctn.html((msg || "").replace(/\r\n/g, "\r").replace(/[\r\n]/g, "<br/>").fbScript());
        var ch = document.documentElement.clientHeight || document.body.clientHeight;
        msgctn.css("maxHeight", ch - 150);
        if ($.browser.msie) {
            var is6 = HOYI.isIE6();
            this.dialog.css("width", "auto");
            if (is6) topctn.css({ "width": "150px", "whiteSpace": "nowrap", "overflow": "visible" });
            var jCap = $(".uidialogtitlebar", this.dialog);
            jCap.hide();
            this.show();
            this.dialog.css("width", topctn.width() + 30);
            if (is6) topctn.css("width", "100%");
            jCap.show();
            this.rePosDefault();
            if (is6) {
                if (msgctn[0].scrollHeight > ch)
                    msgctn.css("height", ch - 150);
                else
                    msgctn.css("height", "auto");
            }
        }
        else {
            this.show();
            this.rePosDefault();
        }
        yesBtn.focus();
    };
    var _closeFun = this.close;
    this.close = function () {
        _closeFun.apply(this, arguments);
        msgctn.html("");
        this.OPT && this.OPT.onClosed && this.OPT.onClosed();
        if ($.browser.msie) this.dialog.css("width", "auto");
    };
};
$(window).ready(function() {
    var pgbar = function() {
        var barContent = $("#progressBar");
        if (!barContent || barContent == null || barContent.length == 0) {
            barContent = $("<div class=\"progressbar\"><div class=\"progressbarimg\">加&nbsp;载...</div></div>").appendTo(document.body);
        }
        HOYI.uiDialog.apply(this, [barContent, ""]);
        this.escAlloways = !$.browser.msie;
        this.isFirst = true;
        $(".uidialogtitlebar", this.dialog).css("display", "none");
        var isIE = HOYI.isIEx(8, true);
        this.dialog.addClass("uidialogprog");
        if (isIE) $(".uidialogcontent", this.dialog).addClass("forie");
        this.setText = function(text) {
            $(".progressbarimg", this.dialog).text(text);
        };
        var _show = this.show;
        var _close = this.close;
        var showcount = 0;
        var overlayZI = 0;
        this.show = function() {
            showcount++;
            _show.call(this);
            if ($.browser.msie) {
                if (HOYI.isIEx(6, true)) {
                    this.dialog.css("display", "inline");
                    this.dialog.css("display", "block");
                }
                this.rePosDefault();
            }
            overlayZI = this.overlay.css("zIndex")
            this.overlay.attr("overall", "y");
            this.overlay.css("zIndex", "9999998");
            this.overlay.css("cursor", "wait");
            this.dialog.css("zIndex", "9999999");
            this.isFirst = false;

        };
        this.close = function() {
            if ((--showcount) <= 0) {
                this.overlay.removeAttr("overall");
                this.overlay.css("cursor", "auto");
                var zi = this.overlay.attr("zIndex");
                if (zi) {
                    this.overlay.removeAttr("zIndex")
                    this.overlay.css("zIndex", zi);
                }
                _close.call(this);
            }
            if (showcount < 0)
                showcount = 0;
        };
        function getInnerObj(q) {
            return $(q, jdc);
        };
    };
    HOYI.progressBar = new pgbar();

    var tipMsg = function(ctn) {
        var tmc = $(ctn || "#tipMsg");
        var msc = null;
        var mst = null;
        var th = null;
        var sth = null;
        this.showTip = function(t, hdt) {
            if (tmc.length == 0)
                return;
            if (msc == null) {
                msc = $("<div class='tips'><a href='javascript:void(0);' class='close' style='float:right;margin-top:4px;'><span></span></a><div style='padding-left:7px;'></div></div>").css("display", "none");
                msc.appendTo(tmc);
                mst = msc.find("div");
                var msa = msc.find("a");
                msa.click(this, function(e) { e.data.hideTip("fast"); });
                if ($.browser.msie) msa.css("marginTop", "0");
                if (HOYI.isIE6()) msa.find("span").css("backgroundPosition", "0px 4px");
            }
            this.hideTip("fast", function() {
                mst.scrollLeft(0);
                mst.text(t);
                msc.fadeIn("slow", function() { scrollText(); });
            });
            if (hdt) {
                var me = this;
                th = window.setTimeout(function() { me.hideTip(); }, hdt);
            }
        };
        this.hideTip = function(p, c) {
            if (th) window.clearTimeout(th);
            th = null;
            mst.text("");
            if (msc != null) msc.fadeOut(p || "slow", c ? c : null);
            if (sth) window.clearInterval(sth);
            sth = null;
        };
        function scrollText() {
            var sw = mst[0].scrollWidth;
            var iw = mst.innerWidth();
            var ssw = sw - iw;
            var lf = true;
            if (sw > iw) {
                sth = window.setInterval(function() {
                    mst.scrollLeft(mst.scrollLeft() + (lf ? 2 : -2));
                    if (lf)
                        lf = mst.scrollLeft() < ssw;
                    else
                        lf = (mst.scrollLeft() <= 0);
                }, 25);
            }
        };
        this.tipId = (new Date()).getTime();
        tmc.attr("tipId", this.tipId);
    };
    var defTipMsg = new tipMsg();
    var tipMsgs = {};
    HOYI.activeTipMsg = null;
    window.$tMsg = function(t, hdt, cnt) {
        var tpMg = cnt ? tipMsgs[$(cnt).attr("tipId")] : defTipMsg;
        if (!tpMg) { tpMg = new tipMsg(cnt); tipMsgs[tpMg.tipId] = tpMg }
        tpMg.showTip(t, hdt);
    };
    HOYI.showTipMsg = function(msg) {
        if (HOYI.activeTipMsg) {
            window.$tMsg(msg, 10000, HOYI.activeTipMsg);
            return true;
        }
        return false;
    };
});
