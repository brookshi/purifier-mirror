﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Common
{
    public static class ObjectCopier
    {

        /// <summary>
        /// Copy properties and fields of a object to another.
        /// </summary>
        /// <typeparam name="S">The type of source object</typeparam>
        /// <typeparam name="D">The type of destination object</typeparam>
        /// <param name="s">A source object copied</param>
        /// <param name="d">A destination object copied</param>
        /// <param name="copyFields">if copy fileds</param>
        public static void Copy<S, D>(S s, D d, bool copyFields, List<string> ignores)
        {
            if (d == null)
                return;
            if (s == null)
                return;
            Type dType = d.GetType(), sType = s.GetType();
            var dPPs = dType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var sPPs = (dType == sType) ? dPPs : sType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var sPPDic = sPPs.ToDictionary(pp => pp.Name);
            foreach (var ppi in dPPs)
            {
                if (ppi.CanWrite && sPPDic.ContainsKey(ppi.Name) && ppi.PropertyType == sPPDic[ppi.Name].PropertyType && (ignores == null || !ignores.Contains(ppi.Name)))
                {
                    ppi.SetValue(d, sPPDic[ppi.Name].GetValue(s, null), null);
                }
            }
            if (copyFields)
            {
                var dFdInfos = dType.GetFields(BindingFlags.Instance | BindingFlags.Public);
                var sFdInfos = sType == dType ? dFdInfos : sType.GetFields(BindingFlags.Instance | BindingFlags.Public);
                var sFdDic = sFdInfos.ToDictionary(fd => fd.Name);

                foreach (var fdi in dFdInfos)
                {
                    if (sFdDic.ContainsKey(fdi.Name) && (ignores == null || !ignores.Contains(fdi.Name)))
                        fdi.SetValue(d, sFdDic[fdi.Name].GetValue(s));
                }
            }
        }

        /// <summary>
        /// Copy properties of a object to another.
        /// </summary>
        /// <typeparam name="S">The type of source object</typeparam>
        /// <typeparam name="D">The type of destination object</typeparam>
        /// <param name="s">A source object copied</param>
        /// <param name="d">A destination object copied</param>
        public static void Copy<S, D>(S s, D d, List<string> ignores)
        {
            Copy<S, D>(s, d, false, ignores);
        }

        /// <summary>
        /// Copy properties of a object to new D()
        /// </summary>
        /// <typeparam name="S">The type of source object</typeparam>
        /// <typeparam name="D">The type of destination object</typeparam>
        /// <param name="s">A source object copied</param>
        /// <returns>Return new D() copied.</returns>
        public static D Copy<S, D>(S s) where D : new()
        {
            var d = new D();
            Copy<S, D>(s, d, null);
            return d;
        }

    }
}
