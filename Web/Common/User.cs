﻿using HOYI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Caching;

namespace HOYI.Web.Common
{
    public class User : IPrincipal, IIdentity
    {
        private Dictionary<string, Role> m_Roels;

        internal User(Guid id)
        {
            Init(GetAccount(id));
        }

        internal User(Account account)
        {
            Init(account);
            CacheAccount(account);
        }

        private void Init(Account account)
        {
            this.Name = account.UserName;
            this.Id = account.Id;
            this.Email = account.Email;
            this.m_Roels = new Dictionary<string,Role>(StringComparer.OrdinalIgnoreCase);
            account.Roles.ForEach(r => this.m_Roels[r.RoleName] = r);
        }

        private static void CacheAccount(Account account)
        {
            HttpContext cxt = HttpContext.Current;
            cxt.Cache.Add(account.Id.ToString(), account, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 5, 0), CacheItemPriority.Normal, null);
        }

        private static Account GetAccount(Guid id)
        {
            HttpContext cxt = HttpContext.Current;
            var acc = cxt.Cache.Get(id.ToString()) as Account;
            if (acc == null)
                acc = Account.Read(id);
            if (acc == null)
                throw new ArgumentException("Invalid Account id.");
            return acc;
        }

        public Account GetAccount()
        {
            return GetAccount(this.Id);
        }

        public IIdentity Identity
        {
            get { return this; }
        }

        public bool IsInRole(string role)
        {
            return (this.m_Roels.ContainsKey(role));
        }

        public string AuthenticationType
        {
            get { return "BASIC"; }
        }

        public bool IsAuthenticated
        {
            get { return true; }
        }

        public string Name
        {
            get;
            private set;
        }

        public string Email
        {
            get;
            private set;
        }

        public Guid Id
        {
            get;
            private set;
        }

        public DateTime LastActiveTime
        {
            get;
            internal set;
        }

        public bool IsTimeout()
        {
            return (LastActiveTime.ToLocalTime().AddHours(1) < DateTime.Now);
        }

        public static User Current
        {
            get { return HttpContext.Current.User as User; }
        }
    }
}