﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{

    [Serializable]
    [DataObj(DBName = "Purifier", PrimaryKeyName = "Id", TableName = "[CitiesView]")]
    public class City : BaseDataObj
    {
        [DataObjField(MapFieldName = "City")]
        public string Text { get; set; }
        public string Id { get; protected set; }
    }
}