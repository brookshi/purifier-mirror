Ext.define('Purifier.override.plugin.PullRefresh', {
    override: 'Ext.plugin.PullRefresh',
    fetchLatest: function() {
        //this.callParent(arguments);
        /*var store = this.getList().getStore();
        var callparent=this.callParent;
        var args = arguments;
        store.currentPage=1;
        store.load(function(){callparent(args);});*/
        //this.fireEvent('latestfetched', this, 'refreshFn, you have to handle toInsert youself');
        this.setState("loaded");
        if (this.getAutoSnapBack()) {
            this.snapBack(true);
        }
        var refreshFn = function(){
            var bodyPanel = Purifier.app.getController('Account').getBodyPanel();
            var id = bodyPanel.getActiveItem().getId();
            var store;
            if(id=='tab_filter')
            { 
                var tabbar = Purifier.app.getController('Purifier').getMyTabbar();
                Ext.getStore("PurifierViewStore").load(
                    function(records, operation, success) {
                    if(success)
                    {
                        var tab = tabbar.down('.tab[title=滤网]');
                        tab.setBadgeText(records.length);
                    }
                    }, this);
            }
            else
            {
                store= Ext.getStore('PurifierNewestRecord');
                store.currentPage=1;
                store.load();
            }
        };
        setTimeout(refreshFn, 100);
    }
    
    
});