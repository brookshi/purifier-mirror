﻿using System;
using System.Linq;

namespace HOYI.Common.Data.Indexers
{
    public class DataObjIndexer : DataIndexer
    {
        BaseDataObj dataObj;
        DataObjInfo info = null;

        internal DataObjIndexer(BaseDataObj dataObj)
        {
            if (dataObj == null)
                throw new ArgumentNullException("dataObj");
            this.dataObj = dataObj;
            info = DataObjInfo.GetDataObjInfo(dataObj.GetType());
        }

        protected override object GetValue(string name)
        {
            var dfi = info.Fileds[name];
            if (dfi != null)
            {
                return dfi.FieldInfo.GetValue(dataObj, null);
            }
            else
                return null;
        }

        public override bool Contains(string name)
        {
            return info.Fileds.FirstOrDefault(f => f.FieldName == name) != null;
        }
    }
}
