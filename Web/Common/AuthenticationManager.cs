﻿using HOYI.Common;
using HOYI.Data;
using HOYI.Web.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace HOYI.Web.Common
{
    public class AuthenticationException : Exception
    {
        internal AuthenticationException(string message)
            : base(message)
        { }
    }

    public class AuthenticationManager
    {
        interface IAuthentication
        {
            bool IsInCharged(HttpContext context);
            void Authenticate(HttpContext context);
            void OnError(HttpContext context, AuthenticationException exception);
            void UpdateTicket();
            void Logout();
        }

        class BrowserAuthentication : IAuthentication
        {
            const string TICKETKEY = "AUTH.TICKET";

            public bool IsInCharged(HttpContext context)
            {
                var brw = context.Request.Browser;
                return brw != null && !string.IsNullOrEmpty(brw.Browser);
            }

            public void Authenticate(HttpContext context)
            {
                var tk = GetTicket(context);
                var user = ParseTicket(tk);
                if (user == null)
                    throw new AuthenticationException("NeedLogin");
                if (user.IsTimeout())
                    throw new AuthenticationException("Timeout");
                context.User = user;
            }

            User ParseTicket(string ticket)
            {
                try
                {
                    if (string.IsNullOrEmpty(ticket)) return null;
                    var bytes = Convert.FromBase64String(ticket);
                    var ticketInfo = EncryptHelper.DecryptText(bytes);
                    var tkObj = JObject.Parse(ticketInfo);
                    var usr = new User(new Guid(tkObj["i"].Value<string>())) { LastActiveTime = DateTime.FromBinary(tkObj["d"].Value<long>()).ToLocalTime() };
                    return usr;

                }
                catch
                {
                    return null;
                }
            }

            string GetTicket(HttpContext context)
            {
                var tkCookies = context.Request.Cookies[TICKETKEY];
                if (tkCookies == null) return null;
                return tkCookies.Value;
            }

            public void OnError(HttpContext context, AuthenticationException exception)
            {
                string loginUrl = string.Format("~/login.html?target={0}&msg={1}", HttpUtility.UrlEncode(context.Request.Url.AbsolutePath), HttpUtility.UrlEncode(exception.Message));
                context.Response.Redirect(loginUrl);
                context.Response.End();
            }

            public void UpdateTicket()
            {
                string ticket = GetTicketString();
                if (string.IsNullOrEmpty(ticket)) return;
                var ticketBytes = EncryptHelper.EncryptText(ticket);
                var context = HttpContext.Current;
                context.Response.SetCookie(new HttpCookie(TICKETKEY, Convert.ToBase64String(ticketBytes)));
            }

            static string GetTicketString()
            {
                var context = HttpContext.Current;
                var user = context.User as User;
                if (user == null) return null;
                user.LastActiveTime = DateTime.UtcNow;

                JObject dic = new JObject();
                dic["n"] = user.Name;
                dic["i"] = user.Id.ToString();
                dic["d"] = user.LastActiveTime.ToBinary();
                return dic.ToString(Newtonsoft.Json.Formatting.None);
            }

            public void Logout()
            {
                var tkco = HttpContext.Current.Request.Cookies[TICKETKEY];
                tkco.Expires = DateTime.Now.AddHours(-24);
                HttpContext.Current.Response.SetCookie(tkco);
            }
        }

        class AppAuthentication : IAuthentication
        {
            
            public bool IsInCharged(HttpContext context)
            {
                var brw = context.Request.Browser;
                return brw == null || string.IsNullOrEmpty(brw.Browser) || brw.Browser == "App";
            }

            public void Authenticate(HttpContext context)
            {
                var tks = GetTicket(context);
                if (tks == null)
                    throw new AuthenticationException("NeedLogin");
                var usr = ParseTicket(tks, context);
                if (usr == null)
                    throw new AuthenticationException("LoginFail");

                context.User = usr;
            }

            Guid GetTicketKey(byte[] ticket)
            {
                var md5 = System.Security.Cryptography.MD5.Create();
                return new Guid(md5.ComputeHash(ticket));
            }

            User ParseTicket(byte[] ticket, HttpContext context)
            {
                var key = GetTicketKey(ticket);
                var cah = context.Cache;
                User usr = cah.Get(key.ToString()) as User;
                if (usr == null || usr.IsTimeout())
                {
                    usr = Login(ticket);
                    cah.Add(key.ToString(), usr, null, Cache.NoAbsoluteExpiration, new TimeSpan(1, 1, 0), CacheItemPriority.Normal, null);
                }

                return usr;
            }

            User Login(byte[] ticket)
            {
                var tickInfo = new string(System.Text.Encoding.ASCII.GetChars(ticket));
                if (string.IsNullOrEmpty(tickInfo)) return null;
                var unpwd=tickInfo.Split(':');
                return AuthenticationManager.Instance.Login(unpwd[0], unpwd[1]);
            }

            public void OnError(HttpContext context, AuthenticationException exception)
            {
                context.Response.StatusCode = 401;
                context.Response.Headers.Add("WWW-authenticate", "BASIC");
                context.Response.Headers.Add("Auth-error", HttpUtility.UrlEncode(exception.Message));
                context.Response.End();
            }

            public void UpdateTicket()
            {
                var context = HttpContext.Current;
                var usr = context.User as User;
                if (usr != null)
                    usr.LastActiveTime = DateTime.UtcNow;
            }

            byte[] GetTicket(HttpContext context)
            {
                foreach (var hd in context.Request.Headers.Keys)
                {
                    if (string.Equals("Authorization", hd.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        var info = context.Request.Headers[hd.ToString()];
                        if (info.StartsWith("BASIC ", StringComparison.OrdinalIgnoreCase))
                        {
                            var bytes = Convert.FromBase64String(info.Split(' ')[1]);
                            return bytes;
                        }
                    }
                }
                return null;
            }

            public void Logout()
            {
            }
        }


        List<IAuthentication> m_Authentications = new List<IAuthentication>();

        private AuthenticationManager()
        {
            m_Authentications.Add(new BrowserAuthentication());
            m_Authentications.Add(new AppAuthentication());
        }

        public void Authenticate(HttpContext context)
        {
            IAuthentication authenticator = GetCurrentAuthentication(context);
            try
            {
                authenticator.Authenticate(context);
            }
            catch (AuthenticationException autExp)
            {
                authenticator.OnError(context, autExp);
            }
        }

        private IAuthentication GetCurrentAuthentication(HttpContext context)
        {
            IAuthentication authenticate = null;
            foreach (var authen in m_Authentications)
            {
                if (authen.IsInCharged(context))
                {
                    authenticate = authen;
                    break;
                }
            }
            if (authenticate == null)
                throw new AuthenticationException("ErrorAuthenticationRequest");
            return authenticate;
        }


        public void UpdateTicket(HttpContext context)
        {
            try
            {
                IAuthentication authenticator = GetCurrentAuthentication(context);
                authenticator.UpdateTicket();
            }
            catch { }
        }

        public User Login(string userName, string password)
        {
            var user = Account.GetUser(userName, password);
            if (user == null)
                throw new AuthenticationException("用户名或者密码不正确。");
            user.LoginDate = DateTime.Now;
            user.Save(true);
            return new User(user);
        }

        public readonly static AuthenticationManager Instance = new AuthenticationManager();

        internal void Logout()
        {
            GetCurrentAuthentication(HttpContext.Current).Logout();
        }
    }
}