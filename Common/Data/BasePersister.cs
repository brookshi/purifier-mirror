﻿using System;
using System.Collections.Generic;

namespace HOYI.Common.Data
{
    public abstract class BasePersister : IDataObjPersister
    {
        protected abstract Dictionary<T, int> SaveObjs<T>(IEnumerable<T> dataObjs) where T : BaseDataObj;


        protected abstract T ReadDataObjByID<T>(object objID) where T : BaseDataObj;

        protected abstract List<T> TopObjs<T>(int count, string orderByPropertyName, OrderDirection direction, WhereCriterion critical) where T : BaseDataObj;

        protected abstract List<DataSummery> LoadDataSummery<T>(int count, string summeryName, OrderDirection direction, WhereCriterion critical) where T : BaseDataObj;


        protected abstract List<T> QueryDataObjs<T>(WhereCriterion critical) where T : BaseDataObj;

        protected abstract int CountObjs<T>(WhereCriterion critical) where T : BaseDataObj;

        protected abstract int Delete<T>(object objId) where T : BaseDataObj;

        protected abstract int ForeUpate<T>(Dictionary<string, object> values, WhereCriterion critical) where T : BaseDataObj;

        protected abstract void BeginTransaction();

        protected abstract IEnumerator<T> EnumerateData<T>(WhereCriterion critical) where T : BaseDataObj;

        protected abstract void Commit();


        protected abstract void Rollback();

        protected internal static bool DataObjIsEmptyId(BaseDataObj dataObj)
        {
            object idValue = GetDataObjIdValue(dataObj);
            if (idValue == null || idValue.GetHashCode() == 0 || string.IsNullOrEmpty(idValue.ToString()) || idValue.GetHashCode() == (0).GetHashCode())
                return true;
            else
                return false;
        }

        internal static object GetDataObjIdValue(BaseDataObj dataObj)
        {
            var doi = DataObjInfo.GetDataObjInfo(dataObj.GetType());
            return doi.PrimaryKeyInfo.GetValue(dataObj, null);
        }

        protected virtual void AssignDataObjId(BaseDataObj dataObj)
        {
            DataObjIdGenerator.Instance.SetId(dataObj);
        }

        protected abstract int DeleteObjs<T>(WhereCriterion critical) where T : BaseDataObj;

        protected virtual T ExecuteMethod<T>(Func<T> method)
        {
            return method();
        }

        protected virtual void ExecuteMethod(Action method)
        {
            method();
        }

        #region IDataObjPersister Members

        Dictionary<T, int> IDataObjPersister.SaveObjs<T>(IEnumerable<T> dataObjs)
        {
            return ExecuteMethod<Dictionary<T, int>>(() => SaveObjs<T>(dataObjs));
        }

        T IDataObjPersister.ReadDataObjByID<T>(object objID)
        {
            return ExecuteMethod<T>(() => ReadDataObjByID<T>(objID));
        }

        List<T> IDataObjPersister.QueryDataObjs<T>(WhereCriterion critical)
        {
            return ExecuteMethod<List<T>>(() => QueryDataObjs<T>(critical));
        }

        void IDataObjPersister.BeginTransaction()
        {
            ExecuteMethod(() => BeginTransaction());
        }

        void IDataObjPersister.Commit()
        {
            ExecuteMethod(() => Commit());
        }

        void IDataObjPersister.Rollback()
        {
            ExecuteMethod(() => Rollback());
        }

        int IDataObjPersister.Delete<T>(object objId)
        {
            return ExecuteMethod<int>(() => Delete<T>(objId));
        }

        int IDataObjPersister.DeleteObjs<T>(WhereCriterion critical)
        {
            return ExecuteMethod<int>(() => DeleteObjs<T>(critical));
        }

        List<T> IDataObjPersister.TopObjs<T>(int count, string orderByPropertyName, OrderDirection direction, WhereCriterion critical)
        {
            return ExecuteMethod<List<T>>(() => TopObjs<T>(count, orderByPropertyName, direction, critical));
        }

        int IDataObjPersister.CountObjs<T>(WhereCriterion critical)
        {
            return ExecuteMethod<int>(() => CountObjs<T>(critical));
        }

        List<DataSummery> IDataObjPersister.LoadDataSummery<T>(int count, string summeryName, OrderDirection direction, WhereCriterion critical)
        {
            return ExecuteMethod<List<DataSummery>>(() => LoadDataSummery<T>(count, summeryName, direction, critical));
        }

        int IDataObjPersister.ForeUpate<T>(Dictionary<string, object> values, WhereCriterion critical)
        {
            return ExecuteMethod<int>(() => ForeUpate<T>(values, critical));
        }

        IEnumerator<T> IDataObjPersister.EnumerateData<T>(WhereCriterion critical)
        {
            return ExecuteMethod<IEnumerator<T>>(() => EnumerateData<T>(critical));
        }

        #endregion
    }
}
