﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;

namespace HOYI.Common.Data
{
    public abstract class BaseDataObjPersister : BasePersister, IDbContext, IDisposable
    {
        internal const BindingFlags ConstPubInsFlag = BindingFlags.Instance | BindingFlags.Public;
        IDbContext Context { get { return (IDbContext)this; } }

        int SaveObj(BaseDataObj dataObj)
        {
            if (dataObj == null) return 0;
            var effRows = 0;
            dataObj.OnBeforeSave();
            var idNotEmpty = EnsureDataObjId(dataObj);
            if (dataObj.IsExisted && idNotEmpty)
            {
                effRows = UpdateDataObjToDb(dataObj);
                if (effRows == -2 && !this.CheckDataObjIfExists(dataObj))
                    effRows = 0;
                if (effRows == 0)
                    effRows = InsertDataObjToDb(dataObj);
            }
            else
            {
                try
                {
                    effRows = InsertDataObjToDb(dataObj);
                }
                catch (Exception ex)
                {
                    effRows = HandleInsertException(ex, dataObj);
                    if (effRows == 0 || (!idNotEmpty && effRows == -2)) throw;
                }
                dataObj.IsExisted = true;
            }
            return effRows;
        }

        protected virtual int HandleInsertException(Exception ex, BaseDataObj dataObj)
        {
            int effRows = 0;
            Tools.ExecAction(() =>
            {
                effRows = UpdateDataObjToDb(dataObj);
                if (effRows == -2 && !this.CheckDataObjIfExists(dataObj))
                    effRows = 0;
            });
            return effRows;
        }

        protected bool EnsureDataObjId(BaseDataObj dataObj)
        {
            bool emptyIdObj = DataObjIsEmptyId(dataObj);
            if (emptyIdObj)
                AssignDataObjId(dataObj);
            return !emptyIdObj;
        }

        protected override Dictionary<T, int> SaveObjs<T>(IEnumerable<T> dataObjs)
        {
            var dic = new Dictionary<T, int>();
            foreach (var obj in dataObjs)
            {
                dic[obj] = SaveObj(obj);
            }
            return dic;
        }

        protected override int Delete<T>(object objId)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));
            using (var delCmd = Context.DbFactory.CreateCommand())
            {
                delCmd.Connection = Context.Connection;
                delCmd.Transaction = transaction;
                delCmd.CommandText = sqlGen.GeneratorDeleteSQL();
                var dbParam = Context.DbFactory.CreateParameter();
                dbParam.ParameterName = sqlGen.Info.PrimaryKeyName;
                dbParam.Direction = ParameterDirection.Input;
                dbParam.Value = objId;
                delCmd.Parameters.Add(dbParam);
                return delCmd.ExecuteNonQuery();
            }
        }


        protected override int DeleteObjs<T>(WhereCriterion critical)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));
            using (var delCmd = Context.DbFactory.CreateCommand())
            {
                try
                {
                    delCmd.Connection = Context.Connection;
                    delCmd.Connection.Open();
                    delCmd.CommandText = sqlGen.GeneratorDeleteSQL(critical);
                    BindSQLParameters(critical.GetParameterNames(), delCmd);
                    return delCmd.ExecuteNonQuery();
                }
                finally
                {
                    CloseConnection();
                }
            }
        }


        private readonly Dictionary<Type, SQLGeneratorBase> generators = new Dictionary<Type, SQLGeneratorBase>();

        private SQLGeneratorBase GetSQLGenerator(Type dataObjType)
        {
            lock (generators)
            {
                if (generators.ContainsKey(dataObjType))
                    return generators[dataObjType];
                generators[dataObjType] = CreateSQLGenerator(dataObjType);
                return generators[dataObjType];
            }
        }

        protected abstract SQLGeneratorBase CreateSQLGenerator(Type dataObjType);

        protected override T ReadDataObjByID<T>(object dataId)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));
            using (var selectCmd = Context.DbFactory.CreateCommand())
            {
                selectCmd.CommandText = sqlGen.GeneratorSelectSQL();
                using (selectCmd.Connection = Context.Connection)
                {
                    string sqlPN = sqlGen.Info.PrimaryKeyName;
                    BindSQLParameters(new Dictionary<string, object>() { { sqlPN, dataId } }, selectCmd);
                    var objs = ReadObjsFromDB<T>(selectCmd);
                    return objs != null && objs.Count > 0 ? objs[0] : (T)null;
                }
            }
        }

        List<T> ReadObjsFromDB<T>(DbCommand readCmd) where T : BaseDataObj
        {
            return ReadObjsFromDB<T>(readCmd, 0, 0);
        }

        List<T> ReadObjsFromDB<T>(DbCommand readCmd, int begin, int end) where T : BaseDataObj
        {
            var dataObjs = new List<T>();
            try
            {
                readCmd.Connection.Open();
                using (IDataReader reader = readCmd.ExecuteReader())
                {
                    int index = 0;
                    while (reader.Read())
                    {
                        if ((begin == end && end == 0) || (begin <= index && end >= index))
                        {
                            var dataObj = BaseDataObj.CreateObjFromPersistence<T>(reader);
                            dataObjs.Add(dataObj);
                        }
                        index++;
                    }
                }
            }
            finally
            {
                CloseConnection();
            }
            return dataObjs;
        }

        protected override List<T> TopObjs<T>(int count, string orderBy, OrderDirection direction, WhereCriterion critical)
        {
            if (count <= 0) count = 1;
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));

            using (var queryCmd = Context.DbFactory.CreateCommand())
            {
                queryCmd.CommandText = sqlGen.GeneratorTopSQL(count, orderBy, direction, critical);
                queryCmd.Connection = Context.Connection;
                if (critical != null && !critical.IsEmpty)
                    BindSQLParameters(critical.GetParameterNames(), queryCmd);
                var objs = ReadObjsFromDB<T>(queryCmd);
                return objs;
            }
        }

        protected override List<DataSummery> LoadDataSummery<T>(int count, string summeryName, OrderDirection direction, WhereCriterion critical)
        {
            if (count <= 0) count = 1;
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));

            using (var queryCmd = Context.DbFactory.CreateCommand())
            {
                queryCmd.CommandText = sqlGen.GeneratorTopSQL(count, summeryName, direction, critical);
                queryCmd.Connection = Context.Connection;
                if (critical != null && !critical.IsEmpty)
                    BindSQLParameters(critical.GetParameterNames(), queryCmd);

                var summeries = new List<DataSummery>();
                try
                {
                    queryCmd.Connection.Open();
                    IDataReader reader = queryCmd.ExecuteReader();
                    while (reader.Read())
                    {
                        summeries.Add(new DataSummery(this).Read(reader, sqlGen.Info.PrimaryKeyName, summeryName));
                    }
                }
                finally
                {
                    CloseConnection();
                }
                return summeries;
            }
        }

        protected override List<T> QueryDataObjs<T>(WhereCriterion critical)
        {
            using (var queryCmd = CreateQueryCmd<T>(critical))
            {
                var objs = ReadObjsFromDB<T>(queryCmd);
                return objs;
            }
        }


        protected abstract DbType GetDbTypeMapping(Type type);
       
        DbCommand CreateQueryCmd<T>(WhereCriterion critical) where T : BaseDataObj
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));
            var queryCmd = Context.DbFactory.CreateCommand();
            queryCmd.CommandText = sqlGen.GeneratorQuerySQL(critical);
            queryCmd.Connection = Context.Connection;
            if (critical != null && !critical.IsEmpty)
                BindSQLParameters(critical.GetParameterNames(), queryCmd);
            return queryCmd;
        }

        protected override IEnumerator<T> EnumerateData<T>(WhereCriterion critical)
        {
            var queryCmd = CreateQueryCmd<T>(critical);
            return new DataObjEnumerator<T>(queryCmd);
        }

        protected override int CountObjs<T>(WhereCriterion critical)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));
            using (var countCmd = Context.DbFactory.CreateCommand())
            {
                countCmd.CommandText = sqlGen.GeneratorCountSQL(critical);
                countCmd.Connection = Context.Connection;
                if (critical != null && !critical.IsEmpty)
                    BindSQLParameters(critical.GetParameterNames(), countCmd);
                try
                {
                    countCmd.Connection.Open();
                    IDataReader reader = countCmd.ExecuteReader();
                    int count = 0;
                    while (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                    return count;
                }
                finally
                {
                    CloseConnection();
                }
            }
        }

        protected virtual int UpdateDataObjToDb(BaseDataObj dataObj)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(dataObj.GetType());
            using (var updCmd = Context.DbFactory.CreateCommand())
            {
                updCmd.Connection = Context.Connection;
                updCmd.Transaction = transaction;
                var fields = sqlGen.Info.GetWriteFields();
                var updateFields = new List<string>();
                foreach (var fd in fields)
                {
                    if (dataObj.IsPropertyChanged(fd))
                    {
                        updateFields.Add(fd);
                    }
                }
                if (updateFields.Count == 0) return -2;
                if (updateFields.Contains(sqlGen.Info.PrimaryKeyName))
                    updateFields.Remove(sqlGen.Info.PrimaryKeyName);
                updCmd.CommandText = sqlGen.GeneratorUpdateSQL(updateFields);
                updateFields.Add(sqlGen.Info.PrimaryKeyName);
                BindSQLParameters(updateFields, dataObj, updCmd);
                return updCmd.ExecuteNonQuery();
            }
        }

        protected override int ForeUpate<T>(Dictionary<string, object> values, WhereCriterion critical)
        {
            if (critical == null || critical.IsEmpty)
                return 0;
            SQLGeneratorBase sqlGen = GetSQLGenerator(typeof(T));
            using (var updCmd = Context.DbFactory.CreateCommand())
            {
                try
                {
                    updCmd.Connection = Context.Connection;
                    updCmd.Connection.Open();
                    var fields = sqlGen.Info.GetWriteFields();
                    var updateDic = new Dictionary<string, object>();
                    foreach (var fd in fields)
                    {
                        if (values.ContainsKey(fd))
                        {
                            updateDic[fd] = values[fd];
                        }
                    }
                    if (updateDic.Count == 0)
                        return 0;

                    updCmd.CommandText = sqlGen.GeneratorForceUpdateSQL(updateDic.Keys.ToList(), critical);
                    BindSQLParameters(updateDic, updCmd);
                    BindSQLParameters(critical.GetParameterNames(), updCmd);
                    return updCmd.ExecuteNonQuery();
                }
                finally
                {
                    CloseConnection();
                }
            }
        }

        protected virtual int InsertDataObjToDb(BaseDataObj dataObj)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(dataObj.GetType());
            using (var insCmd = Context.DbFactory.CreateCommand())
            {
                insCmd.CommandText = sqlGen.GeneratorInsertSQL();
                insCmd.Connection = Context.Connection;
                insCmd.Transaction = transaction;
                BindSQLParameters(sqlGen.Info.GetWriteFields(), dataObj, insCmd);
                return insCmd.ExecuteNonQuery();
            }
        }

        protected virtual bool CheckDataObjIfExists(BaseDataObj dataObj)
        {
            SQLGeneratorBase sqlGen = GetSQLGenerator(dataObj.GetType());
            using (var checkCmd = Context.DbFactory.CreateCommand())
            {
                checkCmd.CommandText = sqlGen.GeneratorCheckSQL();
                checkCmd.Connection = Context.Connection;
                checkCmd.Transaction = transaction;
                BindSQLParameters(new string[] { sqlGen.Info.GetPrimaryKeyDbName() }, dataObj, checkCmd);
                using (DbDataReader reader = checkCmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader.GetInt32(0) > 0;
                    }
                }
                return false;
            }
        }

        void BindSQLParameters(Dictionary<string, object> paramDic, IDbCommand command)
        {
            var dic = new Dictionary<string, Pair<object, Type>>();
            foreach (var key in paramDic.Keys)
            {
                var value = paramDic[key];
                dic[key] = new Pair<object, Type>(value, (value != null ? value.GetType() : typeof(string)));
            }
            BindSQLParameters(dic, command);
        }

        protected virtual void BindSQLParameters(IEnumerable<string> fields, BaseDataObj dataObj, IDbCommand command)
        {
            try
            {
                var paramDic = new Dictionary<string, Pair<object, Type>>(StringComparer.OrdinalIgnoreCase);
                var objInfo = DataObjInfo.GetDataObjInfo(dataObj.GetType());

                foreach (var fld in fields)
                {
                    PropertyInfo fldInfo = null;
                    if (objInfo != null)
                    {
                        fldInfo = objInfo.Fileds[fld].FieldInfo;
                    }
                    if (fldInfo == null)
                    {
                        fldInfo = dataObj.GetType().GetProperty(fld, ConstPubInsFlag);
                    }
                    object pValue = fldInfo.GetValue(dataObj, null);
                    paramDic[fld] = new Pair<object, Type>(pValue == null ? DBNull.Value : pValue, fldInfo.PropertyType);
                }
                BindSQLParameters(paramDic, command);
            }
            catch (Exception)
            {
                throw;
            }
        }

        void BindSQLParameters(Dictionary<string, Pair<object, Type>> paramDic, IDbCommand command)
        {
            SetCommandTimeout(command);

            foreach (var fld in paramDic.Keys)
            {
                var pair = paramDic[fld];
                var pm = Context.DbFactory.CreateParameter();
                pm.ParameterName = fld;
                pm.Direction = ParameterDirection.Input;
                MappingTypeToDbType(pm, pair.Second);
                SetDbParameterValue(pm, pair.First);
                command.Parameters.Add(pm);
            }
        }

        protected virtual void SetDbParameterValue(DbParameter dbParam, object value)
        {
            dbParam.Value = value;
        }

        protected virtual void SetCommandTimeout(IDbCommand command)
        {
        }

        protected virtual void MappingTypeToDbType(DbParameter dbParam, Type type)
        {
        }

        DbTransaction transaction = null;
        DbConnection connection = null;

        protected override void BeginTransaction()
        {
            if (transaction != null)
                return;
            if (Context.Connection.State != ConnectionState.Open)
                Context.Connection.Open();
            transaction = Context.Connection.BeginTransaction();
        }

        protected internal string ConnectionString { get; internal set; }
        protected abstract DbProviderFactory DbFactory { get; }

        protected virtual DbConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = DbFactory.CreateConnection();
                    connection.ConnectionString = ConnectionString;
                }
                return connection;
            }
        }

        void CloseConnection()
        {
            if (transaction != null)
            {
                try
                {
                    if (transaction.Connection.State == ConnectionState.Open)
                        transaction.Connection.Close();
                    transaction.Dispose();
                }
                catch
                {
                }
                finally
                {
                    transaction = null;
                }
            }
            if (connection != null)
            {
                try
                {
                    if (connection.State == ConnectionState.Open)
                        connection.Close();
                    connection.Dispose();
                }
                catch
                {
                }
                finally
                {
                    connection = null;
                }
            }
        }

        protected override void Commit()
        {
            try
            {
                if (transaction != null)
                {
                    var tranConn = transaction.Connection;
                    if (tranConn != null && tranConn.State == ConnectionState.Open)
                        transaction.Commit();
                }
            }
            finally
            {
                CloseConnection();
            }
        }

        protected override void Rollback()
        {
            try
            {
                if (transaction != null)
                    transaction.Rollback();
            }
            finally
            {
                CloseConnection();
            }
        }

        #region IDbContext Members

        DbProviderFactory IDbContext.DbFactory { get { return DbFactory; } }

        DbConnection IDbContext.Connection { get { return Connection; } }

        #endregion

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            try
            {
                CloseConnection();
            }
            catch
            {
            }
        }

        #endregion
    }
}
