﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HOYI.Common.Data;
using HOYI.Data;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;

namespace PurifierLogic
{
    public class CommandAdditional
    {
        public abstract class BinaryCommandBase<TAppSession> : CommandBase<TAppSession, SuperSocket.SocketBase.Protocol.BinaryRequestInfo> where TAppSession : SuperSocket.SocketBase.IAppSession, SuperSocket.SocketBase.IAppSession<TAppSession, SuperSocket.SocketBase.Protocol.BinaryRequestInfo>, new()
        {
            protected BinaryCommandBase() { }
        } 
    }
    public class GetStatusHandler : PurifierLogic.CommandAdditional.BinaryCommandBase<PurifierSession>
    {
        public override string Name
        {
            get { return "100"; }
        }

        public override void ExecuteCommand(PurifierSession session, BinaryRequestInfo requestInfo)
        {
            //string id = requestInfo[1];
            //PurifierInf info = DataHandler.GetInstance().GetSettingData(id);
            //string send = "";
            //if (info == null)
            //    send = "-1|-1";
            //else
            //    send = info.Status + "|" + info.Tear;
            //if (!session.TrySend((send))
            //    session.Close();
        }
    }
}
