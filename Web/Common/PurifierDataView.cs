﻿using HOYI.Common.Data;
using HOYI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    /*
     PurifierDataView.sql
     */
    [Serializable]
    [DataObj(DBName = "Purifier", PrimaryKeyName = "Id", TableName = "[PurifierDataView]")]
    public class PurifierDataView : PurifierRecord
    {
        public virtual string Name { get; protected set; }

        public virtual string SNCode { get; protected set; }

        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public string CreateDateShortString
        {
            get
            {
                return this.CreateDate.ToShortDateString();
            }
        }

        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public string CreateTimeString
        {
            get
            {
                return this.CreatedTime.ToString("HH:mm");
            }
        }
    }
}