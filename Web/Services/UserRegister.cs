﻿using HOYI.Common;
using HOYI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace HOYI.Web.Services
{
    public class UserRegister
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string City
        {
            get;
            set;
        }

        public string Province
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public bool IsAdmin
        {
            get;
            set;
        }

        public UserRegister()
        { }

     
        internal static bool VerifyEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException("email");
            return Account.IsEmailUsed(email);
        }


        internal static bool VerifyUserName(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException("userName");
            return Account.IsExist(username);
        }

        public Guid Register()
        {
            CheckEmail();
            CheckUserName();
            CheckPassword();
            return Create();
        }

        private Guid Create()
        {
            Account acc = Account.Create(UserName, Email);
            acc.SetPassword(Password);
            ObjectCopier.Copy(this, acc, new List<string>() { "UserName", "Email" });
            acc.Save(false);
            return acc.Id;
        }

        internal void UpdateUser(Guid userId)
        {
            Account acc = Account.Read(userId);
            ObjectCopier.Copy(this, acc, new List<string>() { "UserName", "Email" });
            acc.Save(false);
        }

        private void CheckPassword()
        {
            if (string.IsNullOrEmpty(Password))
                throw new InvalidOperationException("注册用户时，密码不能为空。");
            if (Password.Length < 6 || Password.Length > 16)
                throw new InvalidOperationException("密码长度为6~16位。");
            if (Tools.CompareStr(Password, Email) || Tools.CompareStr(Password, UserName))
                throw new InvalidOperationException("密码不能是邮箱或者用户名。");

            if (!(new Regex(@"^(?!\D+$)(?![^a-zA-Z]+$)\S+$", RegexOptions.IgnoreCase)).IsMatch(Password))
                throw new InvalidOperationException("密码必须由数字、字母、特殊字符组成。");
        }

        private void CheckUserName()
        {

            if (string.IsNullOrEmpty(UserName))
                throw new InvalidOperationException("注册用户时，用户名不能为空。");
            if (!(new Regex("[0-9A-Za-z_]{3,20}", RegexOptions.IgnoreCase)).IsMatch(UserName))
                throw new InvalidOperationException("用户名长度3~20，必须由数字、字母、下画线组成。");
            if (VerifyUserName(UserName))
                throw new InvalidOperationException(string.Format("用户名:{0}, 已经被使用。请使用别的用户名。", UserName));
        }

        private void CheckEmail()
        {
            if (string.IsNullOrEmpty(Email))
                throw new InvalidOperationException("注册用户时，邮箱不能为空。");
            if (!(new Regex(@"^[\w.\-]+@(?:[a-z0-9]+(?:-[a-z0-9]+)*\.)+[a-z]{2,6}$", RegexOptions.IgnoreCase)).IsMatch(Email))
                throw new InvalidOperationException(string.Format("无效的邮箱地址:{0}", Email));
            if (VerifyEmail(Email))
                throw new InvalidOperationException(string.Format("邮箱地址:{0}, 已经被注册。请使用别的邮箱。", Email));
        }


    }
}