﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    public class ModelListEx<T>
    {
        public int total = 0;
        public bool success = true;
        public bool isall = true;
        public IList<T> data;
        public string message = "Created record";
    }

    class RetModel<T>
    {
        public int total = 0;
        public bool success = true;
        public T data;
        public string message = "Created record";
    }
    
}