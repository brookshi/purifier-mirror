﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Data;
using HOYI.Web.Common;

namespace HOYI.Web.Services
{
    [WebService(Namespace = "http://hoyi.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Mobile : WebServiceBase
    {
        string GetDateSubString(string date)
        {
            if (date.Length > 10)
                date = date.Substring(0, 10);
            return date;
        }
        public Dictionary<string, string> LoadUsersPurifierMaps()
        {
            List<PurifierInfoView> infos = new List<PurifierInfoView>();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInfoView.LoadAll());
            }
            else
            {
                infos.AddRange(PurifierInfoView.LoadUserPurifiers(CurrentUser.Id));
            }
            ListSorter sorter = new ListSorter("Name", true);
            sorter.Sort(infos);
            return infos.ToDictionary(f => f.Id.ToString(), f => f.Name);
        }

        public Dictionary<long, string> LoadPurifierSNCode()
        {
            List<PurifierInfoView> infos = new List<PurifierInfoView>();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInfoView.LoadAll());
            }
            else
            {
                infos.AddRange(PurifierInfoView.LoadUserPurifiers(CurrentUser.Id));
            }
            return infos.ToDictionary(f => f.Id, f => string.IsNullOrEmpty(f.Name)?(f.SNCode??""):f.Name);
        }

        public Dictionary<string, List<PurifierRecord>> GetDateDictionary(List<PurifierRecord> list, bool inDay)
        {
            Dictionary<string, List<PurifierRecord>> dataDict = new Dictionary<string, List<PurifierRecord>>();
            ListSorter sorter = new ListSorter("CreateDate", true);
            sorter.Sort(list);
            foreach (var item in list)
            {
                string createdate = inDay ? item.CreateDate.Hour.ToString() : item.CreateDate.ToShortDateString();
                if (!dataDict.ContainsKey(createdate))
                    dataDict[createdate] = new List<PurifierRecord>();
                if (!inDay)
                {
                    var obj = dataDict[createdate].FirstOrDefault(o => o.MAC == item.MAC);
                    if(obj!=null)
                    {
                        obj.Pm25 = (obj.Pm25 + item.Pm25)/2;
                        obj.Temperature = (obj.Temperature + item.Temperature) / 2;
                        obj.Humidity = (obj.Humidity + item.Humidity) / 2;
                    }
                    else
                        dataDict[createdate].Add(item);
                }
                else
                    dataDict[createdate].Add(item);
            }
            return dataDict;
        }

        public bool ConvertDate(string start, string end, ref DateTime startDate, ref DateTime endDate)
        {
            //start = GetDateSubString(start);
            //end = GetDateSubString(end);
            start = start.Replace("年", "-");
            start = start.Replace("月", "-");
            start = start.Replace("日", "");
            end = end.Replace("年", "-");
            end = end.Replace("月", "-");
            end = end.Replace("日", "");
            if (!DateTime.TryParse(start+" 00:00:00", out startDate) || !DateTime.TryParse(end+" 00:00:00", out endDate))
                return false;
            return true;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetChartDataByPurifierAndroid(string[] purifierIds, string start, string end)
        {
            try
            {
                DateTime startDate = DateTime.Now, endDate = DateTime.Now;
                if (!ConvertDate(start, end, ref startDate, ref endDate))
                {
                    return new ModelListEx<object>() { success = false };
                }
                bool isOneDay = start == end;
                endDate = endDate.AddDays(1);
                Dictionary<long, string> SNCodes = LoadPurifierSNCode();
                List<PurifierRecord> list = PurifierRecord.LoadPurifierRecordForStatisticsById(purifierIds, startDate, endDate);
                Dictionary<long, List<PurifierRecord>> dict = new Dictionary<long, List<PurifierRecord>>();
                foreach (var record in list)
                {
                    if (!dict.ContainsKey(record.PurifierID))
                        dict[record.PurifierID] = new List<PurifierRecord>();
                    dict[record.PurifierID].Add(record);
                }
                foreach (var item in dict)
                {
                    item.Value.OrderBy(o => o.CreateDate);
                }
                List<object> rstList = new List<object>();
                if (isOneDay)
                {
                    foreach (var item in dict)
                    {
                        Dictionary<int, int> pm25List = new Dictionary<int, int>();
                        for (int i = 0; i < 24; i++)
                        {
                            pm25List[i] = 0;
                        }
                        foreach (var record in item.Value)
                        {
                            int hour = record.CreateDate.Hour;
                            pm25List[hour] = (int)record.Pm25;
                        }
                        rstList.Add(new
                        {
                            sncode = SNCodes.ContainsKey(item.Key) ? (SNCodes[item.Key] ?? item.Key.ToString()) : item.Key.ToString(),
                            isday = true,
                            data = pm25List.Values,
                        });
                    }
                }
                else
                {
                    foreach (var item in dict)
                    {
                        Dictionary<string, int> pm25List = new Dictionary<string, int>();
                        Dictionary<string, int> dayCount = new Dictionary<string, int>();
                        for (DateTime i = startDate; i <= endDate; i=i.AddDays(1))
                        {
                            pm25List[i.ToString("yyyy-MM-dd")] = 0;
                            dayCount[i.ToString("yyyy-MM-dd")] = 0;
                        }
                        foreach (var record in item.Value)
                        {
                            string date = record.CreateDate.ToString("yyyy-MM-dd");
                            pm25List[date] += (int)record.Pm25;
                            dayCount[date]++;
                        }
                        for (int i = 0; i < pm25List.Count; i++)
                        {
                            var day = pm25List.Keys.ToList()[i];
                            if (dayCount[day] != 0)
                                pm25List[day] /= dayCount[day];
                        }
                        rstList.Add(new
                        {
                            sncode = SNCodes.ContainsKey(item.Key) ? (SNCodes[item.Key] ?? item.Key.ToString()) : item.Key.ToString(),
                            isday = false,
                            data = pm25List.Values,
                        });
                    }
                }
                return new ModelListEx<object>() { success = false, data = rstList, total = rstList.Count };
            }
            catch (Exception e)
            {
                return new ModelListEx<object>() { message = e.ToString() };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetChartDataByPurifierExtjs(string purifierIds, string start, string end)
        {
            try
            {
                string[] idList = purifierIds.Split('|');
                DateTime startDate = DateTime.Now, endDate = DateTime.Now;
                if (!ConvertDate(start, end, ref startDate, ref endDate))
                {
                    return "[]";
                }
                bool isOneDay = start == end;
                endDate = endDate.AddDays(1);
                Dictionary<long, string> SNCodes = LoadPurifierSNCode();
                List<PurifierRecord> list = PurifierRecord.LoadPurifierRecordForStatisticsById(idList, startDate, endDate);
                Dictionary<long, List<PurifierRecord>> dict = new Dictionary<long, List<PurifierRecord>>();
                foreach (var record in list)
                {
                    if (!dict.ContainsKey(record.PurifierID))
                        dict[record.PurifierID] = new List<PurifierRecord>();
                    dict[record.PurifierID].Add(record);
                }
                foreach (var item in dict)
                {
                    item.Value.OrderBy(o => o.CreateDate);
                }
                List<object> rstList = new List<object>();
                Dictionary<string, Dictionary<string, int>> tData = new Dictionary<string, Dictionary<string, int>>();
                List<string> dateList = new List<string>();
                if (isOneDay)
                {
                    foreach (var item in dict)
                    {
                        Dictionary<string, int> pm25List = new Dictionary<string, int>();
                        for (int i = 0; i < 24; i++)
                        {
                            pm25List[i.ToString()] = 0;
                        }
                        foreach (var record in item.Value)
                        {
                            int hour = record.CreateDate.Hour;
                            pm25List[hour.ToString()] = (int)record.Pm25;
                        }
                        tData[SNCodes.ContainsKey(item.Key) ? (SNCodes[item.Key] ?? item.Key.ToString()) : item.Key.ToString()] = pm25List;
                        if (dateList.Count == 0)
                            dateList.AddRange(pm25List.Keys);
                    }

                }
                else
                {
                    foreach (var item in dict)
                    {
                        Dictionary<string, int> pm25List = new Dictionary<string, int>();
                        Dictionary<string, int> dayCount = new Dictionary<string, int>();
                        for (DateTime i = startDate; i <= endDate; i = i.AddDays(1))
                        {
                            pm25List[i.ToString("yyyy-MM-dd")] = 0;
                            dayCount[i.ToString("yyyy-MM-dd")] = 0;
                        }
                        foreach (var record in item.Value)
                        {
                            string date = record.CreateDate.ToString("yyyy-MM-dd");
                            pm25List[date] += (int)record.Pm25;
                            dayCount[date]++;
                        }
                        for (int i = 0; i < pm25List.Count; i++)
                        {
                            var day = pm25List.Keys.ToList()[i];
                            if (dayCount[day] != 0)
                                pm25List[day] /= dayCount[day];
                        }
                        tData[SNCodes.ContainsKey(item.Key) ? (SNCodes[item.Key]??item.Key.ToString()) : item.Key.ToString()] = pm25List;
                        if(dateList.Count==0)
                            dateList.AddRange(pm25List.Keys);
                    }
                }
                StringBuilder content = new StringBuilder();
                content.Append("[");
                for (int i = 0; i < dateList.Count; i++)
                {
                    content.Append("{'date':'");
                    content.Append(dateList[i]);
                    content.Append("','");
                    for (int j = 0; j < tData.Keys.Count; j++)
                    {
                        string key = tData.Keys.ToList()[j];
                        content.Append(key);
                        content.Append("':");
                        content.Append(tData[key][dateList[i]]);
                        if (j ==  tData.Keys.Count - 1)
                            content.Append("}");
                        else
                            content.Append(",'");
                    }
                    if (i != dateList.Count - 1)
                        content.Append(",");
                }
                content.Append("]");
                return content.ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetChartDataByPurifier(string purifierIds, string start, string end)
        {
            DateTime startDate = DateTime.Now, endDate = DateTime.Now;
            if(!ConvertDate(start,end, ref startDate, ref endDate))
                return "failed";
            endDate = endDate.AddDays(1);
            StringBuilder content = new StringBuilder();
            content.Append("[");
            string[] idList = purifierIds.Split('|');
            Dictionary<string, string> upMapping = LoadUsersPurifierMaps();
            List<PurifierRecord> list = PurifierRecord.LoadPurifierRecordForStatisticsById(idList, startDate, endDate);
            Dictionary<string, List<PurifierRecord>> dataDict = GetDateDictionary(list, start.Equals(end));

            foreach (var item in dataDict)
            {
                content.Append("{'date':'");
                content.Append(item.Key);
                content.Append("','");
                foreach (var record in item.Value)
                {
                    string key = record.PurifierID.ToString();
                    string name = record.PurifierID.ToString();
                    if (upMapping.ContainsKey(key))
                        name = upMapping[key];
                    content.Append(name);
                    content.Append("':");
                    content.Append(record.Pm25.ToString());
                    if (record.PurifierID == item.Value[item.Value.Count - 1].PurifierID)
                        content.Append("}");
                    else
                        content.Append(",'");
                }
                if (item.Key != dataDict.Last().Key)
                    content.Append(",");
            }
            content.Append("]");
            return content.ToString();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetChartDataByCity(string citys, string start, string end)
        {
            DateTime startDate = DateTime.Now, endDate = DateTime.Now;
            if (!ConvertDate(start, end, ref startDate, ref endDate))
                return "failed";
            endDate = endDate.AddDays(1);

            string[] cityArr = citys.Split('|');
            List<PurifierInformation> infoList = PurifierInformation.GetInfoByCitys(cityArr);
            Dictionary<string, List<long>> cityIdDict = new Dictionary<string, List<long>>();
            foreach (var info in infoList)
            {
                if(string.IsNullOrEmpty(info.City))
                    continue;
                if (!cityIdDict.ContainsKey(info.City))
                    cityIdDict[info.City] = new List<long>();
                cityIdDict[info.City].Add(info.Id);
            }
            string[] ids = infoList.Select(o=>o.Id.ToString()).ToArray();
            List<PurifierRecord> recordList = PurifierRecord.LoadPurifierRecordForStatisticsById(ids, startDate, endDate);
            Dictionary<string, List<PurifierRecord>> dateDict = GetDateDictionary(recordList, start.Equals(end));
            Dictionary<string, Dictionary<string, int>> dateCityPm25Dict = new Dictionary<string, Dictionary<string, int>>();
            Dictionary<string, int> cityPm25Dict;
            foreach (var list in dateDict)
            {
                cityPm25Dict = new Dictionary<string, int>();
                foreach (var record in list.Value)
                {
                    foreach (var item in cityIdDict)
                    {
                        if (item.Value.Contains(record.PurifierID))
                        {
                            cityPm25Dict[item.Key] = (cityPm25Dict.ContainsKey(item.Key) ? (int)((cityPm25Dict[item.Key] + record.Pm25) / 2) : (int)record.Pm25);
                            break;
                        }
                    }
                }
                dateCityPm25Dict[list.Key] = cityPm25Dict;
            }

            StringBuilder content = new StringBuilder();
            content.Append("[");
            foreach (var item in dateCityPm25Dict)
            {
                content.Append("{'date':'");
                content.Append(item.Key);
                content.Append("','");
                foreach (var record in item.Value)
                {
                    content.Append(record.Key);
                    content.Append("':");
                    content.Append(record.Value.ToString());
                    if (record.Key == item.Value.Last().Key)
                        content.Append("}");
                    else
                        content.Append(",'");
                }
                // content.Append("}");
                if (item.Key != dateCityPm25Dict.Last().Key)
                    content.Append(",");
            }
            content.Append("]");
            return content.ToString();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public DataInterval GetIntervalTime()
        {
            return DataInterval.GetInfo(1);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool SetIntervalTime(int uploadTime, int searchTime)
        {
            var info = DataInterval.GetInfo(1);
            info.UploadInterval = uploadTime;
            info.SearchInterval = searchTime;
            info.Save();
            return true;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetNeedFliterPurifier()
        {
             HttpContext context = HttpContext.Current;
             if (context.User == null)
                return null;
            List<object> rstList = new List<object>();
             Account account = (context.User as User).GetAccount();
            List<PurifierInfoView> viewList;
            if (account.IsAdmin)
                viewList = PurifierInfoView.LoadAllNeedFilter();
            else
                viewList = PurifierInfoView.LoadUserPurifiersNeedFilter(account.Id);
            foreach (var info in viewList)
            {
                List<Account> userList = Account.GetUserByPurifierId(info.Id.ToString());
                bool hasuser = userList.Count > 0;
                
                rstList.Add(new
                {
                    SNCode = info.SNCode??"",
                    Name = info.Name,
                    UserName = hasuser ? userList[0].Name : "",
                    Phone = hasuser ? userList[0].Phone: "",
                    Email = hasuser ? userList[0].Email: "",
                    Province = hasuser ? userList[0].Province: "",
                    City = hasuser ? userList[0].City: "",
                    Address = hasuser ? userList[0].Address : "",
                    HasUser = hasuser,
                });
            }
            return new ModelListEx<object>() { success = true, data = rstList, total = rstList.Count };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public int GetNeedFliterPurifierCount()
        {
            HttpContext context = HttpContext.Current;
            if (context.User == null)
                return 0;
            Account account = (context.User as User).GetAccount();
            List<PurifierInfoView> viewList;
            if (account.IsAdmin)
                viewList = PurifierInfoView.LoadAllNeedFilter();
            else
                viewList = PurifierInfoView.LoadUserPurifiersNeedFilter(account.Id);
            return viewList.Count;

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Account> GetUserInfo(long purifierid)
        {
            return Account.GetUserByPurifierId(purifierid.ToString());
        }

        object GetRecordObjectSingle(PurifierInformation info, PurifierRecord o)
        {
            var status = PurifierStatus.GetByID(o.PurifierID);
            if (status == null)
                return (new
                {
                    Id = o.Id,
                    SNCode = info.SNCode??"",
                    Name = info.Name,
                    PurifierID = o.PurifierID,
                    Pm25 = o.Pm25,
                    Temperature = o.Temperature,
                    Humidity = o.Humidity,
                    CreateDate = o.CreateDate,
                });
            else
                return (new
                {
                    Id = o.Id,
                    SNCode = info.SNCode??"",
                    Name = info.Name,
                    PurifierID = o.PurifierID,
                    Pm25 = o.Pm25,
                    Temperature = o.Temperature,
                    Humidity = o.Humidity,
                    CreateDate = o.CreateDate,
                    Tear = status.Tear,
                    OnOffKey = status.OnOffKey,
                    LockKey = status.LockKey,
                    SleepKey = status.SleepKey,
                    AutoKey = status.AutoKey,
                    AnionKey = status.AnionKey,
                    UVKey = status.UVKey,
                    IsFliter = status.IsFliter,
                    Hour = status.TimerHour,
                    Min = status.TimerMin,
                });
        }

        object GetRecordObject(Dictionary<long, PurifierInformation> infoDict, PurifierRecord o)
        {
            var status = PurifierStatus.GetByID(o.PurifierID);
            if (status == null)
                return (new
                {
                    Id = o.Id,
                    SNCode = infoDict.ContainsKey(o.PurifierID) ? (infoDict[o.PurifierID].SNCode??"") : "",
                    Name = infoDict.ContainsKey(o.PurifierID) ? infoDict[o.PurifierID].Name : "",
                    PurifierID = o.PurifierID,
                    Pm25 = o.Pm25,
                    Temperature = o.Temperature,
                    Humidity = o.Humidity,
                    CreateDate = o.CreateDate,
                });
            else
                return (new
                {
                    Id = o.Id,
                    SNCode = infoDict.ContainsKey(o.PurifierID) ? (infoDict[o.PurifierID].SNCode??"") : "",
                    Name = infoDict.ContainsKey(o.PurifierID) ? infoDict[o.PurifierID].Name : "",
                    PurifierID = o.PurifierID,
                    Pm25 = o.Pm25,
                    Temperature = o.Temperature,
                    Humidity = o.Humidity,
                    CreateDate = o.CreateDate,
                    Tear = status.Tear,
                    OnOffKey = status.OnOffKey,
                    LockKey = status.LockKey,
                    SleepKey = status.SleepKey,
                    AutoKey = status.AutoKey,
                    AnionKey = status.AnionKey,
                    UVKey = status.UVKey,
                    IsFliter = status.IsFliter,
                    Hour = status.TimerHour,
                    Min = status.TimerMin,
                });
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetRecordBySNCodeForExtjs(string sncode)
        {
            try
            {
                int total = 0;
                List<object> rstList = new List<object>();

                PurifierInformation info = PurifierInformation.GetInfoBySNCode(sncode);
                if (info != null)
                {
                    if (CurrentUser.IsInRole(Role.AdminRole) ||
                        UserPurifier.GetOwnerIds(info).Any(o => o.UserId == CurrentUser.Id))
                    {
                        PurifierRecord record = PurifierRecord.GetNewestRecord(info.MAC);
                        if (record != null)
                        {
                            rstList.Add(GetRecordObjectSingle(info, record));
                        }
                        total = 1;
                    }
                }
                
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    total = total,
                    isall = (rstList == null || total == rstList.Count),
                    message = "",
                    data = rstList,
                    success = true
                };
                return modelList;
            }
            catch (Exception ex)
            {
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    message = ex.ToString(),
                    success = false
                };
                return modelList;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetRecordBySNCodeForExtjsWithPage(string sncode, int page, int limit)
        {
            try
            {
                int total = 0;
                List<object> rstList = new List<object>();
                if (CurrentUser.IsInRole(Role.AdminRole))
                {
                    List<PurifierInformation> infoList = PurifierInformation.GetInfoBySNCodeEx(sncode);
                    total = infoList.Count;
                    Dictionary<long, PurifierInformation> infoDict = infoList.ToDictionary<PurifierInformation, long>(o => o.Id);
                    List<PurifierRecord> recordList = PurifierRecord.GetNewestRecordsByMac(infoList.Select(o => o.MAC).ToList(), limit, page - 1);
                    if (recordList != null)
                    {
                        foreach (var o in recordList)
                        {
                            rstList.Add(GetRecordObject(infoDict, o));
                        }
                    }
                }
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    total = total,
                    isall = (rstList == null || total == rstList.Count),
                    message = "",
                    data = rstList,
                    success = true
                };
                return modelList;
            }
            catch (Exception ex)
            {
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    message = ex.ToString(),
                    success = false
                };
                return modelList;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetRecordBySNCode(string sncode)
        {
            PurifierInformation info = PurifierInformation.GetInfoBySNCode(sncode);
            List<object> rstList = new List<object>();
            if(info == null)
                return new ModelListEx<object>() { total = 0, message = "", data = rstList };
            int total = 0;
            if (CurrentUser.IsInRole(Role.AdminRole) ||
                UserPurifier.GetOwnerIds(info).Any(o=>o.UserId == CurrentUser.Id))
            {
                PurifierRecord record = PurifierRecord.GetNewestRecord(info.MAC);
                if (record != null)
                {
                    rstList.Add(new
                    {
                        Id = record.Id,
                        SNCode = info.SNCode??"",
                        Name = info.Name,
                        PurifierID = record.PurifierID,
                        Aqi = record.Pm25,
                        Pm25 = record.Pm25,
                        Temperature = record.Temperature,
                        Humidity = record.Humidity,
                        CreateDate = record.CreateDate,
                        IsPurifier = true,
                        NeedChangeFilter = IsFilter(record.PurifierID),
                        Ispm25in = false,
                    });
                }
                total = 1;
            }
            return new ModelListEx<object>() { total = total, message = "搜索完成", data = rstList };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool Login(string userName, string password)
        {
            HttpContext context = HttpContext.Current;
            context.User = AuthenticationManager.Instance.Login(userName, password);
            return context.User.IsInRole(Role.AdminRole);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetAllUser(int page, int rows)
        {
            int total = Account.GetUserCount();
            List<Account> userList = Account.GetUsersByPage(page - 1, rows, "UpdatedDate", OrderDirection.DESC);
            List<object> rstList = new List<object>();
            foreach (var account in userList)
            {
                rstList.Add(new {
                    Purifiers = GetUserPurifers(account.Id),
                    Id = account.Id,
                    Email = account.Email,
                    UserName = account.UserName,
                    PwdHash = account.PwdHash,
                    Name = account.Name,
                    Phone = account.Phone,
                    City = account.City,
                    Province = account.Province,
                    Address = account.Address,
                    LoginDate = account.LoginDate,
                    CreatedDate = account.CreatedDate,
                    UpdatedDate = account.UpdatedDate,
                    IsAdmin = account.IsAdmin
                });
            }
            return new ModelListEx<object>() { total = total, isall = total == rstList.Count, message = "获取用户信息完成", data = rstList, success = true };
        }

        public String GetUserPurifers(Guid userid)
        {
            List<PurifierInformation> list = PurifierInformation.LoadUserPurifiers(userid);
            var rst = "";
            list.ForEach(o => rst += o.SNCode + ",");
            if (rst.Length > 1)
                rst = rst.Substring(0, rst.Length - 1);
            return rst; 
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object AddUser(string UserName, string PwdHash, string Email, string Phone, string Name,
            string Province, string City, string Address, bool IsAdmin)
        {
            var user = Account.Create(UserName, PwdHash, Email, Phone, Name, Province, City, Address, IsAdmin);
            user.Save(false);
            return new {success=true, message="添加用户成功" };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object AddUserWithBinding(string UserName, string PwdHash, string Email, string Phone, string Name,
            string Province, string City, string Address, bool IsAdmin, List<string> Ids)
        {
            int flag = 1;
            var message = "";
            try
            {
                bool isExist = Account.IsExist(UserName);
                if (isExist)
                    flag = -1;
                else
                {
                    var user = Account.Create(UserName, PwdHash, Email, Phone, Name, Province, City, Address, IsAdmin);
                    user.Save(false);

                    if (Ids != null && Ids.Count != 0)
                        flag = UserPurifier.BindingPurifiersForUserName(UserName, GetPurifierIds(Ids));
                }
            }
            catch (Exception ex) { flag = 0; message = ex.ToString(); }
            return new { code = flag, success = true, message = message };
        }

        public List<long> GetPurifierIds(List<string> sncodes)
        {
            List<PurifierInformation> infoList = PurifierInformation.LoadPurifiersForSNCodes(sncodes);
            return infoList.Select(o => o.Id).ToList();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object EditUser(string UserName, string PwdHash, string Email, string Phone, string Name,
            string Province, string City, string Address, bool IsAdmin)
        {
            Account.EditUser(UserName, PwdHash, Email, Phone, Name, Province, City, Address, IsAdmin);
            return new { success = true, message = "修改用户成功" };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object EditUserWithBinding(string UserName, string PwdHash, string Email, string Phone, string Name,
            string Province, string City, string Address, bool IsAdmin, List<string> Ids)
        {
            bool success = true;
            string message="";
            try
            {
                Account.EditUser(UserName, PwdHash, Email, Phone, Name, Province, City, Address, IsAdmin);
                if(Ids!=null && Ids.Count!=0)
                    UserPurifier.BindingPurifiersForUserName(UserName, GetPurifierIds(Ids));
            }
            catch (Exception ex) { success = false; message = ex.ToString(); }
            return new { success = success, message = message };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object EditUserWithBindingExtjs(string UserName, string Email, string Phone, string Name,
            string Province, string City, string Address, bool IsAdmin, List<string> Ids)
        {
            bool success = true;
            string message = "";
            try
            {
                bool editRst = Account.EditUser(UserName, Email, Phone, Name, Province, City, Address, IsAdmin);
                if (!editRst)
                {
                    success = false;
                    message = "1";
                }
                if (Ids != null && Ids.Count != 0)
                    UserPurifier.BindingPurifiersForUserName(UserName, GetPurifierIds(Ids));
            }
            catch (Exception ex) { success = false; message = ex.ToString(); }
            return new { success = success, message = message };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void DelUser(string UserName)
        {
            UserPurifier.DeleteBindsForUserName(UserName);
            Account.DelUser(UserName);
            UserLog.Save("delete " + UserName, CurrentUser.GetAccount().UserName);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetUserBySearch(string username)
        {
            Account account = Account.SearchUser(username);
            List<object> rstList = new List<object>();
            if (account != null)
            {
                rstList.Add(new
                     {
                         Purifiers = GetUserPurifers(account.Id),
                         Id = account.Id,
                         Email = account.Email,
                         UserName = account.UserName,
                         PwdHash = account.PwdHash,
                         Name = account.Name,
                         Phone = account.Phone,
                         City = account.City,
                         Province = account.Province,
                         Address = account.Address,
                         LoginDate = account.LoginDate,
                         CreatedDate = account.CreatedDate,
                         UpdatedDate = account.UpdatedDate,
                         IsAdmin = account.IsAdmin
                     });
            }
            return new ModelListEx<object>() { total = rstList.Count, message = "搜索完成", data = rstList };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object UserBindPurifier(Guid UserId, List<long> Infos)
        {
            UserPurifier.BindingPurifiersForUser(UserId, Infos);
            return new{
                message = "绑定成功",
                success = true
            };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetBindPurifiers(Guid UserId)
        {
            List<PurifierInformation> list = PurifierInformation.LoadUserPurifiers(UserId);
            ModelListEx<object> modelList = new ModelListEx<object>() { 
                total = list.Count,
                message = "",
                data = list.ToList<object>(),
                success = true
            };
            return modelList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetExceptPurifierInfo(Guid UserId, int page, int rows)
        {
            //List<UserPurifier> list = UserPurifier.GetBindsForUser(UserId);
            //string[] ids = list.Select(o => o.PurifierId.ToString()).ToArray();
            List<PurifierInformation> infos = PurifierInformation.LoadAllExcept(UserId, rows, page - 1, "CreatedTime", OrderDirection.DESC); 
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = infos.Count,
                message = "",
                data = infos.ToList<object>(),
                success = true
            };
            return modelList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> SearchExceptPurifierInfo(Guid UserId, string sncode)
        {
            PurifierInformation info = PurifierInformation.GetInfoBySNCodeNotForUser(UserId, sncode);
            List<PurifierInformation> infoList = new List<PurifierInformation>();
            if (info != null)
                infoList.Add(info);
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = infoList.Count,
                message = "",
                data = infoList.ToList<object>(),
                success = true
            };
            return modelList;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool IsExistSNCode(string sncode)
        {
            return PurifierInformation.GetInfoBySNCode(sncode) != null;
        }

        public List<PurifierInformation> GetInfo()
        {
            List<PurifierInformation> infos = new List<PurifierInformation>();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInformation.LoadAll());
            }
            else
            {
                infos.AddRange(PurifierInformation.LoadUserPurifiers(CurrentUser.Id));
            }
            return infos;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetPurifierForExtjs(int page, int limit)
        {
            int total = 0;
            try
            {
                List<PurifierInformation> infoList = GetInfo();
                Dictionary<long, PurifierInformation> infoDict = infoList.ToDictionary<PurifierInformation, long>(o => o.Id);
                if (infoList.Count == 0)
                    return null;

                List<PurifierRecord> recordList = null;
                if (CurrentUser.IsInRole(Role.AdminRole))
                {
                    recordList = PurifierRecord.GetNewestRecordByPage(limit, page - 1, "CreateDate", OrderDirection.DESC);
                    total = PurifierRecord.GetNewestRecordCount();
                }
                else
                {
                    string[] purifierIds = infoList.Select(o => o.Id.ToString()).ToArray();
                    recordList = PurifierRecord.GetNewestRecordByPageForUser(purifierIds, limit, page - 1, "CreateDate", OrderDirection.DESC);
                    total = PurifierRecord.GetNewestRecordCountForUser(purifierIds);
                }
                List<object> rstList = new List<object>();

                Func<long, PurifierStatus> g = pid =>
                {
                    PurifierStatus status = PurifierStatus.GetByID(pid);
                    return status;
                };

                foreach (var o in recordList)
                {
                    rstList.Add(GetRecordObject(infoDict, o));
                }

                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    total = total,
                    isall = (rstList == null || total == rstList.Count),
                    message = "",
                    data = rstList,
                    success = true
                };
                return modelList;
            }
            catch (Exception ex)
            {
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    message = ex.ToString(),
                    success = false
                };
                return modelList;
            }
        }

        public List<object> GetPurifierRecord(int page, int rows, ref int total)
        {
            List<PurifierInformation> infoList = GetInfo();
            Dictionary<long, PurifierInformation> infoDict = infoList.ToDictionary<PurifierInformation, long>(o => o.Id);
            if (infoList.Count == 0)
                return null;

            List<PurifierRecord> recordList = null;
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                recordList = PurifierRecord.GetNewestRecordByPage(rows, page - 1, "CreateDate", OrderDirection.DESC);
                total = PurifierRecord.GetNewestRecordCount();
            }
            else
            {
                string[] purifierIds = infoList.Select(o => o.Id.ToString()).ToArray();
                recordList = PurifierRecord.GetNewestRecordByPageForUser(purifierIds, rows, page - 1, "CreateDate", OrderDirection.DESC);
                total = PurifierRecord.GetNewestRecordCountForUser(purifierIds);
            }
            List<object> rstList = new List<object>();

            recordList.ForEach(o => rstList.Add(new
            {
                Id = o.Id,
                SNCode = infoDict.ContainsKey(o.PurifierID) ? (infoDict[o.PurifierID].SNCode??"") : "",
                Name = infoDict.ContainsKey(o.PurifierID) ? (infoDict[o.PurifierID].Name??"") : "",
                PurifierID = o.PurifierID,
                Aqi = o.Pm25,
                Pm25 = o.Pm25,
                Temperature = o.Temperature,
                Humidity = o.Humidity,
                CreateDate = o.CreateDate,
                IsPurifier = true,
                NeedChangeFilter = IsFilter(o.PurifierID),
                Ispm25in = false,
            }));

            return rstList;
        }

        bool IsFilter(long pid)
        {
            PurifierStatus status = PurifierStatus.GetByID(pid);
            if (status != null)
                return status.IsFliter == 1;
            return false;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetQuality()
        {
            try
            {
                List<UserCity> usercityList = UserCity.GetBindsForUser(CurrentUser.Id).OrderBy(o=>o.sortid).ToList();
                List<string> qm25incitynames = usercityList.Where(o => o.isqm25in).Select(o => o.cityname).ToList();
                List<string> aqicncitynames = usercityList.Where(o => !o.isqm25in).Select(o => o.cityname).ToList();
                List<int> sortid = usercityList.Where(o => !o.isqm25in).Select(o => o.sortid).ToList();
                List<quality_record> records = new List<quality_record>();
                if (qm25incitynames != null && qm25incitynames.Count > 0)
                    records.AddRange(quality_record_qm25in.GetRecordeListByCityNames(qm25incitynames).Cast<quality_record>());

                if (aqicncitynames != null && aqicncitynames.Count > 0)
                {
                   List<quality_record> list = quality_record_aqicn.GetRecordeListByCityNames(aqicncitynames).Cast<quality_record>().ToList();
                   for (int i = 0; i < list.Count; i++)
                   {
                       records.Insert(sortid[i], list[i]);
                   }
                }

                List<object> rstList = new List<object>();
                records.ForEach(o => rstList.Add(new
                {
                    Id = o.Id,
                    Name = o.cityname,
                    PurifierID = o.cityid,
                    Aqi = o.aqi,
                    Pm25 = o.pm25,
                    Temperature = o.pm10,
                    Humidity = o.o3,
                    CreateDate = o.publishtime,
                    Ispm25in = string.IsNullOrEmpty(o.png),
                }));

              
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    total = records.Count,
                    message = "",
                    data = rstList,
                    success = true
                };
                return modelList;
            }
            catch (Exception ex)
            {
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    message = ex.ToString(),
                    success = false
                };
                return modelList;
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetQualityAndPurifier(List<string> qm25incitynames,List<string> aqicncitynames, int page, int rows)
        {
            try
            {
                List<quality_record> records = new List<quality_record>();
                if (page == 1)
                {
                    if (qm25incitynames != null && qm25incitynames.Count > 0)
                        records.AddRange(quality_record_qm25in.GetRecordeListByCityNames(qm25incitynames).Cast<quality_record>());

                    if (aqicncitynames != null && aqicncitynames.Count > 0)
                        records.AddRange(quality_record_aqicn.GetRecordeListByCityNames(aqicncitynames).Cast<quality_record>());
                    records.OrderBy(o => o.cityname);
                }
                int citycount = records.Count;
                int total = 0;
                List<object> purifierList = GetPurifierRecord(page, rows, ref total);

                List<object> rstList = new List<object>();
                records.ForEach(o => rstList.Add(new
                {
                    Id = o.Id,
                    SNCode = "",
                    Name = o.cityname??"",
                    PurifierID = o.cityid,
                    Aqi = o.aqi,
                    Pm25 = o.pm25,
                    Temperature = o.pm10,
                    Humidity = o.o3,
                    CreateDate = o.publishtime,
                    IsPurifier = false,
                    NeedChangeFilter = IsFilter(o.Id),
                    Ispm25in = string.IsNullOrEmpty(o.png),
                }));

                if(purifierList!=null)
                    rstList.AddRange(purifierList);

                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    total = total + citycount,
                    isall = (purifierList==null || total == purifierList.Count),
                    message = "",
                    data = rstList,
                    success = true
                };
                return modelList;
            }
            catch (Exception ex) {
                ModelListEx<object> modelList = new ModelListEx<object>()
                {
                    message = ex.ToString(),
                    success = false
                };
                return modelList;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityname"></param>
        /// <param name="type">0 is qm25in, 1 is aqicn</param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetStationQuality(string cityname, int type)
        {
            List<quality_record> records = new List<quality_record>();
            if (type == 0)
            {
                var stationRecords = quality_record_qm25in.GetStationRecordForCity(cityname);
                if (stationRecords != null)
                    records.AddRange(stationRecords.Cast<quality_record>());
            }
            else if (type == 1)
            {
                var stationRecords = quality_record_aqicn.GetStationRecordForCity(cityname);
                if (stationRecords != null)
                    records.AddRange(stationRecords.Cast<quality_record>());
            }
            List<object> rstList = new List<object>();
            records.ForEach(o => rstList.Add(new
            {
                Id = o.Id,
                SNCode = "",
                Name = o.cityname,
                PurifierID = o.cityid,
                Aqi = o.aqi,
                Pm25 = o.pm25,
                Temperature = o.pm10,
                Humidity = o.o3,
                CreateDate = o.publishtime,
                IsPurifier = false,
                NeedChangeFilter = false,
                Ispm25in = string.IsNullOrEmpty(o.png),
            }));
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = records.Count,
                message = "",
                data = rstList,
                success = true
            };
            return modelList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<object> GetCitys(int type)
        {
            List<object> rstList=new List<object>();
            if (type == 1)
            {
                List<quality_city_qm25in> list = quality_city_qm25in.LoadCitys();
                foreach (var item in list)
                {
                    rstList.Add(new { 
                        name = item.name,
                        py = item.py,
                        sz = item.sz,
                    });
                }
            }
            else
            {
                List<quality_city_aqicn> list = quality_city_aqicn.LoadCitys();
                foreach (var item in list)
                {
                    rstList.Add(new
                    {
                        name = item.name,
                        py = item.py,
                        sz = item.sz,
                    });
                }
            }
            return rstList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<UserCity> GetUserCitys()
        {
            return UserCity.GetBindsForUser(CurrentUser.Id);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public List<UserCity> GetUserCitysExtjs()
        {
            return UserCity.GetBindsForUser(CurrentUser.Id);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> UpdateUserCitys(List<TargetCity> list)
        {
            try
            {
                UserCity.BindingCityForUser(CurrentUser.Id, list);
                return new ModelListEx<object>()
                {
                    success = true
                };
            }
            catch (Exception ex)
            {
                return new ModelListEx<object>()
                {
                    success = false,
                    message = ex.ToString()
                };
            }
        }

    }
}
