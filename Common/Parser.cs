﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Common
{
    public static class Parser
    {
        class _EnumConverter : EnumConverter
        {
            public _EnumConverter(Type type)
                : base(type)
            {
            }

            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                if (base.CanConvertFrom(context, sourceType))
                    return true;
                else
                {
                    return IsNumericType(sourceType);
                }
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                if (value != null && IsNumericType(value.GetType()))
                {
                    return Enum.ToObject(EnumType, value);
                }
                return base.ConvertFrom(context, culture, value);
            }
        }

        class _BooleanConverter : BooleanConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return base.CanConvertFrom(context, sourceType) || IsNumericType(sourceType);
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                try
                {
                    return base.ConvertFrom(context, culture, value);
                }
                catch
                {
                    return Convert(value, culture);
                }
            }

            bool Convert(object value, CultureInfo culture)
            {
                if (value == null) return false;
                if (value is string)
                {
                    if (Tools.CompareStr("yes", (string)value))
                        return true;
                    else if (Tools.IsNumeric((string)value) && Parse(value, 0) > 0)
                        return true;
                    else
                        return false;
                }
                else if (IsNumericType(value.GetType()))
                {
                    return ((IConvertible)value).ToBoolean(culture);
                }
                else
                {
                    return false;
                }
            }
        }

        class _GuidConverter : GuidConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return base.CanConvertFrom(context, sourceType) || sourceType == typeof(string);
            }
            public override bool IsValid(ITypeDescriptorContext context, object value)
            {
                return base.IsValid(context, value) || Tools.IsGuidString(value.ToString());
            }
            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                if (value.GetType() == typeof(string) && Tools.IsGuidString(value.ToString()))
                    return new Guid(value.ToString());
                else
                    return base.ConvertFrom(context, culture, value);
            }
        }

        class _UInt64Converter : UInt64Converter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return base.CanConvertFrom(context, sourceType) || (sourceType == typeof(byte[]));
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                try
                {
                    return base.ConvertFrom(context, culture, value);
                }
                catch
                {
                    if (value is byte[])
                        return ConvertFromBytes((byte[])value);
                    else
                        throw;
                }
            }

            object ConvertFromBytes(byte[] bytes)
            {
                List<byte> list = new List<byte>();
                if (bytes.Length < 8)
                {
                    list.AddRange(bytes);

                    while (list.Count < 8)
                        list.Add(0);
                }
                else if (bytes.Length > 8)
                    list.AddRange(bytes.Take(8));
                else
                    list.AddRange(bytes);
                return BitConverter.ToInt64(list.ToArray(), 0);
            }
        }

        class _Int32Converter : Int32Converter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return base.CanConvertFrom(context, sourceType) || sourceType == typeof(long);
            }
            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                if (value != null && value.GetType() == typeof(long))
                {
                    long lv = (long)value;
                    long imx = (long)int.MaxValue;
                    long imn = (long)int.MinValue;
                    if (lv <= imx || lv >= imn)
                        return (int)lv;
                    else
                        throw new InvalidCastException("Can not convert long value '" + lv + "' to int32");
                }
                return base.ConvertFrom(context, culture, value);
            }
        }
        static bool IsNumericType(Type sourceType)
        {
            return typeof(int) == sourceType || typeof(short) == sourceType
                                || typeof(long) == sourceType || typeof(uint) == sourceType
                                || typeof(uint) == sourceType || typeof(ushort) == sourceType
                                || typeof(ulong) == sourceType;
        }

        static TypeConverter GetConverterFromType(Type type)
        {
            if (type.IsEnum)
            {
                return new _EnumConverter(type);
            }
            if (typeof(int).IsAssignableFrom(type))
            {
                return new _Int32Converter();
            }
            if (typeof(short).IsAssignableFrom(type))
            {
                return new Int16Converter();
            }
            if (typeof(long).IsAssignableFrom(type))
            {
                return new _UInt64Converter();
            }
            if (typeof(uint).IsAssignableFrom(type))
            {
                return new UInt32Converter();
            }
            if (typeof(ushort).IsAssignableFrom(type))
            {
                return new UInt16Converter();
            }
            if (typeof(ulong).IsAssignableFrom(type))
            {
                return new UInt64Converter();
            }
            if (typeof(bool).IsAssignableFrom(type))
            {
                return new _BooleanConverter();
            }
            if (typeof(double).IsAssignableFrom(type))
            {
                return new DoubleConverter();
            }
            if (typeof(float).IsAssignableFrom(type))
            {
                return new SingleConverter();
            }
            if (typeof(byte).IsAssignableFrom(type))
            {
                return new ByteConverter();
            }
            if (typeof(sbyte).IsAssignableFrom(type))
            {
                return new SByteConverter();
            }
            if (typeof(char).IsAssignableFrom(type))
            {
                return new CharConverter();
            }
            if (typeof(decimal).IsAssignableFrom(type))
            {
                return new DecimalConverter();
            }
            if (typeof(TimeSpan).IsAssignableFrom(type))
            {
                return new TimeSpanConverter();
            }
            if (typeof(Guid).IsAssignableFrom(type))
            {
                return new _GuidConverter();
            }
            if (typeof(string).IsAssignableFrom(type))
            {
                return new StringConverter();
            }
            if (typeof(CultureInfo).IsAssignableFrom(type))
            {
                return new CultureInfoConverter();
            }
            if (typeof(DateTime).IsAssignableFrom(type))
            {
                return new DateTimeConverter();
            }
            return null;
        }

        /// <summary>
        /// Create a T instance through parsing an object.
        /// </summary>
        /// <typeparam name="T">A type of result parsed</typeparam>
        /// <param name="value">A object paresed</param>
        /// <returns>Return an instance of T</returns>
        public static T Parse<T>(object value)
        {
            return Parse<T>(value, default(T));
        }

        static readonly Dictionary<Type, TypeConverter> s_TypeConvertersDic = new Dictionary<Type, TypeConverter>();

        /// <summary>
        /// Get a TypeConverter object for one type.
        /// </summary>
        /// <param name="type">A type wants TypeConverter</param>
        /// <returns>Return TypeConverter of the type</returns>
        public static TypeConverter GetTypeConverter(Type type)
        {
            lock (s_TypeConvertersDic)
            {
                if (!s_TypeConvertersDic.ContainsKey(type))
                    s_TypeConvertersDic[type] = GetConverterFromType(type);
                return s_TypeConvertersDic[type];
            }
        }

        /// <summary>
        /// Create a T instance through parsing an object.
        /// </summary>
        /// <typeparam name="T">A type of result parsed</typeparam>
        /// <param name="value">A object paresed</param>
        /// <param name="def">A T instance as default value when parsing fail.</param>
        /// <returns>Return an instance of T</returns>
        public static T Parse<T>(object value, T def)
        {
            if (value == null || value == DBNull.Value)
                return def;
            Type tType = typeof(T);
            if (tType.IsAssignableFrom(value.GetType()))
                return (T)value;
            try
            {
                var converter = GetTypeConverter(tType);
                if (converter != null && converter.CanConvertFrom(value.GetType()))
                {
                    return (T)converter.ConvertFrom(value);
                }
            }
            catch
            {
            }
            try
            {
                if (value is IConvertible)
                {
                    return (T)Convert.ChangeType(value, tType, CultureInfo.InvariantCulture);
                }
                else
                {
                    return (T)Convert.ChangeType(value.ToString(), tType, CultureInfo.InvariantCulture);
                }
            }
            catch
            {
                return def;
            }
        }
    }
}
