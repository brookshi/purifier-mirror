﻿namespace HOYI.Common.Data
{
    public enum OrderDirection : short
    {
        ASC,
        DESC
    }

    public enum RecordFlag
    {
        First = 0,
        Newest = 1,
        Avg = 2,
    }

    public static class RecordCode
    {
        public static RecordFlag From(string code)
        {
            switch (code)
            {
                case "0":
                    return RecordFlag.First;
                case "1":
                    return RecordFlag.Newest;
                case "2":
                    return RecordFlag.Avg;
            }
            return RecordFlag.Avg;
        }

        public static string Convert(this RecordFlag flag)
        {
            switch (flag)
            {
                case RecordFlag.First:
                    return "0";
                case RecordFlag.Newest:
                    return "1";
                case RecordFlag.Avg:
                    return "2";
            }
            return "-1";
        }
    }
}
