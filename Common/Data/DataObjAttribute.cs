﻿using System;

namespace HOYI.Common.Data
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class DataObjAttribute : Attribute
    {
        public DataObjAttribute()
        {
            EnableMultiDb = false;
            DbCount = 0;
            SqlServerDBName = string.Empty;
        }

        public string DBTemplate { get; set; }
        public string TableName { get; set; }
        public string DBName { get; set; }
        public string PrimaryKeyName { get; set; }
        public bool EnableMultiDb { get; set; }

        public int DbCount { get; set; }
        public string SqlServerDBName { get; set; }
        public Type IdGeneratorType { get; set; }
    }
}
