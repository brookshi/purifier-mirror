﻿using System;
using System.Collections.Generic;


namespace HOYI.Common.Data.SqlServer
{
    public class SqlServerPersistenceHelper : PersistenceHelper
    {
       
        protected override bool SupportEnableMultiDb { get { return false; } }

        public SqlServerPersistenceHelper(DBSetting setting)
            : base(setting)
        {

        }

        protected override Dictionary<string, List<T>> GroupDataObjByDb<T>(IEnumerable<T> dataObjs)
        {
            var dic = new Dictionary<string, List<T>>();
            foreach (var obj in dataObjs)
            {
                if (obj != null)
                {
                    string dbName = GetDbName(obj.GetType());
                    if (!dic.ContainsKey(dbName))
                        dic[dbName] = new List<T>();
                    dic[dbName].Add(obj);
                }
            }
            return dic;
        }

        protected override IDataObjPersister CreateDataObjPersister(string dbName)
        {
            var dop = new SqlServerDataObjPersister();
            DBSetting setting = Setting.Clone();
            setting.Database = dbName;
            dop.ConnectionString = GetConnectionString(setting);
            return dop;
        }

        protected override string GetConnectionString(IDBSetting setting)
        {
            string serverName = setting.Server;
            if (string.IsNullOrEmpty(serverName))
                serverName = Environment.MachineName;
            return !string.IsNullOrEmpty(setting.User)
                ? string.Format("Server={0};Database={1};User ID={2};Password={3};Trusted_Connection=False;Max Pool Size=1000;", serverName, setting.Database, setting.User, setting.Password)
                : string.Format("Data Source={0};Initial Catalog={1};Integrated Security=SSPI;Max Pool Size=1000;", serverName, setting.Database);
        }

        protected override string GetDbName(Type dataObjType, object objId)
        {
            return GetDbName(dataObjType);
        }

        string GetDbName(Type dataObjType)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(dataObjType);
            string databaseName = (Setting.Database ?? "").Trim();
            if (string.IsNullOrEmpty(databaseName)) 
                databaseName = doi.SqlServerDBName;
            return databaseName;
        }

    }
}
