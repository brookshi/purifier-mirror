﻿using System;
using System.Data;

namespace HOYI.Common.Data.Indexers
{
    public class DbDataReaderIndexer : DataIndexer
    {
        IDataReader reader;

        internal DbDataReaderIndexer(IDataReader reader)
        {
            if (reader == null)
                throw new ArgumentNullException("reader");
            this.reader = reader;
        }

        protected override object GetValue(string name)
        {
            int order = reader.GetOrdinal(name);
            if (order < 0)
                throw new ArgumentException("Can't find '" + name + "' in data source.");
            object value = reader.GetValue(order);
            return value;
        }

        public override bool Contains(string name)
        {
            try
            {
                int order = reader.GetOrdinal(name);
                return (order >= 0);
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
    }
}
