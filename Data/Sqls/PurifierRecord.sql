USE [Purifier]
GO

/****** Object:  Table [dbo].[PurifierRecord]    Script Date: 01/06/2014 18:04:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PurifierRecord](
	[ID] [uniqueidentifier] NOT NULL,
	[PurifierID] [bigint] NOT NULL,
	[MAC] [varchar](20) NULL,
	[IP] [varchar](20) NULL,
	[PM25] [float] NULL,
	[Temperature] [float] NULL,
	[Humidity] [float] NULL,
	[LastUpdateTime] [datetime] NULL,
	[Flag] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreatedTime] [datetime] NULL,
 CONSTRAINT [PK_PURIFIERRECORD] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

