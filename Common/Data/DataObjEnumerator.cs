﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using HOYI.Common.Data.Indexers;

namespace HOYI.Common.Data
{
    public class DataObjEnumerator<T> : IEnumerator<T> where T : BaseDataObj
    {
        IDataReader m_Reader;
        IDbCommand m_Command;
        DbDataReaderIndexer m_Indexer;
        T m_Current;

        internal DataObjEnumerator(IDbCommand command)
        {
            m_Command = command;
            m_Reader = null;
        }

        public T Current { get { return m_Current; } }

        public void Dispose()
        {
            try
            {
                if (m_Reader != null)
                {
                    m_Reader.Close();
                    m_Reader.Dispose();
                }
            }
            catch
            {
            }
            try
            {
                if (m_Command != null)
                    m_Command.Connection.Close();
            }
            catch
            {
            }
            try
            {
                if (m_Command != null)
                    m_Command.Dispose();
                m_Command = null;
            }
            catch
            {
            }
        }

        object IEnumerator.Current { get { return Current; } }

        public bool MoveNext()
        {
            if (m_Reader == null)
                Reset();
            bool r = m_Reader.Read();
            if (r)
                m_Current = BaseDataObj.CreateObjFromPersistence<T>(m_Indexer);
            else
                m_Current = null;

            return r;
        }

        public void Reset()
        {
            if (m_Reader != null)
            {
                m_Reader.Close();
                m_Indexer = null;
                m_Reader = null;
            }
            if (m_Command.Connection.State != ConnectionState.Open)
                m_Command.Connection.Open();
            m_Reader = m_Command.ExecuteReader();
            m_Indexer = new DbDataReaderIndexer(m_Reader);
        }
    }
}
