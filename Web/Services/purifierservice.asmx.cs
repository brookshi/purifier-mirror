﻿using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Data;
using HOYI.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Text;
using Newtonsoft.Json.Linq;

namespace HOYI.Web.Services
{

    [WebService(Namespace = "http://hoyi.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class purfierservice : WebServiceBase
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PageResult<PurifierInfoView> LoadPurifiers(UIPager pager, string queryMode, string queryObj)
        {
            WhereCriterion wc = new WhereCriterion();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                if (queryObj != "ALL")
                {
                    if (queryMode == "city")
                        wc.And("City", OperatorFlag.Equal, queryObj);
                    else
                    {
                        if (!Tools.IsGuidString(queryObj)) throw new ArgumentException("queryObj");
                        SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, queryObj));
                        wc.IN("Id", subWC);
                    }
                }
            }
            else
            {
                SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, CurrentUser.Id));
                wc.IN("Id", subWC);
            }
            PageResult<PurifierInfoView> result = new PageResult<PurifierInfoView>(pager);
            result.LoadData(wc);
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Dictionary<string, string> LoadUsersPurifierMaps()
        {
            List<PurifierInfoView> infos = new List<PurifierInfoView>();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInfoView.LoadAll());
            }
            else
            {
                infos.AddRange(PurifierInfoView.LoadUserPurifiers(CurrentUser.Id));
            }
            ListSorter sorter = new ListSorter("Name", true);
            sorter.Sort(infos);
            return infos.ToDictionary(f => f.Id.ToString(), f => f.Name);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PurifierInfoView AddPurifier(PurifierController info)
        {
            return info.AddPurifier();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PurifierInfoView ModifyPurifier(PurifierController info)
        {
            return info.ModifyPurifier();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<long> DeletePurifier(PurifierController[] infos)
        {
            var isAdmin = IsAdminRole();
            if (!isAdmin) return null;
            return this.DeletePurifiers(infos, this.CurrentUser.Id);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<long> DeletePurifiers(PurifierController[] infos, Guid userId)
        {
            var isAdmin = IsAdminRole();
            if (!isAdmin) return null;
            if (infos == null || infos.Length == 0) return null;
            List<long> ids = new List<long>();
            if (userId != default(Guid))
            {
                foreach (var pfi in infos)
                {
                    pfi.Delete(userId);
                    ids.Add(pfi.Id);
                }
            }
            else
            {
                foreach (var pfi in infos)
                {
                    pfi.Delete();
                    ids.Add(pfi.Id);
                }
            }
            return ids;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PageResult<PurifierRecord> LoadRecords(UIPager pager, RecordFlag flag)
        {
            var wc = new WhereCriterion("Flag", OperatorFlag.Equal, flag);
            PageResult<PurifierRecord> result = new PageResult<PurifierRecord>(pager);
            result.LoadData(wc);
            return result;
        }

        public List<object> GetPurifierRecord(int page, int rows, ref int total)
        {
            List<PurifierInformation> infoList = GetInfo();
            Dictionary<long, PurifierInformation> infoDict = infoList.ToDictionary<PurifierInformation, long>(o => o.Id);
            if (infoList.Count == 0)
                return null;
            total = infoList.Count;
            List<PurifierRecord> recordList = null;
            if (CurrentUser.IsInRole(Role.AdminRole))
                recordList = PurifierRecord.GetNewestRecordByPage(rows, page - 1, "CreateDate", OrderDirection.DESC);
            else
                recordList = PurifierRecord.GetNewestRecordByPageForUser(infoList.Select(o => o.Id.ToString()).ToArray(), rows, page - 1, "CreateDate", OrderDirection.DESC);

            List<object> rstList = new List<object>();
            recordList.ForEach(o => rstList.Add(new
            {
                Id = o.Id,
                SNCode = infoDict.ContainsKey(o.PurifierID) ? (infoDict[o.PurifierID].SNCode??"") : "",
                Name = infoDict.ContainsKey(o.PurifierID) ? infoDict[o.PurifierID].Name : "",
                PurifierID = o.PurifierID,
                Pm25 = o.Pm25,
                Temperature = o.Temperature,
                Humidity = o.Humidity,
                CreateDate = o.CreateDate,
                IsPurifier = true,
            }));

            return rstList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetRecords(int page, int rows)
        {
            int total = 0;
            List<object> rstList = GetPurifierRecord(page, rows, ref total);
            return new ModelListEx<object>() { total = total, message = "加载成功", data = rstList };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> UpdateStatus(long Id, string MAC, int Tear, int OnOffKey, int LockKey, int AutoKey, int SleepKey, int AnionKey, int UVKey, int Hour, int Min)
        {
            PurifierStatus.UpdateStatus(Id, MAC, Tear, OnOffKey, LockKey, AutoKey, SleepKey, AnionKey, UVKey, Hour, Min);
            return new ModelListEx<object>() { message = "操作成功" };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> UpdateStatusEx(long Id, string Name, string MAC, int Tear, int OnOffKey, int LockKey, int AutoKey, int SleepKey, int AnionKey, int UVKey, int Hour, int Min)
        {
            PurifierStatus.UpdateStatus(Id, MAC, Tear, OnOffKey, LockKey, AutoKey, SleepKey, AnionKey, UVKey, Hour, Min);
            PurifierInformation info = PurifierInformation.GetInfo(MAC);
            info.UpdateNameAndDesc(Name, "");
            return new ModelListEx<object>() { message = "操作成功" };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public object GetStatus(long id)
        {
            PurifierStatus status = PurifierStatus.GetByID(id);
            return new { 
                Id=status.Id,
                MAC=status.MAC,
                Tear=status.Tear,
                OnOffKey=status.OnOffKey,
                LockKey=status.LockKey,
                AutoKey=status.AutoKey,
                SleepKey=status.SleepKey,
                AnionKey=status.AnionKey,
                UVKey=status.UVKey,
                Hour=status.TimerHour,
                Min=status.TimerMin,
            };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public object GetStatusEx(long id)
        {
            PurifierInfoView status = PurifierInfoView.GetInfoView(id);
            return new
            {
                Id = status.Id,
                MAC = status.MAC,
                Tear = status.Tear,
                OnOffKey = status.OnOffKey,
                LockKey = status.LockKey,
                AutoKey = status.AutoKey,
                SleepKey = status.SleepKey,
                AnionKey = status.AnionKey,
                UVKey = status.UVKey,
                Hour = status.TimerHour,
                Min = status.TimerMin,
                Name = status.Name,
            };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public ModelListEx<object> GetInfoInPage(int page, int rows)
        {
            List<PurifierInformation> infos = new List<PurifierInformation>();
            int total = 0;
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInformation.GetAllInfoByPage(rows, page-1, "SNCode", OrderDirection.ASC));
                total = PurifierInformation.GetAllCount();
            }
            else
            {
                infos.AddRange(PurifierInformation.GetUserInfoByPage(CurrentUser.Id,rows, page-1, "SNCode", OrderDirection.ASC));
                total = PurifierInformation.GetCountOfUserPurifiers(CurrentUser.Id);
            }
            return new ModelListEx<object>() { total = total, message = "加载成功", data = infos.ToList<object>(), isall=total==infos.Count };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetInfoBySNCode(string sncode)
        {
            PurifierInformation info = PurifierInformation.GetInfoBySNCode(sncode);
            List<object> rstList = new List<object>();
            int total = 0;
            if (info !=null && (CurrentUser.IsInRole(Role.AdminRole) ||
                UserPurifier.GetOwnerIds(info).Any(o => o.UserId == CurrentUser.Id)))
            {
                rstList.Add(info);
                total = 1;
            }
            return new ModelListEx<object>() { total = total, message = "搜索完成", data = rstList };
        }

        public List<PurifierInformation> GetInfo()
        {
            List<PurifierInformation> infos = new List<PurifierInformation>();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInformation.LoadAll());
            }
            else
            {
                infos.AddRange(PurifierInformation.LoadUserPurifiers(CurrentUser.Id));
            }
            return infos;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public List<object> GetCitys(int page, int rows)
        {
            if (page != 1)
                return null;
            List<PurifierInformation> infos = GetInfo();
            List<object> citys = new List<object>();
            Dictionary<string, bool> cityDict = new Dictionary<string, bool>();
            infos.ForEach(o => {
                if (!string.IsNullOrEmpty(o.City) && !cityDict.ContainsKey(o.City))
                {
                    cityDict[o.City] = true;
                    citys.Add(new { Name = o.City }); 
                }
            });
            return citys;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PageResult<PurifierDataView> LoadPurifierPagedData(List<long> purifierIds, DateTime startTime, DateTime endTime, UIPager pager)
        {
            WhereCriterion wc = new WhereCriterion();
            wc.IN("PurifierID", purifierIds.Cast<Object>().ToArray());
            wc.And("CreateDate", OperatorFlag.GTE, startTime);
            wc.And("CreateDate", OperatorFlag.LTE, endTime);
            if (startTime.Date == endTime.Date)
                wc.IN("Flag", new object[] { RecordFlag.First, RecordFlag.Newest });
            else
                wc.And("Flag", OperatorFlag.Equal, RecordFlag.Avg);

            var result = new PageResult<PurifierDataView>(pager);
            result.LoadData(wc);
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<PurifierDataView> LoadPurifierData(List<long> purifierIds, DateTime startTime, DateTime endTime)
        {
            var res = PurifierDataView.LoadData<PurifierDataView>(purifierIds, startTime, endTime);
            ListSorter sorter = new ListSorter("CreatedTime", true);
            sorter.Sort(res);
            return res;
        }
        
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetChartDataByPurifier(string purifierIds, string start, string end)
        {
            Func<string, DateTime> getDateFromString = date =>
            {
                if (date.Length > 10)
                    date = date.Substring(0, 10);
                return Parser.Parse<DateTime>(date, default(DateTime));
            };


            DateTime startDate = getDateFromString(start);
            DateTime endDate = getDateFromString(end);

            if (!Tools.IsValid(startDate) || !Tools.IsValid(endDate))
                return null;

            List<long> idList = purifierIds.Split('|').Select(i => Parser.Parse(i, 0L)).ToList();
            var records = LoadPurifierData(idList, startDate, endDate);
            ListSorter sorter = new ListSorter("MAC", true);
            sorter.Sort(records);
            JArray array = new JArray();

            foreach (var item in records)
            {
                JObject jo = new JObject();
                jo["date"] = item.CreateDateShortString;
                jo[item.Name ?? item.MAC] = item.Pm25.ToString();
                array.Add(jo);
            }

            JObject result = new JObject();
            result["d"] = array;
            return result.ToString(Newtonsoft.Json.Formatting.None);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<City> LoadCities()
        {
            var isAdmin = IsAdminRole();
            if (!isAdmin) return null;
            var cities= PersistenceHelper.Default.Query<City>(new WhereCriterion());
            ListSorter sorter = new ListSorter("Text", true);
            sorter.Sort(cities);
            return cities;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<City> Load()
        {
            var isAdmin = IsAdminRole();
            if (!isAdmin) return null;
            var cities = PersistenceHelper.Default.Query<City>(new WhereCriterion());
            ListSorter sorter = new ListSorter("Text", true);
            sorter.Sort(cities);
            return cities;
        }

        private bool IsAdminRole()
        {
            var isAdmin = CurrentUser.IsInRole(Role.AdminRole);
            return isAdmin;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PageResult<PurifierInformation> LoadPurifiersInIds(UIPager pager, List<long> ids)
        {
            var isAdmin = IsAdminRole();
            if (!isAdmin) return null;
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", ids.Cast<object>().ToArray());
            PageResult<PurifierInformation> result = new PageResult<PurifierInformation>(pager);
            result.LoadData(wc);
            return result;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PageResult<PurifierInformation> SearchPurifiers(UIPager pager, string key)
        {
            var isAdmin = IsAdminRole();
            if (!isAdmin) return null;
            WhereCriterion wc = new WhereCriterion("SNCode", OperatorFlag.Like, string.Format("%{0}%", key));
            PageResult<PurifierInformation> result = new PageResult<PurifierInformation>(pager);
            result.LoadData(wc);
            return result;
        }
    }
}
