﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Morningstar.PSReporting.Common;
using HOYI.Common.Data;
using HOYI.Common.Data.Indexers;

namespace HOYI.Common.Data.SqlServer
{
    public class SqlServerSPPersister 
    {
        protected IDBSetting Setting { get; private set; }
        public SqlServerAccessor Accessor { get; private set; }
        public SqlServerSPPersister(IDBSetting setting)
        {
            Setting = setting;
            Accessor = CreateSqlServerAccessor();
        }

        protected virtual SqlServerAccessor CreateSqlServerAccessor()
        {
            return new SqlServerAccessor(SqlServerUtils.GetConnectionString(Setting));
        }

        bool EnsureObjId(BaseDataObj dataObj)
        {
            bool emptyIdObj = BasePersister.DataObjIsEmptyId(dataObj);
            if (emptyIdObj)
                DataObjIdGenerator.Instance.SetId(dataObj);
            return !emptyIdObj;
        }

        public int Save(BaseDataObj dataObj)
        {
            if (dataObj == null) return -100;
            var r = SaveObj(dataObj);
            dataObj.IsExisted = true;
            dataObj.ClearChangeds();
            return r;
        }

        private int SaveObj(BaseDataObj dataObj)
        {
            if (dataObj == null) throw new ArgumentNullException("dataObj");
            var info = DataObjInfo.GetDataObjInfo(dataObj.GetType());
            var writeSp = (info.SPs == null) ? "" : info.SPs.SPForWriting;
            if (string.IsNullOrEmpty(writeSp))
                throw new InvalidOperationException("Unknow writing stored procedure:" + dataObj.GetType().Name);
            var verField = info.SPs.VersionField;

            EnsureObjId(dataObj);
            try
            {
                if (string.IsNullOrEmpty(verField))
                    return Accessor.ExecuteNonQuery<BaseDataObj>(writeSp, dataObj);
                else
                {
                    var vf = info.Fileds[verField];
                    if (vf == null)
                        throw new InvalidOperationException("Can not find version field '" + verField + "'.");
                    var result = Accessor.ExecuteReader<BaseDataObj>(writeSp, dataObj);
                    using (var reader = result.First)
                    {
                        while (reader.Read())
                        {
                            var rix = DataIndexer.GetIndexer(reader);
                            if (!BaseDataObj.SetField(rix, dataObj, vf))
                                throw new InvalidOperationException("Miss version when save data");
                        }
                    }
                    return (int)result.Second.Parameters["@RETURN_VALUE"].Value;
                }
            }
            catch (SqlException exp)
            {
                if (exp.Message == "VERSION.ERROR")
                    throw new DataObjVersionException(dataObj.GetType().Name + ":" + BasePersister.GetDataObjIdValue(dataObj), exp);
                else
                    throw;
            }
        }

        public Dictionary<T, int> Save<T>(IEnumerable<T> dataObjs) where T:BaseDataObj
        {
            if (dataObjs == null) throw new ArgumentNullException("dataObjs");
            Dictionary<T, int> results = new Dictionary<T, int>();
            using (var transpx = new TransactionProxy(Accessor))
            {
                transpx.Begin();
                foreach (var obj in dataObjs)
                {
                    if (obj == null) continue;
                    results[obj] = Save(obj);
                }
                transpx.Commit();
            }
            foreach (var obj in dataObjs)
            {
                obj.IsExisted = true;
                obj.ClearChangeds();
            }
            return results;
        }

        public T Read<T>(object objID) where T : BaseDataObj
        {
            using (var reader = GetReader<T>(objID))
            {
                while (reader.Read())
                {
                    return BaseDataObj.CreateObjFromPersistence<T>(reader);
                }
            }
            return null;           
        }

        private IDataReader GetReader<T>(object objID) where T : BaseDataObj
        {
            var info = DataObjInfo.GetDataObjInfo(typeof(T));
            var readSp = (info.SPs == null) ? "" : info.SPs.SPForReading;
            if (string.IsNullOrEmpty(readSp))
                throw new InvalidOperationException("Unknow reading stored procedure:" + typeof(T).Name);
            var rdp = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            rdp[info.GetPrimaryKeyDbName()] = objID;
            return Accessor.ExecuteReader(readSp, rdp).First;
        }

        public void Reset<T>(T obj) where T : BaseDataObj
        {
            using (var reader = GetReader<T>(BasePersister.GetDataObjIdValue(obj)))
            {
                while (reader.Read())
                {
                    BaseDataObj.SetFields<T>(DataIndexer.GetIndexer(reader), obj);
                }
            }
        }

        public Pair<int, List<T>> QueryAndTotal<T>(Dictionary<string, object> queryParams) where T : BaseDataObj
        {
            var info = DataObjInfo.GetDataObjInfo(typeof(T));
            var quySp = (info.SPs == null) ? "" : info.SPs.SPForQuerying;
            if (string.IsNullOrEmpty(quySp))
                throw new InvalidOperationException("Unknow querying stored procedure:" + typeof(T).Name);
            List<T> objs = new List<T>();
            var result = Accessor.ExecuteReader(quySp, queryParams);
            using (var reader = result.First)
            {
                while (reader.Read())
                {
                    objs.Add(BaseDataObj.CreateObjFromPersistence<T>(reader));
                }
            }
            return new Pair<int, List<T>>(Tools.Parse<int>(result.Second.Parameters["@RETURN_VALUE"].Value), objs);
        }

        public List<T> Query<T>(Dictionary<string, object> queryParams) where T : BaseDataObj
        {
            var res = QueryAndTotal<T>(queryParams);
            return res.Second;
        }

        public int Delete(object objID, Type objType)
        {
            return Delete(objID, objType, long.MaxValue);
        }

        public int Delete(object objID, Type objType, long version)
        {
            try
            {
                if (objType == null) throw new ArgumentNullException("objType");
                var info = DataObjInfo.GetDataObjInfo(objType);
                var deleteSp = (info.SPs == null) ? "" : info.SPs.SPForDeleting;
                if (string.IsNullOrEmpty(deleteSp))
                    throw new InvalidOperationException("Unknow deleting stored procedure:" + objType.Name);
                Dictionary<string, object> rdp = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                rdp[info.GetPrimaryKeyDbName()] = objID;
                if (!string.IsNullOrEmpty(info.SPs.VersionField) && version != long.MaxValue)
                {
                    rdp[info.SPs.VersionField] = version;
                }
                return Accessor.ExecuteNonQuery(deleteSp, rdp);
            }
            catch (SqlException exp)
            {
                if (exp.Message == "VERSION.ERROR")
                    throw new DataObjVersionException(objType.Name + ":" + version, exp);
                else
                    throw;
            }
        }

        public Dictionary<T, int> Delete<T>(List<T> objs) where T : BaseDataObj
        {
            if (objs == null) throw new ArgumentNullException("objs");
            Dictionary<T, int> results = new Dictionary<T, int>();
            using (var transpx = new TransactionProxy(Accessor))
            {
                transpx.Begin();
                foreach (var obj in objs)
                {
                    if (obj == null) continue;
                    results[obj] = Delete(BasePersister.GetDataObjIdValue(obj), obj.GetType());
                }
                transpx.Commit();
            }
            return results;
        }
    }
}
