﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Morningstar.PSReporting.Common;

namespace HOYI.Common.Data.SqlCe
{
    public class SqlCePersistenceHelper : PersistenceHelper
    {
        const string c_AppDataDirName = "App_Data";
        readonly DBSetting m_Setting;
        public SqlCePersistenceHelper(DBSetting setting)
        {
            m_Setting = setting;
        }

        protected override IDataObjPersister CreateDataObjPersister(string dbName)
        {
            var dop = new SqlCeDataObjPersister();
            dop.ConnectionString = string.Format("Data Source={0};Persist Security Info=False;Max Database Size=3072", dbName);
            return dop;
        }

        static readonly Dictionary<string, string> typeDbPathDic = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        void ClearUnusefulDBFiles(string appDataRoot, string dbFile)
        {
#if DEBUG
            const int sevenDays = 3600 * 24 * 1;
#else
            const int sevenDays = 3600 * 24 * 7;
#endif
            var dirInfo = new DirectoryInfo(appDataRoot);
            if (dirInfo.Exists)
            {
                var files = dirInfo.GetFiles(dbFile, SearchOption.AllDirectories);
                foreach (var f in files)
                {
                    if (Tools.DiffNowOut(f.LastWriteTimeUtc, sevenDays))
                    {
                        try
                        {
                            f.Delete();
                        }
                        catch
                        {
                        }
                    }
                }
                var cDirs = dirInfo.GetDirectories();
                foreach (var c in cDirs)
                {
                    if (Tools.DiffNowOut(c.CreationTimeUtc, sevenDays))
                    {
                        var cfs = c.GetFiles("*", SearchOption.AllDirectories);
                        if (cfs == null || cfs.Length == 0)
                        {
                            try
                            {
                                c.Delete(true);
                            }
                            catch
                            {
                            }
                        }
                    }
                }
            }
        }

        string GetSqlCeDbName(Type dataObjType, object objId)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(dataObjType);
            string tempPath = doi.DBTemplate;
            string dbName = (m_Setting.Database ?? "").Trim();
            if (string.IsNullOrEmpty(dbName)) dbName = doi.DBName;

            if (doi.EnableMultiDb && doi.DbCount > 1)
            {
                int dbNum = (Math.Abs(objId.GetHashCode()) % doi.DbCount) + 1;
                dbName = dbName + dbNum + ".sdf";
            }
            else
            {
                dbName = dbName + ".sdf";
            }

            lock (typeDbPathDic)
            {
                if (typeDbPathDic.ContainsKey(dbName))
                    return typeDbPathDic[dbName];

                var dbDirs = new List<string>() { c_AppDataDirName };
                var dmName = AppDomain.CurrentDomain.FriendlyName ?? "";
                if (dmName.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
                {
                    dmName = dmName.Substring(0, dmName.Length - 4);
                }
                dbDirs.Add(Tools.RemoveInvalidFileNameChars(dmName));
                dbDirs.Add(Tools.RemoveInvalidFileNameChars(ConfigurationManager.AppSettings["ProductMode"]));
                string dbDir = AppDomain.CurrentDomain.BaseDirectory;
                foreach (var dbdr in dbDirs)
                {
                    if (string.IsNullOrEmpty(dbDir)) continue;
                    string panDir = dbDir;
                    dbDir = Path.Combine(dbDir, dbdr);
                    if (!Directory.Exists(dbDir))
                    {
                        try
                        {
                            Tools.CreateDir(dbDir);
                        }
                        catch
                        {
                            if (!Directory.Exists(dbDir)) throw;
                        }
                    }
                    if (dbdr == c_AppDataDirName)
                        ClearUnusefulDBFiles(dbDir, dbName);
                }
                var dbPath = Path.Combine(dbDir, dbName);
                if (!File.Exists(dbPath))
                {
                    using (var tempRes = dataObjType.Assembly.GetManifestResourceStream(tempPath))
                    {
                        if (tempRes == null)
                            throw new InvalidOperationException("Missing DBTemplate Resource '" + tempPath + "'");
                        using (var strw = new BinaryReader(tempRes))
                        {
                            using (FileStream fs = File.OpenWrite(dbPath))
                            {
                                var buff = new byte[512];
                                int len = 0;
                                while (true)
                                {
                                    Array.Clear(buff, 0, 512);
                                    len = strw.Read(buff, 0, 512);
                                    fs.Write(buff, 0, len);
                                    if (len < 512)
                                        break;
                                }
                            }
                        }
                    }
                }
                typeDbPathDic[dbName] = dbPath;
                return dbPath;
            }
        }

        protected override string GetDbName(Type dataObjType, object objId)
        {
            return ExecuteMethod<string>(() => GetSqlCeDbName(dataObjType, objId));
        }
    }
}
