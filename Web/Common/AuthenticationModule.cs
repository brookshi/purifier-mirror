﻿using HOYI.Common;
using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    public class AuthenticationModule : IHttpModule
    {
        static AuthenticationModule()
        {
            PersistenceHelper.InitDefault(new DBSetting("HOYI"));
        }

        private HashSet<string> staticResFlags;

        public AuthenticationModule()
        {
            staticResFlags = new HashSet<string>(StringComparer.OrdinalIgnoreCase) { ".js", ".css", ".jpg", ".bmp", ".png", ".gif" };
        }

        public void Dispose()
        {
           
        }

        public void Init(HttpApplication context)
        {
            
            context.BeginRequest += OnBeginRequest;
            context.EndRequest += OnEndRequest;
        }

        void OnEndRequest(object sender, EventArgs e)
        {
            HttpContext context = GetContext(sender);
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
         
            if (context.User != null)
                AuthenticationManager.Instance.UpdateTicket(context);
        }

        private static HttpContext GetContext(object sender)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;
            return context;
        }

        void OnBeginRequest(object sender, EventArgs e)
        {
            HttpContext context = GetContext(sender);

            if (IsStaticRequest(context) || IsPassRequest(context)) return;

            foreach (var hd in context.Request.Headers.Keys)
            {
                Debug.WriteLine(string.Format("{0}:{1}\r\n", hd, context.Request.Headers[hd.ToString()]));
            }

            AuthenticationManager.Instance.Authenticate(context);

        }

        bool IsPassRequest(HttpContext context)
        {
            var abPath = context.Request.Url.AbsolutePath;
            var isTestPage = abPath.IndexOf("test.aspx", StringComparison.OrdinalIgnoreCase) > 0;
            var isLoginPage = abPath.IndexOf("login.html", StringComparison.OrdinalIgnoreCase) > 0;
            var isRegPage = abPath.IndexOf("register.html", StringComparison.OrdinalIgnoreCase) > 0;
            var isForgetPage = abPath.IndexOf("forget.html", StringComparison.OrdinalIgnoreCase) > 0;
            var isChartPage = abPath.IndexOf("chart.html", StringComparison.OrdinalIgnoreCase) > 0;
            var isLoginService = abPath.EndsWith("userservice.asmx/login", StringComparison.OrdinalIgnoreCase);
            var isLoginServiceEx = abPath.EndsWith("userservice.asmx/LoginWithUserInfo", StringComparison.OrdinalIgnoreCase);
            var isMobileLoginService = abPath.EndsWith("mobile.asmx/login", StringComparison.OrdinalIgnoreCase);
            var isVerifyMail = abPath.EndsWith("userservice.asmx/VerifyEmail", StringComparison.OrdinalIgnoreCase);
            var isVerifyUserName = abPath.EndsWith("userservice.asmx/VerifyUserName", StringComparison.OrdinalIgnoreCase);
            var isRegisterUser = abPath.EndsWith("userservice.asmx/RegisterUser", StringComparison.OrdinalIgnoreCase);
            var isChart = abPath.EndsWith("purifierservice.asmx/LoadPurifierData", StringComparison.OrdinalIgnoreCase);
            return isRegPage || isChartPage || isChart || isLoginPage || isLoginService || isLoginServiceEx || isTestPage || isForgetPage || isVerifyMail || isVerifyUserName || isRegisterUser || isMobileLoginService;
        }

        bool IsStaticRequest(HttpContext context)
        {
            var abPath = context.Request.Url.AbsolutePath;
            return staticResFlags.Where(s => abPath.EndsWith(s, StringComparison.OrdinalIgnoreCase)).Count() > 0;
        }
       
    }

}