﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using Newtonsoft.Json.Linq;

namespace HOYI.Common.Data.Indexers
{
    public abstract class DataIndexer
    {
        static DataIndexer()
        {
            RegisterDataIndexer(typeof(IDictionary), typeof(DictionaryIndexer));
            RegisterDataIndexer(typeof(DataRow), typeof(TableDataRowIndexer));
            RegisterDataIndexer(typeof(IDataReader), typeof(DbDataReaderIndexer));
            RegisterDataIndexer(typeof(BaseDataObj), typeof(DataObjIndexer));
            RegisterDataIndexer(typeof(JObject), typeof(JObjectIndexer));
        }

        protected abstract object GetValue(string name);
        public abstract bool Contains(string name);

        public object this[string name]
        {
            get
            {
                if (string.IsNullOrEmpty(name))
                    throw new ArgumentNullException("name");

                object value = GetValue(name);
                if (value == DBNull.Value)
                    return null;
                else
                    return value;
            }
        }

        static readonly Dictionary<Type, ConstructorInfo> dataIndexers = new Dictionary<Type, ConstructorInfo>();

        static void RegisterDataIndexer(Type forType, Type dataIndexerType)
        {
            lock (dataIndexers)
            {
                if (dataIndexers.ContainsKey(forType))
                    return;
                if (dataIndexerType.IsSubclassOf(typeof(DataIndexer)))
                {
                    var cts = dataIndexerType.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
                    foreach (var ct in cts)
                    {
                        if (ct.GetParameters().Length == 1)
                        {
                            dataIndexers[forType] = ct;
                            break;
                        }
                    }
                    if (!dataIndexers.ContainsKey(forType))
                        throw new ArgumentException("Invalid DataIndexer, it hasn't necessary constructor.");
                }
                else
                {
                    throw new ArgumentException("Invalid DataIndexer, it isn't sub class of DataIndexer.");
                }
            }
        }

        public static DataIndexer GetIndexer<T>(T data)
        {
            ConstructorInfo indexerContstruct = null;
            Type forType = typeof(T);
            if (forType == typeof(object) && data != null)
                forType = data.GetType();
            lock (dataIndexers)
            {
                if (dataIndexers.ContainsKey(forType))
                {
                    indexerContstruct = dataIndexers[forType];
                }
                else
                {
                    foreach (var tp in dataIndexers.Keys)
                    {
                        if (tp.IsAssignableFrom(forType))
                            indexerContstruct = dataIndexers[tp];
                    }
                }
            }
            if (indexerContstruct != null)
            {
                return (DataIndexer)indexerContstruct.Invoke(new object[] { data });
            }
            else
            {
                throw new ArgumentException("Don't find DataIndexer for '" + typeof(T).FullName);
            }
        }
    }
}
