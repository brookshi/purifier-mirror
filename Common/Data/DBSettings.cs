﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Configuration;

namespace HOYI.Common.Data
{
    public enum DbProvider
    {
        SqlCe,
        SqlServer,
        Oracle
    }

    public interface IDBSetting
    {
        string Server { get; set; }
        string User { get; set; }
        string Password { get; set; }
        DbProvider DbType { get; set; }
        string Database { get; set; }
    }


    public class DBSetting : IDBSetting
    {
        public string Server { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public DbProvider DbType { get; set; }
        public string Database { get; set; }

        public DBSetting()
        {
            DbType = DbProvider.SqlCe;
        }

        public DBSetting(string connName)
        {
            var cns = ConfigurationManager.ConnectionStrings[connName];
            string connStr = (cns != null ? cns.ConnectionString : null);
            if (!string.IsNullOrEmpty(connStr))
            {
                JsonConvert.PopulateObject(connStr, this);
            }
        }

        public DBSetting Clone()
        {
            return ObjectCopier.Copy<DBSetting, DBSetting>(this);
        }

        public static DBSetting FromJSON(string json)
        {
            if (string.IsNullOrEmpty(json))
                return new DBSetting();
            else
                return JsonConvert.DeserializeObject<DBSetting>(json);
        }
    }

}
