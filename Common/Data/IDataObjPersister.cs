﻿using System.Collections.Generic;

namespace HOYI.Common.Data
{
    public interface IDataObjPersister
    {
        Dictionary<T, int> SaveObjs<T>(IEnumerable<T> dataObjs) where T : BaseDataObj;
        T ReadDataObjByID<T>(object objID) where T : BaseDataObj;
        List<T> TopObjs<T>(int count, string orderByPropertyName, OrderDirection direction, WhereCriterion critical) where T : BaseDataObj;
        List<DataSummery> LoadDataSummery<T>(int count, string summeryName, OrderDirection direction, WhereCriterion critical) where T : BaseDataObj;
        List<T> QueryDataObjs<T>(WhereCriterion critical) where T : BaseDataObj;
        int CountObjs<T>(WhereCriterion critical) where T : BaseDataObj;
        int DeleteObjs<T>(WhereCriterion critical) where T : BaseDataObj;
        int Delete<T>(object objId) where T : BaseDataObj;
        int ForeUpate<T>(Dictionary<string, object> values, WhereCriterion critical) where T : BaseDataObj;
        void BeginTransaction();
        void Commit();
        void Rollback();
        IEnumerator<T> EnumerateData<T>(WhereCriterion critical) where T : BaseDataObj;
    }
}
