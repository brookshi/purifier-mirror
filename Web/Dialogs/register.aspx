﻿<%@ Page Language="C#" Inherits="HOYI.Web.Common.PageBase" %>
<link  type="text/css" rel="stylesheet" href="themes/defaultpc/register.css"/>
<script type="text/javascript" src="scripts/register.js"></script>
<div class="suc_kuang">
    <div class="hei_513">
        <dl class="login-dl clearfix">
            <dt class="dt_l">邮箱：</dt>
	        <dd class="dd_r clearfix">
                <input name="Email" class="input_kuang val_m" id="emailInp" type="text" value="" autocomplete="off" rule="^[\w.\-]+@(?:[a-z0-9]+(?:-[a-z0-9]+)*\.)+[a-z]{2,6}$" isrequired="true" />
                <span class="check_tips error_tip">邮箱格式错误</span>
                <span class="check_tips empty_tip">请输入邮箱</span>
	            <span class="check_tips succ_tips"></span>
                </dd>
            <dt class="dt_l" style="margin-top: 10px;">用户名：</dt>
            <dd class="dd_r clearfix" style="margin-top: 10px;">
                <input name="UserName" class="input_kuang val_m" id="imginput" type="text" autocomplete="off" isrequired="true"  rule="[0-9A-Za-z_]{3,20}"/>
                <span class="check_tips error_tip">用户名长度3~20，数字、字母、下画线组成</span>
                <span class="check_tips empty_tip">请输入用户名</span>
	            <span class="check_tips succ_tips"></span>
            </dd>
	        <dt class="dt_l">设置密码：</dt>
	        <dd class="dd_r dd_r_pos clearfix" id="pwdTd">
	            <input name="Password" class="input_kuang val_m password" id="pwd" type="password" autocomplete="off" isrequired="true"/>
                <span class="check_tips empty_tip">请输入密码</span>
                <span class="check_tips tips_1">密码长度为6~16位</span>
                <span class="check_tips tips_2">密码不能全为字母</span>
                <span class="check_tips tips_3">密码不能全为数字</span>
                <span class="check_tips tips_4">密码不能与邮箱相同</span>
                <span class="check_tips tips_5">密码不能与用户名相同</span>
                <span class="check_tips tips_6">密码必须由数字、字母、特殊字符组成</span>
	        </dd>
            <dt class="dt_l">确认密码：</dt>
	        <dd class="dd_r clearfix">
                <input name="Repassword" class="input_kuang val_m" type="password" autocomplete="off" isrequired="true" repeat="#pwd" />
                <span class="check_tips repeat_tip">密码输入不一致</span>
                <span class="check_tips empty_tip">请输入确认密码</span>
	            <span class="check_tips succ_tips"></span>
	        </dd>
            <dt class="dt_l">是管理员：</dt>
	        <dd class="dd_r clearfix">
                <input name="IsAdmin" class="input_kuang val_m" type="checkbox"  value="true" style="margin-top: 8px;width: auto;"/>
	        </dd>
            <dt class="dt_l">姓名：</dt>
	        <dd class="dd_r clearfix">
                <input name="Name" class="input_kuang val_m" type="text" autocomplete="off" maxlength="10" />
	        </dd>
            <dt class="dt_l">手机：</dt>
	        <dd class="dd_r clearfix">
                <input name="Phone" class="input_kuang val_m" type="text" autocomplete="off" maxlength="11"/>
                <span class="check_tips tips_1">手机号无效。</span>
	        </dd>
            <dt class="dt_l">城市：</dt>
	        <dd class="dd_r clearfix">
                <input name="City" class="input_kuang val_m" type="text" autocomplete="off" readonly="readonly"/>
	        </dd>
            <dt class="dt_l">详细地址：</dt>
	        <dd class="dd_r clearfix">
                <input name="Address" class="input_kuang val_m" type="text" autocomplete="off" maxlength="500"/>
	        </dd>
	        <dt class="dt_l">&nbsp;</dt>
	        <dd class="dd_r la_height clearfix">
                <div class="sub_login flt_l">
                    <button type="button" class="regbtn"> 确 定 </button>
                </div>
            </dd>
	    </dl>
    </div>
</div>


<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        if (!this.IsAdmin)
            throw new HttpException("禁止访问此资源");
        base.OnLoad(e);
    }
</script>