﻿using HOYI.Common.Data;
using HOYI.Common.Data.Indexers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{
    [Serializable]
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "DataInterval")]
    public class DataInterval : BaseDataObj
    {
        protected DataInterval()
        {
        }

        public int Id
        {
            get;
            private set;
        }

        public virtual int UploadInterval
        {
            get;
            set;
        }

        public virtual int SearchInterval
        {
            get;
            set;
        }

        public static DataInterval GetInfo(int id)
        {
            return PersistenceHelper.Default.Read<DataInterval>(id);
        }

        public void Save()
        {
            PersistenceHelper.Default.Save(this);
        }
    }
}
