/*
 * Util
 */
if (!Util) {
    var Util = {};
}

//Simple Drag & Drop with Proxy
Util.DragProxy = function(){
    var proxy = {};
    return function(group){
        if(proxy[group]){
            return proxy[group];
        }else{
            proxy[group] = $('<div class="rtq-dd-proxy">PROXY</div>').appendTo('body');
            return proxy[group];
        }
    }
}();
//Static Class
Util.DragingGroup = function(){
    var dragingGroup = '';
    return {
        get:function(){
            return dragingGroup;
        },
        set:function(v){
            dragingGroup = v;
        },
        empty:function(){
            dragingGroup = '';
        }
    }
}();
Util.Dragable = function(dom, callbacks, config){
    this.dom = dom;
    this.el = null;
    this.offsetX = 0;
    this.offsetY = 0;
    this.proxy = null;
    this.cb = {
        onDragStart:function(){},
        onDrag:function(){},
        onDragEnd:function(){}
    };
    this.cfg = {
        autoProxy: true,
        proxy: null,
        delay: 200,
        group: 'QS_UTIL_DD_GROUP',
        customClass: ''
    };
    $.extend(this.cfg, config);
    this.timer = null;
    $.extend(this.cb, callbacks);
    this.init();
};
Util.Dragable.prototype = {
    init:function(){
        var self = this;
        var isTouchDev = this.isTouchDev = ("createTouch" in document);
        var startDrag = (isTouchDev ? "touchstart" : "mousedown") + ".Dragable";
        var endDrag = (isTouchDev ? "touchend" : "mouseup") + ".Dragable";
        var moveDrap = (isTouchDev ? "touchmove" : "mousemove") + ".Dragable";
        this.dragEvents = { start: startDrag, move: moveDrap, end: endDrag };
        this.el = $(this.dom).bind(startDrag, function (e) {
            if (!isTouchDev) e.preventDefault();
            self.wrapTchEvent(e);
            $(document).bind('selectstart.Dragable', function () { return false; });
            self.timer = setTimeout(function () {
                self.startDrag(e);
            }, self.cfg.delay);
        }).bind(endDrag, function (e) {
            e.preventDefault();
            self.wrapTchEvent(e);
            clearTimeout(self.timer);
            self.timer = -1;
            $(document).unbind('selectstart.Dragable');
        }).bind(moveDrap, function (e) {
            e.preventDefault();
        });
        this.proxy = this.cfg.autoProxy ? Util.DragProxy(this.cfg.group) : this.cfg.proxy;
        if(this.cfg.customClass && this.cfg.customClass != ''){
            this.proxy.addClass(this.cfg.customClass);
        }
    },
    wrapTchEvent: function (e) {
        if (e.clientX == undefined) {
            var oe = e.originalEvent;
            if (oe && oe.touches && oe.touches.length > 0) {
                e.clientX = oe.touches[0].clientX;
                e.clientY = oe.touches[0].clientY;
            }
        }
    },
    startDrag:function(e){
        var self = this;
        
        Util.DragingGroup.set(this.cfg.group);
        
        this.offsetY = $(document).scrollTop();
        this.offsetX = $(document).scrollLeft();
       

        if (this.proxy) {
            this.proxy.css({
                left: e.clientX + this.offsetX,
                top: e.clientY + this.offsetY
            }).show();
        }
        
        $(document).bind(self.dragEvents.move, function (e) {
            self.wrapTchEvent(e);
            self.Drag(e);
        }).bind(self.dragEvents.end, function (e) {
            self.wrapTchEvent(e);
            self.endDrag(e);
        });
        
        this.cb.onDragStart(e, this.proxy);
    },
    Drag:function(e){
        e.preventDefault();
        var pos = {
            left: e.clientX + this.offsetX,
            top: e.clientY + this.offsetY
        };
        if (this.proxy) 
            this.proxy.css(pos);
        this.cb.onDrag(e, this.proxy);
        if (this.isTouchDev) {
            for (var d = 0; d < Util.Dropable.dropers.length; d++)
                Util.Dropable.dropers[d].over(pos.left, pos.top);
        }
        return false;
    },
    endDrag: function (e) {
        if (this.isTouchDev) {
            for (var d = 0; d < Util.Dropable.dropers.length; d++)
                Util.Dropable.dropers[d].up();
        }
        $(document).unbind(this.dragEvents.end).unbind(this.dragEvents.move);
        if (this.proxy) {
            this.proxy.hide();
        }
        Util.DragingGroup.empty();
        
        this.cb.onDragEnd(e, this.proxy);
        return false;
    }
};

Util.Dropable = function(dom, callbacks, config){
    this.dom = dom;
    this.el = null;
    this.cfg = {
        group:'QS_UTIL_DD_GROUP'
    };
    $.extend(this.cfg, config);
    this.cb = {
        onEnter:function(){},
        onDrop:function(){},
        onOut:function(){}
    };
    $.extend(this.cb, callbacks);
    this.init();
};
Util.Dropable.dropers = [];
Util.Dropable.cleanDropers = function () {
    var inDoc = function (el) {
        var e = el, ep = el.parentNode;
        while (ep) {
            e = ep;
            ep = ep.parentNode;
        }
        return e == document || (e.tagName && e.tagName.toUpperCase() == "HTML");
    };
    var dps = Util.Dropable.dropers;
    var ss = Util.Dropable.dropers = [];
    for (var d = 0; d < dps.length; d++) {
        if (inDoc(dps[d].el[0]))
            ss.push(dps[d]);
    }
};
Util.Dropable.prototype = {
    init: function () {
        var self = this;
        Util.Dropable.dropers.push(self);
        this.proxy = Util.DragProxy(this.cfg.group);

        this.el = $(this.dom).bind('mouseover.Dropable', function (e) {
            if (Util.DragingGroup.get() == self.cfg.group) {
                self.proxy.addClass('rtq-dd-drop-yes');
                self.cb.onEnter(e, self.proxy);
            }
        }).bind('mouseout.Dropable', function (e) {
            if (Util.DragingGroup.get() == self.cfg.group) {
                self.proxy.removeClass('rtq-dd-drop-yes');
                self.cb.onOut(e, self.proxy);
            }
        }).bind('mouseup.Dropable', function (e) {
            if (Util.DragingGroup.get() == self.cfg.group) {
                self.cb.onDrop(e, self.proxy);
            }
        });
    },
    isEnter: function (x, y) {
        var elps = this.el.offset();
        var ew = this.el.width();
        var eh = this.el.height();
        if ((x > elps.left && x < (elps.left + ew)) && (y > elps.top && y < (elps.top + eh)))
            return true;
        else
            return false;
    },
    over: function (x, y) {
        if (this.isEnter(x, y)) {
            if (!this.hasEnter) {
                this.hasEnter = true;
                this.el.trigger("mouseover");
            }
        }
        else
            this.out();
    },
    out: function () {
        if (this.hasEnter) {
            this.hasEnter = false;
            this.el.trigger("mouseout");
        }
    },
    up: function () {
        if (this.hasEnter) {
            this.hasEnter = false;
            this.el.trigger("mouseup");
        }
    }
};