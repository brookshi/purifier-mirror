﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Common
{
    public class Pair<X,Y> 
    {
        public X First { get; set; }
        public Y Second { get; set; }
        public Pair(X x, Y y)
        {
            First = x;
            Second = y;
        }
    }
}
