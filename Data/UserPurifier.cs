﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "UserPurifier")]
    public abstract class UserPurifier : BaseDataObj
    {
        public Guid Id { get; private set; }
        public Guid UserId { get; private set; }
        public long PurifierId { get; private set; }

        public DateTime CreatedDate
        {
            get;
            protected set;
        }
        
        protected UserPurifier()
        {
 
        }

        public static UserPurifier Create(Account account, PurifierInformation info)
        {
            return BindPurifer(account.Id, info);
        }

        public static UserPurifier BindPurifer(Guid userId, PurifierInformation info)
        {
            var owners = GetOwnerIds(info);
            if (owners.Select(u => u.UserId).ToList().Contains(userId))
                return owners.FirstOrDefault(p => p.UserId == userId);

            var up = CreateDataObj<UserPurifier>();
            up.UserId = userId;
            up.PurifierId = info.Id;
            up.CreatedDate = DateTime.Now;
            PersistenceHelper.Default.Save<UserPurifier>(up);
            return up;
        }

        public static List<UserPurifier> GetOwnerIds(PurifierInformation purifier)
        {
            return GetOwnerIds(purifier.Id);
        }

        public static List<UserPurifier> GetOwnerIds(long purifierId)
        {
            WhereCriterion wc = new WhereCriterion("PurifierId", OperatorFlag.Equal, purifierId);
            return PersistenceHelper.Default.Query<UserPurifier>(wc);
        }

        public static void Delete(Guid userId, long purifierId)
        {
            WhereCriterion wc = new WhereCriterion("PurifierId", OperatorFlag.Equal, purifierId);
            wc.And("UserId", OperatorFlag.Equal, userId);
            PersistenceHelper.Default.Delete<UserPurifier>(wc);
        }

        public static List<UserPurifier> GetBindsForUser(Guid userId)
        {
            WhereCriterion wc = new WhereCriterion("UserId", OperatorFlag.Equal, userId);
            return PersistenceHelper.Default.Query<UserPurifier>(wc);
        }

        public static void DeleteBindsForUser(Guid userId)
        {
            WhereCriterion wc = new WhereCriterion("UserId", OperatorFlag.Equal, userId);
            PersistenceHelper.Default.Delete<UserPurifier>(wc);
        }

        public static void DeleteBindsForUserName(String userName)
        {
            Account account = Account.SearchUser(userName);
            DeleteBindsForUser(account.Id);
        }

        public static int BindingPurifiersForUserName(String userName, List<long> purifierIds)
        {
            Account account = Account.SearchUser(userName);
            if (account == null)
                return -1;
            DeleteBindsForUser(account.Id);
            Account user = Account.Read(account.Id);
            IList<UserPurifier> purifierList = new List<UserPurifier>();
            foreach (var id in purifierIds)
            {
                var up = CreateDataObj<UserPurifier>();
                up.UserId = account.Id;
                up.PurifierId = id;
                up.CreatedDate = DateTime.Now;
                purifierList.Add(up);
            }
            PersistenceHelper.Default.Save<UserPurifier>(purifierList);
            List<PurifierInformation> infoList = PurifierInformation.LoadUserPurifiers(account.Id);
            infoList.ForEach(o => o.City = user.City);
            PersistenceHelper.Default.Save<PurifierInformation>(infoList);
            return 1;
        }

        public static void BindingPurifiersForUser(Guid userId, List<long> purifierIds)
        {
            DeleteBindsForUser(userId);
            Account user = Account.Read(userId);
            IList<UserPurifier> purifierList = new List<UserPurifier>();
            foreach (var id in purifierIds)
            {
                var up = CreateDataObj<UserPurifier>();
                up.UserId = userId;
                up.PurifierId = id;
                up.CreatedDate = DateTime.Now;
                purifierList.Add(up);
            }
            PersistenceHelper.Default.Save<UserPurifier>(purifierList);
            List<PurifierInformation> infoList = PurifierInformation.LoadUserPurifiers(userId);
            infoList.ForEach(o => o.City = user.City);
            PersistenceHelper.Default.Save<PurifierInformation>(infoList);
        }
    }
}
