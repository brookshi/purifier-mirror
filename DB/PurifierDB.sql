﻿/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     2013/12/14 23:39:29                          */
/*==============================================================*/


if exists (select 1
            from  sysobjects
           where  id = object_id('PurifierInfo')
            and   type = 'U')
   drop table PurifierInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PurifierRecord')
            and   type = 'U')
   drop table PurifierRecord
go

if exists (select 1
            from  sysobjects
           where  id = object_id('"User"')
            and   type = 'U')
   drop table "User"
go

/*==============================================================*/
/* Table: PurifierInfo                                          */
/*==============================================================*/
create table PurifierInfo (
   ID                   uniqueidentifier         not null,
   SerialNumber         varchar(50)          null,
   City                 varchar(50)          null,
   Address              varchar(50)          null,
   Status               varchar(10)          null,
   constraint PK_PURIFIERINFO primary key (ID)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('PurifierInfo') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'PurifierInfo' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '净化器信息', 
   'user', @CurrentUser, 'table', 'PurifierInfo'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ID')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'ID'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '净化器ID',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'ID'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'SerialNumber')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'SerialNumber'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '净化器序列号',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'SerialNumber'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RunTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'RunTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '运行时间',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'RunTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'City')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'City'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '城市',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'City'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Address')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'Address'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '详细地址',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'Address'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FilterMediumChangeTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'FilterMediumChangeTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '滤材更换时间',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'FilterMediumChangeTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Status')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'Status'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '开关状态',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'Status'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Tear')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'Tear'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '档位',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'Tear'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UpdateInterval')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'UpdateInterval'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '上传时间间隔',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'UpdateInterval'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'OperatorInterval')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'OperatorInterval'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '轮询操作状态时间间隔',
   'user', @CurrentUser, 'table', 'PurifierInfo', 'column', 'OperatorInterval'
go

/*==============================================================*/
/* Table: PurifierRecord                                        */
/*==============================================================*/
create table PurifierRecord (
   ID                   uniqueidentifier          not null,
   MacAddr              varchar(20)          null,
   IP                   varchar(20)          null,
   PM25                 float                null,
   Temperature          float                null,
   Humidity             float                null,
   OnOffKey				int				     null,
   LockKey				int					 null,
   SleepKey				int					 null,
   AutoKey				int					 null,
   AnionKey				int					 null,
   UVKey				int					 null,
   IsFliter				int					 null,
   LastUpdateTime       datetime             null,
   Flag					int					 null,
   constraint PK_PURIFIERRECORD primary key (ID)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('PurifierRecord') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'PurifierRecord' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '净化器上传数据记录表', 
   'user', @CurrentUser, 'table', 'PurifierRecord'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ID')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'ID'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '记录ID',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'ID'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PurifierID')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'PurifierID'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '净化器ID',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'PurifierID'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'MacAddr')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'MacAddr'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '净化器网卡Mac地址',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'MacAddr'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'IP')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'IP'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '净化器IP',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'IP'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'RunTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'RunTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '运行时间',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'RunTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'FilterMediumTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'FilterMediumTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '滤材时间 ',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'FilterMediumTime'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PM25')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'PM25'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   'PM2.5值',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'PM25'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Temperature')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'Temperature'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '温度',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'Temperature'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Humidity')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'Humidity'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '湿度',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'Humidity'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('PurifierRecord')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LastUpdateTime')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'LastUpdateTime'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '上次更新时间',
   'user', @CurrentUser, 'table', 'PurifierRecord', 'column', 'LastUpdateTime'
go

/*==============================================================*/
/* Table: "User"                                                */
/*==============================================================*/
create table "User" (
   ID                  uniqueidentifier          not null,
   UserName             varchar(20)          null,
   Password             varchar(50)          null,
   Name                 varchar(50)          null,
   Address              varchar(100)         null,
   Contacts             varchar(50)          null,
   Company              varchar(50)          null,
   Department           varchar(50)          null,
   PurifierIDs          varchar(500)         null,
   Permission           varchar(50)          null,
   constraint PK_USER primary key (ID)
)
go

if exists (select 1 from  sys.extended_properties
           where major_id = object_id('"User"') and minor_id = 0)
begin 
   declare @CurrentUser sysname 
select @CurrentUser = user_name() 
execute sp_dropextendedproperty 'MS_Description',  
   'user', @CurrentUser, 'table', 'User' 
 
end 


select @CurrentUser = user_name() 
execute sp_addextendedproperty 'MS_Description',  
   '用户信息表', 
   'user', @CurrentUser, 'table', 'User'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'ID')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'ID'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户ID',
   'user', @CurrentUser, 'table', 'User', 'column', 'ID'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'UserName')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'UserName'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '用户名',
   'user', @CurrentUser, 'table', 'User', 'column', 'UserName'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Password')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Password'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '密码',
   'user', @CurrentUser, 'table', 'User', 'column', 'Password'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Name')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Name'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '名字',
   'user', @CurrentUser, 'table', 'User', 'column', 'Name'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Address')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Address'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '地址',
   'user', @CurrentUser, 'table', 'User', 'column', 'Address'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Contacts')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Contacts'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '联系方式',
   'user', @CurrentUser, 'table', 'User', 'column', 'Contacts'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Company')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Company'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '公司',
   'user', @CurrentUser, 'table', 'User', 'column', 'Company'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Department')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Department'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '部门',
   'user', @CurrentUser, 'table', 'User', 'column', 'Department'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'PurifierIDs')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'PurifierIDs'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '可控制净化器ID集合',
   'user', @CurrentUser, 'table', 'User', 'column', 'PurifierIDs'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('"User"')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Permission')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'User', 'column', 'Permission'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '权限标记',
   'user', @CurrentUser, 'table', 'User', 'column', 'Permission'
go

