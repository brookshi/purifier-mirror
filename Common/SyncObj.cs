﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;

namespace HOYI.Common
{
    /// <summary>
    /// Providers a class that manage great amount synchronous objects.
    /// </summary>
    public class SyncObj : IDisposable
    {
        class InnerState
        {
            
            Dictionary<int, int> m_LockTraceDic = new Dictionary<int, int>();

            Thread m_Locker;
            long m_LastLockedTime;

            public int RefCount { get; set; }

            public DateTime GetLastLockedTime()
            {
                long dtBin = 0;
                Interlocked.Exchange(ref dtBin, m_LastLockedTime);
                if (dtBin == 0)
                    return default(DateTime);
                else
                    return DateTime.FromBinary(dtBin);
            }

            void SetLastLockedTime()
            {
                SetLastLockedTime(DateTime.UtcNow);
            }

            void SetLastLockedTime(DateTime lockedTime)
            {
                var setBin = lockedTime.ToBinary();
                Interlocked.Exchange(ref m_LastLockedTime, setBin);
            }

            public InnerState()
            {
                RefCount = 0;
                SetLastLockedTime(DateTime.SpecifyKind(DateTime.Parse("1990-01-01"), DateTimeKind.Utc));
            }

            public Thread GetCurrentLocker()
            {
                Thread locker = null;
                Interlocked.Exchange<Thread>(ref locker, m_Locker);
                return locker;
            }

            void SetLocker(Thread thread)
            {
                Interlocked.Exchange<Thread>(ref m_Locker, thread);
            }

            void SetCurrentLocker()
            {
                Interlocked.Exchange<Thread>(ref m_Locker, Thread.CurrentThread);
            }

            public int TraceThreadLockStatus(bool isLockedFlag)
            {
                var thHash = Thread.CurrentThread.GetHashCode();
                SetLastLockedTime();
                if (!m_LockTraceDic.ContainsKey(thHash)) m_LockTraceDic[thHash] = 0;
                var cn = m_LockTraceDic[thHash];
                if (isLockedFlag)
                    cn = cn + 1;
                else if (cn > 0)
                    cn = cn - 1;
                if (cn > 0)
                {
                    m_LockTraceDic[thHash] = cn;
                    SetCurrentLocker();
                }
                else
                {
                    SetLocker(null);
                    m_LockTraceDic.Remove(thHash);
                }
                return cn;
            }
        }

        InnerState m_State;

        /// <summary>
        /// Get a id of the syncobj
        /// </summary>
        public string SyncId { get; private set; }
        
        /// <summary>
        /// Get a time of the object last locked.
        /// </summary>
        public DateTime LastLockedTime { get { return m_State.GetLastLockedTime(); } }

        /// <summary>
        /// Get a value, true : the object is locked, false:the object is not locked.
        /// </summary>
        public bool IsHeld { get { return m_State.GetCurrentLocker() != null; } }

        protected internal SyncObj(Guid syncId)
            : this(syncId.ToString())
        {
        }

        protected internal SyncObj(string syncId)
        {
            SyncId = syncId;
            Init();
        }

        protected virtual void Init()
        {
            AutoDelete = true;
            m_State = new InnerState();
        }

        /// <summary>
        /// Delete the object.
        /// </summary>
        public void Delete()
        {
            Delete(false);
        }

        /// <summary>
        /// Delete or release the object.
        /// </summary>
        /// <param name="checkRefCount">Directive if check reference count.</param>
        public void Delete(bool checkRefCount)
        {
            lock (syncObjs)
            {
                if (!checkRefCount || (checkRefCount && m_State.RefCount <= 0))
                {
                    string key = "";
                    foreach (var soi in syncObjs)
                    {
                        if (soi.Value == this)
                        {
                            key = soi.Key;
                            break;
                        }
                    }
                    if (syncObjs.ContainsKey(key))
                        syncObjs.Remove(key);
                }
            }
        }

        /// <summary>
        /// Recycle the object.
        /// </summary>
        public void Recycle()
        {
            lock (syncObjs)
            {
                syncObjs[SyncId] = this;
            }
        }

        /// <summary>
        /// Lock the object.
        /// </summary>
        /// <returns>return the sync object.</returns>
        public SyncObj Lock()
        {
            Monitor.Enter(this);
            m_State.TraceThreadLockStatus(true);
            return this;
        }

        /// <summary>
        /// Unlock the object
        /// </summary>
        protected void Unlock()
        {
            m_State.TraceThreadLockStatus(false);
            Monitor.Exit(this);
        }

        static Dictionary<string, SyncObj> syncObjs = new Dictionary<string, SyncObj>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Get a sync object by id and auto to delete after unlock.
        /// </summary>
        /// <param name="objId">A string as id of the sync obj</param>
        /// <returns>Return a sync object</returns>
        public static SyncObj GetSyncObj(string objId)
        {
            return GetSyncObj(objId, true);
        }

        /// <summary>
        ///  Get a sync object by a class instance and don't auto to delete after unlock.
        /// </summary>
        /// <param name="keyObj">Create SyncObj id by the object</param>
        /// <returns>Return a sync object</returns>
        public static SyncObj GetSyncObj(object keyObj)
        {
            if (keyObj == null)
                throw new ArgumentNullException("keyObj");
            return GetSyncObj(string.Format("{0}.{1}", keyObj.GetType().Name, keyObj.GetHashCode()), false);
        }

        /// <summary>
        ///  Get a sync object by a class instance.
        /// </summary>
        /// <param name="objId">A string as id of the sync obj</param>
        /// <param name="autoDelete">Directive if auto to delete after unlock</param>
        /// <returns>Return a sync object</returns>
        public static SyncObj GetSyncObj(string objId, bool autoDelete)
        {
            lock (syncObjs)
            {
                SyncObj so = null;
                var innerKey = CreateInnerKey(objId, autoDelete);
                if (syncObjs.ContainsKey(innerKey))
                    so = syncObjs[innerKey];
                if (so == null)
                {
                    so = new SyncObj(objId) { AutoDelete = autoDelete };
                    syncObjs[innerKey] = so;
                }
                if (so.AutoDelete)
                {
                    if (so.m_State.RefCount < 0)
                        so.m_State.RefCount = 0;
                    ++so.m_State.RefCount;
                }
                return so;
            }
        }

        static string CreateInnerKey(string objId, bool autoDelete)
        {
            var innerKey = string.Format("{0}.{1}", objId, autoDelete ? "AD" : "ND");
            return innerKey;
        }

        /// <summary>
        /// Detects whether the auto deleting sync obj of the id is exists
        /// </summary>
        /// <param name="objId">A string as id deleted</param>
        /// <returns>true:is exists,false: is not exists.</returns>
        public static bool IsExist(string objId)
        {
            return IsExist(objId, true);
        }


        /// <summary>
        /// Detects whether the sync obj of the id is exists
        /// </summary>
        /// <param name="objId">A string as id deleted</param>
        /// <param name="autoDelete">A bool, true: auto deleting sync object.</param>
        /// <returns>true:is exists,false: is not exists.</returns>
        public static bool IsExist(string objId,  bool autoDelete)
        {
            lock (syncObjs)
            {
                var key = CreateInnerKey(objId, autoDelete);
                return syncObjs.ContainsKey(key);
            }
        }

        /// <summary>
        /// Get a bool value to indicate if the object will auto delete after  unlock.
        /// </summary>
        public bool AutoDelete { get; private set; }
        protected bool IsLockedByCurrent { get { return m_State.GetCurrentLocker() == Thread.CurrentThread; } }
        
        /// <summary>
        /// Release the sync object
        /// </summary>
        public void Dispose()
        {
            lock (syncObjs)
            {
                if (AutoDelete)
                {
                    if ((--m_State.RefCount) <= 0)
                        Delete();
                }
            }
            if (IsLockedByCurrent)
                Unlock();
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            Dispose();
        }

        #endregion
    }
}