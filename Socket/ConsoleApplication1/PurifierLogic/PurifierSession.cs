﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;

namespace PurifierLogic
{
    public class PurifierSession : AppSession<PurifierSession, BinaryRequestInfo>
    {
        protected override void OnSessionStarted()
        {
            
        }

        protected override void HandleUnknownRequest(BinaryRequestInfo requestInfo)
        {
            Logger.WarnFormat("====unknow request info in {0}: requestinfo={1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), requestInfo.Body);
            Close();
        }

        protected override void HandleException(Exception e)
        {
            Logger.ErrorFormat("=====exception in {0}======\nMessage:{1}\nSource:{2}\nTrace:{3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), e.Message, e.Source, e.StackTrace);
            Close();
        }

        protected override void OnSessionClosed(CloseReason reason)
        {
            base.OnSessionClosed(reason);
        }
    }
}
