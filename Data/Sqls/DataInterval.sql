USE [Purifier]
GO

/****** Object:  Table [dbo].[DataInterval]    Script Date: 12/29/2013 22:51:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DataInterval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UploadInterval] [int] NULL,
	[SearchInterval] [int] NULL,
 CONSTRAINT [PK_DataInterval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上传数据时间间隔' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DataInterval', @level2type=N'COLUMN',@level2name=N'UploadInterval'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'查询配置时间间隔' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DataInterval', @level2type=N'COLUMN',@level2name=N'SearchInterval'
GO


