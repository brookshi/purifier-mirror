﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "UserLog")]
    public abstract class UserLog : BaseDataObj
    {
        public Guid Id { get; private set; }
        public string username { get; private set; }
        public string client { get; private set; }

        public DateTime CreatedDate
        {
            get;
            protected set;
        }

        protected UserLog()
        {
 
        }

        public static void Save(string client,string name)
        {
            UserLog log = BaseDataObj.CreateDataObj<UserLog>();
            log.Id = new Guid();
            log.username = name;
            log.client = client;
            log.CreatedDate = DateTime.Now;
            PersistenceHelper.Default.Save<UserLog>(log);
        }
    }
}
