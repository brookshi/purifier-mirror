﻿using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Common.Data.Indexers;
using HOYI.Data;
using HOYI.Web.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Services
{
    public class PurifierController : PurifierInfoView
    {
        public Guid UserId { get; set; }

        protected Guid TargetUserId
        {
            get
            {
                var cu = User.Current;
                if (cu.IsInRole(Role.AdminRole))
                {
                    if (UserId == default(Guid))
                        throw new ArgumentException("UserId");
                    return UserId;
                }
                else
                {
                    if (UserId != default(Guid))
                        throw new InvalidOperationException("Invalid Paramerters");
                    return cu.Id;
                }
            }
        }

        public PurifierInfoView AddPurifier()
        {
            var cu = User.Current;
            if (!cu.IsInRole(Role.AdminRole)) 
                throw new InvalidOperationException("非法的操作");

            if (string.IsNullOrEmpty(this.MAC))
                throw new ArgumentException("设备MAC地址不能为空");

            GuardName();
            GuardCity();
            GuardAddr();
            using (var so = SyncObj.GetSyncObj(this.MAC, true))
            {
                this.CreatedBy = User.Current.Id;
                UpdatePurifierStatus(true);
                var info = UpdateInfo(true);

                UserPurifier.BindPurifer(this.TargetUserId, info);
                return PurifierInfoView.GetInfoView(info.Id);
            }
        }

        private PurifierInformation UpdateInfo(bool autoCreate)
        {
            var info = PurifierInformation.GetInfo(this.MAC);
            if (info == null)
            {
                if (!autoCreate) return null;
                info = PurifierInformation.Create(DataIndexer.GetIndexer(JObject.FromObject(this)));
            }
            else
            {
                var igs = new List<string>() { "Description", "SNCode", "CreatedBy", "Id", "MAC" };
                if (!User.Current.IsInRole(Role.AdminRole))
                {
                    igs.Add("City");
                    igs.Add("Addr");
                }
                ObjectCopier.Copy(this, info, igs);
            }
            info.Save();
            return info;
        }

        private void UpdatePurifierStatus(bool autoCreate)
        {
            var status = PurifierStatus.GetByMAC(this.MAC);
            if (status == null)
            {
                if (!autoCreate) return;
                status = PurifierStatus.Create(DataIndexer.GetIndexer(JObject.FromObject(this)));
                status.Save();
            }
            else
            {
                //ObjectCopier.Copy(this, status, new List<string>() { "IP", "Id", "MAC" });
                bool needChangeStatusFlag = status.NeedChangeStatusFlag(Tear, OnOffKey, LockKey, AutoKey,
                    SleepKey, AnionKey, UVKey, status.IsFliter);
                bool needChangeTimeFlag = status.NeedChangeTimeFlag(TimerHour);
                if (needChangeStatusFlag)
                    status.StatusFlag = status.StatusFlag < 255 ? status.StatusFlag + 1 : 0;
                if (needChangeTimeFlag)
                    status.TimeFlag = status.TimeFlag < 255 ? status.TimeFlag + 1 : 0;
                if (needChangeStatusFlag || needChangeTimeFlag)
                {
                    status.HasChanged = true;
                    status.UpdatePropertyAndSave(Tear, OnOffKey, LockKey, AutoKey,
                    SleepKey, AnionKey, UVKey, status.IsFliter, TimerHour, TimerMin);
                }
            }
        }

        private void GuardCity()
        {
            if (!string.IsNullOrEmpty(this.City) && this.City.Length > 20)
                throw new ArgumentException("输入的设备描述太长，请不要超过20个字符");
        }

        private void GuardAddr()
        {
            if (!string.IsNullOrEmpty(this.Addr) && this.Addr.Length > 200)
                throw new ArgumentException("输入的设备描述太长，请不要超过200个字符");
        }

        private void GuardName()
        {
            if (string.IsNullOrEmpty(this.Name))
                throw new ArgumentException("请输入设备名称");
            if (this.Name.Length > 50)
                throw new ArgumentException("输入设备的名称太长，请不要超过50个字符");
        }

        public void Delete(Guid userId)
        {
            this.InitPurifierId();
            UserPurifier.Delete(userId, Id);
        }

        public void Delete()
        {
            this.InitPurifierId();
            var pfs = UserPurifier.GetOwnerIds(this.Id);
            PersistenceHelper.Default.DeleteObjs<UserPurifier>(pfs);
        }

        internal PurifierInfoView ModifyPurifier()
        {
            GuardName();
            GuardCity();
            GuardAddr();
            this.CreatedBy = User.Current.Id;
            var info = UpdateInfo(false);
            if (info == null) return null;
            UpdatePurifierStatus(true);
            var piv = PurifierInfoView.GetInfoView(info.Id);
            return piv;
        }
    }
}