﻿using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Common.Data.Indexers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{
    [Serializable]
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "PurifierInformation")]
    public class PurifierInformation : PurifierBase
    {
        public PurifierInformation()
        {
        }

        public long Id
        {
            get;
            protected set;
        }

        protected override void SetPurifierId(long purifierId)
        {
            this.Id = purifierId;
        }

        public virtual string Name
        {
            get;
            protected set;
        }

        public Guid CreatedBy
        {
            get;
            protected set;
        }

        public virtual string Description
        {
            get;
            protected set;
        }

        public virtual string SNCode
        {
            get;
            set;
        }

        public virtual string City
        {
            get;
            set;
        }

        public virtual string Addr
        {
            get;
            set;
        }

        public void Save()
        {
            PersistenceHelper.Default.Save(this);
        }

        public void UpdateNameAndDesc(string name, string desc)
        {
            this.Name = name;
            this.Description = desc;
            //if (this.IsPropertyChanged("Name") && this.IsNameUsed())
            //{
            //    throw new InvalidOperationException(string.Format("设备名:{0}已经被使用，请换一个。", Name));
            //}
            this.Save();
        }

        public bool IsNameUsed()
        {
            WhereCriterion wc = new WhereCriterion("Id", OperatorFlag.Notequal, this.Id);
            wc.And("Name", OperatorFlag.Equal, Name);
            return PersistenceHelper.Default.Count<PurifierInformation>(wc) > 0;
        }

        public static PurifierInformation GetInfo(string mac)
        {
            var id = CalcPurifierId(mac);
            return PersistenceHelper.Default.Read<PurifierInformation>(id);
        }

        public static List<PurifierInformation> LoadInfos(List<long> ids)
        {
            var gps = Tools.GroupList(ids, 100);
            List<PurifierInformation> infos = new List<PurifierInformation>();
            while (gps.MoveNext())
            {
                WhereCriterion wc = new WhereCriterion();
                wc.IN("Id", (gps.Current.ToList().Cast<object>().ToArray()));
                infos.AddRange(PersistenceHelper.Default.Query<PurifierInformation>(wc));
            }
            return infos;
        }

        public static List<PurifierInformation> LoadAll()
        {
            WhereCriterion wc = new WhereCriterion();
            return PersistenceHelper.Default.Query<PurifierInformation>(wc);
        }


        public static List<PurifierInformation> LoadAllExcept(Guid userId, int pageCount, int pageIndex, string orderName, OrderDirection oder)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.NotIN("Id", subWC);
            return PersistenceHelper.Default.Query<PurifierInformation>(wc);
        }

        public static int GetAllCount()
        {
            WhereCriterion wc = new WhereCriterion();
            return PersistenceHelper.Default.Count<PurifierInformation>(wc);
        }

        public static List<PurifierInformation> LoadUserPurifiers(Guid userId)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            var pfs = PersistenceHelper.Default.Query<PurifierInformation>(wc);
            return pfs;
        }

        public static List<PurifierInformation> LoadPurifiersForSNCodes(List<string> sncodes)
        {
            WhereCriterion wc = new WhereCriterion();
            wc.IN("SNCode", sncodes.ToArray());
            var pfs = PersistenceHelper.Default.Query<PurifierInformation>(wc);
            return pfs;
        }

        public static int GetCountOfUserPurifiers(Guid userId)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            var count = PersistenceHelper.Default.Count<PurifierInformation>(wc);
            return count;
        }

        public static List<PurifierInformation> GetAllInfoByPage(int pageCount, int pageIndex, string orderName, OrderDirection oder)
        {
            WhereCriterion wc = new WhereCriterion();
            return PersistenceHelper.Default.PageObjs<PurifierInformation>(pageIndex, pageCount, orderName, oder, wc);
        }

        public static List<PurifierInformation> GetUserInfoByPage(Guid userId, int pageCount, int pageIndex, string orderName, OrderDirection oder)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            return PersistenceHelper.Default.PageObjs<PurifierInformation>(pageIndex, pageCount, orderName, oder, wc);
        }

        public static List<PurifierInformation> GetInfoByCitys(string[] citys)
        {
            WhereCriterion wc = new WhereCriterion();
            wc.IN("City", citys);
            return PersistenceHelper.Default.Query<PurifierInformation>(wc);
        }

        public static PurifierInformation GetInfoBySNCode(string sncode)
        {
            WhereCriterion wc = new WhereCriterion("SNCode", OperatorFlag.Equal, sncode);
            var info = PersistenceHelper.Default.TopOne<PurifierInformation>("SNCode", OrderDirection.DESC, wc);
            return info;
        }

        public static List<PurifierInformation> GetInfoBySNCodeEx(string sncode)
        {
            WhereCriterion wc = new WhereCriterion("SNCode", OperatorFlag.Like, string.Format("%{0}%", sncode));
            return PersistenceHelper.Default.Query<PurifierInformation>(wc);
        }

        public static PurifierInformation GetInfoBySNCodeForUser(Guid userId, string sncode)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            wc.And("SNCode", OperatorFlag.Equal, sncode);
            var info = PersistenceHelper.Default.TopOne<PurifierInformation>("SNCode", OrderDirection.DESC, wc);
            return info;
        }

        public static PurifierInformation GetInfoBySNCodeNotForUser(Guid userId, string sncode)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.NotIN("Id", subWC);
            wc.And("SNCode", OperatorFlag.Equal, sncode);
            var info = PersistenceHelper.Default.TopOne<PurifierInformation>("SNCode", OrderDirection.DESC, wc);
            return info;
        }

        public static bool IsExist(string mac)
        {
            var id = CalcPurifierId(mac);
            WhereCriterion wc = new WhereCriterion("ID", OperatorFlag.Equal, id);
            return PersistenceHelper.Default.Count<PurifierInformation>(wc) > 0;
        }

        public static PurifierInformation Create(string macAddr)
        {
            var info = BaseDataObj.CreateDataObj<PurifierInformation>();
            info.MAC = macAddr;
            info.Name = "";
            info.CreatedBy = Guid.NewGuid();
            return info;
        }

        public static PurifierInformation Create(DataIndexer di)
        {
            return PurifierInformation.CreateDataObj<PurifierInformation>(di);
        }

        
    }
}
