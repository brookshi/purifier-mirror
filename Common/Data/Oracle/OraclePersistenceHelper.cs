﻿using System;
using System.Collections.Generic;


namespace HOYI.Common.Data.Oracle
{
    public class OraclePersistenceHelper : PersistenceHelper
    {
        protected override bool SupportEnableMultiDb { get { return false; } }

        public OraclePersistenceHelper(DBSetting setting)
            : base(setting)
        {

        }


        protected override Dictionary<string, List<T>> GroupDataObjByDb<T>(IEnumerable<T> dataObjs)
        {
            var dic = new Dictionary<string, List<T>>();
            foreach (var obj in dataObjs)
            {
                if (obj != null)
                {
                    string dbName = GetDbName(obj.GetType(), null);
                    if (!dic.ContainsKey(dbName))
                        dic[dbName] = new List<T>();
                    dic[dbName].Add(obj);
                }
            }
            return dic;
        }

        protected override IDataObjPersister CreateDataObjPersister(string dbName)
        {
            var dop = new OracleDataObjPersister();
            DBSetting setting = Setting.Clone();
            setting.Database = dbName;
            dop.ConnectionString = GetConnectionString(setting);
            return dop;
        }

        protected override string GetConnectionString(IDBSetting setting)
        {
            string serverName = setting.Server;
            if (string.IsNullOrEmpty(serverName))
                serverName = Environment.MachineName;
            return string.Format("DATA SOURCE={0};USER ID={1};PASSWORD={2};", serverName, setting.User, setting.Password);
        }


        protected override string GetDbName(Type dataObjType, object objId)
        {
            return "Default";
        }
    }
}
