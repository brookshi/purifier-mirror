Ext.define('Purifier.config.GlobalInfo', {
    singleton : true,

    config : {
        isAdmin:false,
		isAddUser:true
    },
    constructor : function(config) {
        this.initConfig(config);
    }
});