﻿using HOYI.Common;
using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HOYI.Data
{
    [Serializable]
    [DataObj(DBName = "Purifier", PrimaryKeyName = "Id", TableName = "[PurifierRecord]")]
    public  class PurifierRecord : PurifierBase
    {
        public PurifierRecord()
        {
            CreateDate = PersistenceHelper.MinDateTime;
        }

        public Guid Id
        {
            get;
            protected set;
        }

        public void SetID(Guid id)
        {
            Id = id;
        }

        public virtual long PurifierID
        {
            get;
            set;
        }

        protected override void SetPurifierId(long purifierId)
        {
            PurifierID = purifierId;
        }

        public virtual string Ip
        {
            get;
            set;
        }

        public virtual double Pm25
        {
            get;
            set;
        }

        public virtual double Temperature
        {
            get;
            set;
        }

        public virtual double Humidity
        {
            get;
            set;
        }

        public virtual DateTime CreateDate
        {
            get;
            set;
        }

        public virtual RecordFlag Flag
        {
            get;
            set;
        }

        public void Save()
        {
            PersistenceHelper.Default.Save(this);
        }

        public bool NeedUpdate(double pm25, double temperature, double humidity)
        {
            return this.Pm25 != pm25 || this.Temperature != temperature || this.Humidity != humidity;
        }

        public static void ApplyProperty(PurifierRecord newRecord, PurifierRecord sourceRecord)
        {
            newRecord.PurifierID = sourceRecord.PurifierID;
            newRecord.MAC = sourceRecord.MAC;
            newRecord.Ip = sourceRecord.Ip;
            newRecord.Pm25 = sourceRecord.Pm25;
            newRecord.Temperature = sourceRecord.Temperature;
            newRecord.Humidity = sourceRecord.Humidity;
            newRecord.Flag = sourceRecord.Flag;
            newRecord.CreateDate = DateTime.Now;
            newRecord.InitPurifierId();
        }

        public static void ApplyProperty(PurifierRecord record, double pm25, double temperature, double humidity)
        {
            record.Pm25 = pm25;
            record.Temperature = temperature;
            record.Humidity = humidity;
            record.CreateDate = DateTime.Now;
        }

        public static PurifierRecord Create(string macAddr, string ip,
            double pm25, double temperature, double humidity, RecordFlag flag)
        {
            var record = BaseDataObj.CreateDataObj<PurifierRecord>();
            record.MAC = macAddr;
            record.Ip = ip;
            record.Flag = flag;
            ApplyProperty(record, pm25, temperature, humidity);
            return record;
        }

        public static PurifierRecord GetNewestRecord(string macAddr)
        {
            var id = CalcPurifierId(macAddr);
            WhereCriterion wc = new WhereCriterion("PurifierID", OperatorFlag.Equal, id).And("Flag", OperatorFlag.Equal, RecordFlag.Newest);
            return PersistenceHelper.Default.TopOne<PurifierRecord>("CreateDate", OrderDirection.DESC, wc);
        }

        public static List<PurifierRecord> GetNewestRecordsByMac(List<string> macAddrs, int pageCount, int pageIndex)
        {
            List<string> Ids = new List<string>();
            macAddrs.ForEach(o => Ids.Add(CalcPurifierId(o).ToString()));
            WhereCriterion wc = new WhereCriterion("Flag", OperatorFlag.Equal, RecordFlag.Newest);
            wc.IN("PurifierID", Ids.ToArray());
            return PersistenceHelper.Default.PageObjs<PurifierRecord>(pageIndex, pageCount, "CreateDate", OrderDirection.DESC, wc);
        }

        public static PurifierRecord GetAvgRecord(string macAddr)//need change, get avg record for current date
        {
            var id = CalcPurifierId(macAddr);
            WhereCriterion wc = new WhereCriterion("PurifierID", OperatorFlag.Equal, id).And("Flag", OperatorFlag.Equal, RecordFlag.Avg);
            return PersistenceHelper.Default.TopOne<PurifierRecord>("CreateDate", OrderDirection.DESC, wc);
        }

        public static bool IsExist(string recordid)
        {
            WhereCriterion wc = new WhereCriterion("id", OperatorFlag.Equal, recordid);
            return PersistenceHelper.Default.Count<PurifierRecord>(wc) > 0;
        }

        public static List<PurifierRecord> GetNewestRecordByPage(int pageCount, int pageIndex, string orderName, OrderDirection oder)
        {
            WhereCriterion wc = new WhereCriterion("Flag", OperatorFlag.Equal, RecordFlag.Newest);
            return PersistenceHelper.Default.PageObjs<PurifierRecord>(pageIndex, pageCount, orderName, oder, wc);
        }

        public static List<PurifierRecord> GetNewestRecordByPageForUser(string[] purifierIds, int pageCount, int pageIndex, string orderName, OrderDirection oder)
        {
            WhereCriterion wc = new WhereCriterion("Flag", OperatorFlag.Equal, RecordFlag.Newest);
            wc.IN("PurifierID", purifierIds);
            return PersistenceHelper.Default.PageObjs<PurifierRecord>(pageIndex, pageCount, orderName, oder, wc);
        }

        public static int GetNewestRecordCountForUser(string[] purifierIds)
        {
            WhereCriterion wc = new WhereCriterion("Flag", OperatorFlag.Equal, RecordFlag.Newest);
            wc.IN("PurifierID", purifierIds);
            return PersistenceHelper.Default.Count<PurifierRecord>(wc);
        }

        public static int GetNewestRecordCount()
        {
            WhereCriterion wc = new WhereCriterion("Flag", OperatorFlag.Equal, RecordFlag.Newest);
            return PersistenceHelper.Default.Count<PurifierRecord>(wc);
        }

        public static List<PurifierRecord> LoadPurifierData(List<long> pufs)
        {
            WhereCriterion wc = new WhereCriterion();
            wc.IN("PurifierID", pufs.Cast<object>().ToArray());
            return PersistenceHelper.Default.Query<PurifierRecord>(wc);
        }

        public static List<PurifierRecord> LoadPurifierRecordForStatisticsById(string[] PurifierIds, DateTime startDate, DateTime endDate) 
        {
            if (PurifierIds == null) return null;
            return LoadPurifierRecordForStatisticsById<PurifierRecord>(PurifierIds.Select(p => Parser.Parse(p, 0L)), startDate, endDate);
        }

        public static List<T> LoadPurifierRecordForStatisticsById<T>(IEnumerable<long> PurifierIds, DateTime startDate, DateTime endDate) where T:PurifierRecord
        {
            WhereCriterion wc = new WhereCriterion();
            wc.IN("PurifierID", PurifierIds.Cast<Object>().ToArray());
            wc.And("CreateDate", OperatorFlag.GTE, startDate);
            wc.And("CreateDate", OperatorFlag.LT, endDate);
            wc.And("Flag", OperatorFlag.Equal, RecordFlag.Avg);
            return PersistenceHelper.Default.Query<T>(wc);
        }

        public static List<T> LoadData<T>(IEnumerable<long> PurifierIds, DateTime startDate, DateTime endDate) where T : PurifierRecord
        {
            WhereCriterion wc = new WhereCriterion();
            wc.IN("PurifierID", PurifierIds.Cast<Object>().ToArray());
            wc.And("CreateDate", OperatorFlag.GTE, startDate);
            wc.And("CreateDate", OperatorFlag.LT, endDate);
            wc.And("Flag", OperatorFlag.Equal, RecordFlag.Avg);
            return PersistenceHelper.Default.Query<T>(wc);
        }

    }
}