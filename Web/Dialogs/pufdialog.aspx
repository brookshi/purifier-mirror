﻿<%@ Page Language="C#" Inherits="HOYI.Web.Common.PageBase" %>
<div class="dlgdevadd">
    <div class="inputZn">
            <asp:PlaceHolder id="plhUN" runat="server">
        <dl nomd="1">
                <dt>绑定用户*</dt>
                <dd>
                    <input type="text" autocomplete="off" name="UserName" maxlength="100" class="txtinput" readonly="readonly"/>
                    <input type="hidden" name="UserId"/>
                </dd>
        </dl>
            </asp:PlaceHolder>
        <dl>
            <dt>MAC地址*</dt>
            <dd><input type="text" autocomplete="off" title="请输入MAC" name="PfMAC" maxlength="100" class="txtinput"  regx="([0-9A-F]{12})|(([0-9A-F]{2}\-){5}[0-9A-F]{2})" regmsg="MAC地址格式不正确，请输入正确的MAC。"/></dd>
        </dl>
         <dl>
            <dt>设备名称*</dt>
            <dd><input type="text" title="给这个设备起个名字，方便使用" name="Name" maxlength="20" class="txtinput" /></dd>
        </dl>
        <dl>
            <dt>城市*</dt>
            <dd><input type="text" name="City" title="请选择城市" maxlength="20" class="txtinput"  readonly="readonly"/></dd>
         </dl>
         <dl>
            <dt>档 位</dt>
            <dd>
                <select name="Tear" cvt="toInt">
                    <option value="1">一 档</option>
                    <option value="2">二 档</option>
                    <option value="3">三 档</option>
                    <option value="4">四 档</option>
                    <option value="5">五 档</option>
                </select>
            </dd>
        </dl>
        <dl>
            <dt>开 / 关</dt>
            <dd><input type="checkbox"  name="OnOffKey" dv="1"  class="cb"/></dd>
            </dl>
            <dl>
            <dt>童 锁</dt>
            <dd><input type="checkbox"  name="LockKey" dv="0" class="cb"/></dd>
        </dl>
        <dl>
            <dt>睡 眠</dt>
            <dd><input type="checkbox"  name="SleepKey" dv="0" class="cb"/></dd>
            </dl>
            <dl>
            <dt>自动键</dt>
            <dd><input type="checkbox"  name="AutoKey"  dv="0" class="cb"/></dd>
            </dl>
            <dl>
            <dt>负离子键</dt>
            <dd><input type="checkbox"  name="AnionKey" dv="0" class="cb"/></dd>
            </dl>
            <dl>
            <dt>UV键</dt>
            <dd><input type="checkbox"  name="UVKey" dv="0" class="cb"/></dd>
        </dl>
        <dl>
            <dt>定时时间</dt>
            <dd><input type="text"  name="TimerHour" class="txtinput" style="width:100px" cvt="toInt"/> <span>小时</span> <input type="text"  name="TimerMin" class="txtinput" style="width:100px" cvt="toInt"/> 分钟</dd>
            </dl>
            <dl>
            <dt>地 址</dt>
            <dd style="height:auto;"><textarea name="Addr" maxlength="2000" class="txtinput" rows="5" cols="10" style="height:auto;" <%=this.IsAdmin?"":"readonly=\"readonly\"" %> ></textarea></dd>
        </dl>
    </div>
    <div class="tipmsg">
    </div>
    <div class="btnZn" style="width:100%;">
        <div style="width:100%;margin:0; margin-top:1px;">
            <hr>
        </div>
        <div style="text-align:right; padding:0; margin:10px 0;width:100%;*margin:0">
            <input type="button" class="button" style="margin-right: 0.5em;" value="确 定" fn="save"/>
            <input type="button" class="button" value="取 消" fn="cancel"/>
        </div>
    </div>
</div>
<script runat="server">
    protected override void OnLoad(EventArgs e)
    {
        this.plhUN.Visible = this.IsAdmin;
        base.OnLoad(e);
    }
</script>
<!--Purifier Manager Dialog-->