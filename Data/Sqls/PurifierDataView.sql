﻿
CREATE VIEW [dbo].[PurifierDataView]
AS
SELECT   r.ID, r.PurifierID, r.MAC, r.IP, r.PM25, r.Temperature, r.Humidity, r.Flag, r.CreateDate, r.CreatedTime, ISNULL(i.Name, 
                r.MAC) AS Name, i.SNCode
FROM      dbo.PurifierRecord AS r WITH (NOLOCK) LEFT OUTER JOIN
                dbo.PurifierInformation AS i WITH (NOLOCK) ON r.PurifierID = i.ID

GO
