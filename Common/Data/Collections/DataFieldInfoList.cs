﻿using System;
using System.Collections.Generic;

namespace HOYI.Common.Data.Collections
{
    public class DataFieldInfoList : List<DataFieldInfo>
    {
        public DataFieldInfo GetDataFieldInfo(string fieldName)
        {
            if (string.IsNullOrEmpty(fieldName))
            {
                throw new ArgumentNullException("fieldName");
            }
            foreach (var dfi in this)
            {
                if (dfi.FieldName == fieldName)
                    return dfi;
            }
            return null;
        }

        public DataFieldInfo this[string name] { get { return GetDataFieldInfo(name); } }
    }
}
