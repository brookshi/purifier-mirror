Ext.define('Purifier.override.field.Text', {
    override: 'Ext.field.Text',
    initialize: function () {
        this.callParent();
        this.initializedAt = new Date().getTime();
     },
     
     onFocus: function (e) {
        this.callParent();
        if (e.time < this.initializedAt + 500) {
            console.log("Ext.field.Text override: bluring component as it was focused automatically");
            this.blur();
        }
     }
});