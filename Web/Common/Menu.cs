﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    public class Menu
    {
        public string Name { get; private set; }
        public string Action { get; private set; }
        internal string Role { get; private set; }

        protected Menu(string name, string action, string role)
        {
            this.Name = name;
            this.Action = action;
            this.Role = role;
        }

        public bool IsAuthen()
        {
            var cu = HOYI.Web.Common.User.Current;
            return cu.IsInRole(this.Role);
        }

        public readonly static List<Menu> Menus = new List<Menu>() { 
             new Menu("用户管理","ManageUser",HOYI.Data.Role.AdminRole),
            new Menu("净化器管理","ManagePurifier",HOYI.Data.Role.UserRole),
            new Menu("净化器数据","PurifierData",HOYI.Data.Role.UserRole),
            new Menu("数据分析","DataAnalyst",HOYI.Data.Role.UserRole),
            new Menu("净化器序列号","SerialNo",HOYI.Data.Role.AdminRole)
        };
    }
}