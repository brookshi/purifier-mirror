﻿/// <reference path="jquery-vsdoc.js" />
(function($) {
    $.menu = function (container, menus) {
        var mnuc = [];
        function createMenuItem(m) {
            var ihm = "<li></li>";
            var jmi = $(ihm);
            if (m.isCheckable) {
                var cbHm = $("<input type='checkbox' />");
                var cbId = "mcb." + (new Date()).getTime() + m.value;
                cbHm.val(m.value);
                cbHm.attr("id", cbId);
                cbHm.attr("caption", m.caption);
                cbHm.appendTo(jmi);
                var lbHm = $("<label></label>");
                lbHm.text(m.caption);
                lbHm.attr("for", cbId);
                lbHm.appendTo(jmi);
            }
            else {
                var ahm = $("<a href=\"#\">" + m.caption + "</a>");
                ahm.appendTo(jmi);
                if (m.href)
                    ahm.click(m.href, function (e) { window.document.location.href = e.data });
                else
                    ahm.click(function (e) {
                        if (m.delay)
                            window.setTimeout(function () { if (m.click) m.click(e); }, 30);
                        else if (m.click)
                            m.click(e);
                    });
            }
            if (m.css) jmi.css(m.css);
            return jmi;
        };
        function init() {
            var mul = $("<ul class=\"menu\"></ul>");
            for (var m = 0; m < menus.length; m++) {
                menus[m].level = 0;
                createMenu(menus[m], mul);
            }
            if (typeof (container) == "string")
                container = $(container);
            mul.appendTo(container[0] || container);
            var popMn = function(ev) {
                if (this.menuChild) {
                    var cn = this.menuChild;
                    var slideUpCN = function (e) {
                        if (ev && e && ev.target == e.target)
                            return;
                        cn.slideUp(100);
                        $(window.document).unbind("click", slideUpCN);
                        menus.onSlideUp && menus.onSlideUp(mnuc);
                    };
                    if (cn.css("display") == "none") {
                        menus.onPrePop && menus.onPrePop(mnuc);
                        cn.stop(true, true).slideDown(20);
                        var re = container;
                        var pos = re.offset();

                        if (re.css("display") == "none" || pos.top <= 3 && pos.left <= 3) {
                            re = $(container[0].parentNode);
                            pos = re.offset();
                        }
                        cn.css("top", (pos.top + re.outerHeight()) + "px");
                        cn.css("left", pos.left + "px");
                        $(window.document).bind("click", slideUpCN);
                    }
                    else {
                        slideUpCN();
                    }
                }
                else {
                    var jth = $(this);
                    !jth.has("input") && jth.parents("div", ".mulc").slideUp(20)
                    !jth.has("input") && menus.onSlideUp && menus.onSlideUp(mnuc);
                }
                ev.stopPropagation();
            };
            if (menus.length == 1 && container.length > 0) {
                var cpn = $(container[0].parentNode).css("cursor", "pointer");
                cpn.click(function (ev) { popMn.call($("li[hasul='y']", mul)[0], ev) });
            }
            $("li[hasul='y']", mul).add(".mulc .mul li").click(popMn);
            $(".mulc .mul li").hover(function() { },
                function() {
                    if (this.menuChild)
                        this.menuChild.slideUp(20);
                });
        };
        function createMenu(m, p) {
            var mi = createMenuItem(m);
            p.append(mi);
            if (!m.items)
                return;
            if (m.items.length > 0) {
                var mul = $("<ul class='mul'></ul>");
                for (var i = 0; i < m.items.length; i++) {
                    m.items[i].level = m.level + 1;
                    createMenu(m.items[i], mul);
                }
                var mulc = $("<div class='mulc'></div>");
                mulc.appendTo(document.body);
                mul.appendTo(mulc[0]);
                mi[0].menuChild = mulc;
                mi.attr("hasul", "y");
                mnuc.push(mulc);
            }
        };
        init();
        return mnuc;
    }
})(jQuery);