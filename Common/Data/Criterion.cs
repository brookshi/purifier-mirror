﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HOYI.Common.Data
{
    public interface ICriteria
    {
        string GetSentence();
    }

    public interface ICriteriaParam
    {
        void SetParameter(Dictionary<string, object> paramDic);
    }

    public enum OperatorFlag
    {
        Equal,
        Notequal,
        GT,
        GTE, //greate than equal
        LT,
        LTE, //less than equal
        Like
    }

    public class LeftBracketWord : ICriteria
    {
        internal LeftBracketWord()
        {
        }

        public string GetSentence()
        {
            return "(";
        }
    }

    public class RightBracketWord : ICriteria
    {
        internal RightBracketWord()
        {
        }

        public string GetSentence()
        {
            return ")";
        }
    }

    public class AndLinkWord : ICriteria
    {
        internal AndLinkWord()
        {
        }

        public string GetSentence()
        {
            return " AND ";
        }
    }

    public class OrLinkWord : ICriteria
    {
        internal OrLinkWord()
        {
        }

        public string GetSentence()
        {
            return " OR ";
        }
    }

    public class INLink : ICriteria
    {
        public string FieldName { get; private set; }

        public INLink(string fieldName)
        {
            if (string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            FieldName = fieldName;
        }

        public string GetSentence()
        {
            return string.Format(" {0} IN ", FieldName);
        }
    }

    internal class NotInLink : ICriteria
    {
        public string FieldName { get; private set; }

        public NotInLink(string fieldName)
        {
            if (string.IsNullOrEmpty(fieldName))
                throw new ArgumentNullException("fieldName");
            FieldName = fieldName;
        }

        public string GetSentence()
        {
            return string.Format(" {0} NOT IN ", FieldName);
        }
    }

    public class Condition : ICriteria, ICriteriaParam
    {
        public string Name { get; private set; }
        public object Value { get; set; }
        public OperatorFlag OP { get; private set; }
        public int AliasIndex { get; set; }
        public string ParamName { get { return Name + (AliasIndex > 0 ? AliasIndex.ToString() : ""); } }

        public Condition(string name, OperatorFlag op)
            : this(name, op, null)
        {
        }

        public Condition(string name, OperatorFlag op, object value)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException("name");
            }
            Name = name;
            OP = op;
            Value = value;
            AliasIndex = 0;
        }

        string GetOpString()
        {
            switch (OP)
            {
                case OperatorFlag.Equal:
                    return " = ";
                case OperatorFlag.GT:
                    return " > ";
                case OperatorFlag.GTE:
                    return " >= ";
                case OperatorFlag.Like:
                    return " LIKE ";
                case OperatorFlag.LT:
                    return " < ";
                case OperatorFlag.LTE:
                    return " <= ";
                case OperatorFlag.Notequal:
                    return " != ";
            }
            return "";
        }

        protected string GetSentence()
        {
            return string.Format("{0}{1}@{2}", Name, GetOpString(), ParamName);
        }

        #region ICritical Members

        string ICriteria.GetSentence()
        {
            return GetSentence();
        }

        #endregion

        #region ICriteriaParam Members

        void ICriteriaParam.SetParameter(Dictionary<string, object> paramDic)
        {
            paramDic[ParamName] = Value;
        }

        #endregion
    }

    public class SelectSentence : ICriteria, ICriteriaParam
    {
        public string FieldName { get; private set; }
        public Type DataObjType { get; private set; }
        public WhereCriterion Criterion { get; private set; }

        public SelectSentence(Type dataObjType, WhereCriterion criterion)
            : this(string.Empty, dataObjType, criterion)
        {
        }

        public SelectSentence(string fieldName, Type dataObjType, WhereCriterion criterion)
        {
            if (dataObjType == null)
                throw new ArgumentNullException("dataObjType");
            if (string.IsNullOrEmpty(fieldName))
                FieldName = DataObjInfo.GetDataObjInfo(dataObjType).PrimaryKeyName;
            else
                FieldName = fieldName;
            DataObjType = dataObjType;
            Criterion = criterion;
        }

        #region ICriteria Members

        string ICriteria.GetSentence()
        {
            var cu = PersistenceHelper.Current;
            if (cu == null) throw new InvalidOperationException("Error.");
            var isOrc = cu.Setting.DbType == DbProvider.Oracle;
            var sqlGn = (isOrc ? (SQLGeneratorBase)new HOYI.Common.Data.Oracle.SQLGenerator(DataObjType) : new HOYI.Common.Data.SqlServer.SQLGenerator(DataObjType));
            return sqlGn.GeneratorSelectSQL(FieldName, Criterion);
        }

        #endregion

        #region ICriteriaParam Members

        void ICriteriaParam.SetParameter(Dictionary<string, object> paramDic)
        {
            if (Criterion != null)
            {
                (Criterion as ICriteriaParam).SetParameter(paramDic);
            }
        }

        #endregion
    }

    public class WhereCriterion : ICriteria, ICriteriaParam
    {
        List<ICriteria> criterias = new List<ICriteria>();

        public WhereCriterion()
        {
        }

        public WhereCriterion(string name, OperatorFlag op, object value)
        {
            AddCriteria(name, op, value);
        }

        public WhereCriterion AddCriteria(ICriteria criteria)
        {
            if (criteria is Condition)
            {
                var cd = criteria as Condition;
                foreach (var cr in criterias)
                {
                    if (cr is Condition)
                    {
                        if (string.Equals((cr as Condition).Name, cd.Name, StringComparison.OrdinalIgnoreCase))
                        {
                            cd.AliasIndex++;
                        }
                    }
                }
            }
            criterias.Add(criteria);
            return this;
        }

        public WhereCriterion AddCriteria(string name, OperatorFlag op, object value)
        {
            return AddCriteria(new Condition(name, op, value));
        }

        public WhereCriterion And(string name, OperatorFlag op, object value)
        {
            if (IsEmpty == false)
                AddCriteria(AndLink);
            return AddCriteria(name, op, value);
        }

        public WhereCriterion Or(string name, OperatorFlag op, object value)
        {
            AddCriteria(OrLink);
            return AddCriteria(name, op, value);
        }

        public WhereCriterion Bracket(WhereCriterion criteria)
        {
            AddCriteria(LeftBracket);
            AddCriteria(criteria);
            return AddCriteria(RightBracket);
        }
        
        public WhereCriterion NotIN(string name, object[] values)
        {
            return Combine(name, values, OperatorFlag.Equal);
        }

        public WhereCriterion IN(string name, object[] values)
        {
            return Combine(name, values, OperatorFlag.Equal);
        }

        WhereCriterion Combine(string name, object[] values, OperatorFlag flag)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");
            if (values == null || values.Length == 0)
                return this;
            WhereCriterion c = null;
            if (criterias.Count > 0)
                c = AddCriteria(AndLink);
            else
                c = this;
            c = c.AddCriteria(LeftBracket);
            foreach (var v in values.Distinct())
            {
                if (Array.IndexOf(values, v) > 0)
                {
                    if (flag == OperatorFlag.Equal)
                        c = c.Or(name, flag, v);
                    else
                        c = c.And(name, flag, v);
                }
                else
                    c = c.AddCriteria(name, flag, v);
            }
            c = c.AddCriteria(RightBracket);
            return c;
        }

        public WhereCriterion IN(string name, SelectSentence subSentence)
        {
            AddCriteria(new INLink(name));
            AddCriteria(LeftBracket);
            AddCriteria(subSentence);
            AddCriteria(RightBracket);
            return this;
        }

        public WhereCriterion NotIN(string name, SelectSentence subSentence)
        {
            AddCriteria(new NotInLink(name));
            AddCriteria(LeftBracket);
            AddCriteria(subSentence);
            AddCriteria(RightBracket);
            return this;
        }

        public string GetSentence()
        {
            var sb = new StringBuilder();
            criterias.ForEach(c => sb.Append(c.GetSentence()));
            return sb.ToString();
        }

        public bool IsEmpty { get { return criterias.Count == 0; } }

        public Dictionary<string, object> GetParameterNames()
        {
            var ccs = new Dictionary<string, object>();
            (this as ICriteriaParam).SetParameter(ccs);
            return ccs;
        }

        public static readonly ICriteria LeftBracket = new LeftBracketWord();
        public static readonly ICriteria RightBracket = new RightBracketWord();
        public static readonly ICriteria AndLink = new AndLinkWord();
        public static readonly ICriteria OrLink = new OrLinkWord();

        #region ICriteriaParam Members

        void ICriteriaParam.SetParameter(Dictionary<string, object> paramDic)
        {
            foreach (var cc in criterias)
            {
                if (cc is ICriteriaParam)
                {
                    var cdi = (cc as ICriteriaParam);
                    cdi.SetParameter(paramDic);
                }
            }
        }

        #endregion
    }
}
