﻿///<reference path="jquery-vsdoc.js" />
if (!Util) {
    var Util = {};
}
/*data grid*/
Util.DataGrid = function(container, data, oConfig, callback) {
    this.el = null;
    this.container = $(container);
    this.rows = {};
    this.header = null;
    this.body = null;
    this.DOMMap = {};
    this.lastSortCellKey = '';
    this.lastSortOrder = '';
    this._emptyStr = '';
    this.selectedRow = null;
    this.selectedRows = {};
    this.selectedRowsNo = 0;
    this.fxCols = null;
    this.cfg = {
        column: [],
        indexColKey: 'id',
        colMinWidth: 20,
        width: 300,
        height: 200,
        //fixSize:true,
        headerRow: true,
        sortable: true,
        cellResize: true,
        cellRearrange: true,
        removeButton: true,
        checkBoxCellWidth: "20px",
        checkBoxButton: true,
        checkBoxNumber: true,
        autoWidth: false,
        fixable: false,
        rowLayout: 'list', //grid
        htmlLayout: 'div', //div/table
        rowDragable: false,
        rowDropable: false,
        rowDragDropGroup: '',
        rowSelectable: true,
        multiSelect: false,
        sortDataType: 'string',
        outSideHeader: false,
        groupDragable: false,
        groupDropable: false,
        groupDataIndex: null, //only for Div layer
        fillBlankRows: Util.defCfg.pageSize
    };
    $.extend(this.cfg, oConfig);
    this.cb = {
        onHeaderDblClick: null,
        beforeRowRemove: null,
        onCellEdit: null,
        onRowDoubleClick: null,
        onRowContextmenu: null,
        onRowRemove: null,
        onRowDragStart: null,  //e, proxy, row;
        onRowRearrange: null,  //function(Row, insertBeforeRow){}
        onRowSelected: null,  //function(e, Row){}
        onGroupDragStart: null, //function(proxy, group){}
        beforeGroupDrop: null, //function(proxy, group, toGroup){},
        onCellSort: null,  //function(cellKey, order){}
        onCellRearrange: null,  //function(cell, insertBeforeCell){}
        afterCellResize: null
    };
    $.extend(this.cb, callback);
    this._freezdTitileCb = function() { };
    this.dataGroup = null;
    this.rowNumStart = 0;
    this.HTML = '<div class="rtq-grid"><div class="rtq-grid-sz"><div class="rtq-grid-hd"></div><div class="rtq-grid-scroll"><div class="rtq-grid-bd"></div></div></div></div>';
    this.init();
};
Util.DataGrid.prototype = {
    init: function () {
        var self = this;
        this.el = $(this.HTML);
        this.body = this.el.find('.rtq-grid-bd');
        this.el.appendTo(this.container);
        HOYI.touch.touch2DblClick(this.el);
        if (this.cfg.groupDataIndex) {
            this.cfg.htmlLayout = 'div';
            this.dataGroup = new Util.dataGroup(this.body, {
                dragable: this.cfg.groupDragable,
                dropable: this.cfg.groupDropable,
                group: 'dataGridGroup'
            }, this);
        }
        if (this.cfg.fixable && !this.cfg.groupDataIndex) {
            var sze = this.el.find(".rtq-grid-sz");
            this.fxCols = $("<div class='rtq-grid-fxcols' style='display:none; width:2000px'><div class='rtq-grid-hd'></div><div class='rtq-grid-bdz'><div class='rtq-grid-bd'></div></div></div>");
            if (HOYI.isIEx(6)) {
                var tbc = $("<table border='0' cellpadding='0' cellspacing='0'><tr><td></td><td></td></tr></table>");
                sze.appendTo(tbc.find("td:last"));
                this.fxCols.appendTo(tbc.find("td:first"));
                tbc.appendTo(this.el);
                this.fxCols.css({ "float": "none", "position": "relative" });
            }
            else {
                this.fxCols.insertBefore(sze);
            }
            this.fxCols.bdz = this.fxCols.find(".rtq-grid-bdz");
            this.fxCols.bd = this.fxCols.find(".rtq-grid-bd");
            this.fxCols.hd = this.fxCols.find(".rtq-grid-hd");
            sze.css("float", "left");
            this.scBodys = this.el.find(".rtq-grid-bd");
            this.el = sze;
        }
        else
            this.scBodys = this.el.find(".rtq-grid-bd");
        if (this.cfg.autoWidth) {
            this.el.addClass('rtq-grid-auto');
            this.cfg.width = '100%';
            this.cfg.cellResize = false;
            var rowWidth = 0;
        }
        if (this.cfg.checkBoxButton) {
            this.cfg.column.unshift({ header: "checkbox", dataIndex: "checkbox", type: "checkbox", width: this.cfg.checkBoxCellWidth });
            this.cfg.multiSelect = true;
        }
        var fixCW = 0, fixIndex = 0;
        if (this.fxCols) {
            for (var i = 0; i < this.cfg.column.length; i++) {
                var ccfg = this.cfg.column[i];
                if (ccfg.isfixed) {
                    fixIndex = i;
                }
            }
            for (var f = 0; f <= fixIndex; f++) {
                var clg = this.cfg.column[f];
                clg.isfixed = true;
                fixCW += clg.width;
                clg.width = clg.width - (f ? 1 : 2);
            }
            this.fxCols.fixIndex = fixIndex;
        }
        else {
            for (var c = 0; c < this.cfg.column.length; c++) {
                var jCfg = this.cfg.column[c];
                jCfg.isfixed = false;
            }
        }
        this._createHeader();
        if (this.fxCols) {
            this.header._setCellsIndex();
            this.fxCols.fcw = fixCW;
            this.fxCols.css({ width: fixCW, display: "", height: this.cfg.height });
        }
        if (this.cfg.htmlLayout == 'table') {
            this.el.addClass('rtq-grid-tblyr');
            this._createTableBody();
        }
        this.scrollPanel = new Util.ScrollPanel(this.el.find('.rtq-grid-scroll'), {
            showX: !this.cfg.autoWidth
        }, {
            onScroll: self._freezdTitileCb
        });
        this.scrollPanel.forGrid = this;
        this.scrollPanel.onBarShowed = function (x, y) {
            var gd = this.forGrid, cfgmn = null;
            if (!x && y & !this.adjGdc) {
                this.adjGdc = true;
                var cl = gd.cfg.column.length, asz = this.cfg.arrowSize, stix = gd.cfg.checkBoxButton ? 1 : 0;
                if (cl == 0) return;
                var gtw = function (wdth) {
                    return $.type(wdth) == "string" ? parseInt(wdth) : wdth;
                };
                var lw = asz % cl, presz = (asz - lw) / cl;
                gd.cfg.column[stix]._width = gtw(gd.cfg.column[stix].width);
                gd.cfg.column[stix].width = gd.cfg.column[stix]._width - lw;
                for (var c = 0; c < cl; c++) {
                    cfgmn = gd.cfg.column[c];
                    var cfw = gtw(cfgmn.width);
                    if (!cfgmn._width) cfgmn._width = cfw;
                    cfgmn.width = cfw - presz;
                    gd.resizeCell(cfgmn.dataIndex, cfgmn.width);
                }
                this.el[0].scrollTop = 0;
                this.target[0].scrollTop = 0;
                gd._resizeBody();
            }
            else if (!y && this.adjGdc) {
                this.adjGdc = false;
                for (var nc = 0, ncl = gd.cfg.column.length; nc < ncl; nc++) {
                    cfgmn = gd.cfg.column[nc];
                    cfgmn.width = cfgmn._width;
                    gd.resizeCell(cfgmn.dataIndex, cfgmn.width);
                }
                gd._resizeBody();
            }
        };
        if (fixCW > 0 && $.browser.msie) fixCW += 2;
        if (($.browser.msie && parseInt($.browser.version) <= 6)) {
            this.header.el.css("display", "none");
            this.header.el.css("display", "");
            if (this.fxCols) {
                this.el.width(this.cfg.width - fixCW);
                this.el.css({ "float": "none", "overflow": "hidden" });
            }
        };
        this.scrollPanel.cnc = this.el.find(".rtq-grid-scroll .rtq-grid-bd");
        this.resize(this.cfg.width - fixCW, this.cfg.height);
        
        if (this.fxCols) {
            if (this._checkWidthRange())
                this.resize(this.cfg.width - fixCW - 1, this.cfg.height);

            var sct = this.scrollPanel.scrollbarY.scrollTo;
            this.scrollPanel.scrollbarY.scrollTo = function (to) {
                var s = sct.call(this, to);
                self.fxCols.bdz[0].scrollTop = self.scrollPanel.target[0].scrollTop;
                return s;
            }
        }
    },
    _checkWidthRange: function () {
        var ttw = this.fxCols.outerWidth() + this.el.outerWidth();
        var piw = this.el.parent().innerWidth();
        return ttw > piw;
    },
    _createHeader: function () {
        if (this.cfg.headerRow) {
            var self = this;
            if (this.header) {
                this.header.clear();
            }
            this.header = new Util.GridHeader(this, {
                afterResize: function (cellKey, cellWidth) {
                    self.resizeCol.apply(self, arguments);
                },
                afterColSort: function (cellKey, index) {
                    self._appendColTo.apply(self, arguments);
                },
                onCellDblClick: self.cfg.sortable ? function (cellKey) {
                    self.sortByCell.apply(self, arguments);
                } : function (cellKey) {
                    self._trigger.apply(self, ['headerDblClick', [cellKey]]);
                }

            });
            this._freezdTitileCb = function (l, t) {
                self.header.el.scrollLeft(l);
            };
        }
    },
    _createTableBody: function () {
        this.body.append($('<table class="rtq-grid-table"><tbody></tbody></table>'));
    },
    _getBody: function (rowData, prepend) {
        if (this.cfg.htmlLayout == 'table') {
            return this.body.children('table').children('tbody')[0];
        }
        else {
            if (this.cfg.groupDataIndex) {
                var d = rowData[this.cfg.groupDataIndex];
                return this.dataGroup.getGroupBox(d, prepend).body[0];
            }
            else {
                return this.body[0];
            }
        }
    },
    _bindData: function (data) {
        var key = this.cfg.indexColKey || this.cfg.column[0].dataIndex;
        var ds = data; data = [];
        for (var d = 0; d < ds.length; d++)
            if (ds[d]) data.push(ds[d]);
        if (data.length < this.cfg.fillBlankRows) {
            for (var n = data.length; n < this.cfg.fillBlankRows; n++) {
                var fld = { "isGDBlankData": true };
                for (var c = 0, cs = this.cfg.column.length; c < cs; c++) {
                    fld[this.cfg.column[c].dataIndex] = "";
                }
                fld[key] = (new Date()).getTime() + n;
                data.push(fld);
            }
        }
        var i = 0, l = 0;
        if (this.cfg.groupDataIndex) {

            for (i = 0, l = data.length; i < l; i++) {
                this._addRow(this._getBody(data[i]), data[i]);
            }

        }
        else {

            var frag = document.createDocumentFragment(); //For Performance
            for (i = 0, l = data.length; i < l; i++) {
                this._addRow(frag, data[i]);
            }
            this._getBody().appendChild(frag);

        }
        this._setAlternateRow();

        Util.DelayRun('resize', this._resizeBody, 0, this);
        /*
        this._delayRun('resize', function(){
        self._resizeBody();
        //self.body.css('visibility', 'visible');
        }, 0);
        */

    },
    reBindData: function (data) {
        this.empty();
        this._bindData(data);
    },
    _addRow: function (container, data, id, idx) {
        id = id || (data ? data[this.cfg.indexColKey] || data[this.cfg.column[0].dataIndex] : null);
        if (!id) { return; }
        if (this.data) {
            this.data.push(data);
        } else {
            this.data = [data];
        }
        var self = this;
        this.rows[id] = new Util.DataRow(container, this.cfg.column, data, id, function (id) {
            if (self.cfg.removeMsg) {
                if (window.confirm(self.cfg.removeMsg)) {
                    self.removeRow(id);
                    self._trigger('rowremove', [id, self.rows[id]]);
                }
            } else {
                self.removeRow(id);
                self._trigger('rowremove', [id, self.rows[id]]);
            }
        }, this.cfg.htmlLayout == 'table' ? 'tr' : 'div', idx == 0 ? true : false, this.fxCols ? this.fxCols.bd : null);
        this.rows[id].gd = this;
        var isBlank = this.rows[id].isBlank = data.isGDBlankData;
        if (!data.isGDBlankData) {
            if (this.cfg.rowDragable) {
                var drag = new Util.Dragable(this.rows[id].el, {
                    onDragStart: function (e, proxy) {
                        proxy.text(self.rows[id].id);
                        proxy.dataObj = self.rows[id];
                        proxy.gridObj = self;

                        self._trigger('rowdragstart', [e, proxy, self.rows[id]]);
                    },
                    onDrag: function (e, proxy) {
                        //proxy.text(self.rows[id].id);
                    }
                }, {
                    group: this.cfg.rowDragDropGroup
                });
            }
            if (this.cfg.rowDropable) {
                var drop = new Util.Dropable(this.rows[id].el, {
                    onEnter: function (e, proxy) {
                        if (proxy.gridObj && proxy.gridObj == self) {
                            if (self.cfg.groupDataIndex && proxy.dataObj.data[self.cfg.groupDataIndex] != self.rows[id].data[self.cfg.groupDataIndex]) {
                                proxy.removeClass('rtq-dd-drop-yes');
                                return; //dont allow drop in different group
                            }
                            self.rows[id].el.addClass('rtq-grid-row-drop');
                        }
                    },
                    onDrop: function (e, proxy) {
                        if (proxy.gridObj && proxy.gridObj == self) {
                            self.rows[id].el.removeClass('rtq-grid-row-drop');
                            if (self.cfg.groupDataIndex && proxy.dataObj.data[self.cfg.groupDataIndex] != self.rows[id].data[self.cfg.groupDataIndex]) {
                                return; //dont allow drop in different group
                            }
                            if (proxy.dataObj && proxy.dataObj != self.rows[id]) {
                                self._appendRowBrfore(proxy.dataObj, self.rows[id]);

                                self._trigger('rowrearrange', [proxy.dataObj, self.rows[id]]);
                            }
                        }
                    },
                    onOut: function (e, proxy) {
                        if (proxy.gridObj && proxy.gridObj == self) {
                            self.rows[id].el.removeClass('rtq-grid-row-drop');
                        }
                    }
                }, {
                    group: this.cfg.rowDragDropGroup
                });
            }
            if (this.cb.onRowDoubleClick) {
                if ($.browser.msie) {
                    var dd = true;
                    this.rows[id].el.bind("mouseup.DataGrid", function (e) { dd = false; });
                    this.rows[id].el.bind("mousedown.DataGrid", function (e) { dd = true; });
                    this.rows[id].el.bind("selectstart.DataGrid", function (e) { return dd; });
                }
                this.rows[id].el.bind('dblclick.DataGrid', function (e) {
                    self.cb.onRowDoubleClick(e, self.rows[id]);
                });
            }
            if (this.cb.onRowContextmenu) {
                this.rows[id].el.bind('contextmenu.DataGrid', function (e) {
                    self.cb.onRowContextmenu(e, self.rows[id]);
                });
            }
            if (this.cfg.rowSelectable) {
                this.rows[id].el.bind('click.DataGrid', function (e) {
                    self.selectRow(id, e);
                });
            }
            if (this.cfg.checkBoxButton) {
                this.rows[id].cells["checkbox"].checkboxbutton.cb.onClick = function () {
                    self.selectRow(id, arguments.length > 0 ? arguments[1] : null);
                };
            }
        }
        if (isBlank) {
            var keyCell = this.rows[id].cells[this.cfg.indexColKey];
            if (keyCell)
                keyCell.innerDom.innerHTML = "";
        }
        this._clearSort();

        this.DOMMap[id] = this.rows[id].DOMMap;
    },
    addRow: function (data, id, idx) {
        this._addRow(this._getBody(data, idx == 0 ? true : false), data, id, idx);  //idx==0?true:false for group prepend
        /*
        if(idx>-1){
        this._appendRowTo(this.rows[id||data[this.cfg.indexColKey]||data[this.cfg.column[0].dataIndex]], idx);
        }
        */
        this._setAlternateRow(100); //for Performance
        Util.DelayRun('resize', this._resizeBody, 0, this);
    },
    removeRow: function (id) {
        if (this.cb.beforeRowRemove) {
            var flag = this._trigger("beforeRowRemove", [this.rows[id]]);
            if (!flag) {
                return;
            }
        }
        this.rows[id] && this.rows[id].remove();
        if (this.selectedRows[id]) {
            this.selectedRows[id] = null;
            delete this.selectedRows[id];
            this.selectedRowsNo--;
        } else if (this.selectedRow == this.rows[id]) {
            this.selectedRow = null;
        }
        for (var i = 0, l = this.data.length; i < l; i++) {
            if (this.data[i][this.cfg.indexColKey] == id) {
                this.data.splice(i, 1);
                break;
            }
        }
        this.rows[id] = null;
        this.DOMMap[id] = null;
        delete this.rows[id];
        delete this.DOMMap[id];

        this._setAlternateRow(100); //for Performance
        Util.DelayRun('resize', this._resizeBody, 0, this);
    },
    empty: function () {
        //this.body.empty();
        //this._getBody().innerHTML = '';//+80% Pfm
        this.body[0].innerHTML = '';
        this.selectedRows = {};
        this.selectedRow = null;
        this.selectedRowsNo = 0;
        if (this.cfg.htmlhtmlLayout == 'table') {
            this._createTableBody();
        }

        for (var k in this.rows) {
            this.rows[k].clear();
        }
        delete this.rows;
        delete this.DOMMap;
        this.rows = {};
        this.DOMMap = {};
        if (this.dataGroup) {
            this.dataGroup.empty();
        }

        this._setAlternateRow(100); //for Performance
        Util.DelayRun('resize', this._resizeBody, 100, this);
    },

    selectRow: function (rowid, e) {
        e = e || null;
        var row = this.rows[rowid], rwSelClass = "rtq-grid-row-selected";
        if (row && !row.isBlank) {
            var setRowSelectedClass = function (rw, sl) {
                if (sl) {
                    rw.el.addClass(rwSelClass);
                    rw.fxe && rw.fxe.addClass(rwSelClass);
                }
                else {
                    rw.el.removeClass(rwSelClass);
                    rw.fxe && rw.fxe.removeClass(rwSelClass);
                }
            };
            if (this.cfg.multiSelect) {
                var rwcb = null;
                if (!e) {
                    rwcb = row.cells["checkbox"].checkboxbutton;
                }
                var totalcheckbox = null;
                if (this.selectedRows[rowid]) {
                    this.selectedRowsNo--;
                    setRowSelectedClass(row, false);
                    if (this.cfg.checkBoxButton) {
                        totalcheckbox = this.header.row.cells["checkbox"].checkboxbutton;
                        if (totalcheckbox.isSelected) {
                            totalcheckbox.inverse();
                        }
                    }
                    this.selectedRows[rowid] = null;
                    delete this.selectedRows[rowid];
                    if (rwcb && rwcb.isSelected)
                        rwcb.inverse();

                }
                else {
                    this.selectedRowsNo++;
                    setRowSelectedClass(row, true);
                    if (this.cfg.checkBoxButton) {
                        if (this.selectedRowsNo == this.data.length) {
                            totalcheckbox = this.header.row.cells["checkbox"].checkboxbutton;
                            if (!totalcheckbox.isSelected) {
                                totalcheckbox.select();
                            }
                        }
                    }
                    this.selectedRows[rowid] = row;
                    if (rwcb)
                        rwcb.select();
                }
            }
            else {
                if (this.selectedRow) {
                    setRowSelectedClass(this.selectedRow, false);
                    if (this.selectedRow == row) {
                        var editAbleCols = [];
                        var i = 0, l = row.column.length;
                        for (; i < l; i++) {
                            var column = row.column[i];
                            if (column.editAble) {
                                editAbleCols.push(column.dataIndex);
                            }
                        }
                        var et = e ? e.target : null;
                        if (et && et.className == "rtq-grid-cell-ctn") {
                            var cDataIndex = "";
                            for (var prop in row.DOMMap) {
                                if (row.DOMMap[prop] == et) {
                                    cDataIndex = prop;
                                }
                            }
                            var self = this;
                            l = editAbleCols.length;
                            for (i = 0; i < l; i++) {
                                if (cDataIndex == editAbleCols[i]) {
                                    var _et = $(et);
                                    var text = _et.html();
                                    _et.html("");
                                    if (!this.editingInput) {
                                        var str = $.quoteString(text);
                                        text = str.substring(1, str.length - 1);
                                        this.editingInput = $("<input type='text' value='" + text + "' style='width:100%;height:100%;border:none;font-size:11px;font-family: tahoma,arial,verdana,sans-serif,\"Bitstream Vera Sans\";'>").appendTo(_et);
                                        this.editingCell = et;
                                        this.editingInput.focus();
                                        this.editingInput.blur(function () {
                                            var value = this.value;
                                            _et.html(Util.escapeXmlChars(value));
                                            row.data[cDataIndex] = value;
                                            self._trigger('cellEdit', [row.id, row.data, "edit"]);
                                            self.selectedRow = null;
                                            self.editingCell = null;
                                            self.editingInput = null;
                                        });
                                    }
                                }
                            }
                        }
                    }
                    else {
                        et = e ? e.target : null;
                        if (this.editingCell && this.editingCell != et) {
                            this.editingInput.blur();
                        }
                        setRowSelectedClass(row, true);
                        this.selectedRow = row;
                        this._trigger('rowselected', [e, this.selectedRow]);
                    }
                }
                else {
                    setRowSelectedClass(row, true);
                    this.selectedRow = row;
                    this._trigger('rowselected', [e, this.selectedRow]);
                }
            }
        }
    },
    _appendRowTo: function (row, index) {
        if (row.fxz) {
            row.fxz[0].insertBefore(row.fxe[0], row.fxz[0].childNodes[index]);
        }
        this._getBody(row.data).insertBefore(row.el[0], this._getAllRowsDOM().eq(index)[0]);
        this._clearSort();
        this._setAlternateRow();
    },
    _appendRowBrfore: function (row, targetRow) {
        var bd = this._getBody(row.data), el = targetRow.el[0];
        if (row.fxz) {
            for (var c = 0; c < bd.childNodes.length; c++) {
                if (bd.childNodes[c] == el) {
                    this._appendRowTo(row, c);
                }
            }
        }
        else {
            bd.insertBefore(row.el[0], el);
            this._clearSort();
            this._setAlternateRow();
        }
    },
    replaceRow: function (id, data) {
        var row = this.rows[id];
        this.addRow(data, data.id);
        this._appendRowBrfore(this.rows[data.id], row);
        this.removeRow(id);
    },
    _appendColTo: function (cellKey, index) {
        for (var k in this.rows) {
            this.rows[k].appendCellTo(cellKey, index);
        }
        //re-arrange column
        var cols = this.cfg.column;
        var oldIdx = 0;
        for (var i = 0; i < cols.length; i++) {
            if (cellKey == cols[i].dataIndex) {
                oldIdx = i;
                break;
            }
        }
        var c = cols[oldIdx];
        cols.splice(oldIdx, 1);
        cols.splice(oldIdx > index ? index : index - 1, 0, c);

        this._trigger('cellrearrange', [cellKey, index]);
    },
    appendColTo: function (cellKey, index) {
        this.header.appendCellTo(cellKey, index);
        this._appendColTo(cellKey, index);
    },
    resize: function (w, h) {
        if (w > 0 && this.el.width() != w)
            this.el.width(w);
        if (h > 0 && this.el.height() != h)
            this.el.height(h);
        if (w > 0 || h > 0) {
            this._resizeBody(w, h);
        }
    },
    resizeCell: function (cn, w, h) {
        var rws = [this.header.row];
        for (var rid in this.rows)
            rws.push(this.rows[rid]);
        for (var r = 0; r < rws.length; r++) {
            rws[r] && rws[r].cells[cn] && rws[r].cells[cn].resize(w, h);
        }
    },
    reszieFixLayout: function () {
        if (this.fxCols) {
            var fixIndex = this.fxCols.fixIndex, fixCW = 0, ofcw = this.fxCols.fcw || 0;
            for (var f = 0; f <= fixIndex; f++) {
                var clg = this.cfg.column[f];
                fixCW += clg.width + (f ? 2 : 1);
            }
            if (ofcw != fixCW) {
                this.fxCols.fcw = fixCW;
                this.fxCols.css("width", this.fxCols.fcw);
                this.el.width(this.cfg.width - fixCW);
            }
        }
    },
    _resizeBody: function (w, h) {
        this.reszieFixLayout();
        var rowW = this._getRowWidth();
        var sw = w || this.el.width();
        var sh = (h || this.el.height()) - (this.cfg.outSideHeader ? 0 : this.cfg.headerRow ? this.header.el.height() : 0);
        if (!this.cfg.autoWidth) {
            if (rowW > 0 && this.cfg.rowLayout == 'list') {
                this.body.width(rowW);
            }
            else {
                this.body.width(sw);
            }
        }
        if (sw > 0 && sh > 0) {
            this.scrollPanel.reSize(sw, sh);
            if (this.cfg.autoWidth && this.header) { //fix header width in autoWidth mode
                if (this.scrollPanel.scrollbarY.isShow) {
                    this.header.el[0].style.marginRight = '12px';
                } else {
                    this.header.el[0].style.marginRight = '0';
                }
            }
            if (this.fxCols) {
                this.fxCols.bd.height(this.scrollPanel.cnc.height());
                this.fxCols.bdz.height(this.scrollPanel.target.height());
            }
        }

    },
    _getRowWidth: function () {
        var w = 0;
        if (this.cfg.headerRow) {
            w = this.header.row.getRowWidth();
        }
        else {
            for (var k in this.rows) {
                w = this.rows[k].getRowWidth();
                break;
            }
        }
        if (this.fxCols)
            w = w - this.fxCols.fcw;
        return w > 0 ? w : 0;
    },
    _setAlternateRow: function (delay) {
        var self = this;
        delay = delay || -1;
        var fn = function () {
            self._getAllRowsDOM().each(function (i) {
                if (self.cfg.checkBoxNumber) {
                    var row = self.rows[this.getAttribute("rowid")];
                    var chb = row.cells["checkbox"];
                    if (chb) {
                        var checkbox = row.cells["checkbox"].checkboxbutton;
                        checkbox.reSetText(self.rowNumStart + i + 1);
                    }
                }
                if (i % 2 == 0) {
                    $(this).removeClass('rtq-grid-row-alt');
                }
                else {
                    $(this).addClass('rtq-grid-row-alt');
                }
            });
        }
        if (delay < 0) {
            fn();
        } else {
            Util.DelayRun('altRow', fn, delay, this);
        }
        if (this.cfg.checkBoxButton && this.rows) {
            var rowLength = this.getRowLength();
            var checkbox = this.header.row.cells["checkbox"].checkboxbutton;
            if (rowLength === 0) {
                checkbox.inverse()
                checkbox.disable(true);
            } else {
                checkbox.disable(false);
            }
        }
    },
    _getAllRowsDOM: function () {
        return this.body.children();
    },
    getIDs: function () {
        var a = [];
        var self = this;
        this._getAllRowsDOM().each(function (i) {
            a.push(this.getAttribute('rowid'));
        });
        return a;
    },
    getRowLength: function () {
        var i = 0;
        for (var r in this.rows) {
            i++;
        }
        return i;
    },
    resizeCol: function (cellKey, w, h) {
        for (var k in this.rows) {
            this.rows[k].resizeCell(cellKey, w, h);
        }
        this._trigger("afterCellResize", [cellKey, w]);
        this._resizeBody();
    },
    _dataTypeConvert: function (data, type) { //type:string(def), date, time, int, float
        type = type || this.cfg.sortDataType;
        if (data === '' || data === null) {
            return NaN;
        }
        switch (type.toLowerCase()) {
            case 'int':
                {
                    return parseInt(data);
                } break;
            case 'float':
                {
                    return parseFloat(data);
                } break;
            case 'time':
                {
                    return new Date(data + ' ' + (new Date()).toDateString());
                } break;
            case 'date':
                {
                    return new Date(data);
                } break;
            default:
                {
                    return data.toString();
                } break;
        }
    },
    _sortByCell: function (cellKey, order, rowsDOM, container) {
        var self = this;
        //object to array
        var a = [];
        rowsDOM.each(function (i) {  //use DOMMAP ??
            a.push(self.rows[this.getAttribute('rowid')]);
        });
        //sort
        this.lastSortOrder = order;
        this._clearSort();
        if (this.lastSortOrder == 'ASC') {
            a.sort(function (row1, row2) {
                return self._sort(row1, row2, cellKey);  //ASC
            });
            this.header.row.cells[cellKey].el.addClass('rtq-grid-cell-sd');
        } else {
            a.sort(function (row1, row2) {
                return self._sort(row2, row1, cellKey, true); //DESC
            });
            this.header.row.cells[cellKey].el.addClass('rtq-grid-cell-sa');
        }
        this.lastSortCellKey = cellKey;
        //re-append
        var frag = document.createDocumentFragment(); //For Performance
        for (var i = 0; i < a.length; i++) {
            frag.appendChild(a[i].el[0]);
        }
        container.appendChild(frag);
    },
    _sort: function (row1, row2, cellKey, isDESC) {
        var type = row1.cells[cellKey].dataType || this.cfg.sortDataType;
        var v1 = this._dataTypeConvert(row1.cells[cellKey].getValue(), type);
        var v2 = this._dataTypeConvert(row2.cells[cellKey].getValue(), type);
        if (type != 'string') {
            if (isNaN(v1)) {
                return isDESC ? -1 : 1;
            }
            if (isNaN(v2)) {
                return isDESC ? 1 : -1;
            }
        }
        if (v1 > v2) {
            return -1;
        }
        else
            if (v1 == v2) {
                return 0;
            }
            else {
                return 1;
            }
    },
    _clearSort: function () {
        if (this.lastSortCellKey) {
            this.header.row.cells[this.lastSortCellKey].el.removeClass('rtq-grid-cell-sd').removeClass('rtq-grid-cell-sa');
        }
    },
    sortByCell: function (cellKey, order) {
        if (!this.cfg.headerRow) { return; }

        order = order || (this.header.row.cells[cellKey].el.hasClass('rtq-grid-cell-sa') ? 'ASC' : 'DESC');

        if (this.cfg.groupDataIndex) {

            for (var g in this.dataGroup.groups) {
                this._sortByCell(cellKey, order, this.dataGroup.groups[g].body.find('.rtq-grid-row'), this.dataGroup.groups[g].body[0]);
            }
        }
        else {
            this._sortByCell(cellKey, order, this._getAllRowsDOM(), this._getBody());
        }

        this._setAlternateRow();

        this._trigger('cellsort', [this.lastSortCellKey, this.lastSortOrder]);
    },
    setColumnCfg: function (cols) {
        this.empty();
        this.cfg.column = cols.slice();
        this._createHeader();
    },
    getColumnCfg: function () {
        return this.cfg.column;
    },
    setGroup: function (groupDataIndex) {
        this.cfg.groupDataIndex = groupDataIndex;
        this.cfg.htmlLayout = 'div';
        if (!this.dataGroup) {
            this.dataGroup = new Util.dataGroup(this.body, {
                dragable: this.cfg.groupDragable,
                dropable: this.cfg.groupDropable,
                group: 'dataGridGroup'
            }, this);
        }
    },
    //trigger custom event
    _trigger: function (event, args) {
        var cb = null;
        switch (event.toLowerCase()) {
            case 'cellsort':
                cb = this.cb.onCellSort;
                break;
            case 'rowremove':
                cb = this.cb.onRowRemove;
                break;
            case 'rowrearrange':
                cb = this.cb.onRowRearrange;
                break;
            case 'rowdragstart':
                cb = this.cb.onRowDragStart;
                break;
            case 'rowdoubleclick':
                cb = this.cb.onRowDoubleClick;
                break;
            case 'rowcontextmenu':
                cb = this.cb.onRowContextmenu;
                break;
            case 'rowselected':
                cb = this.cb.onRowSelected;
                break;
            case 'cellrearrange':
                cb = this.cb.onCellRearrange;
                break;
            case 'celledit':
                cb = this.cb.onCellEdit;
                break;
            case 'beforerowremove':
                cb = this.cb.beforeRowRemove;
                break;
            case 'aftercellresize':
                cb = this.cb.afterCellResize;
                break;
            case 'headerdblclick':
                cb = this.cb.onHeaderDblClick;
                break;
        }
        if (cb) {
            return cb.apply(this, args);
        }
    },
    bind: function (event, callback) {
        var self = this, r = null;
        switch (event.toLowerCase()) {
            case 'rowdoubleclick':
                {
                    for (r in this.rows) {
                        this.rows[r].el.unbind('dblclick.DataGrid');
                        this.rows[r].el.bind('dblclick.DataGrid', function (e) {
                            callback(e, self.rows[r]);
                        });
                    }
                    this.cb.onRowDoubleClick = callback;
                } break;
            case 'rowcontextmenu':
                {
                    for (r in this.rows) {
                        this.rows[r].el.unbind('contextmenu.DataGrid');
                        this.rows[r].el.bind('contextmenu.DataGrid', function (e) {
                            callback(e, self.rows[r]);
                        });
                    }
                    this.cb.onRowContextmenu = callback;
                } break;
        }
    },
    setSortByCell: function (cellKey, order) {
        if (!this.cfg.headerRow) { return; }
        order = order || (this.header.row.cells[cellKey].el.hasClass('rtq-grid-cell-sd') ? 'ASC' : 'DESC');
        this.lastSortOrder = order;
        this.lastSortCellKey = cellKey;
    },
    setDGCellKeyAndOrder: function () {
        var cellKey = this.lastSortCellKey;
        if (cellKey) {
            if (this.lastSortOrder == 'ASC') {
                this.header.row.cells[cellKey].el.addClass('rtq-grid-cell-sa');
            } else {
                this.header.row.cells[cellKey].el.addClass('rtq-grid-cell-sd');
            }
        }
    },
    disableRow: function (row, isdisable) {
        if (row.disable === isdisable) return;
        row.disable = isdisable === true ? true : false;
        var rowid = row.id;
        if (row.disable) {
            if (row.el.parent().hasClass("rtq-grid-disablerow")) {
                row.el.unwrap();
                row.blockLayer.remove();
                row.blockLayer = null;
            }
            row.blockLayer = Util.disableBlock(row.cells[this.cfg.column[this.cfg.checkBoxButton ? 1 : 0].dataIndex].dom);
            if (this.selectedRows[rowid]) {
                this.selectRow(rowid);
                row.cells["checkbox"].checkboxbutton.inverse();
            }
            if (this.cfg.checkBoxButton) {
                row.cells["checkbox"].checkboxbutton.disable(true);
            }
        } else {
            if (!row.blockLayer) return;
            row.blockLayer.remove();
            row.blockLayer = null;
            if (this.cfg.checkBoxButton) {
                row.cells["checkbox"].checkboxbutton.disable(false);
            }
        }
    }
};

Util.dataGroup = function(container, configs, datagrid) {
    this.groups = {};
    this.datagrid = datagrid;
    this.cfg = {
        dragable: false,
        dropable: false,
        group: 'dataGridGroup',
        collapsible: true
    };
    $.extend(this.cfg, configs);
    this.container = container;
    this.init();
};
Util.dataGroup.prototype = {
    init: function() {

    },
    inGroup: function(value) {
        return this.groups[value];
    },
    _createGroup: function(value, prepend) {
        var self = this;
        var g = document.createElement('div');
        g.className = 'rtq-grid-group';
        g.innerHTML = '<h3 class="rtq-grid-group-til">' + value + '</h3><div class="rtq-grid-group-bd"></div>';
        this.groups[value] = {};
        if (prepend) {
            this.container[0].insertBefore(g, this.container[0].firstChild);
        } else {
            this.container[0].appendChild(g);
        }
        this.groups[value].value = value;
        this.groups[value].el = $(g);
        this.groups[value].header = this.groups[value].el.children('.rtq-grid-group-til');
        this.groups[value].body = this.groups[value].el.children('.rtq-grid-group-bd');

        //event
        this.groups[value].el.hover(function(e) {
            $(this).addClass('rtq-grid-group-hover');
        }, function(e) {
            $(this).removeClass('rtq-grid-group-hover');
        });

        if (this.cfg.dragable) {
            var drag = new Util.Dragable(this.groups[value].header, {
                onDragStart: function(e, proxy) {
                    proxy.text(value);
                    proxy.groupObj = self;
                    proxy.dragGroup = self.groups[value];
                    self.groups[value].el.addClass('rtq-grid-group-drag');
                    if (self.datagrid.cb.onGroupDragStart) {
                        self.datagrid.cb.onGroupDragStart(proxy, self.groups[value]);
                    }
                },
                onDragEnd: function(e, proxy) {
                    proxy.dragGroup = null;
                    if (self.groups[value]) {
                        self.groups[value].el.removeClass('rtq-grid-group-drag');
                    }
                }
            }, {
                group: this.cfg.group
            });
        }

        if (this.cfg.dropable) {
            var drop = new Util.Dropable(this.groups[value].el, {
                onEnter: function(e, proxy) {
                    self.groups[value].el.addClass('rtq-grid-group-drop');
                },
                onDrop: function(e, proxy) {
                    if (self.datagrid.cb.beforeGroupDrop) {
                        self.datagrid.cb.beforeGroupDrop(proxy, proxy.dragGroup, self.groups[value]);
                    }
                    if (proxy.groupObj && proxy.groupObj == self && !proxy._stopEvent) {
                        self.insertBefore(proxy.dragGroup, self.groups[value]);
                    }
                    self.groups[value].el.removeClass('rtq-grid-group-drop');
                },
                onOut: function(e, proxy) {
                    self.groups[value].el.removeClass('rtq-grid-group-drop');
                }
            }, {
                group: this.cfg.group
            });
        }

        if (this.cfg.collapsible) {
            var a = document.createElement('a');
            a.className = 'rtq-grid-group-tool-clp';
            a.innerHTML = 'collapsible';
            $(a).appendTo(this.groups[value].header).click(function() {
                self.groups[value].el.toggleClass('rtq-grid-group-clp');
                return false;
            });
        }

        return this.groups[value];
    },
    insertBefore: function(group, beforeGroup) {
        this.container[0].insertBefore(group.el[0], beforeGroup.el[0]);
    },
    getGroupBox: function(value, prepend) {
        value = typeof value == 'undefined' || value.length < 1 ? '(none)' : value;
        if (this.groups[value]) {
            return this.groups[value];
        } else {
            return this._createGroup(value, prepend);
        }
    },
    getIDs: function(groupKey) {
        var a = [];
        var self = this;
        if (this.groups[groupKey]) {
            this.groups[groupKey].el.find('.rtq-grid-row').each(function(i) {
                a.push(this.getAttribute('rowid'));
            });
        }
        return a;
    },
    removeGroup: function(groupKey) {
        var ids = this.getIDs(groupKey);
        for (var i = 0; i < ids.length; i++) {
            this.datagrid.removeRow(ids[i]);
        }
        this.groups[groupKey].el.remove();
        this.groups[groupKey] = null;
        delete this.groups[groupKey];
    },
    length: function() {
        var i = 0;
        for (var g in this.groups) {
            i++
        }
        return i;
    },
    empty: function() {
        this.groups = {};
    }
};


Util.GridHeader = function(grid, callbacks) {
    this.grid = grid;
    this.el = null;
    this.row = null;
    this.resizeProxy = null;
    this.colSortProxy = null;
    this.colInsertTip = null;
    this.sorting = -1;
    this.sortTimer = null;
    this.appendToCell_el = null;
    this.insertToIdx = 0;
    this.cb = callbacks || {}; //afterResize, afterColSort, onCellClick
    this.columnCfg = grid.cfg.column;
    this.cfg = {
        cellResize: grid.cfg.cellResize,
        cellRearrange: grid.cfg.cellRearrange
    }
    this.htmlTag = this.grid.cfg.htmlLayout == 'table' ? 'tr' : 'div';
    this.init();
}
Util.GridHeader.prototype = {
    init: function() {
        var self = this;
        this.el = this.grid.el.find('.rtq-grid-hd');
        var data = {}, cols = [];
        //create header row column
        for (var i = 0, l = this.columnCfg.length; i < l; i++) {
            data[this.columnCfg[i].dataIndex] = this.columnCfg[i].header;
            var c = {};
            for (var g in this.columnCfg[i]) {
                c[g] = this.columnCfg[i][g];
                if (g == 'renderer') {
                    c[g] = null;
                }
            }
            cols.push(c);
        }
        if (this.grid.cfg.htmlLayout == 'table') {
            var table = $('<table class="rtq-grid-table"><tbody></tbody></table>');
            this.row = new Util.DataRow(table.children('tbody')[0], cols, data, null, null, this.htmlTag, false, this.grid.fxCols ? this.grid.fxCols.hd : null);
            //table.style.width = this.row.getRowWidth() + 'px';
            this.el.append(table);
        } else {
            this.row = new Util.DataRow(this.el, cols, data, null, null, this.htmlTag, false, this.grid.fxCols ? this.grid.fxCols.hd : null);
        }
        this.row.el.addClass('rtq-grid-rzrow');
        this.row.fxe && this.row.fxe.addClass('rtq-grid-rzrow');
        this.row.gd = this.grid;
        for (var cl in this.row.cells) {
            var cell = self.row.cells[cl];
            cell.el[0].cellKey = cell.key;
        }
        //Cell Resize
        if (this.cfg.cellResize) {
            this.resizeProxy = $('<div class="rtq-grid-rzproxy">').appendTo(this.grid.el);
        }
        //Cell Rearrange
        if (this.cfg.cellRearrange) {
            this.colSortProxy = $('<div class="rtq-grid-csproxy">').appendTo(this.grid.el);
            this.colInsertTip = $('<div class="rtq-grid-citip">').appendTo(this.grid.el);
        }

        //event
        for (var k in this.row.cells) {
            (function(i) {
                var cell = self.row.cells[i];
                //Cell Resize
                if (self.cfg.cellResize) {
                    cell.resizeTg = $('<div class="rtq-grid-rztg">').html('&nbsp;').appendTo(cell.el.children('div'));
                }

                cell.el.mousedown(function(e) {
                    var target = $(e.target);
                    if (target.hasClass('rtq-grid-rztg')) {
                        //Cell resize
                        self.onResizeDragStart(e, cell);
                    } else {
                        //Cell Rearrange
                        if (self.cfg.cellRearrange) {
                            e.preventDefault(); // jQuery bug in FF/IE ??
                            self.sortTimer = setTimeout(function() {
                                self.onColSortDragStart(e, cell);
                            }, 300);
                        }
                    }
                }).mouseup(function(e) {
                    clearTimeout(self.sortTimer);
                    self.sortTimer = -1;
                }).mousemove(function(e) {
                    self.onColumnMousemove(e, cell);
                }).mouseover(function(e) {
                    $(this).addClass('rtq-grid-cell-over');
                }).mouseout(function(e) {
                    $(this).removeClass('rtq-grid-cell-over');
                }).click(function(e) {
                    if (self.cb.onCellClick && i != "checkbox") {
                        self.cb.onCellClick(cell.key);
                    } else if (i == "checkbox") {
                        if (e.target.tagName.toLowerCase() == "a") {
                            var checkbox = cell.checkboxbutton, row = null, checkboxbutton = null;
                            if (checkbox.isSelected) {
                                for (var id in self.grid.rows) {
                                    row = self.grid.rows[id];
                                    checkboxbutton = row.cells["checkbox"].checkboxbutton;
                                    if (!checkboxbutton.isSelected && !row.disable) {
                                        checkboxbutton.select();
                                        self.grid.selectRow(id, checkboxbutton);
                                    }
                                }
                            } else {
                                for (var sid in self.grid.selectedRows) {
                                    row = self.grid.rows[sid];
                                    checkboxbutton = row.cells["checkbox"].checkboxbutton;
                                    if (!row.disable) {
                                        checkboxbutton.inverse();
                                        self.grid.selectRow(sid, checkboxbutton);
                                    }
                                }
                            }
                        }
                    }
                }).dblclick(function(e) {
                    if (self.cb.onCellDblClick) {
                        self.cb.onCellDblClick(cell.key);
                    }
                });
            })(k);
        }
    },
    _getDomIndex: function(el) {
        return el.parent().children().index(el);
    },
    onResizeDragStart: function(e, cellObj) {
        e.preventDefault();
        var self = this;
        cellObj.left = cellObj.el.offset().left;
        this.resizeProxy.css({
            left: cellObj.left - this.el.offset().left, // + this.el.scrollLeft(),
            width: cellObj.el.width()
        }).show();

        $(document).bind('mousemove.DataGrid', function(e) {
            self.onResizeDraging(e, cellObj);
        }).bind('mouseup.DataGrid', function(e) {
            self.onResizeEnd(e, cellObj);
        });
    },
    onResizeDraging: function(e, cellObj) {
        e.preventDefault();
        var w = e.clientX - cellObj.left;
        this.resizeProxy.css({
            width: w > this.grid.cfg.colMinWidth ? w : this.grid.cfg.colMinWidth
        });
        return false;
    },
    onResizeEnd: function(e, cellObj) {
        $(document).unbind('mouseup.DataGrid').unbind('mousemove.DataGrid');
        this.resizeProxy.hide();
        var w = e.clientX - cellObj.left, isfxcol = false, gdcfg = this.row.gd.cfg, ccfg, othfxcw = 0;
        w = w > this.grid.cfg.colMinWidth ? w : this.grid.cfg.colMinWidth;
        for (var i = 0, l = this.columnCfg.length; i < l; i++) {
            if (this.columnCfg[i].dataIndex === cellObj.key) {
                ccfg = this.columnCfg[i];
                isfxcol = gdcfg.fixable && ccfg.isfixed;
                break;
            }
            else if (this.columnCfg[i].isfixed) {
                if (this.columnCfg[i].width)
                    othfxcw += this.columnCfg[i].width;
            }
        }
        if (isfxcol && w > (gdcfg.width - othfxcw - 20)) {
            return;
        }
        if (ccfg.width)
            ccfg.width = w;
        cellObj.resize(w);
        if (this.cb.afterResize) {
            this.cb.afterResize(cellObj.key, w);
        }
        return false;
    },
    onColSortDragStart: function(e, cellObj) {
        //e.preventDefault(); // jQuery bug in FF/IE ??
        var self = this;
        cellObj.el.addClass('onsort');
        cellObj.left = cellObj.el.offset().left;
        cellObj.dragX = this.el.offset().left - 20; // - this.el.scrollLeft();
        this.colSortProxy.css({
            left: e.clientX - cellObj.dragX,
            width: cellObj.el.width()
        }).html(cellObj.getValue()).show();

        $(document).bind('mousemove.DataGrid', function(e) {
            self.onColSortDraging(e, cellObj);
        }).bind('mouseup.DataGrid', function(e) {
            self.onColSortEnd(e, cellObj);
        });

        this._setCellsIndex();
        this.sorting = cellObj.el[0].idx;
        this.insertToIdx = cellObj.el[0].idx;
    },
    onColSortDraging: function(e, cellObj) {
        e.preventDefault();
        this.colSortProxy.css({
            left: e.clientX - cellObj.dragX
        });
        return false;
    },
    onColSortEnd: function(e, cellObj) {
        $(document).unbind('mouseup.DataGrid').unbind('mousemove.DataGrid');
        this.colSortProxy.hide();
        this.colInsertTip.hide();
        cellObj.el.removeClass('onsort');
        this.sorting = -1;

        if (this.insertToIdx > -1 && this.insertToIdx != cellObj.el[0].idx) {
            this.appendCellTo(cellObj.key, this.insertToIdx);

            if (this.cb.afterColSort) {
                this.cb.afterColSort(cellObj.key, this.insertToIdx);
            }
        }
        return false;
    },
    appendCellTo: function(cellKey, index) {
        this.row.appendCellTo(cellKey, index);
        this._setCellsIndex();
    },
    _setCellsIndex: function() {
        var s = 0;
        this.row.fxe && this.row.fxe.find('.rtq-grid-cell').each(function(i) {
            this.idx = i;
            s = i;
        });
        this.el.find('.rtq-grid-cell').each(function(i) {
            this.idx = i + s;
        });
    },
    onColumnMousemove: function(e, cellObj) {
        e.preventDefault();
        if (this.sorting > -1) {

            if (this.sorting == cellObj.el[0].idx) {
                this.insertToIdx = -1;
                this.colInsertTip.hide();
                return;
            }

            var eLeft = e.clientX;
            var offsetLeft = cellObj.el.offset().left;
            var offsetWidth = cellObj.el.width();
            var offsetMiddle = offsetLeft + offsetWidth / 2;

            this.insertToIdx = cellObj.el[0].idx;
            if (this.sorting == this.insertToIdx + 1) { //insert before left el
                this.colInsertTip.css({
                    left: offsetLeft - this.el.offset().left
                }).show();
            }
            else
                if (this.sorting == this.insertToIdx - 1) { //insert after right el
                this.colInsertTip.css({
                    left: offsetLeft - this.el.offset().left + offsetWidth
                }).show();
                this.insertToIdx++;
            }
            else
                if (eLeft > offsetMiddle) {
                this.colInsertTip.css({
                    left: offsetLeft - this.el.offset().left + offsetWidth
                }).show();
                this.insertToIdx++;
            }
            else {
                this.colInsertTip.css({
                    left: offsetLeft - this.el.offset().left
                }).show();
            }
        }
    },
    clear: function() {
        this.el.empty();
        for (var p in this) {
            if (!p || p == 'clear') {
                continue;
            }
            this[p] = null;
            delete this[p];
        }
    }
}
/*data row*/
Util.DataRow = function(container, column, data, rowid, delBtnClick, htmlTag, prepend, fxz) {
    this.dom = null;
    this.el = null;
    this.container = container[0] ? container[0] : container;
    this.column = column;
    this.id = rowid;
    this.index = 0;
    this.data = data;
    this.prepend = prepend || false;
    this.cfg = {};
    this.delBtnClick = delBtnClick || function() { };
    this.cells = {};
    this.DOMMap = {};
    this.htmlTag = htmlTag == 'div' ? 'div' : 'tr';
    this.fxz = fxz;
    this.init();
};
Util.DataRow.prototype = {
    init: function() {
        //var s = (new Date()).getTime();
        var self = this, gd = this.gd;
        this.dom = document.createElement(this.htmlTag);
        this.dom.className = 'rtq-grid-row';
        this.dom.setAttribute('rowid', this.id || '');

        var cell, col, frag = document.createDocumentFragment(), fxFrag = null, fxRow = null; ;
        for (var i = 0, l = this.column.length; i < l; i++) {
            col = this.column[i];
            cell = new Util.DataCell({
                text: this.data[col.dataIndex],
                width: col.width,
                align: col.align,
                hide: col.hide,
                dataType: col.dataType,
                type: col.type,
                renderer: col.renderer,
                className: col.className,
                rowid: this.id,
                additional: col.additional,
                hideCb: this.data.isGDBlankData == true
            }, null, this.htmlTag);
            cell.key = col.dataIndex;
            if (col.isfixed && this.fxz) {
                if (!fxFrag) {
                    fxFrag = document.createDocumentFragment();
                    fxRow = $(this.dom.cloneNode(false));
                }
                fxFrag.appendChild(cell.dom);
            }
            else {
                frag.appendChild(cell.dom);
            }
            this.cells[col.dataIndex] = cell;
            this.DOMMap[col.dataIndex] = cell.innerDom;

            if (col.type == 'remove') {
                cell.onClick = function() {
                    self.delBtnClick(self.id);
                };
            }
            if (i === 0) {
                cell.el.addClass("rtq-grid-row-firstCell");
            }
        }
        if (fxFrag) {
            fxRow.appendTo(this.fxz);
            fxRow[0].appendChild(fxFrag);
            this.fxe = fxRow;
        }
        this.dom.appendChild(frag); // +25% Pfm
        this.el = $(this.dom);
        if (this.prepend) {
            this.container.insertBefore(this.dom, this.container.firstChild);
        } else {
            this.container.appendChild(this.dom);
        }
        if (!this.data.isGDBlankData) {
            (fxRow ? $([fxRow[0], this.dom]) : this.el).mouseover(function(e) {
                self.el.addClass('rtq-grid-row-over');
                self.fxe && self.fxe.addClass('rtq-grid-row-over');
            }).mouseout(function(e) {
                self.el.removeClass('rtq-grid-row-over');
                self.fxe && self.fxe.removeClass('rtq-grid-row-over');
            });
            //console.log('BuildRow Time:' + ((new Date()).getTime()-s) + ' ms');
        }
    },
    remove: function() {
        this.fxe && this.fxe.remove();
        this.el.remove();
        this.clear();
    },
    resizeCell: function(cellKey, w, h) {
        this.cells[cellKey].resize(w, h);
    },
    appendCellTo: function(cellKey, index) {
        var children = [];
        this.fxe && children.add(this.fxe.children());
        children.add(this.el.children());
        if (children.length > index) {
            children.eq(index).before(this.cells[cellKey].el);
        } else {
            this.el.append(this.cells[cellKey].el);
        }
    },
    getRowWidth: function () {
        var w = 0;
        for (var k in this.cells) {
            w += this.cells[k].getWidth();
        }
        if (HOYI.isIEx(8, true)) {
            var cw = this.el.parent().innerWidth();
            return Math.abs(cw - w) < 10 ? cw : (w + this.column.length);
        }
        else
            return w;
    },
    clear: function() {
        for (var k in this.cells) {
            this.cells[k].clear();
        }
        for (var p in this) {
            if (!p || p == 'clear') { continue; }
            this[p] = null;
            delete this[p];
        }
    }
};

/*data cell*/
//Util.DataCell = function(text, width, align, hide, dataType, type, renderer, callbacks, htmlTag, className){
//config: text, width, align, hide, dataType, type, renderer
Util.DataCell = function(config, callbacks, htmlTag) {
    this.dom = null;
    this.innerDom = null;
    this.el = null;
    this.text = config.renderer ? (typeof config.renderer == 'function' ? config.renderer(config.text) : config.renderer.replace(/{d}/g, config.text)) : (config.text || '');
    this.type = config.type || 'normal';
    this.align = config.align;
    this.additional = config.additional;
    this.width = isNaN(config.width) ? config.width : (config.width > -1) ? config.width + 'px' : null;
    this.height = -1;
    this.hide = config.hide || false;
    this.className = config.className || '';
    this.dataType = config.dataType || '';
    this.onClick = callbacks ? callbacks.onClick : function() { };
    this.htmlTag = htmlTag == 'div' ? 'div' : 'td';
    this.rowid = config.rowid;
    this.hideCb = config.hideCb;
    this.init();
};
Util.DataCell.prototype = {
    init: function() {

        //this.HTML = '<div class="rtq-grid-cell" style="'+this.widthStr+this.alignStr+'"><div class="rtq-grid-cell-ctn">' + this.text + '</div></div>';
        // +50% Pfm
        this.dom = document.createElement(this.htmlTag);
        this.dom.className = 'rtq-grid-cell ' + this.className;
        if (this.hide) {
            this.dom.className += ' rtq-grid-cell-hide';
        }
        this.dom.style.cssText = ((this.width) ? 'width:' + this.width + ';' : '') + (this.align ? 'text-align:' + this.align + ';' : '');

        this.innerDom = document.createElement('div');
        this.innerDom.className = 'rtq-grid-cell-ctn';
        this.innerDom.innerHTML = this.text;
        this.dom.appendChild(this.innerDom);
        this.el = $(this.dom);
        //this.innerDom = this.el.children('div')[0]; //-30% Pfm
        if (this.additional && this.additional.showTitle) {
            this.innerDom.setAttribute("title", this.text);
            this.innerDom.className += " ellipsis";
        }
        if (this.type == 'remove') {
            var self = this;
            this.dom.className += ' rtq-grid-cell-del';
            this.el.click(function(e) {
                self.onClick();
            });
        }
        if (this.type == 'checkbox') {
            var innerd = $(this.innerDom);
            innerd.html("");
            var div = $("<div></div>").appendTo(innerd);
            if (this.hideCb) div.css("visibility", "hidden");
            this.checkboxbutton = new Util.SelectButton(div, { txt: '', val: this.rowid }, {
                onClick: function(v) {
                }
            }, { type: 'checkbox' });
            if (!this.rowid) {
                this.checkboxbutton.val = "selectAll";
            }
        }
    },
    resize: function(w, h) {
        this.width = w || this.el.width();
        this.height = h || this.el.height();
        this.el.css({ width: this.width, height: this.height });
    },
    getWidth: function() {
        if (this.hide) {
            return 0;
        }
        else {
            var w = this.el.outerWidth();
            var w1 = this.el.css("width");
            if (w1.indexOf(".") > 0) {
                w = w + 1;
            }
            return w; //1px border
        }
    },
    getValue: function() {
        return this.el.children('div').html();
    },
    remove: function() {
        this.el.remove();
        this.clear();
    },
    clear: function() {
        for (var p in this) {
            if (!p || p == 'clear') { continue; }
            this[p] = null;
            delete this[p];
        }
    }
};

Util.PageLayer = function(element, config) {
    this.cfg = {
        pageSize: Util.defCfg.pageSize,
        totalRow: 0
    };
    this.currentPage = 1;
    this.nextPage = 1;
    $.extend(this.cfg, config);
    this.el = element;
    this.init();
};
Util.PageLayer.prototype = {
    init: function() {
        var self = this;
        this.el.click(function(e) {
            self.changePage(e);
        });
        var isTuchDv = ("createTouch" in document);
        var startEvt = isTuchDv ? "touchstart" : "mousedown";
        var endEvt = isTuchDv ? "touchend" : "mouseup";;
        this.el.bind(startEvt, function (e) {
            if (self.gesTmHd) window.clearTimeout(self.gesTmHd);
            if (e.target.tagName != "DIV") return;
            self.gesObj = { x: 0, y: 0 };
            var es = isTuchDv ? e.originalEvent.touches[0] : e;
            self.gesObj.x = es.pageX;
            self.gesObj.y = es.pageY;
        });
        if (isTuchDv)
        {
            this.el.bind("touchmove", function (e) {
                var tp = e.originalEvent.touches[0];
                self.thGes = { pageX: tp.pageX, pageY: tp.pageY };
            });
        }
        this.el.bind(endEvt, function (e) {
            if (!self.gesObj) return;
            if (isTuchDv && !self.thGes) return;
            var es = self.thGes || e;
            var x = es.pageX, y = es.pageY;
            var ds = isTuchDv ? 20 : 10;
            if (Math.abs(y - self.gesObj.y) < ds && Math.abs(x - self.gesObj.x) > ds) {
                var toPg = self.getCurrentPage() + ((x - self.gesObj.x > 0) ? 1 : -1);
                self.goToPage(toPg);
                var pga = $("a[rel=" + toPg + "]", self.el);
                self.gesTmHd = window.setTimeout(function () {
                    self.gesTmHd = 0;
                    pga.trigger("click");
                }, 200);
            }
            self.gesObj = null;
            self.thGes = null;
        })
    },
    getTotalPage: function() {
        return this.getTotalRow() % this.getPageSize() == 0 ? parseInt(this.getTotalRow() / this.getPageSize()) : parseInt(this.getTotalRow() / this.getPageSize()) + 1;
    },
    getTotalRow: function() {
        return parseInt(this.cfg.totalRow);
    },
    getCurrentPage: function() {
        return this.currentPage;
    },
    getPageSize: function() {
        return parseInt(this.cfg.pageSize);
    },
    getCurrentRowCount: function() {
        if (this.getTotalPage() === 0) {
            return 0;
        } else {
            if (this.getCurrentPage() === this.getTotalPage()) {
                return this.getTotalRow() - (this.getTotalPage() - 1) * this.getPageSize();
            } else {
                return this.getPageSize();
            }
        }
    },
    setCurrentPage: function(pageid) {
        this.currentPage = pageid;
    },
    setTotalRow: function(rowCount) {
        this.cfg.totalRow = rowCount;
    },
    changePage: function(e) {
        var et = e.target;
        if (et.tagName !== "A" || !et.rel) {
            return false;
        }
        switch (et.rel) {
            case "first":
                this.goToFirstPage();
                break;
            case "prev":
                this.goToPrevPage();
                break;
            case "next":
                this.goToNextPage();
                break;
            case "last":
                this.goToLastPage();
                break;
            default:
                this.goToPage(et.rel);
                break;
        }
    },
    goToFirstPage: function() {
        this.currentPage = 1;
    },
    goToPrevPage: function() {
        this.currentPage--;
    },
    goToNextPage: function() {
        this.currentPage++;
    },
    goToLastPage: function() {
        this.currentPage = this.getTotalPage();
    },
    goToPage: function(n) {
        var num = parseInt(n);
        if (num < 1) {
            this.currentPage = 1;
        } else if (num > this.getTotalPage()) {
            this.currentPage = this.getTotalPage();
        } else {
            this.currentPage = num;
        }
    },
    setPageParam: function() {
        var pageid = this.nextPage;
        var pageSize = this.getPageSize();
        if (this.cfg.totalRow && pageid > 1 && (pageid - 1) * pageSize >= this.cfg.totalRow) {
            pageid = pageid - 1;
        }
        if (!pageid) {
            this.setCurrentPage(1);
        } else {
            this.setCurrentPage(pageid);
        }
        var pageParam = "rows=" + pageSize + "&page=" + pageid;
        return pageParam;
    },
    pageChanged: function(e) {
        var et = e.target;
        var self = e.data[0];
        var me = e.data[1];
        var pageLabel = me.labelCon();
        if (et.tagName === "A" && et.rel) {
            var curpg = me.getCurrentPage();
            self.call(me, curpg, curpg);
        }
    },
    setPageLabel: function(d, self, param) {
        var pageConpoment = this;
        pageConpoment.setTotalRow(d.TotalRecords);
        var pageLabel = this.labelCon();
        if (self) {
            pageLabel.unbind("click", this.pageChanged);
            pageLabel.click([self, this], this.pageChanged);
        }
        if (pageConpoment.getCurrentPage() != d.CurrPage)
            pageConpoment.setCurrentPage(d.CurrPage);
        if (!pageLabel.data("hasPageLabel")) {
            var strBuffer = ["<span class='pageLabel_totalRow'>总共发现: <span inid='pageLabel_totalRowText' class='pageLabel_totalRowText'>" + pageConpoment.getTotalRow() + "</span>条</span>"];
            strBuffer[1] = "<span class='pageLabel_selectPage'><span style='display: inline;'>第</span> <input type='text' value='1' inid='currentPage' class='currentPage'/> <span style='display: inline;'>页 / </span>共<span inid='pageLabel_totalPageText' class='pageLabel_totalPageText'>" + pageConpoment.getTotalPage() + "</span>页<a rel='1' href='javascript:' inid='goPage' class='goPage' style='visibility:hidden'>go</a></span> ";
            strBuffer[2] = "<span class='pageLabel_pageNumber'><span inid='pageNumber' class='pageNumber'></span></span>";
            var str = strBuffer.join("");
            $(str).appendTo(pageLabel);
            this.getInElt("goPage").css("display", "none").click(function() {
                var value = pageConpoment.getInElt("currentPage").val();
                this.rel = value;
            });
            this.getInElt("currentPage").keypress(function(e) {
                var code = e.keyCode;
                if (code === 13) {
                    pageConpoment.getInElt("goPage").click();
                    return false;
                }
            });
            pageLabel.data("hasPageLabel", true);
        } else {
            if (pageLabel.data("hidePageLabel")) {
                pageLabel.find("*").show();
                pageLabel.data("hidePageLabel", false);
            }
            pageConpoment.getInElt("currentPage").val(pageConpoment.getCurrentPage());
            pageConpoment.getInElt("goPage").attr("rel", pageConpoment.getCurrentPage());
            pageConpoment.getInElt("pageLabel_totalRowText").html(pageConpoment.getTotalRow());
            pageConpoment.getInElt("pageLabel_totalPageText").html(pageConpoment.getTotalPage());
        }
        /*var setCurrentPageLabelPosition = function(el) {
            var pnw = pageConpoment.getInElt("pageNumber").width();
            var pltrw = $(".pageLabel_totalRow", pageLabel).width();
            var plw = el.width();
            var plspw = $(".pageLabel_selectPage", pageLabel).width();
            var p = (plw - pltrw - pnw - plspw) / 2;
            $(".pageLabel_totalRow", pageLabel).css("margin-right", p + "px");
        };*/
        this.setPageNumber(this.getInElt("pageNumber"), pageConpoment.getCurrentPage(), pageConpoment.getTotalPage());
        //setCurrentPageLabelPosition(pageLabel);
        (d.CurrPage == 1) && HOYI.adjustBottomPos && HOYI.adjustBottomPos(pageLabel);
    },
    setPageNumber: function(el, currentPage, totalPage) {
        var strBuffer = [], i = 0;
        if (currentPage === 1) {
            strBuffer[0] = "<a class='disablePage'>前一页</a>";
        } else {
            strBuffer[0] = "<a rel='prev' href='javascript:'>前一页</a>";
        }
        if (totalPage > 10) {
            if (currentPage < 3) {
                for (i = 1; i < 9; i++) {
                    if (currentPage === i) {
                        strBuffer[strBuffer.length] = "<a class='selectedPage'>" + i + "</a>";
                    } else {
                        strBuffer[strBuffer.length] = "<a href='javascript:' rel='" + i + "'>" + i + "</a>";
                    }
                }
                strBuffer[strBuffer.length] = "...<a href='javascript:' rel='" + totalPage + "'>" + totalPage + "</a>";
            } else if (currentPage > totalPage - 5) {
                strBuffer[strBuffer.length] = "<a href='javascript:' rel='1'>1</a>...";
                for (i = totalPage - 7; i <= totalPage; i++) {
                    if (currentPage === i) {
                        strBuffer[strBuffer.length] = "<a class='selectedPage'>" + i + "</a>";
                    } else {
                        strBuffer[strBuffer.length] = "<a href='javascript:' rel='" + i + "'>" + i + "</a>";
                    }
                }
            } else {
                strBuffer[strBuffer.length] = "<a href='javascript:' rel='1'>1</a>...";
                for (i = currentPage - 1; i < currentPage + 5; i++) {
                    if (currentPage === i) {
                        strBuffer[strBuffer.length] = "<a class='selectedPage'>" + i + "</a>";
                    } else {
                        strBuffer[strBuffer.length] = "<a href='javascript:' rel='" + i + "'>" + i + "</a>";
                    }

                }

                strBuffer[strBuffer.length] = "...<a href='javascript:' rel='" + totalPage + "'>" + totalPage + "</a>";
            }
        } else {
            for (i = 1; i <= totalPage; i++) {
                if (currentPage === i) {
                    strBuffer[strBuffer.length] = "<a class='selectedPage'>" + i + "</a>";
                } else {
                    strBuffer[strBuffer.length] = "<a href='javascript:' rel='" + i + "'>" + i + "</a>";
                }
            }
        }
        if (currentPage === totalPage) {
            strBuffer[strBuffer.length] = "<a class='disablePage'>下一页</a>";
        } else {
            strBuffer[strBuffer.length] = "<a rel='next' href='javascript:'>下一页</a>";
        }
        var str = strBuffer.join("");
        el.html(str);
    },
    clear: function() {
        this.goToPage(1);
        var pgc = this.labelCon();
        pgc.find("*").hide();
        pgc.data("hidePageLabel", true);
    },
    labelCon: function() {
        var pageLabel = $("#pageLabel");
        if (!pageLabel || pageLabel.length == 0)
            pageLabel = this.el;
        return pageLabel;
    },
    getInElt: function(inid) {
        return $("[inid=" + inid + "]", this.el || "#pageLabel");
    }
};
CM.com.ready(function () {
    Util.DataGrid.defaultSetting = {
        width: (HOYI.totalWidth || 1100) - 2,
        height: HOYI.isMobileStyle() ? 495 : 656,
        htmlLayer: 'div',
        rowDropable: false,
        checkBoxButton: true,
        checkBoxNumber: true,
        checkBoxCellWidth: "47px",
        fixable: false,
        sortable: false,
        cellRearrange: false,
        rowSelectable: false,
        indexColKey: "Id",
        pageSize: HOYI.isMobileStyle() ? 10 : 25
    };
    Util.defCfg = Util.DataGrid.defaultSetting;
});