Ext.define('purifier.config.GlobalInfo', {
    singleton : true,

    config : {
        isAdmin:false,
	userName:'',
		isAddUser:true
    },
    constructor : function(config) {
        this.initConfig(config);
    }
});