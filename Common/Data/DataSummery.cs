﻿using System.Data;

namespace HOYI.Common.Data
{
    public class DataSummery
    {
        public IDataObjPersister Persister { get; private set; }

        internal DataSummery(IDataObjPersister persister)
        {
            Persister = persister;
        }

        public object ObjId { get; private set; }
        public object Summery { get; private set; }

        internal DataSummery Read(IDataReader reader, string objIdName, string summeryName)
        {
            ObjId = reader[objIdName];
            Summery = reader[summeryName];
            return this;
        }
    }
}
