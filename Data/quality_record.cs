﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOYI.Common.Data;

namespace HOYI.Data
{
    public class quality_record : BaseDataObj
    {
        public virtual int Id { get; set; }
        public virtual string cityid { get; set; }
        public virtual string cityname { get; set; }
        public virtual int pm25 { get; set; }
        public virtual int pm25_24 { get; set; }
        public virtual int pm10 { get; set; }
        public virtual int pm10_24 { get; set; }
        public virtual int so2 { get; set; }
        public virtual int so2_24 { get; set; }
        public virtual int no2 { get; set; }
        public virtual int no2_24 { get; set; }
        public virtual int o3 { get; set; }
        public virtual int o3_24 { get; set; }
        public virtual double co { get; set; }
        public virtual double co_24 { get; set; }
        public virtual int temp { get; set; }
        public virtual int hum { get; set; }
        public virtual System.DateTime publishtime { get; set; }
        public virtual System.DateTime updatetime { get; set; }
        public virtual bool iscity { get; set; }
        public virtual int aqi { get; set; }
        public virtual string png { get; set; }

    }
}
