﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using org.in2bits.MyXls;
using HOYI.Data;
using HOYI.Common.Data;
using HOYI.Common;
using Newtonsoft.Json.Linq;
using HOYI.Web.Common;

namespace HOYI.Web
{
    /// <summary>
    /// uploadfile 的摘要说明
    /// </summary>
    public class uploadfile : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var isAdmin = User.Current.IsInRole(Role.AdminRole);
            if (!isAdmin) throw new HttpException("401");

            string msg = "OK";
            var ids = new List<long>();
            try
            {
                var files = context.Request.Files;
                if (files.Count == 0) throw new HttpException("没有上传文件。");
                for (var f = 0; f < files.Count;f++ )
                {
                    var file = files[f];
                    if (!string.Equals(".xls", Path.GetExtension(file.FileName)))
                        throw new HttpException("只允许上传XLS文件。");
                    ids.AddRange(HandleFile(file));
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            context.Response.ContentType = "application/json";

            JObject jo = new JObject();
            jo["status"] = msg;
            jo["ids"] = JArray.FromObject(ids);
            context.Response.Write(jo.ToString(Newtonsoft.Json.Formatting.None));
        }

        string ResetMac(string mac)
        {
            string rst = "";
            if (mac.IndexOf('-') < 0)
            {
                int i = 0;
                foreach (char c in mac)
                {
                    i++;
                    rst += c.ToString();
                    if (i < 12 && i % 2 == 0)
                        rst += "-";
                }
            }
            else
                rst = mac;
            return rst.ToLower();
        }

        private List<long> HandleFile(HttpPostedFile file)
        {
            var tmp = SaveFile(file);
            try
            {
                XlsDocument doc = new XlsDocument(tmp);
                var sheet = doc.Workbook.Worksheets[0];
                if (sheet.Rows.Count > 1)
                {
                    int idex = 0;
                    Dictionary<long, Pair<string, string>> snDic = new Dictionary<long, Pair<string, string>>();
                    foreach (Row row in sheet.Rows)
                    {
                        if ((idex++) == 0) continue;
                        if (row != null && row.CellExists(1) && row.CellExists(2))
                        {
                            var v1 = row.GetCell(1);
                            var v2 = row.GetCell(2);
                            var c1v = ResetMac((v1.Value ?? "").ToString());
                            var c2v = (v2.Value ?? "").ToString();
                            snDic[PurifierInformation.CalcPurifierId(c1v)] = new Pair<string, string>(c1v, c2v);
                        }
                    }

                    var infos = PurifierInformation.LoadInfos(snDic.Keys.ToList());
                    infos.ForEach(info => info.SNCode = snDic[info.Id].Second);

                    var infIds = infos.Select(pi => pi.Id).ToList();

                    snDic.Where(kv => !infIds.Contains(kv.Key)).ToList().ForEach(kv =>
                    {
                        var npi = PurifierInformation.Create(kv.Value.First);
                        npi.SNCode = kv.Value.Second;
                        infos.Add(npi);
                    });

                    PersistenceHelper.Default.Save<PurifierInformation>(infos);
                    return infos.Select(i => i.Id).ToList();
                }
            }
            finally
            {
                DeleteFile(tmp);
            }
            return new List<long>();
        }

        private void DeleteFile(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch { }
        }

        private static string SaveFile(HttpPostedFile file)
        {
            var tmp = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".xls");
            file.SaveAs(tmp);
            return tmp;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}