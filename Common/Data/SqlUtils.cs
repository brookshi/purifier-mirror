﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HOYI.Common.Data
{
    static class SqlUtils
    {
        public static byte[] GetValueBytes(object value)
        {
            var type = value.GetType();
            if (type == typeof(int))
                return BitConverter.GetBytes((int)value);
            else if (type == typeof(long))
                return BitConverter.GetBytes((long)value);
            else if (type == typeof(short))
                return BitConverter.GetBytes((short)value);
            else if (type == typeof(double))
                return BitConverter.GetBytes((double)value);
            else if (type == typeof(DateTime))
                return BitConverter.GetBytes(((DateTime)value).ToBinary());
            else if (type == typeof(float))
                return BitConverter.GetBytes((float)value);
            else if (type == typeof(byte))
                return new byte[] { (byte)value };
            else if (type == typeof(bool))
                return BitConverter.GetBytes((bool)value);
            else if (type == typeof(UInt32))
                return BitConverter.GetBytes((UInt32)value);
            else if (type == typeof(UInt16))
                return BitConverter.GetBytes((UInt16)value);
            else if (type == typeof(UInt64))
                return BitConverter.GetBytes((UInt64)value);
            else if (type == typeof(sbyte))
                return BitConverter.GetBytes((sbyte)value);
            else if (type == typeof(Guid))
                return ((Guid)value).ToByteArray();
            else if (typeof(Enum).IsAssignableFrom(type))
                return BitConverter.GetBytes((int)value);
            else
                return new byte[] { 0 };
        }

    }
}
