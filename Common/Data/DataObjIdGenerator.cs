﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace HOYI.Common.Data
{
    public interface IIdGenerator
    {
        object GenerateId(string idName);
    }

    #region ID Generators

    public abstract class IdGeneratorBase : IIdGenerator
    {
        public abstract Type IdType { get; }
        protected abstract object GenerateId(string idName);

        #region IIdGenerator Members

        object IIdGenerator.GenerateId(string idName)
        {
            return GenerateId(idName);
        }

        #endregion
    }

    public abstract class RandomIdGenerator<T> : IdGeneratorBase where T : struct
    {
        RandomNumberGenerator m_Random;

        protected RandomIdGenerator()
        {
            m_Random = RandomNumberGenerator.Create();
        }

        protected virtual bool AllNegative { get { return false; } }
        public override Type IdType { get { return typeof(T); } }

        protected override object GenerateId(string idName)
        {
            const byte unsignMask = (byte)0x7F;
            int sz = Marshal.SizeOf(IdType);
            var bts = new byte[sz];
            m_Random.GetBytes(bts);
            if (!AllNegative)
                bts[sz - 1] = (byte)(bts[sz - 1] & unsignMask);

            return Convert(bts);
        }

        protected abstract T Convert(byte[] bts);
    }

    public sealed class IntRandomGenerator : RandomIdGenerator<int>
    {
        protected override int Convert(byte[] bts)
        {
            return BitConverter.ToInt32(bts, 0);
        }

        public int GenerateId()
        {
            return (int)GenerateId("RandomInt");
        }
    }

    public sealed class LongIdGenerator : RandomIdGenerator<long>
    {
        protected override long Convert(byte[] bts)
        {
            return BitConverter.ToInt64(bts, 0);
        }

        public static readonly LongIdGenerator Instance = new LongIdGenerator();
    }

    sealed class Int16IdGenerator : RandomIdGenerator<Int16>
    {
        protected override Int16 Convert(byte[] bts)
        {
            return BitConverter.ToInt16(bts, 0);
        }
    }

    sealed class GuidIdGenerator : IdGeneratorBase
    {
        public override Type IdType { get { return typeof(Guid); } }

        protected override object GenerateId(string idName)
        {
            while (true)
            {
                Guid id = Guid.NewGuid();
                if (id != Guid.Empty)
                    return id;
            }
        }
    }

    sealed class StringIdGenerator : IdGeneratorBase
    {
        public override Type IdType { get { return typeof(string); } }

        protected override object GenerateId(string idName)
        {
            while (true)
            {
                Guid id = Guid.NewGuid();
                if (id != Guid.Empty)
                    return id.ToString().Replace("-", "").ToUpper();
            }
        }
    }

    #endregion

    public class DataObjIdGenerator
    {
        List<IdGeneratorBase> m_IdGenerators = new List<IdGeneratorBase>();

        DataObjIdGenerator()
        {
            m_IdGenerators.Add(new IntRandomGenerator());
            m_IdGenerators.Add(LongIdGenerator.Instance);
            m_IdGenerators.Add(new Int16IdGenerator());
            m_IdGenerators.Add(new GuidIdGenerator());
            m_IdGenerators.Add(new StringIdGenerator());
        }

        protected virtual object GenerateId(Type dataObjType)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(dataObjType);
            PropertyInfo pmKeyInfo = doi.PrimaryKeyInfo;
            foreach (var gener in m_IdGenerators)
            {
                if (gener.IdType == pmKeyInfo.PropertyType)
                    return ((IIdGenerator)gener).GenerateId(dataObjType.FullName + ".IDGENERATOR");
            }
            throw new NotSupportedException(string.Format("Not Support ID Type '{0}'", pmKeyInfo.PropertyType.FullName));
        }

        public void SetId(BaseDataObj dataObj)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(dataObj.GetType());
            PropertyInfo pmKeyInfo = doi.PrimaryKeyInfo;
            pmKeyInfo.SetValue(dataObj, GenerateId(dataObj.GetType()), null);
        }

        public static readonly DataObjIdGenerator Instance = new DataObjIdGenerator();
    }
}
