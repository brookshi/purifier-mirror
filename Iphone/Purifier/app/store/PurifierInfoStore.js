﻿/*
 * File: app/store/PurifierInfoStore.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.3.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.3.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Purifier.store.PurifierInfoStore', {
    extend: 'Ext.data.Store',
    alias: 'store.purifierinfostore',

    requires: [
        'Purifier.model.PurifierInfoModel',
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json'
    ],

    config: {
        model: 'Purifier.model.PurifierInfoModel',
        storeId: 'PurifierInfoStore',
        proxy: {
            type: 'ajax',
            batchActions: false,
            limitParam: 'rows',
            url: 'http://dat.hoyigroup.com.cn/purifier/services/purifierservice.asmx/GetInfoInPage',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            reader: {
                type: 'json',
                messageProperty: 'd.message',
                rootProperty: 'd.data',
                successProperty: 'd.success',
                totalProperty: 'd.total'
            }
        }
    }
});