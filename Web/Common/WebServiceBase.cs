﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    public class WebServiceBase : System.Web.Services.WebService
    {
        static WebServiceBase()
        {
            PersistenceHelper.InitDefault(new DBSetting("HOYI"));
        }

        protected HOYI.Web.Common.User CurrentUser { get { return HOYI.Web.Common.User.Current; } }
    }
}