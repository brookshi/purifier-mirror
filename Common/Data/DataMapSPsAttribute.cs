﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HOYI.Common.Data
{
    /// <summary>
    /// Data Object Map to stored procedures attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class DataMapSPsAttribute : Attribute
    {
        public string SPForReading { get; set; }
        public string SPForDeleting { get; set; }
        public string SPForWriting { get; set; }
        public string SPForQuerying { get; set; }
        public string VersionField { get; set; }
    }
}
