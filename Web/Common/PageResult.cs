﻿using HOYI.Common;
using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    public class PageResult<T> where T:BaseDataObj
    {
        public int TotalRecords { get; private set; }
        public int CurrPage { get; private set; }
        public List<T> List{get;private set;}

        private UIPager m_Pager;

        public PageResult()
            : this(new UIPager() { Order = "ASC", OrderName = PersistenceHelper.GetKeyDbName(typeof(T)) })
        {
        }

        public PageResult(UIPager pager)
        {
            m_Pager = pager;
            CurrPage = m_Pager.PageIndex;
        }

        public void LoadData(WhereCriterion wc)
        {
            this.TotalRecords = PersistenceHelper.Default.Count<T>(wc);
            List = m_Pager.PageObjs<T>(wc);
            ListSorter sorter = new ListSorter("CreateDate", true);
            sorter.Sort(List);
        }
    }
}