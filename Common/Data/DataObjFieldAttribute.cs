﻿using System;

namespace HOYI.Common.Data
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DataObjFieldAttribute : Attribute
    {
        public bool IgnoreReadDb { get; set; }
        public bool IgnoreWriteDb { get; set; }
        public string MapFieldName { get; set; }

        public IConvertible Converter { get; set; }
    }
}
