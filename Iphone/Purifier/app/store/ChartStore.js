﻿/*
 * File: app/store/ChartStore.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.3.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.3.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Purifier.store.ChartStore', {
    extend: 'Ext.data.Store',
    alias: 'store.chartstore',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.Field'
    ],

    config: {
        storeId: 'ChartStore',
        proxy: {
            type: 'ajax',
            batchActions: false,
            enablePagingParams: false,
            url: 'http://dat.hoyigroup.com.cn/purifier/services/Mobile.asmx/GetChartDataByPurifier',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            reader: {
                type: 'json'
            }
        },
        fields: [
            {
                name: 'date'
            }
        ]
    }
});