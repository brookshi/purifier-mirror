﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Common.Data.Oracle
{
    class SQLGenerator : SQLGeneratorBase
    {
        public SQLGenerator(Type objType)
            : base(objType)
        { }

        public override string GeneratorCheckSQL()
        {
            return FormatSqlServerSQL(base.GeneratorCheckSQL());
        }

        private string FormatSqlServerSQL(string sql)
        {
            return sql.Replace("(SELECTNOLOCK)", "").Replace("[", @"""").Replace("]", @"""").Replace("@", this.ParamPrefix);
        }

        public override string GeneratorCountSQL(WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorCountSQL(critical));
        }

        public override string GeneratorQuerySQL(WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorQuerySQL(critical));
        }

        public override string GeneratorSelectSQL()
        {
            return FormatSqlServerSQL(base.GeneratorSelectSQL());
        }

        public override string GeneratorSelectSQL(string fieldName, WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorSelectSQL(fieldName, critical));
        }

        public override string GeneratorTopSQL(int count, string orderName, OrderDirection direction, WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorTopSQL(count, orderName, direction, critical));
        }

        public override string GeneratorDeleteSQL()
        {
            return FormatSqlServerSQL(base.GeneratorDeleteSQL());
        }

        public override string GeneratorDeleteSQL(WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorDeleteSQL(critical));
        }

        public override string GeneratorForceUpdateSQL(List<string> updateFields, WhereCriterion critical)
        {
            return FormatSqlServerSQL(base.GeneratorForceUpdateSQL(updateFields, critical));
        }

        public override string GeneratorInsertSQL()
        {
            return FormatSqlServerSQL(base.GeneratorInsertSQL());
        }

        public override string GeneratorUpdateSQL(List<string> updateFields)
        {
            return FormatSqlServerSQL(base.GeneratorUpdateSQL(updateFields));
        }
        protected override string ParamPrefix
        {
            get { return ":"; }
        }
    }
}
