# ext-theme-classic-5d780aef-2000-407c-b4a9-996cc381fc9c/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-classic-5d780aef-2000-407c-b4a9-996cc381fc9c/sass/etc
    ext-theme-classic-5d780aef-2000-407c-b4a9-996cc381fc9c/sass/src
    ext-theme-classic-5d780aef-2000-407c-b4a9-996cc381fc9c/sass/var
