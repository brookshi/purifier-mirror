﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static Form1()
        {
            ASCII = Encoding.UTF8;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txt_send.Text = "5ACCC0A8016309{0}090602E803{1}0{2}19500B1D03007D";
            timer.Interval = 1000;
            timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            Parallel.For(10, 40, i =>
            {
                SendMessage(i);
            });
        }
        private static readonly Encoding ASCII;
        private const int PORT = 2012;
        // #0#11#00_00_00_00_00_00#10.10.10.10#11530#500#2.52#20.8#2013-12-11 21:51:00#0#
        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        public void SendMessage(int id)
        {
            Byte[] sendBytes = HexStringToByteArray(string.Format(txt_send.Text, id, RandomStatic.ProduceIntRandom(10, 25), RandomStatic.ProduceIntRandom(1,9)));
            Byte[] recvBytes = new Byte[256];
            IPAddress localAddr = IPAddress.Parse("127.0.0.1");
            IPEndPoint ephost = new IPEndPoint(localAddr, PORT);
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                s.Connect(ephost);
                s.Send(sendBytes, sendBytes.Length, SocketFlags.None);
                Int32 bytes = s.Receive(recvBytes, recvBytes.Length, SocketFlags.None);

                StringBuilder buff = new StringBuilder();
                if (bytes > 0)
                {
                    String str = ASCII.GetString(recvBytes, 0, bytes);
                    buff.Append(str);
                }

                rtb_recv.Text = buff.ToString();

            }
            catch (Exception ex)
            {
                Console.WriteLine("连接/发送/接收过程中，发生了错误！");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                s.Close();
            }
        }
        Timer timer = new Timer();
        private void btn_send_Click(object sender, EventArgs e)
        {
            timer.Start();
        }

        private void stop_Click(object sender, EventArgs e)
        {
            timer.Stop();
        }
    }
    public static class RandomStatic
    {
        //产生[0,1)的随机小数 
        public static double ProduceDblRandom()
        {
            Random r = new Random(Guid.NewGuid().GetHashCode());//使用Guid的哈希值做种子 
            return r.NextDouble();
        }
        //产生制定范围内的随机整数 
        public static int ProduceIntRandom(int minValue, int maxValue)
        {
            Random r = new Random(Guid.NewGuid().GetHashCode());
            return r.Next(minValue, maxValue + 1);
        }
    } 
}
