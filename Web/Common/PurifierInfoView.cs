﻿using HOYI.Common.Data;
using HOYI.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Web.Common
{
    [Serializable]
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "[PurifierInfoView]")]
    public class PurifierInfoView : PurifierStatus
    {
        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public string PfMAC
        {
            get { return MAC; }
            set { MAC = value; }
        }

        public string Name
        {
            get;
            set;
        }
        
        public virtual string SNCode
        {
            get;
            set;
        }

        public Guid CreatedBy
        {
            get;
            protected set;
        }

        public virtual string City
        {
            get;
            set;
        }

        public virtual string Addr
        {
            get;
            set;
        }

        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public string TimerString
        {
            get
            {
                return string.Format("{0} 小时 {1} 分钟", this.TimerHour, this.TimerMin);
            }
        }

        public PurifierInfoView()
        { }

        public static List<PurifierInfoView> LoadAll()
        {
            WhereCriterion wc = new WhereCriterion();
            return PersistenceHelper.Default.Query<PurifierInfoView>(wc);
        }

        public static List<PurifierInfoView> LoadAllNeedFilter()
        {
            WhereCriterion wc = new WhereCriterion("IsFliter", OperatorFlag.Equal, 1);
            return PersistenceHelper.Default.Query<PurifierInfoView>(wc);
        }

        public static List<PurifierInfoView> LoadUserPurifiers(Guid userId)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            var pfs = PersistenceHelper.Default.Query<PurifierInfoView>(wc);
            return pfs;
        }

        public static List<PurifierInfoView> LoadUserPurifiersNeedFilter(Guid userId)
        {
            SelectSentence subWC = new SelectSentence("PurifierId", typeof(UserPurifier), new WhereCriterion("UserId", OperatorFlag.Equal, userId));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            wc.And("IsFliter", OperatorFlag.Equal, 1);
            var pfs = PersistenceHelper.Default.Query<PurifierInfoView>(wc);
            return pfs;
        }

        public static PurifierInfoView GetInfoView(long id)
        {
            return PersistenceHelper.Default.Read<PurifierInfoView>(id);
        }
    }
}
