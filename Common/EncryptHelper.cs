﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace HOYI.Common
{
    public static class EncryptHelper
    {

        readonly static DESCryptoServiceProvider DESProvider;
        static EncryptHelper()
        {
            Guid seed = new Guid("5838053d-d472-4419-8d7e-9cd26b5cb7ab");
            DESProvider = (DESCryptoServiceProvider)DESCryptoServiceProvider.Create();
            DESProvider.Key = seed.ToByteArray().Take(8).ToArray();
            DESProvider.IV = seed.ToByteArray().Take(8).ToArray();
        }

        public static byte[] EncryptText(string text)
        {
            if (text == null || text.Length == 0) return null;
            var bytes = Encoding.UTF8.GetBytes(text);
            return DESProvider.CreateEncryptor().TransformFinalBlock(bytes, 0, bytes.Length);
        }

        public static string DecryptText(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0) return null;
            var result = DESProvider.CreateDecryptor().TransformFinalBlock(bytes, 0, bytes.Length);
            return new string(Encoding.UTF8.GetChars(result));
        }

    }
}
