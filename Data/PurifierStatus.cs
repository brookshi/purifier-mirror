﻿using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Common.Data.Indexers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{
    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "[PurifierStatus]")]
    public abstract class PurifierStatus : PurifierBase
    {
        protected PurifierStatus()
        {
        }

        public long Id
        {
            get;
            protected set;
        }

        protected override void SetPurifierId(long purifierId)
        {
            this.Id = purifierId;
        }

        public virtual string IP
        {
            get;
            set;
        }

        public virtual int Tear//档位
        {
            get;
            set;
        }

        public virtual int OnOffKey//开关
        {
            get;
            set;
        }

        public virtual int LockKey//童锁
        {
            get;
            set;
        }

        public virtual int SleepKey//睡眠
        {
            get;
            set;
        }

        public virtual int AutoKey//自动键
        {
            get;
            set;
        }

        public virtual int AnionKey//负离子键
        {
            get;
            set;
        }

        public virtual int UVKey//UV键
        {
            get;
            set;
        }

        public virtual int IsFliter//是否需要更换过滤网
        {
            get;
            set;
        }

        protected override void OnBeforeSave()
        {
            //HasChanged = HasPropertyChanged;//仅仅是用户软件改的需要设置为true,socket也会更改，但不做设置
            base.OnBeforeSave();
        }

        public virtual bool HasChanged { get; set; }//软件上对status的改动后需要将HasChanged设为true

        public virtual int TimerHour { get; set; }//定时时间--小时

        public virtual int TimerMin { get; set; }//定时时间--分钟

        public virtual int StatusFlag { get; set; }//状态是否修改标识，修改后加1，硬件那边用

        public virtual int TimeFlag { get; set; }//时间是否修改标识，修改后加1，硬件那边用

        public void Save()
        {
            PersistenceHelper.Default.Save(this);
        }

        public bool NeedUpdate(int tear, int onOffKey, int lockKey, int autoKey,
            int sleepKey, int anionKey, int uvKey, int isFliter, int timerHour, int timerMin)
        {
            return this.Tear != tear || this.OnOffKey != onOffKey || this.LockKey != lockKey ||
                this.AutoKey != autoKey || this.SleepKey != sleepKey || this.AnionKey != anionKey ||
                this.UVKey != uvKey || this.IsFliter != isFliter || this.TimerHour != timerHour || this.TimerMin != timerMin;
        }

        public bool NeedChangeStatusFlag(int tear, int onOffKey, int lockKey, int autoKey,
            int sleepKey, int anionKey, int uvKey, int isFliter)
        {
            return this.Tear != tear || this.OnOffKey != onOffKey || this.LockKey != lockKey ||
                this.AutoKey != autoKey || this.SleepKey != sleepKey || this.AnionKey != anionKey ||
                this.UVKey != uvKey || this.IsFliter != isFliter;
        }

        public bool NeedChangeTimeFlag(int Hour)
        {
            return this.TimerHour != Hour;
        }
        public void UpdatePropertyAndSave(int tear, int onOffKey, int lockKey, int autoKey,
            int sleepKey, int anionKey, int uvKey, int isFliter, int timerHour, int timerMin)//socket
        {
            this.Tear = tear;
            this.OnOffKey = onOffKey;
            this.LockKey = lockKey;
            this.AutoKey = autoKey;
            this.SleepKey = sleepKey;
            this.AnionKey = anionKey;
            this.UVKey = uvKey;
            this.IsFliter = isFliter;
            this.TimerHour = timerHour;
            this.TimerMin = timerMin;
            this.Save();
        }

        public static PurifierStatus Create(string macAddr, string ip, int tear, int onOffKey, int lockKey,
            int autoKey, int sleepKey, int anionKey, int uvKey, int isFliter, int timerHour, int timerMin)//socket 
        {
            var info = BaseDataObj.CreateDataObj<PurifierStatus>();
            info.IP = ip;
            info.MAC = macAddr;
            info.Tear = tear;
            info.OnOffKey = onOffKey;
            info.LockKey = lockKey;
            info.AutoKey = autoKey;
            info.SleepKey = sleepKey;
            info.AnionKey = anionKey;
            info.UVKey = uvKey;
            info.IsFliter = isFliter;
            info.TimerHour = timerHour;
            info.TimerMin = timerMin;
            info.StatusFlag = 0;
            info.TimeFlag = 0;
            return info;
        }

        public static bool IsExist(string mac)
        {
            var id = CalcPurifierId(mac);
            WhereCriterion wc = new WhereCriterion("ID", OperatorFlag.Equal, id);
            return PersistenceHelper.Default.Count<PurifierStatus>(wc) > 0;
        }

        public static PurifierStatus GetByMAC(string mac)
        {
            return GetByID(CalcPurifierId(mac));
        }

        public static PurifierStatus GetByID(long id)
        {
            return PersistenceHelper.Default.Read<PurifierStatus>(id);
        }

        public static PurifierStatus Create(DataIndexer di)
        {
            return PurifierStatus.CreateDataObj<PurifierStatus>(di);
        }

        public static void UpdateStatus(long Id, string macAddr, int tear, int onOffKey, int lockKey,
            int autoKey, int sleepKey, int anionKey, int uvKey, int timerHour, int timerMin)
        {
            PurifierStatus status = GetByID(Id);
            bool needChangeStatusFlag = status.NeedChangeStatusFlag(tear, onOffKey, lockKey, autoKey,
            sleepKey, anionKey, uvKey, status.IsFliter);
            bool needChangeTimeFlag = status.NeedChangeTimeFlag(timerHour);
            if (needChangeStatusFlag)
                status.StatusFlag = status.StatusFlag < 255 ? status.StatusFlag+1 : 0;
            if (needChangeTimeFlag)
                status.TimeFlag = status.TimeFlag < 255 ? status.TimeFlag+1 : 0;
            if (needChangeStatusFlag || needChangeTimeFlag)
            {
                status.HasChanged = true;
                status.UpdatePropertyAndSave(tear, onOffKey, lockKey, autoKey,
                sleepKey, anionKey, uvKey, status.IsFliter, timerHour, timerMin);
            }
        }
    }
}