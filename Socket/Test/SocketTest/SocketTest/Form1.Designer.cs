﻿namespace SocketTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_send = new System.Windows.Forms.Button();
            this.txt_send = new System.Windows.Forms.TextBox();
            this.rtb_recv = new System.Windows.Forms.RichTextBox();
            this.stop = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(227, 96);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(75, 23);
            this.btn_send.TabIndex = 0;
            this.btn_send.Text = "start";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // txt_send
            // 
            this.txt_send.Location = new System.Drawing.Point(12, 12);
            this.txt_send.Name = "txt_send";
            this.txt_send.Size = new System.Drawing.Size(304, 20);
            this.txt_send.TabIndex = 1;
            // 
            // rtb_recv
            // 
            this.rtb_recv.Enabled = false;
            this.rtb_recv.Location = new System.Drawing.Point(12, 38);
            this.rtb_recv.Name = "rtb_recv";
            this.rtb_recv.ReadOnly = true;
            this.rtb_recv.Size = new System.Drawing.Size(304, 47);
            this.rtb_recv.TabIndex = 3;
            this.rtb_recv.Text = "";
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(53, 96);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 23);
            this.stop.TabIndex = 0;
            this.stop.Text = "stop";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 131);
            this.Controls.Add(this.rtb_recv);
            this.Controls.Add(this.txt_send);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.btn_send);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.TextBox txt_send;
        private System.Windows.Forms.RichTextBox rtb_recv;
        private System.Windows.Forms.Button stop;
    }
}

