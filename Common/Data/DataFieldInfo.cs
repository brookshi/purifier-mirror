﻿using System;
using System.Reflection;
using HOYI.Common.Data.Collections;

namespace HOYI.Common.Data
{
    public class DataFieldInfo
    {
        protected string MapFieldName { get; private set; }

        public string FieldName
        {
            get
            {
                if (string.IsNullOrEmpty(MapFieldName))
                    return FieldInfo.Name;
                else
                    return MapFieldName;
            }
        }

        public PropertyInfo FieldInfo { get; private set; }
        public bool CanWriteDb { get; private set; }
        public bool CanReadDb { get; private set; }
        public Type FieldType { get; private set; }
        public bool IsPrimaryKey { get; internal set; }

        DataFieldInfo(PropertyInfo fieldInfo, bool canWriteDb, bool canReadDb, Type fieldType)
        {
            FieldInfo = fieldInfo;
            CanWriteDb = canWriteDb;
            CanReadDb = canReadDb;
            FieldType = fieldType;
        }

        internal static DataFieldInfoList GetDataFieldInfo(Type dataObjType)
        {
            var infos = new DataFieldInfoList();
            PropertyInfo[] pinfos = dataObjType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var pinfo in pinfos)
            {
                object[] dfas = pinfo.GetCustomAttributes(typeof(DataObjFieldAttribute), true);
                if (dfas.Length > 0)
                {
                    var dfa = (DataObjFieldAttribute)dfas[0];
                    infos.Add(new DataFieldInfo(pinfo, !dfa.IgnoreWriteDb && pinfo.CanRead, !dfa.IgnoreReadDb && pinfo.CanWrite, pinfo.PropertyType) { MapFieldName = dfa.MapFieldName });
                }
                else
                {
                    infos.Add(new DataFieldInfo(pinfo, pinfo.CanRead, pinfo.CanWrite, pinfo.PropertyType));
                }
            }
            return infos;
        }
    }
}
