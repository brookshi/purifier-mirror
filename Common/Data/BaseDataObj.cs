﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Reflection.Emit;
using HOYI.Common.Data.Indexers;

namespace HOYI.Common.Data
{
    public interface IDataObj
    {
        bool IsPropertyChanged(string ppName);
    }

    public abstract class BaseDataObj : IDataObj
    {
        List<string> changedProperties = new List<string>();

        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public bool EnableTraceChanging { get; set; }
        
        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        protected internal bool IsExisted
        {
            get;
            internal set;
        }

        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public bool HasPropertyChanged
        {
            get { return EnableTraceChanging && changedProperties.Count > 0; }
        }

        protected BaseDataObj()
        {
            EnableTraceChanging = true;
            IsExisted = false;
        }

        protected void BindNotifyMethod(string ppName, object value)
        {
            const BindingFlags bdFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            string ppAlias = string.Empty;
            PropertyInfo ppi = base.GetType().BaseType.GetProperty(ppName, bdFlags);
            object[] dfas = ppi.GetCustomAttributes(typeof(DataObjFieldAttribute), true);
            if (dfas.Length > 0)
                ppAlias = (dfas[0] as DataObjFieldAttribute).MapFieldName;

            MethodInfo getm = ppi.GetGetMethod();
            if (getm == null) return;
            object oldv = getm.Invoke(this, null);
            if (Equals(oldv, value) == false)
            {
                OnPropertyChanged(string.IsNullOrEmpty(ppAlias) ? ppName : ppAlias, value, oldv);
                var setmb = base.GetType().GetMethod(string.Format("Emit_Set{0}Base", ppName), bdFlags);
                setmb.Invoke(this, new object[] { value });
            }
        }

        protected virtual void OnPropertyChanged(string ppn, object newValue, object oldValue)
        {
            if (EnableTraceChanging == false)
                return;
            if (changedProperties.Contains(ppn))
                return;
            else
                changedProperties.Add(ppn);
        }

        internal protected virtual void OnBeforeSave()
        {

        }

        public void ForceUpdateProperty(string ppName)
        {
            if (changedProperties.Contains(ppName))
                return;
            else
                changedProperties.Add(ppName);
        }

        public bool IsPropertyChanged(string ppName)
        {
            if (EnableTraceChanging)
                return changedProperties.Contains(ppName);
            else
                return true;
        }

        internal void ClearChangeds()
        {
            changedProperties.Clear();
        }

        public new Type GetType()
        {
            var myType = base.GetType();
            if (myType.Namespace.StartsWith(myType.BaseType.Namespace) && myType.Namespace.EndsWith(".Emit"))
                myType = myType.BaseType;
            return myType;
        }

        static ModuleBuilder dataObjModuleBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName("DataObjects_Assembly"), AssemblyBuilderAccess.Run).DefineDynamicModule("DataObjectsModule");
        static Dictionary<Type, Type> emitDataTypes = new Dictionary<Type, Type>();
        static object[] emptyObjs = new object[] { };
        static IIdGenerator idg = new LongIdGenerator();

        static Type GetDataObjPototype<T>() where T : BaseDataObj
        {
            lock (dataObjModuleBuilder)
            {
                if (!emitDataTypes.ContainsKey(typeof(T)))
                {
                    string typeName = string.Format("{0}.Emit.{1}{2}", typeof(T).Namespace, typeof(T).Name, typeof(T).IsGenericType ? idg.GenerateId("DOBJTYPE").ToString() : "");
                    TypeBuilder dotBuilder = dataObjModuleBuilder.DefineType(typeName, TypeAttributes.Public | TypeAttributes.Sealed, typeof(T));
                    PropertyInfo[] pps = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    foreach (var ppi in pps)
                    {
                        MethodInfo setMth = ppi.GetSetMethod() ?? ppi.GetSetMethod(true);
                        if (setMth == null || !setMth.IsVirtual)
                            continue;
                        var baseSetMth = dotBuilder.DefineMethod(string.Format("Emit_Set{0}Base", ppi.Name), MethodAttributes.Private | MethodAttributes.NewSlot | MethodAttributes.Virtual | MethodAttributes.HideBySig, null, new Type[] { ppi.PropertyType });
                        var baseSetIL = baseSetMth.GetILGenerator();
                        baseSetIL.Emit(OpCodes.Ldarg_0);
                        baseSetIL.Emit(OpCodes.Ldarg_1);
                        baseSetIL.Emit(OpCodes.Call, setMth);
                        baseSetIL.Emit(OpCodes.Ret);

                        var ovSetMth = dotBuilder.DefineMethod(string.Format("Set_{0}", ppi.Name), MethodAttributes.Private | MethodAttributes.NewSlot | MethodAttributes.Virtual | MethodAttributes.HideBySig | MethodAttributes.Final, null, new Type[] { ppi.PropertyType });
                        var ovSetIL = ovSetMth.GetILGenerator();
                        ovSetIL.Emit(OpCodes.Ldarg_0);
                        ovSetIL.Emit(OpCodes.Ldstr, ppi.Name);
                        ovSetIL.Emit(OpCodes.Ldarg_1);


                        if (ppi.PropertyType.IsSubclassOf(typeof(ValueType)))
                            ovSetIL.Emit(OpCodes.Box, ppi.PropertyType);

                        ovSetIL.Emit(OpCodes.Call, typeof(T).GetMethod("BindNotifyMethod", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod));
                        ovSetIL.Emit(OpCodes.Ret);
                        dotBuilder.DefineMethodOverride(ovSetMth, setMth);
                    }
                    emitDataTypes[typeof(T)] = dotBuilder.CreateType();
                }
            }

            return emitDataTypes[typeof(T)];
        }

        public static T CreateDataObj<T>() where T : BaseDataObj
        {
            var objType = GetDataObjPototype<T>();
            var constructor = objType.GetConstructor(Type.EmptyTypes);
            if (constructor == null)
                throw new InvalidOperationException("No default constructor.");
            return (T)constructor.Invoke(emptyObjs);
        }

        static Type[] DataIndexerTypeArray = new Type[] { typeof(DataIndexer) };

        internal static T CreateObjFromPersistence<T>(DbDataReaderIndexer dbIndexer) where T : BaseDataObj
        {
            var obj = CreateDataObj<T>(dbIndexer);
            if (obj != null) obj.IsExisted = true;
            return obj;
        }

        internal static T CreateObjFromPersistence<T>(IDataReader reader) where T : BaseDataObj
        {
            return CreateObjFromPersistence<T>(new DbDataReaderIndexer(reader));
        }

        public static T CreateDataObj<T>(IDictionary dic) where T : BaseDataObj
        {
            return CreateDataObj<T>(DataIndexer.GetIndexer(dic));
        }

        public static T CreateDataObj<T>(DataIndexer indexer) where T : BaseDataObj
        {
            if (indexer == null)
                throw new ArgumentNullException("indexer");
            var objType = GetDataObjPototype<T>();
            T obj = null;
            var constructor = objType.GetConstructor(DataIndexerTypeArray);
            if (constructor != null)
            {
                obj = (T)constructor.Invoke(new object[] { indexer });
            }
            else
            {
                constructor = objType.GetConstructor(Type.EmptyTypes);
                obj = (T)constructor.Invoke(null);
                var info = SetFields<T>(indexer, obj);
                if (info.PrimaryKeyInfo.CanWrite && indexer.Contains(info.PrimaryKeyInfo.Name))
                {
                    var kv = indexer[info.PrimaryKeyInfo.Name];
                    SetProperty(obj, info.PrimaryKeyInfo, kv);
                }
            }
            if (obj == null)
            {
                throw new InvalidOperationException("Can't find necessary constructor of '" + typeof(T).Name + "'.");
            }
            obj.ClearChangeds();
            return obj;
        }

        public static DataObjInfo SetFields<T>(DataIndexer indexer, T obj) where T : BaseDataObj
        {
            var info = DataObjInfo.GetDataObjInfo(typeof(T));
            foreach (var fi in info.Fileds)
            {
                SetField(indexer, obj, fi);
            }
            return info;
        }

        internal static bool SetField(DataIndexer indexer, object obj, DataFieldInfo fi) 
        {
            if (fi.CanReadDb && indexer.Contains(fi.FieldName))
            {
                var v = indexer[fi.FieldName];
                SetProperty(obj, fi.FieldInfo, v);
                return true;
            }
            return false;
        }

        static void SetProperty(object obj, PropertyInfo pro, object v)
        {
            if (v == null)
                pro.SetValue(obj, null, null);
            else
            {
                if (pro.PropertyType.IsAssignableFrom(v.GetType()))
                    pro.SetValue(obj, v, null);
                else
                {
                    var cv = Parser.GetTypeConverter(pro.PropertyType);
                    if (cv != null)
                        pro.SetValue(obj, cv.ConvertFrom(v), null);
                    else
                        pro.SetValue(obj, v, null);
                }
            }
        }

        public static T Clone<T>(T prototype) where T : BaseDataObj
        {
            return CreateDataObj<T>(DataIndexer.GetIndexer(prototype));
        }
    }
}
