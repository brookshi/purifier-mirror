﻿using HOYI.Common;
using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HOYI.Web.Common
{
    public class UIPager
    {
        private HttpContext m_Context;
        public UIPager()
        {
            m_Context = HttpContext.Current;
        }

        public int PageSize
        {
            get
            {
                var sz = Parser.Parse(m_Context.Request["rows"], -1);
                if (sz < 0)
                    return int.MaxValue;
                else
                    return sz;
            }
        }


        public int PageIndex
        {
            get
            {
                return Math.Max(1, Parser.Parse(m_Context.Request["page"], 0));
            }
        }

        public string Order
        {
            get;
            set;
        }

        public string OrderName
        {
            get;
            set;
        }

        public OrderDirection OrderDir {
            get { return Tools.CompareStr(Order, "ASC") ? OrderDirection.ASC : OrderDirection.DESC; }
        }

        public List<T> PageObjs<T>(WhereCriterion @where) where T : BaseDataObj
        {
            return PersistenceHelper.Default.PageObjs<T>(this.PageIndex - 1, this.PageSize, this.OrderName, this.OrderDir, @where);
        }
    }
}