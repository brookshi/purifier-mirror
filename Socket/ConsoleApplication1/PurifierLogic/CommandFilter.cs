﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Metadata;

namespace PurifierLogic
{
    public class CommandFilter : CommandFilterAttribute
    {
        public override void OnCommandExecuting(CommandExecutingContext commandContext)
        {
            if (commandContext.RequestInfo.Key != "0" || commandContext.RequestInfo.Key != "1")
                commandContext.Cancel = true;
        }

        public override void OnCommandExecuted(CommandExecutingContext commandContext)
        {
        }
    }
}
