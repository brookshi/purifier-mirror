﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HOYI.Common.Data;
using HOYI.Data;

namespace PurifierLogic
{
    public class DataHandler
    {
        private volatile static DataHandler _instance = null;
        private static readonly object _lockObj = new object();
        private static readonly ConcurrentDictionary<string, PurifierRecord> _newestDict = new ConcurrentDictionary<string, PurifierRecord>();
        private static readonly ConcurrentDictionary<string, PurifierRecord> _avgDict = new ConcurrentDictionary<string, PurifierRecord>();
        
        private DataHandler() { }

        public static DataHandler GetInstance()
        {
            if (_instance == null)
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                        _instance = new DataHandler();
                }
            }
            return _instance;
        }

        public void ReceiveData(string macAddr, string ip, int tear, int onOffKey, int lockKey, int autoKey,
            int sleepKey, int anionKey, int uvKey, int isFliter, double pm25, double temperature, 
            double humidity, int timerHour, int timerMin)
        {
            if (pm25 == 0)
                return;
            if (onOffKey == 0)
            {
                var status = PurifierStatus.GetByMAC(macAddr);
                if (status.OnOffKey == 0)
                    return;
            }

            int oldOnOffKey = 0;

            UpdatePurifierStatus(macAddr, ip, tear, onOffKey, lockKey, autoKey, sleepKey, anionKey, uvKey, isFliter, timerHour, timerMin, ref oldOnOffKey);

            UpdatePurifierInformation(macAddr);

            UpdateNewestRecord(macAddr, ip, pm25, temperature, humidity);

            CreateFistRecord(macAddr, ip, pm25, temperature, humidity, oldOnOffKey, onOffKey);

            UpdateAvgRecord(macAddr, ip, pm25, temperature, humidity);
        }

        protected void UpdatePurifierStatus(string macAddr, string ip, int tear, int onOffKey, int lockKey, int autoKey,
            int sleepKey, int anionKey, int uvKey, int isFliter, int timerHour, int timerMin, ref int oldOnOffKey)
        {
            PurifierStatus status = null;
            if (!PurifierStatus.IsExist(macAddr))
            {
                status = PurifierStatus.Create(macAddr, ip, tear, onOffKey,
                    lockKey, autoKey, sleepKey, anionKey, uvKey, isFliter, timerHour, timerMin);
                status.Save();
            }
            else
            {
                status = PurifierStatus.GetByMAC(macAddr);
                oldOnOffKey = status.OnOffKey;
                if (status.HasChanged)
                    return;
                if (status.NeedUpdate(tear, onOffKey, lockKey, autoKey, sleepKey, anionKey, uvKey, isFliter, timerHour, timerMin))
                    status.UpdatePropertyAndSave(tear, onOffKey, lockKey, autoKey, sleepKey, anionKey, uvKey, isFliter, timerHour, timerMin);
            }
        }

        protected void UpdatePurifierInformation(string macAddr)
        {
            if (!PurifierInformation.IsExist(macAddr))
                PurifierInformation.Create(macAddr).Save();
        }

        protected PurifierRecord GetNewestRecord(string macAddr)
        {
            return PurifierRecord.GetNewestRecord(macAddr);
        }

        protected void UpdateNewestRecord(string macAddr, string ip, double pm25, double temperature, double humidity)
        {
            PurifierRecord record = null;
            if (_newestDict.ContainsKey(macAddr))
                record = _newestDict[macAddr];
            else
                record = GetNewestRecord(macAddr);
            if (record == null)
            {
                record = PurifierRecord.Create(macAddr, ip, pm25, temperature, humidity, RecordFlag.Newest);
                record.Save();
            }
            else// if(record.NeedUpdate(pm25, temperature, humidity))
            {
                PurifierRecord.ApplyProperty(record, pm25, temperature, humidity);
                record.Save();
            }
            _newestDict[macAddr] = record;
        }

        protected void CreateFistRecord(string macAddr, string ip, double pm25, double temperature, double humidity, int oldOnOffKey, int onOffKey)
        {
            if (oldOnOffKey == 0 && onOffKey == 1)
                PurifierRecord.Create(macAddr, ip, pm25, temperature, humidity, RecordFlag.First).Save();
        }

        protected void UpdateAvgRecord(string macAddr, string ip, double pm25, double temperature, double humidity)
        {
            if (_avgDict.ContainsKey(macAddr) && !IsDateEqual(_avgDict[macAddr].CreateDate, DateTime.Now))
                _avgDict.Clear();
            PurifierRecord record = null;
            if (_avgDict.ContainsKey(macAddr))
                record = _avgDict[macAddr];
            else
            {
                record = GetAvgRecord(macAddr);
                if (record!=null && !IsDateEqual(record.CreateDate, DateTime.Now))
                    record = null;
            }
            if (record == null)
            {
                record = PurifierRecord.Create(macAddr, ip, pm25, temperature, humidity, RecordFlag.Avg);
                record.Save();
            }
            else// if (record.NeedUpdate(pm25, temperature, humidity))
            {
                CalcAvgRecord(record, pm25, temperature, humidity);
                record.Save();
            }
            _avgDict[macAddr] = record;
        }

        protected bool IsDateEqual(DateTime firstDate, DateTime secDate)
        {
            return firstDate.Year == secDate.Year && firstDate.Month == secDate.Month &&
                firstDate.Day == secDate.Day && firstDate.Hour == secDate.Hour;
        }

        public PurifierRecord GetAvgRecord(string macAddr)
        {
            return PurifierRecord.GetAvgRecord(macAddr);
        }

        public void CalcAvgRecord(PurifierRecord record, double pm25, double temperature, double humidity)
        {
            record.Pm25 = (pm25 + record.Pm25) / 2;
            record.Temperature = (temperature + record.Temperature) / 2;
            record.Humidity = (humidity + record.Humidity) / 2;
            record.CreateDate = DateTime.Now;
        }


        public PurifierStatus GetStatus(string mac)
        {
            PurifierStatus status = PurifierStatus.GetByMAC(mac);
            if (status == null)
                return null;
            if (status.HasChanged)
            {
                status.HasChanged = false;
                status.Save();
            }
            return status;
        }

        public DataInterval GetDateInverval()
        {
            return DataInterval.GetInfo(1);
        }
    }
}
