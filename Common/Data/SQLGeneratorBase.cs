﻿using System;
using System.Collections.Generic;

namespace HOYI.Common.Data
{
    public abstract class SQLGeneratorBase
    {
        string insertSQL = string.Empty;
        string checkSQL = string.Empty;
        string selectSQL = string.Empty;
        string deleteSQL = string.Empty;
        Type dataObjType = null;
        public DataObjInfo Info { get; private set; }

        public SQLGeneratorBase(Type dataObjType)
        {
            this.dataObjType = dataObjType;
            Info = DataObjInfo.GetDataObjInfo(this.dataObjType);
        }

        public virtual string GeneratorInsertSQL()
        {
            if (!string.IsNullOrEmpty(insertSQL))
            {
                return insertSQL;
            }
            lock (this)
            {
                if (!string.IsNullOrEmpty(insertSQL))
                {
                    return insertSQL;
                }
                DataObjInfo info = Info;
                insertSQL = string.Format("Insert into {0} ({1})VALUES({2})\r\n", info.TableName, string.Join(",", info.GetWriteFields().ToArray()), string.Join(",", CreateSQLParameters(info.GetWriteFields()).ToArray()));
                return insertSQL;
            }
        }

        public virtual string GeneratorUpdateSQL(List<string> updateFields)
        {
            DataObjInfo info = Info;
            List<string> updateParams = CreateSQLParameters(updateFields);
            for (int i = 0; i < updateFields.Count; i++)
            {
                updateParams[i] = updateFields[i] + "=" + updateParams[i];
            }
            return string.Format("UPDATE {0} SET {2} WHERE {1}=@{1}\r\n", info.TableName, info.PrimaryKeyName, string.Join(",", updateParams.ToArray()));
        }

        public virtual string GeneratorForceUpdateSQL(List<string> updateFields, WhereCriterion critical)
        {
            DataObjInfo info = Info;
            List<string> updateParams = CreateSQLParameters(updateFields);
            for (int i = 0; i < updateFields.Count; i++)
            {
                updateParams[i] = updateFields[i] + "=" + updateParams[i];
            }
            return string.Format("UPDATE {0} SET {1} WHERE {2}\r\n", info.TableName, string.Join(",", updateParams.ToArray()), critical.GetSentence());
        }

        public virtual string GeneratorCheckSQL()
        {
            if (!string.IsNullOrEmpty(checkSQL))
            {
                return checkSQL;
            }
            lock (this)
            {
                if (!string.IsNullOrEmpty(checkSQL))
                {
                    return checkSQL;
                }
                DataObjInfo info = Info;
                string keyName = info.GetPrimaryKeyDbName();
                checkSQL = string.Format("SELECT COUNT(*) AS count FROM {0} (SELECTNOLOCK) WHERE {1}=@{2}\r\n", info.TableName, keyName, keyName);
                return checkSQL;
            }
        }

        public virtual string GeneratorSelectSQL()
        {
            if (!string.IsNullOrEmpty(selectSQL))
            {
                return selectSQL;
            }
            lock (this)
            {
                if (!string.IsNullOrEmpty(selectSQL))
                {
                    return selectSQL;
                }
                DataObjInfo info = Info;
                selectSQL = string.Format("SELECT * FROM {0} (SELECTNOLOCK) WHERE {1}=@{1}\r\n", info.TableName, info.PrimaryKeyName);
                return selectSQL;
            }
        }

        public virtual string GeneratorDeleteSQL()
        {
            if (!string.IsNullOrEmpty(deleteSQL))
            {
                return deleteSQL;
            }
            lock (this)
            {
                if (!string.IsNullOrEmpty(deleteSQL))
                {
                    return deleteSQL;
                }
                DataObjInfo info = Info;
                deleteSQL = string.Format("DELETE {0} WHERE {1}=@{1}\r\n", info.TableName, info.PrimaryKeyName);
                return deleteSQL;
            }
        }

        public virtual string GeneratorDeleteSQL(WhereCriterion critical)
        {
            const string deleteTemp = "DELETE {0} WHERE {1}\r\n";
            DataObjInfo info = Info;
            string delSQL = string.Format(deleteTemp, info.TableName, critical.GetSentence());
            return delSQL;
        }

        public virtual string GeneratorQuerySQL(WhereCriterion critical)
        {
            const string querySelectTemp = "SELECT * FROM {0} (SELECTNOLOCK){1}{2}\r\n";
            string querySQL = "";
            DataObjInfo info = Info;
            if (critical == null || critical.IsEmpty)
            {
                querySQL = string.Format(querySelectTemp, info.TableName, "", "");
            }
            else
            {
                querySQL = string.Format(querySelectTemp, info.TableName, " WHERE ", critical.GetSentence());
            }
            return querySQL;
        }

        public virtual string GeneratorSelectSQL(string fieldName, WhereCriterion critical)
        {
            const string selectSelectTemp = "SELECT {0} FROM {1} (SELECTNOLOCK){2}{3}\r\n";
            string subSQL = "";
            DataObjInfo info = Info;
            if (critical == null || critical.IsEmpty)
            {
                subSQL = string.Format(selectSelectTemp, fieldName, info.TableName, "", "");
            }
            else
            {
                subSQL = string.Format(selectSelectTemp, fieldName, info.TableName, " WHERE ", critical.GetSentence());
            }
            return subSQL;
        }

        public virtual string GeneratorCountSQL(WhereCriterion critical)
        {
            const string countSelectTemp = "SELECT Count(*) total FROM {0} (SELECTNOLOCK){1}{2}\r\n";
            string countSQL = "";
            DataObjInfo info = Info;
            if (critical == null || critical.IsEmpty)
            {
                countSQL = string.Format(countSelectTemp, info.TableName, "", "");
            }
            else
            {
                countSQL = string.Format(countSelectTemp, info.TableName, " WHERE ", critical.GetSentence());
            }
            return countSQL;
        }

        public virtual string GeneratorTopSQL(int count, string orderName, OrderDirection direction, WhereCriterion critical)
        {
            if (count <= 0)
                count = 1;
            const string topSelectTemp = "SELECT TOP ({0}) * FROM {1} (SELECTNOLOCK){2}{3}{4}\r\n";
            DataObjInfo info = Info;
            string orderString = (string.IsNullOrEmpty(orderName) ? "" : string.Format(" ORDER BY {0} {1}", orderName, direction.ToString()));
            if (critical == null || critical.IsEmpty)
            {
                return string.Format(topSelectTemp, count, info.TableName, "", "", orderString);
            }
            else
            {
                return string.Format(topSelectTemp, count, info.TableName, " WHERE ", critical.GetSentence(), orderString);
            }
        }

        protected virtual List<string> CreateSQLParameters(List<string> fields)
        {
            var sqlParas = new List<string>();
            foreach (var fd in fields)
                sqlParas.Add(ParamPrefix + fd);
            return sqlParas;
        }

        protected abstract string ParamPrefix { get; }

    }
}
