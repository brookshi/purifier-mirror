USE [Purifier]
GO

/****** Object:  Table [dbo].[PurifierInformation]    Script Date: 12/29/2013 23:13:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PurifierInformation](
	[ID] [bigint] NOT NULL,
	[SNCode] [varchar](20) NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Addr] [nvarchar](200) NULL,
	[City] [nvarchar](20) NULL,
	[Description] [nvarchar](4000) NULL,
	[MAC] [varchar](20) NULL,
	[CreatedBy] [uniqueidentifier] NOT NULL,
	[CreatedTime] [datetime] NULL,
 CONSTRAINT [PK_PURIFIERINFO] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


