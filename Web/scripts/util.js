/*
 * Util
 * David li 090922
 */
(function(c){var a=["DOMMouseScroll","mousewheel"];c.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var d=a.length;d;){this.addEventListener(a[--d],b,false)}}else{this.onmousewheel=b}},teardown:function(){if(this.removeEventListener){for(var d=a.length;d;){this.removeEventListener(a[--d],b,false)}}else{this.onmousewheel=null}}};c.fn.extend({mousewheel:function(d){return d?this.bind("mousewheel",d):this.trigger("mousewheel")},unmousewheel:function(d){return this.unbind("mousewheel",d)}});function b(f){var d=[].slice.call(arguments,1),g=0,e=true;f=c.event.fix(f||window.event);f.type="mousewheel";if(f.wheelDelta){g=f.wheelDelta/120}if(f.detail){g=-f.detail/3}d.unshift(f,g);return c.event.handle.apply(this,d)}})(jQuery);
if (!Util) {
    var Util = {};
}

Util.FixIE = (function(){
    if (document.all) {
        document.execCommand("BackgroundImageCache", false, true);
    }
})();

Util.escapeXmlChars = function (text, noAnd) {
    if (!this.pair)
        this.pair = [{ reg: /&/g, text: "&amp;" }, { reg: /</g, text: "&lt;" }, { reg: />/g, text: "&gt;" }];
    if (text && text.replace) {
        for (var p = (noAnd ? 1 : 0) ; p < this.pair.length; p++) {
            var rpr = this.pair[p];
            text = text.replace(rpr.reg, rpr.text);
        }
    }
    return text || "";
};
/*
 * Util.DelayRun('resize', this._resizeBody, 10, this);
 * */
Util.DelayRun = function(timerKey, fn, delay, object){
    if (object.__DelayRunTimer){
        if (object.__DelayRunTimer[timerKey]) {
            clearTimeout(object.__DelayRunTimer[timerKey]);
            object.__DelayRunTimer[timerKey] = -1;
        }
    }else{
        object.__DelayRunTimer = {};
    }
    object.__DelayRunTimer[timerKey] = setTimeout(function(){
        fn.call(object);
    }, delay);
};

//contains check f contains c
Util.contains = function(f,c){
    if(f==null){return false;}
    var bCB2F = false;
    if(f.contains){bCB2F = f.contains(c);}else{bCB2F = (f.compareDocumentPosition(c)==20)?true:false;}
    return bCB2F;
}

//panel
Util.panel = function(){
    var html = '<div class="rtq-panel">'
             + '<div class="container">'
             + '<div class="hd"><h4>Title</h4><div class="hd-btn"><a class="close">X</a></div></div>'
             + '<div class="ctn"></div>'
             + '</div>'
             + '<div class="shadow"></div>'
             + '</div>';
    this.jqDOM = $(html);
    this.title = null;
    this.ctn = null;
    this._init();
    return this;
};
Util.panel.prototype = {
    _init:function(){
        var self = this;
        this.title = this.jqDOM.find('.hd h4');
        this.ctn = this.jqDOM.find('.ctn');
        this.jqDOM.find('.hd a.close').click(function(){
            self.hide();
        });
        
        this.jqDOM.appendTo(document.body);
    },
    addToContent:function(obj){
        this.ctn.append(obj);
    },
    setTitle:function(str){
        this.title.html(Util.escapeXmlChars(str));
    },
    show:function(){
        this.jqDOM.find('.shadow').css({height:this.jqDOM.height()});
        this.jqDOM.show();
    },
    hide:function(){
        this.jqDOM.hide();
    },
    del:function(){
        this.jqDOM.remove();
    }
};


Util.promptObj = null;
Util.prompt = function(msg, callback){
    if(Util.promptObj){
        Util.promptObj.callback = callback||function(){};
        Util.promptObj.input.val('');
        Util.promptObj.setTitle(msg);
        Util.promptObj.show();
        Util.promptObj.input.focus();
    }else{
        var panel = new Util.panel();
        panel.jqDOM.addClass('rtq-prompt rtq-a-c-p');
        panel.addToContent('<input class="name-ipt" type="text" /><div class="btn-ctn"><div class="act-btn"><a href="javascript:;" class="ok">OK</a><a class="cancel" href="javascript:;">Cancel</a></div></div>');
        panel.setTitle('Confirm');
        panel.callback = function(){};
        panel.input = panel.ctn.find('.name-ipt');
        var btnOK = panel.ctn.find('.ok');
        var btnCancel = panel.ctn.find('.cancel');
        
        var fnSubmit = function(){
            panel.hide();
            panel.callback(panel.input.val());
        };
        
        panel.input.bind('keydown',function(e){
            if(e.keyCode==13){
                fnSubmit();
            }
        });
        
        btnOK.click(fnSubmit);
        
        btnCancel.click(function(e){
            panel.hide();
            return false;
        });
        
        Util.promptObj = panel;
    }
};
Util.prompt();

Util.confirmObj = null;
Util.confirm = function(msg, callback, config){ //config:{title:'Title'}
    if(Util.confirmObj){
        Util.confirmObj.callback = callback||function(){};
        Util.confirmObj.msg.html(Util.escapeXmlChars(msg));
        if(config){
            if (config.title) {
                Util.confirmObj.setTitle(config.title);
            }
        }
        Util.confirmObj.show();
        Util.confirmObj.ctn.find('input').focus();
    }else{
        var panel = new Util.panel();
        panel.jqDOM.addClass('rtq-confirm rtq-a-c-p');
        panel.addToContent('<h5>Message</h5><div class="btn-ctn"><div class="act-btn"><input type="text" value=""/><a href="javascript:;" class="ok">OK</a><a class="cancel" href="javascript:;">Cancel</a></div></div>');
        panel.setTitle('Confirm');
        panel.msg = panel.ctn.find('h5');
        panel.callback = function(){};
        
        var fnSubmit = function(){
            panel.hide();
            panel.callback(true);
        };
        
        panel.ctn.find('.ok').click(fnSubmit);
        panel.ctn.find('.cancel').click(function(e){
            panel.hide();
            panel.callback(false);
        });
        panel.ctn.find('input').bind('keydown',function(e){
            if(e.keyCode==13){
                fnSubmit();
            }
        });

        //panel.appendTo(document.body);
        Util.confirmObj = panel;
    }
};
Util.confirm();

Util.alertObj = null;
Util.alert = function(msg, config){ //config:{title:'Title'}
    if(Util.alertObj){
        Util.alertObj.msg.html(Util.escapeXmlChars(msg));
        if(config){
            if (config.title) {
                Util.alertObj.setTitle(config.title);
            }
        }
        Util.alertObj.show();
    }else{
        var panel = new Util.panel();
        panel.jqDOM.addClass('rtq-alert rtq-a-c-p');
        panel.addToContent('<h5>Message</h5><div class="btn-ctn"><div class="act-btn"><a href="javascript:;" class="ok">OK</a></div></div>');
        panel.setTitle('Alert');
        panel.msg = panel.ctn.find('h5');
        
        var fnSubmit = function(){
            panel.hide();
            return;
        };
        
        panel.ctn.find('.ok').click(fnSubmit);
        
        panel.jqDOM.bind('keydown',function(e){
            if(e.keyCode==13){
                fnSubmit();
            }
        });
        Util.alertObj = panel;
    }
};
Util.alert();

Util.messageObj = null;
Util.message = function(msg){
    if(Util.messageObj){
        Util.messageObj.msg.html(Util.escapeXmlChars(msg));
        Util.messageObj.show();
        Util.messageObj.autoHide();
    }else{
        var panel = new Util.panel();
        panel.jqDOM.addClass('rtq-msg rtq-a-c-p');
        panel.addToContent('<h5>Message</h5>');
        panel.msg = panel.ctn.find('h5');
        var timer = null;
        
        panel.autoHide = function(dalay){
            if(timer){
                clearTimeout(timer);
                timer = -1;
            }
            timer = setTimeout(function(){
                panel.hide();
            }, dalay||3000);
        };

        panel.jqDOM.bind('keydown',function(e){
            if(e.keyCode==13){
                panel.hide();
            }
        });
        Util.messageObj = panel;
    }
};
Util.message();

//DropDownList
Util.DropDownList = function(domEL,onChange,data,oConfig){
    this.dom = domEL[0] || domEL;
    this.mod;
    this.ipt;
    this.menu;
    this.data = data;
    this.cfg = {
        triggerByClick:false,
        animSlide:false,
        width:80,
        disable:false,
        actionAtInit:true,
        syncListWidth:true,
        className: ''
    };
    $.extend(this.cfg, oConfig);
    this.selectedIndex = 0;
    this.onChange = onChange||function(){};
    this.showTimer = -1;
    this.init();
};
Util.DropDownList.CurrentMENU = null;
Util.DropDownList.prototype = {
    init:function(){
        var self = this;
        //dom
        if($(this.dom).hasClass('rtq-ddl')){
            this.mod = $(this.dom);
            this.menu = this.mod.find('.rtq-ddl-menu');
            this.ipt = this.mod.children().eq(0).attr('readonly','readonly');
        }else if(this.data){
            //create DOM by data
            this.mod = $(document.createElement('div')).addClass('rtq-ddl');
            this.menu = $(document.createElement('ul')).addClass('rtq-ddl-menu');
            this.ipt = $('<div class="rtq-ddl-ipt"></div>').attr('readonly','readonly');
            this._bindData(this.data);
            $(this.dom).append(this.mod.append(this.ipt).append(this.menu));
        }
        this.mod.css({width:this.cfg.width}).addClass(this.cfg.className);
        this.ipt.css({width:this.cfg.width-20});
        if(this.cfg.disable){
            this.setDisable(true);
        }
        //event
        if(this.cfg.triggerByClick){
            this.mod.click(function(e){
                if(self.menu.css('display')=='none'){
                    self.showMenu();
                }else{
                    self.hideMenu();
                }
            });
        }else{
            this.mod.mouseover(function(e){
                clearTimeout(self.showTimer);
                self.showTimer = setTimeout(function(){
                    self.showMenu.call(self);
                }, 200);
            }).mouseout(function(e){
                clearTimeout(self.showTimer);
                self.showTimer = setTimeout(function(){
                    self.hideMenu.call(self);
                }, 200);
            });
        }
        //this.mod.hover(function(){self.mod.addClass('rtq-ddl-hover');},function(){self.mod.removeClass('rtq-ddl-hover');});
        
        this.menu.click(function(e){
            if(e.target.tagName.toLowerCase()=='a'){
                self.selectedIndex = self.menu.children('li').index($(e.target).parent());
                self._changeValue($(e.target).html(),$(e.target).attr('value'));
                self.menu.find('li a').removeClass('on');
                $(e.target).addClass('on');
                self.hideMenu();
                return false;
            }
        });
        
        $(document.body).click(function(e){
            if(!Util.contains(self.dom,e.target)&&e.target!=self.dom){
                self.hideMenu();
            }
            /*
            e = window.event||e;
            var target = e.srcElement || e.target;
            console.log(e.srcElement)
            if (!Util.contains(self.dom, target) && target != self.dom) {
                self.hideMenu();
            }
            */
        });
        
        if (this.cfg.actionAtInit) {
            this.selectItemByIndex(this.selectedIndex);
        }else{
            this.setTextValue(null, this.menu.children('li').eq(this.selectedIndex).children('a').attr('value'));
        }
    },
    showMenu:function(){
        if(this.cfg.disable){return;}
        if(Util.DropDownList.CurrentMENU){
            $(Util.DropDownList.CurrentMENU).hide();
        }
        this.cfg.animSlide?this.menu.slideDown('fast'):this.menu.show();
        this.mod.css({zIndex:100});
        this.ipt.css({width:this.mod.width()-20});
        var style = {zIndex:9999,top:this.mod.height(),left:'-1px'};
        if(this.cfg.syncListWidth){
            style.width = this.mod.width();
        }
        this.menu.css(style);
        Util.DropDownList.CurrentMENU = this.menu[0];
    },
    hideMenu:function(){
        this.menu.hide();
        this.mod.css({zIndex:90});
    },
    _changeValue:function(t,v){
        this.ipt.html(t.replace(/&gt;/g,'>').replace(/&lt;/g,'<'));
        this.ipt.attr('val',v);
        this.onChange(t,v);
    },
    _bindData:function(data){
        this.clearList();
        for(var i=0,l=data.length;i<l;i++){
            var val = typeof data[i].val == 'undefined' ? data[i].id :data[i].val;
            this.menu.append('<li><a value="'+val+'" href="#">'+data[i].txt+'</a></li>');
        }
    },
    getValue:function(){
        return this.ipt.attr('val');
    },
    getText:function(){
        return this.ipt.html();
    },
    getSelectedIndex:function(){
        return this.selectedIndex;
    },
    clearList:function(){
        this.menu.html('');
    },
    rebindData:function(data){
        this._bindData(data);
    },
    insertItem:function(txt,val){
        this.menu.append('<li><a value="'+val+'" href="#">'+txt+'</a></li>');
    },
    removeItemByValue:function(val){
        this.menu.find('a[value='+val+']').parent().remove();
    },
    selectItemByValue:function(val){
        this.menu.find('a[value='+val+']').click();
    },
    selectItemByIndex:function(idx){
        this.menu.find('li a').eq(idx).click();
    },
    setTextValue:function(t,v){
        if(t){
            this.ipt.html(t.replace(/&gt;/g,'>').replace(/&lt;/g,'<'));
        }
        if(v){
            this.ipt.attr('val', v);
        }
        var el = this.menu.find('li a[value='+v+']');
        if (el.length>0) {
            this.ipt.html(el.html().replace(/&gt;/g,'>').replace(/&lt;/g,'<'));
            this.selectedIndex = this.menu.children('li').index(el.parent());
            this.menu.find('li a').removeClass('on');
            el.addClass('on');
            this.hideMenu();
        }
    },
    hideItemByValue:function(val){
        this.menu.find('a[value='+val+']').parent().hide();
    },
    showItemByValue:function(val){
        this.menu.find('a[value='+val+']').parent().show();
    },
    setDisable:function(b){
        this.cfg.disable = (typeof b == 'undefined')?true:b;
        if(b){
            this.mod.addClass('rtq-ddl-disable');
        }else{
            this.mod.removeClass('rtq-ddl-disable');
        }
    },
    clearUp:function(){
        this.mod.remove();
        this.dom = this.mod = this.ipt = this.menu = this.data = this.cfg = this.onChange = this.ss = null;
    }
};



// Range Slider Component
Util.Silder = function(container,_option) {
    this.options = {
        min:0,
        max:100,
        values:[0,10],
        disabled: false,
        widthOfHandle:7
    };
    this.onChange = null;
    this.onChanged = null;
    this.element = $(container);
    this.handles;
	this.midHandle;
    this._keySliding = false;
    this._handleIndex = null;
    this._dragStart = false;
    this._lastValPercent;
    this.perHandleWidth = 1;
	this._clickOffset= {x:0,y:0,dx:0}; // drag middle the inital positon of mouse.
    if(_option) {
        $.extend(this.options,_option);
    }
    this._init();
}
Util.Silder.prototype = {
    _init: function() {
        var self = this, o = this.options;
        this.element.addClass("rtq-slider");
        this.elementSize = {
            width: this.element.outerWidth(),
            height: this.element.outerHeight()
        };
        this.element.find('.rtq-slider-handle').remove();
        //this.elementOffset = this.element.offset();
        this.perHandleWidth = 100*this.options.widthOfHandle / this.elementSize.width;
        this.element.append('<div class = "rtq-slider-handle"><a class="rtq-slider-left" href="#"></a></div><div class="rtq-slider-mid"><a></a></div><div class = "rtq-slider-handle"><a class="rtq-slider-right" href="#"></a></div>');
        this.handles = this.element.find(".rtq-slider-handle");
        this.handles.each(function(i) {
            $(this).data("index.slider-handle", i);
            $(this).mousedown(
                function(e) {
                    var elm = e.target.tagName.toLowerCase();
                    if(elm == "a") {   
                        self._handleIndex = $(e.target).parent().data("index.slider-handle");
                        self._startDrag(e);
                    }
                });                                      
        });
		this.midHandle = this.element.find(".rtq-slider-mid");
		this.midHandle.find("a").mousedown(
			function(e){
				self._handleIndex = 2;
				self._clickOffset = self._getMousePostion(e);
		        self._clickOffset.dx=0;
				self._startDrag(e);
			});
        this._refreshValue();
    },
    _startDrag: function(e) {
        e.preventDefault();
        this._dragStart = true;
        $(document).bind('mousemove', this, this._mouseDrag);
        $(document).bind('mouseup', this, this._mouseStop);
    },
    _mouseDrag: function(event) {
        var self = event.data;
        if(!self._dragStart) {
            return false;
        }
        event.preventDefault();
        var position = self._getMousePostion(event);//{x: event.pageX, y: event.pageY };
        var pixelTotal = self.elementSize.width , pixelMouse, o = self.options;
		var valueTotal = o.max - o.min;
        if(self._handleIndex==2){ // drag middle.
			var dx = position.x - self._clickOffset.x - self._clickOffset.dx;
			var dv = valueTotal*dx/pixelTotal;
	        self._clickOffset.dx+=dx;
			self._slide(event, self._handleIndex, dv.toFixed(2));
		}else{
			pixelMouse = position.x - self.element.offset().left;
		    var percentMouse = (pixelMouse / pixelTotal);
		    if (percentMouse > 1) percentMouse = 1;
		    if (percentMouse < 0) percentMouse = 0;
		   	var  valueMouse = percentMouse * valueTotal,
		        normValue = o.min + valueMouse;     
		    self._slide(event, self._handleIndex, normValue.toFixed(2));
		}
        
    },
    _mouseStop: function(event) {
        var self = event.data;
        $(document).unbind("mousemove", self._mouseDrag);
        $(document).unbind("mouseup", self._mouseStop);
        self._dragStart = false;
        self._stop(event, self._handleIndex);
        self._handleIndex = null;
    },
    _slide: function(event, index, val) {
        var newVal = parseFloat(val);
        var o = this.options;
		var flag = false;
        if (o.values && o.values.length > 1) {
			if(index == 2){  // drag middle.
				var dx = newVal;
				if (o.values[0] + newVal < o.min || o.values[1] + newVal > o.max) {
					return;
				}
				o.values[0] = o.values[0]+dx, o.values[1] = o.values[1]+dx;
				flag = dx != 0;
			}else{
				var otherVal = o.values[index == 1 ? 0 : 1];
	            if ((o.values.length == 2) && ((index == 0 && newVal > otherVal) || (index == 1 && newVal < otherVal))){
	                newVal = otherVal;
	            }
				flag = (newVal != o.values[index]);
				if(flag){
					o.values[index] = newVal;
				}
			}
			if (flag) {
                if(this.onchange) {
                    this.onchange(o.values);
                }
                this._refreshValue();
            }   
        } 
    },
    _stop: function(event, index) {                                 
        if(this.onchanged) {
            this.onchanged(this.options.values);
        }
    },
    _refreshValue: function() {
        var o = this.options, self = this;
        if (this.options.values && this.options.values.length) {
			var valPercent = []; 
			for(var i=0;i<2;i++){
				valPercent[i] = (o.values[i] - o.min) / (o.max - o.min) * 100;
                valPercent[i] = valPercent[i] > (100 - self.perHandleWidth)?100 - self.perHandleWidth:valPercent[i]; 
                valPercent[i] = valPercent[i] < self.perHandleWidth ?self.perHandleWidth:valPercent[i];
			}		    
            this.handles.each(function(i, j) {            
                if(i == 0) {
					$(this).css({width: valPercent[0]+'%' });
				} 
                if (i == 1) {
					$(this).css({left: valPercent[1]+'%',width:(100 - valPercent[1])+'%' });
				}		
            });
            this._lastValPercent = valPercent;
			this.midHandle.css({left:valPercent[0]+"%",width:valPercent[1]-valPercent[0]+"%"});
        }
    },
	_getMousePostion:function(e){
		var de = document.documentElement, b = document.body, pos={};
		var sl= de && de.scrollLeft || b.scrollLeft || 0, sr = de && de.scrollTop || b.scrollTop || 0;
        if (e.pageX == null && e.clientX != null) { // IE
            pos.x = e.clientX + sl;
            pos.y = e.clientY + sr;
        }
        else {
            pos.x  = e.pageX+sl;
            pos.y = e.pageY+sr;
        }
		return pos;
	},
    changeOutSize:function(w,h){
        this.elementSize = {
            width: w,
            height: h
        };
        this.perHandleWidth = 100*this.options.widthOfHandle / this.elementSize.width;
        this._handleIndex = null;
    },
    setOption:function(op) {
        $.extend(this.options,op);
        if(op.values) {
           this._refreshValue(); 
        }
    },
    clear: function() {
        this.handles.remove();
        this.element
            .removeClass("rtq-slider")
            .removeData("slider")
            .unbind(".slider");
    }   
};

/*Auto compelte*/
// reg: "USA", lan:"EN-US", scope:8 (stock, ETF,index,open fund), scope:2 (stock, ETF,Index, open fund, close fund,)
Util.createAutoCompleteBox = function(inputId,downListID,reg,lan,scope,isPostionDownList,callbackName,bAlignRight){
    if(typeof(AutoCompleteBox)== "function"){
        if(typeof(ID_AutoCompleteBoxCache) == "undefined"){
            ID_AutoCompleteBoxCache = new Array(SCOPE_SIZE) // SCOPE_SIZE is a member of AutoCompleteBox object.
        }
        if(typeof(ID_AutoCompleteBoxPos) == "undefined"){
            ID_AutoCompleteBoxPos = new Array(SCOPE_SIZE)
        }
        if(typeof(qs_server) == "undefined"){
            qs_server = "http://qt.morningstar.com/switchservice/qtswitch.ashx?symbol=";
        }
        initCache(ID_AutoCompleteBoxCache, ID_AutoCompleteBoxPos);
        var myBox = new AutoCompleteBox(inputId, downListID, qs_server, ID_AutoCompleteBoxCache, ID_AutoCompleteBoxPos, false);
        //myBox.SetPreference(reg,lan,scope);
		//myBox.SetPreference("CAN","en",scope);
		myBox.SetPreference('CUS', 'EN',scope)
        myBox.SubmitCallback = callbackName;
        if(isPostionDownList){
            var pp = $("#"+inputId).offset();
            var ppRelate = $("#"+downListID).offsetParent().offset();
			if(bAlignRight){
				$("#"+downListID).css({ left: 0-$("#"+inputId).outerWidth() + 'px', top: pp.top - ppRelate.top + $("#"+inputId).outerHeight() + 'px' });
			}else{
				$("#"+downListID).css({ left: pp.left - ppRelate.left + 'px', top: pp.top - ppRelate.top + $("#"+inputId).outerHeight() + 'px' });
			}
        }
    }
};

//data: [{name:'Name',id:'123'}];
Util.GroupBox = function(container, data, oConfig, callbacks){
    this.container = container[0] || container;
    this.el;
    this.groups = {};
    this.data = data;
    this.cfg = {
        disable:false,
        width:'100%',
        groupBoxHeight:100,
        animate: false
    };
    $.extend(this.cfg, oConfig);
    this.cb = {};
    $.extend(this.cb, callbacks);
    this.currentGroup = null;
    this.selectedIndex = 0;
    this.html = '<div class="rtq-groupbox"></div>';
    this.init();
};
Util.GroupBox.prototype = {
    init: function(){
        var self = this;
        this.el = $(this.html).width(this.cfg.width);
        this.bindData(this.data);
        for (var g in this.groups) {
            if (this.groups[g]) {
                setTimeout(function(){
                    self.groups[g].expand();
                },0);
                break;
            }
        }
        
        this.el.appendTo(this.container);
    },
    bindData:function(dataArray){
        this.data = dataArray;
        for(var i=0;i<dataArray.length;i++){
            this.createGroup(dataArray[i]);
        }
    },
    createGroup:function(data){
        var self = this;
        var group = {};
        $.extend(group, data);
        group.el = $('<div class="rtq-groupbox-group"><h3 class="rtq-groupbox-group-hd">'+data.name+'</h3><div class="rtq-groupbox-group-bd" style="height:'+this.cfg.groupBoxHeight+'px"></div></div>');
        group.hd = group.el.children('.rtq-groupbox-group-hd');
        group.bd = group.el.children('.rtq-groupbox-group-bd');
        group.expand = function(){
            if(self.currentGroup && self.currentGroup!=group){
                self.currentGroup.dexpand();
            }
            group.isShow = true;
            group.el.addClass('rtq-groupbox-group-open');
            self.currentGroup = group;
            
            if (self.cfg.animate) {
                group.bd.height(0).animate({
                    height: self.cfg.groupBoxHeight
                });
            }else{
                group.bd.show();
            }
            
        };
        group.dexpand = function(){
            group.isShow = false;
            group.el.removeClass('rtq-groupbox-group-open');
            
            if (self.cfg.animate) {
                group.bd.animate({
                    height: 0
                });
            }
            else {
                group.bd.hide();
            }
        };
        group.hd.click(function(){
            if(group.isShow){
                group.dexpand();
            }else{
                group.expand(); 
            }
        });
        this.groups[data.id] = group;
        this.el.append(group.el);
    },
    getGroup:function(id){
        for (var g in this.groups) {
            if (this.groups[g].id == id) {
                return this.groups[g];
            }
        }
        return null;
    }
};

//Radio button/Checkbox
Util.SelectButton = function(container, data, callbacks, config){
    this.container = container[0] ? container : $(container);
    this.el;
    this.data = data;
    this.txt = data.txt;
    this.val = data.val;
    this.isSelected = false;
    this.cb = {
        onClick:function(value, item){},
        afterSelect:function(value, item){},
        afterInverse:function(value, item){}
    };
    $.extend(this.cb, callbacks);
    this.cfg = {
        type: 'radio', //'checkbox'
        withGroup: false,
        disable:false
    };
    $.extend(this.cfg, config);
    this.className = this.cfg.type == 'radio'? 'rtq-radio' : 'rtq-checkbox';
    this.html = '';
    this.init();
};
Util.SelectButton.prototype = {
    init: function () {
        var a = document.createElement('a');
        a.innerHTML = this.txt;
        a.className = this.className;
        var self = this;
        this.el = $(a).click(function (e) {
            if (self.cfg.disable) {
                return;
            }
            if (!self.cfg.withGroup) {
                if (self.isSelected) {
                    self.inverse();
                }
                else {
                    self.select();
                }
            }
            self.cb.onClick(self.val, self);
        });
        if (this.cfg.disable) {
            this.disable(true);
        }
        this.container.append(a);
    },
    disable: function (isDiasble) {
        this.cfg.disable = isDiasble === true ? true : false;
        if (this.cfg.disable) {
            this.el.addClass('rtq-btn-disable');
        } else {
            this.el.removeClass('rtq-btn-disable');
        }
    },
    select: function () {
        if (!this.cfg.disable) {
            this.el.addClass(this.className + '-on');
            this.isSelected = true;
            this.cb.afterSelect(this.val, this);
        }
    },
    inverse: function () {
        if (!this.cfg.disable) {
            this.el.removeClass(this.className + '-on');
            this.isSelected = false;
            this.cb.afterInverse(this.val, this);
        }
    },
    reSetText: function (text) {
        this.el.html(text);
        this.data.txt = text;
        this.txt = text;
    }
};

Util.ButtonGroup = function(container, data, callbacks, config){
    this.container = container[0] ? container : $(container);
    this.el;
    this.data = data;
    this.btns = {};
    this.cb = {
        onClick:function(value, item){}
    };
    $.extend(this.cb, callbacks);
    this.cfg = {
        type: 'radio', //'checkbox'
        multiSelect: false,
        inverse: true,
        vertical: false,
        disable: false
    };
    $.extend(this.cfg, config);
    this.cfg.multiSelect = this.cfg.type == 'checkbox'? true : false;
    this.init();
};
Util.ButtonGroup.prototype = {
    init: function(){
        this.el = $('<div class="rtq-btngroup'+(this.cfg.vertical?' rtq-btngroup-v':'')+'"></div>').appendTo(this.container);
        this.bindData(this.data);
        if(this.cfg.disable){
            this.disable(true);
        }
    },
    bindData:function(data){
        var self = this;
        this.data = data;
        this.el.empty();
        this.btns = {};
        for (var i = 0; i < data.length; i++) {
            this.btns[data[i].val] = new Util.SelectButton(this.el, {
                txt: data[i].txt,
                val: data[i].val
            }, {
                onClick: function(v, btn){
                    self._onClick(v, btn);
                }
            },{
                type:this.cfg.type,
                withGroup:true
            });
        }
    },
    _onClick:function(v, btn){
        var isSelf = btn.isSelected;
        if (!this.cfg.multiSelect) {
            for (var k in this.btns) {
                this.btns[k].inverse();
            }
        }
        if(this.cfg.inverse){
            if (isSelf) {
                btn.inverse();}
            else {
                btn.select();
            }
        }else{btn.select();}
        this.cb.onClick(v, btn);
    },
    disable: function(isDiasble){
        this.cfg.disable = isDiasble === true ? true : false;
        for (var k in this.btns) {
            this.btns[k].disable(this.cfg.disable);
        }
    },
    getValue:function(){
        var v = [];
        for (var k in this.btns) {
            if (this.btns[k].isSelected) {
                v.push(this.btns[k].val);
            }
        }
        return v;
    }
};
Util.CascadeMenu = function(_container,_option,_data,_callback){
	this.option = {
		display:true,
		isTraceMouse:false,    // if right menu need trace mouse.
		autoHide:false         // when mouse out of menu, hide the menu
	};
	this.callback = _callback;
	this.data = _data;
	this.menu ;
	this.container = _container;
	this.menuItems={};
	this._timeOut = null;
	$.extend(true,this.option,_option)
	this._init();
};
Util.CascadeMenu.prototype = {
	_init:function(){
		this.menuItems = {};
        if(this.menu){
            this.menu.remove();
            this.menu = null;
        }
		this.menu = this._createMenuItems(this.container,this.data,0,"",null);// create the cascade menu UI.
		this.menu.addClass("rtq-menu");
		var self = this;
		this.menu.find("li").hover(function(e){                           // bind the hover event to the menu item.
			$(this).find(">ul").show();
		},function(e){
			$(this).find(">ul").hide()
		});
		this.menu.bind("click.cascadeMenu",this,this.menuOnClick).bind("mouseover.cascadeMenu",function(e){
			self.show();
			if(self._timeOut){
				clearTimeout(self._timeOut)
			}
		});
		if(this.option.autoHide){
			this.menu.bind("mouseout.cascadeMenu",function(e){
			if(self._timeOut){
				clearTimeout(self._timeOut)
			}
			self._timeOut = setTimeout(function(){
			 	self.hide();
			},100);
		});
		}
		if(!this.option.display){
			this.hide();
		}
	},
	_getDataObj:function(sq,index){// 12345...
		if(typeof(sq)== "undefined" || sq==null)return null;
		var data = null,indx;
		for(var i=0;i<=sq;i++){
			indx = parseInt(index[i]);
			if(i==0)data = this.data[indx];
			else data = data.subMenu[indx];
		}
		return data;
	},
	_createMenuItems:function(container,data,sq,inds,parent){  // sq is series index.
		var str = "", cls="", cls2 ="",ind = inds,ty,$ul;
		if(data == null)return;
		if(data.length){
			$ul = $('<ul class ="rtq-menu-ul"></ul>').appendTo(container);
			for(var i=0;i<data.length;i++){
				ind=inds+"_"+i;
				this.menuItems[ind]={};
				this.menuItems[ind]["data"]= this._copyItemData(data[i]);
				this.menuItems[ind]["parent"]= parent;
				this.menuItems[ind]["el"]= $('<li class="rtq-menu-item"></li>').appendTo($ul);
				if(typeof(data[i].renderer)=="function"){
					data[i].renderer(this.menuItems[ind]["el"]);
				}else if (data[i].type == "separator") {
					this.menuItems[ind]["el"].append('<div class="rtq-menu-separator"></div>');
				}else {
					cls2 = "rtq-menu-a";
					if (data[i].subMenu && data[i].subMenu.length) {
						cls2 += ' rtq-menu-arrow';
					}
					if (data[i].type == "checkbox") {
						cls = 'class = "rtq-chkbox rtq-chkbox-off ' + cls2 + '"';
					}
					else 
						if (data[i].type == "ratio") {
							cls = 'class = "rtq-ratio rtq-ratio-off ' + cls2 + '"';
						}
						else 
							if (data[i].type == "switch") {
								cls = 'class = "on ' + cls2 + '"';
							}
							else {
								cls = 'class = "' + cls2 + '"';
							}
					ty = data[i].type ? data[i].type : "";
					str = '<a ' + cls + 'type=' + ty + ' sq=' + sq + ' index=' + ind + ' >' + data[i].txt + '</a>';
					this.menuItems[ind]["el"].append(str);
				}	
				if(data[i].subMenu&&data[i].subMenu.length){
					this._createMenuItems(this.menuItems[ind]["el"],data[i].subMenu,sq+1,ind,this.menuItems[ind]);
				}
			}
		}
		return $ul;
	},
	_copyItemData:function(data){
		var d={};
		for(var o in data){
			if(o!="subMenu"){
				d[o]=data[o];
			}
		}	
		return d;	
	},
	_getPostion:function(e,$container){
		var scrollx = 0;
		var scrolly = 0;
		if( typeof( window.pageYOffset ) == 'number' ) {
			scrollx = window.pageXOffset;
			scrolly = window.pageYOffset;
		} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
			scrollx = document.documentElement.scrollLeft;
			scrolly = document.documentElement.scrollTop;
		} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
			scrollx = document.body.scrollLeft;
			scrolly = document.body.scrollTop;
		}
		var oSet = $container.offset();
		return{left:e.pageX + scrollx - oSet.left,top:e.pageY + scrolly - oSet.top};
	},
    setPosition:function(_left,_top){
        this.menu.css({top:_top, left:_left});
    },
	_selectMenuItem:function(item,flag){
		var ty = item.data.type, t = item.el.find(">a");
		if(ty=="ratio"){
			if(!flag){
				t.addClass("rtq-ratio-off");
			}else{
				if(t.hasClass("rtq-ratio-off")){  /// not selcted
					t.closest("ul",self.menu).find(">li a").addClass("rtq-ratio-off");
					t.removeClass("rtq-ratio-off");
				}
			}
		}else if(ty=="checkbox"){
			if (flag) {
				t.removeClass("rtq-chkbox-off");
			}else {
				t.addClass("rtq-chkbox-off");
			}		
			item.checked = flag;
		}else if(ty == "switch"){
			if (flag) {
				t.addClass("on");
			}else {
				t.removeClass("on");
			}		
			var inx = flag?0:1;
			t.html(item.data.sTxt[inx]);
			item["switch"] = flag?"on":"off";
		}
	},
	menuOnClick:function(e){
		var self = e.data;
		var flag = true;
		var t = $(e.target);
		var index = t.attr("index");
		if(typeof(index)=="undefined")return false;
		var item = self.menuItems[index];
		if(item.data&&(typeof(item.data.selectable)== "undefined"||item.data.selectable)){ // if not define selectable its default value is true.
			if(item.data.type == "checkbox"){
				flag = t.hasClass("rtq-chkbox-off");  // not checked.
			}else if(item.data.type == "switch"){              
				flag = !t.hasClass("on");
			}
			self._selectMenuItem(item,flag);
			if(typeof(self.callback)== "function"){
				self.callback(item);
			}
		}
	},
	show:function(e){
		if(this.option.isTraceMouse&&typeof(e)!= "undefined"){
			var pos = this._getPostion(e,this.container);
			this.menu.css(pos);
		}
		this.menu.show();
	},
	hide:function(){
		this.menu.hide();
	},
	changeUIState:function(val,parentVal,flag){  // only change the UI state not excute callback.
		var d, p;
		for(var o in this.menuItems){
			d = this.menuItems[o].data;
			p = this.menuItems[o].parent==null?null:this.menuItems[o].parent.data;
			if(val==d.val&&(typeof(parentVal)== "undefined"||parentVal==null||(p!=null&&parentVal==p.val))){ // find the menuItem
				this._selectMenuItem(this.menuItems[o],flag);
			}
		}
	},
	reBind:function(data){
		this.data = data;
		this._init();
	},
	clear:function(){
		for(var o in this.menuItems){
			this.menuItems[o].parent = null;
			this.menuItems[o].el = null;
		}
		this.menuItems = null;
		this.menu.unbind(".cascadeMenu");
		this.menu.find("li").unbind();
		this.menu.remove();
	}
};

Util.disableBlock = function(container, method) {
    var c = $(container);
    var div = $("<div></div>").addClass("ps_blockLayer");
    if (method === "after") {
        div.insertAfter(c);
    } else if (method === "before") {
        div.insertBefore(c);
    } else {
        div.appendTo(c);
        c.css("position", "relative");
    }
    var pw = div.parent().width();
    var ph = div.parent().height();
    div.css({ width: pw + "px", height: ph + "px" });
    return div;
};


