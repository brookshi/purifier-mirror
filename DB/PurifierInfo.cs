﻿// File:    PurifierInfo.cs
// Author:  iwxiaot
// Created: 2013年12月14日 23:48:15
// Purpose: Definition of Class PurifierInfo

using System;

/// 净化器信息
public class PurifierInfo
{
   public string Id
   {
      get
      {
         return id;
      }
      set
      {
         this.id = value;
      }
   }
   
   public string SerialNumber
   {
      get
      {
         return serialNumber;
      }
      set
      {
         this.serialNumber = value;
      }
   }
   
   public string RunTime
   {
      get
      {
         return runTime;
      }
      set
      {
         this.runTime = value;
      }
   }
   
   public string City
   {
      get
      {
         return city;
      }
      set
      {
         this.city = value;
      }
   }
   
   public string Address
   {
      get
      {
         return address;
      }
      set
      {
         this.address = value;
      }
   }
   
   public string FilterMediumChangeTime
   {
      get
      {
         return filterMediumChangeTime;
      }
      set
      {
         this.filterMediumChangeTime = value;
      }
   }
   
   public string Status
   {
      get
      {
         return status;
      }
      set
      {
         this.status = value;
      }
   }
   
   public string Tear
   {
      get
      {
         return tear;
      }
      set
      {
         this.tear = value;
      }
   }
   
   public int UpdateInterval
   {
      get
      {
         return updateInterval;
      }
      set
      {
         this.updateInterval = value;
      }
   }
   
   public int OperatorInterval
   {
      get
      {
         return operatorInterval;
      }
      set
      {
         this.operatorInterval = value;
      }
   }
   
   /// 净化器ID
   public string id;
   /// 净化器序列号
   public string serialNumber;
   /// 运行时间
   public string runTime;
   /// 城市
   public string city;
   /// 详细地址
   public string address;
   /// 滤材更换时间
   public string filterMediumChangeTime;
   /// 开关状态
   public string status;
   /// 档位
   public string tear;
   /// 上传时间间隔
   public int updateInterval;
   /// 轮询操作状态时间间隔
   public int operatorInterval;

}