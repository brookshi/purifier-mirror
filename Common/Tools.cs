﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HOYI.Common
{
    public static class Tools
    {
        public static void ExecAction(Action action)
        {
            try
            {
                if (action != null)
                    action();
            }
            catch { }
        }

        public static object GetValueObj(this JToken token)
        {
            if (token != null)
            {
                if (token is JValue)
                {
                    return (token as JValue).Value;
                }
            }
            return token;
        }

        /// <summary>
        /// Compares strign with OrdinalIgnoreCase
        /// </summary>
        /// <param name="a">A string</param>
        /// <param name="b">A string</param>
        /// <returns>true a and b are equal; otherwise, else is not. </returns>
        public static bool CompareStr(string a, string b)
        {
            return string.Equals(a, b, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Determines whether a string is a guid
        /// </summary>
        /// <param name="guidStr">A string determined</param>
        /// <returns>true the string is guid string; otherwise, false is not.</returns>
        public static bool IsGuidString(string guidStr)
        {
            if (string.IsNullOrEmpty(guidStr))
                return false;
            return Regex.IsMatch(guidStr, string.Format("^[\\x20]*{0}{{8}}(?:-{0}{{4}}){{3}}-{0}{{12}}[\\x20]*\\b", "[0-9a-f]"), RegexOptions.IgnoreCase);
        }


        static Regex s_iRegex = new Regex(@"^\-{0,1}[0-9]+$", RegexOptions.Compiled | RegexOptions.Singleline);
        static Regex s_dRegex = new Regex(@"^\-{0,1}[0-9]+(\.[0-9]+){0,1}$", RegexOptions.Compiled | RegexOptions.Singleline);

        /// <summary>
        /// Detects if a string can be convert to numeric.
        /// </summary>
        /// <param name="text">A string detected</param>
        /// <returns>true: the string is numeric, false: is not numeric</returns>
        public static bool IsNumeric(string text)
        {
            return IsNumeric(text, false);
        }

        /// <summary>
        ///  Detects if a string can be convert to float or int.
        /// </summary>
        /// <param name="text">A string detected</param>
        /// <param name="isInt">true: int,false: int and float</param>
        /// <returns>true: the string is int or float, false: is not</returns>
        public static bool IsNumeric(string text, bool isInt)
        {
            return (isInt ? s_iRegex : s_dRegex).IsMatch(text);
        }

        /// <summary>
        /// Convert a datetime to string "yyyy-MM-dd HH:mm:ss.fff"
        /// </summary>
        /// <param name="dt">A datetime</param>
        /// <returns>Return a datetime string "yyyy-MM-dd HH:mm:ss.fff"</returns>
        public static string ToStandardString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        /// <summary>
        /// Determines whether a datetime is valid.
        /// </summary>
        /// <param name="dateTime">A DateTime determined</param>
        /// <returns>true the datetime is valid; otherwise, else is invalid.</returns>
        public static bool IsValid(DateTime dateTime)
        {
            return dateTime.Year > 2000;
        }

        /// <summary>
        /// Determines whether a string is a datetime
        /// </summary>
        /// <param name="dateString">A string determined</param>
        /// <returns>true the string is date time string; otherwise, false is not.</returns>
        public static bool IsDateTimeString(string dateString)
        {
            DateTime dt = default(DateTime);
            return DateTime.TryParse(dateString, out dt);
        }

        /// <summary>
        /// Determine a datetime exceeds a certain range.
        /// </summary>
        /// <param name="diffDate">A datetime determined</param>
        /// <param name="secondsDiff">A range of seconds value</param>
        /// <returns>return true or false. true: exceeds,false: does not exceed</returns>
        public static bool DiffNowOut(DateTime diffDate, int secondsDiff)
        {
            if (!IsValid(diffDate)) return false;
            DateTime now = diffDate.Kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;
            return diffDate.AddSeconds(secondsDiff) < now;
        }

        /// <summary>
        /// Determine a datetime still in a certain range.
        /// </summary>
        /// <param name="diffDate">A datetime determined</param>
        /// <param name="secondsDiff">A range of seconds value</param>
        /// <returns>return true or false. true: yes,false: no</returns>
        public static bool DiffNowIn(DateTime diffDate, int secondsDiff)
        {
            if (!IsValid(diffDate)) return false;
            DateTime now = diffDate.Kind == DateTimeKind.Utc ? DateTime.UtcNow : DateTime.Now;
            return diffDate <= now && diffDate.AddSeconds(secondsDiff) > now;
        }

        /// <summary>
        ///Divide a list into groups according to the amount
        /// </summary>
        /// <typeparam name="T">The item type of the list.</typeparam>
        /// <param name="list">A list grouped.</param>
        /// <param name="count">A number every group.</param>
        /// <returns>Return a result grouped.</returns>
        public static IEnumerator<IEnumerable<T>> GroupList<T>(List<T> list, int count)
        {
            for (int g = 1; ; g++)
            {
                var ids = list.Where(i =>
                {
                    var ix = list.IndexOf(i);
                    return ix >= (g - 1) * count && ix < g * count;
                });
                if (ids.Count() == 0)
                    yield break;
                else
                    yield return ids;
            }
        }
    }
}
