/*
 * File: app/controller/Setting.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.3.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.3.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Purifier.controller.Setting', {
    extend: 'Ext.app.Controller',
    alias: 'controller.setting',

    config: {
        refs: {
            passwordPanel: {
                autoCreate: true,
                selector: 'formpanel#password',
                xtype: 'password'
            },
            oldPwd: 'passwordfield#oldpwd',
            newPwd1: 'passwordfield#newpwd1',
            newPwd2: 'passwordfield#newpwd2',
            timePanel: {
                autoCreate: true,
                selector: 'formpanel#time',
                xtype: 'time'
            },
            uploadSpin: 'spinnerfield#uploadtime',
            searchSpin: 'spinnerfield#searchtime'
        },

        control: {
            "button#reset": {
                tap: 'onReset'
            },
            "button#changepwd": {
                tap: 'onChangePwd'
            },
            "button#changetime": {
                tap: 'onChangeTime'
            }
        }
    },

    onReset: function(button, e, eOpts) {
        var oldpwd = this.getOldPwd();
        var newpwd1 = this.getNewPwd1();
        var newpwd2 = this.getNewPwd2();
        oldpwd.setValue("");
        newpwd1.setValue("");
        newpwd2.setValue("");
    },

    onChangePwd: function(button, e, eOpts) {

        //var pwdForm = this.getPasswordPanel();
        //var values = pwdForm.getValues();
        var oldpwdField=this.getOldPwd();
        var newpwdField1=this.getNewPwd1();
        var newpwdField2=this.getNewPwd2();
        var oldpwd=oldpwdField.getValue();
        var newpwd=newpwdField1.getValue();
        var newpwd2=newpwdField2.getValue();
        if(newpwd!=newpwd2)
        {
            Ext.Msg.alert("两个新密码不一致，请重新输入");
            return;
        }
        if(newpwd==="")
        {
            Ext.Msg.alert("密码不能为空");
            return;
        }
        Purifier.app.getController('GlobalData').showMask();

        var successCallback = function(resp, ops) {

            Ext.Msg.alert("修改密码成功");
        };


        var failureCallback = function(resp, ops) {
            Purifier.app.getController('GlobalData').hideMask();
            Ext.Msg.alert("修改密码失败");

        };

         Ext.Ajax.request({
        		url: Purifier.app.getController('GlobalData').getServer()+"userservice.asmx/CheckOldPwd",
             method:'POST',
             headers: {
                        'Content-Type': ' application/json; charset=utf-8'
                    },
             jsonData:{'oldpwd':oldpwd},
             success: function(resp, ops){
                 if(!resp.d)
                 {
                    Purifier.app.getController('GlobalData').hideMask();
                     Ext.Msg.alert("旧密码错误，请重新输入");
                     return;
                 }
                 Ext.Ajax.request({
        		url: Purifier.app.getController('GlobalData').getServer()+"userservice.asmx/ChangePwd",
                method:'POST',
                     jsonData: {'oldpwd':oldpwd, 'newpwd':newpwd},
                success: successCallback,
                failure: failureCallback
         });
             },
             failure: function(resp, ops){
                Purifier.app.getController('GlobalData').hideMask();
                 Ext.Msg.alert("检查旧密码失败");
             }
         });


    },

    onChangeTime: function(button, e, eOpts) {
        Purifier.app.getController('GlobalData').showMask();
        var uploadTime = this.getUploadSpin();
        var searchTime = this.getSearchSpin();
             Ext.Ajax.request({
        		url: Purifier.app.getController('GlobalData').getServer()+"Mobile.asmx/SetIntervalTime",
                 method:'POST',
            headers: {
                        'Content-Type': ' application/json; charset=utf-8'
                    },
                 jsonData:{'uploadTime':uploadTime.getValue(),'searchTime':searchTime.getValue()},
                 success: function(resp, ops){
                     Purifier.app.getController('GlobalData').hideMask();
                     Ext.Msg.alert("修改成功");
                 },
                 failure: function(resp, ops){
                     Purifier.app.getController('GlobalData').hideMask();
                     Ext.Msg.alert("修改数据失败");
                 }
         });
    }

});