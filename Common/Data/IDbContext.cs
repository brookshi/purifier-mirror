﻿using System.Data.Common;

namespace HOYI.Common.Data
{
    public interface IDbContext
    {
        DbProviderFactory DbFactory { get; }
        DbConnection Connection { get; }
    }
}
