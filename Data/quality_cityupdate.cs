//------------------------------------------------------------------------------
// <auto-generated>
//     此代码已从模板生成。
//
//     手动更改此文件可能导致应用程序出现意外的行为。
//     如果重新生成代码，将覆盖对此文件的手动更改。
// </auto-generated>
//------------------------------------------------------------------------------

namespace HOYI.Data
{
    using System;
    using System.Collections.Generic;
    using HOYI.Common.Data;

    public partial class quality_cityupdate : BaseDataObj
    {
        public int id { get; set; }
        public virtual int isaqicnchanged { get; set; }
        public virtual int isqm25inchanged { get; set; }

        public static quality_cityupdate GetInfo()
        {
            return PersistenceHelper.Default.Read<quality_cityupdate>(0);
        }
    }
}
