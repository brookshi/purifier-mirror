﻿using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace HOYI.Web.Common
{
    public class PageBase : Page
    {
        public bool IsAdmin
        {
            get
            {
                var cu = HOYI.Web.Common.User.Current;
                return cu.IsInRole(HOYI.Data.Role.AdminRole);
            }
        }
    }
}