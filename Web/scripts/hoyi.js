﻿window.HOYI = window.hoYi = {
    DateFmtStr: "yyyy/MM/dd HH:mm:ss",
    getDlgHtml: function (src, cb) {
        $.get(src, null, function (response, status, xhr) {
            if (status == "success")
                cb(response);
        }, "html");
    },
    callService: function (svcUrl, data, onSuccess, onError, reqType) {
        var axo = {
            type: reqType || "POST",
            contentType: "application/json;charset=utf-8",
            url: svcUrl,
            reqParam: data,
            data: $.toJSON(data),
            dataType: 'json',
            beforeSend: function (x) {
                x.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                HOYI.progressBar && HOYI.progressBar.show();
            },
            error: function (x, e) {
                HOYI.progressBar && HOYI.progressBar.close();
                var rs = hoYi.parseJSON(x.responseText);
                if (onError && onError(x, e, rs)) {
                    if (rs) return;
                }
                if (x.status == 401)
                    hoYi.loadPage("login.html");
                else
                    alert(x.responseText);
            }
        };
        axo.success = (function (a, b) {
            HOYI.progressBar && HOYI.progressBar.close();
            this.cb && this.cb(a, b, this.param);
        }).bind({ param: data, cb: onSuccess })
        $.ajax(axo);
    },
    parseJSON: function (json) {
        try{
            return $.parseJSON(json);
        }
        catch (x) { }
        return null;
    },
    getJSON: function (cn) {
        var jcn = cn.attr ? cn : $(cn);
        var jo = {};
        cn.find("input").each(function (i) {
            jo[this.name || this.getAttribute("dn")] = this.value;
        });
        return jo;
    },
    loadPage: function (url) {

        window.location.href = url;
    },
    showDialog: function (url, _width, _height, _left, _top, _resizable, _status, _scroll) {

        var iTop = (window.screen.availHeight - 30 - _height) / 2;
        var iLeft = (window.screen.availWidth - 10 - _width) / 2;

        if ((!_left || _left == -1) || (!_top && _top == -1)) {
            _left = iLeft;
            _top = iTop;
        }
        var ftu = "";

        if (window.showModalDialog) {
            ftu = "dialogWidth=" + _width + "px;" +
                "dialogHeight=" + _height + "px;" +
                "dialogLeft=" + _left + "px;" +
                "dialogTop=" + _top + "px;" +
                "resizable=" + _resizable + ";" +
                "status=" + _status + ";" +
                "scroll=" + _scroll;
            return window.showModalDialog(url, window, ftu);
        }
        else {
            ftu = "width=" + _width + "," +
                "height=" + _height + "," +
                "left=" + _left + "," +
                "top=" + _top + "," +
                "resizable=" + _resizable + "," +
                "status=" + _status + "," +
                "scrollable=" + _scroll;
            return window.open(url, '_blank', otherFeatures);
        }
    }
};


///<reference path="./jquery-vsdoc.js" />
//JQuery migrate-1.2.1
/*!
 * jQuery Migrate - v1.2.1 - 2013-05-08
 * https://github.com/jquery/jquery-migrate
 * Copyright 2005, 2013 jQuery Foundation, Inc. and other contributors; Licensed MIT
 */

function $jqm(jQuery, window, undefined) {
    // See http://bugs.jquery.com/ticket/13335
    // "use strict";


    var warnedAbout = {};

    // List of warnings already given; public read only
    jQuery.migrateWarnings = [];

    // Set to true to prevent console output; migrateWarnings still maintained
    jQuery.migrateMute = false;

    // Show a message on the console so devs know we're active
    if (jQuery.migrateMute && window.console && window.console.log) {
        window.console.log("JQMIGRATE: Logging is active");
    }

    // Set to false to disable traces that appear with warnings
    jQuery.migrateTrace = false;

    // Forget any warnings we've already given; public
    jQuery.migrateReset = function () {
        warnedAbout = {};
        jQuery.migrateWarnings.length = 0;
    };

    function migrateWarn(msg) {
        var console = window.console;
        if (!warnedAbout[msg]) {
            warnedAbout[msg] = true;
            jQuery.migrateWarnings.push(msg);
            if (console && console.warn && jQuery.migrateMute) {
                console.warn("JQMIGRATE: " + msg);
                if (jQuery.migrateTrace && console.trace) {
                    console.trace();
                }
            }
        }
    }

    function migrateWarnProp(obj, prop, value, msg) {
        if (Object.defineProperty) {
            // On ES5 browsers (non-oldIE), warn if the code tries to get prop;
            // allow property to be overwritten in case some other plugin wants it
            try {
                Object.defineProperty(obj, prop, {
                    configurable: true,
                    enumerable: true,
                    get: function () {
                        migrateWarn(msg);
                        return value;
                    },
                    set: function (newValue) {
                        migrateWarn(msg);
                        value = newValue;
                    }
                });
                return;
            } catch (err) {
                // IE8 is a dope about Object.defineProperty, can't warn there
            }
        }

        // Non-ES5 (or broken) browser; just set the property
        jQuery._definePropertyBroken = true;
        obj[prop] = value;
    }

    if (document.compatMode === "BackCompat") {
        // jQuery has never supported or tested Quirks Mode
        migrateWarn("jQuery is not compatible with Quirks Mode");
    }


    var attrFn = jQuery("<input/>", { size: 1 }).attr("size") && jQuery.attrFn,
        oldAttr = jQuery.attr,
        valueAttrGet = jQuery.attrHooks.value && jQuery.attrHooks.value.get ||
            function () { return null; },
        valueAttrSet = jQuery.attrHooks.value && jQuery.attrHooks.value.set ||
            function () { return undefined; },
        rnoType = /^(?:input|button)$/i,
        rnoAttrNodeType = /^[238]$/,
        rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        ruseDefault = /^(?:checked|selected)$/i;

    // jQuery.attrFn
    migrateWarnProp(jQuery, "attrFn", attrFn || {}, "jQuery.attrFn is deprecated");

    jQuery.attr = function (elem, name, value, pass) {
        var lowerName = name.toLowerCase(),
            nType = elem && elem.nodeType;

        if (pass) {
            // Since pass is used internally, we only warn for new jQuery
            // versions where there isn't a pass arg in the formal params
            if (oldAttr.length < 4) {
                migrateWarn("jQuery.fn.attr( props, pass ) is deprecated");
            }
            if (elem && !rnoAttrNodeType.test(nType) &&
                (attrFn ? name in attrFn : jQuery.isFunction(jQuery.fn[name]))) {
                return jQuery(elem)[name](value);
            }
        }

        // Warn if user tries to set `type`, since it breaks on IE 6/7/8; by checking
        // for disconnected elements we don't warn on $( "<button>", { type: "button" } ).
        if (name === "type" && value !== undefined && rnoType.test(elem.nodeName) && elem.parentNode) {
            migrateWarn("Can't change the 'type' of an input or button in IE 6/7/8");
        }

        // Restore boolHook for boolean property/attribute synchronization
        if (!jQuery.attrHooks[lowerName] && rboolean.test(lowerName)) {
            jQuery.attrHooks[lowerName] = {
                get: function (elem, name) {
                    // Align boolean attributes with corresponding properties
                    // Fall back to attribute presence where some booleans are not supported
                    var attrNode,
                        property = jQuery.prop(elem, name);
                    return property === true || typeof property !== "boolean" &&
                        (attrNode = elem.getAttributeNode(name)) && attrNode.nodeValue !== false ?

                        name.toLowerCase() :
                        undefined;
                },
                set: function (elem, value, name) {
                    var propName;
                    if (value === false) {
                        // Remove boolean attributes when set to false
                        jQuery.removeAttr(elem, name);
                    } else {
                        // value is true since we know at this point it's type boolean and not false
                        // Set boolean attributes to the same name and set the DOM property
                        propName = jQuery.propFix[name] || name;
                        if (propName in elem) {
                            // Only set the IDL specifically if it already exists on the element
                            elem[propName] = true;
                        }

                        elem.setAttribute(name, name.toLowerCase());
                    }
                    return name;
                }
            };

            // Warn only for attributes that can remain distinct from their properties post-1.9
            if (ruseDefault.test(lowerName)) {
                migrateWarn("jQuery.fn.attr('" + lowerName + "') may use property instead of attribute");
            }
        }

        return oldAttr.call(jQuery, elem, name, value);
    };

    // attrHooks: value
    jQuery.attrHooks.value = {
        get: function (elem, name) {
            var nodeName = (elem.nodeName || "").toLowerCase();
            if (nodeName === "button") {
                return valueAttrGet.apply(this, arguments);
            }
            if (nodeName !== "input" && nodeName !== "option") {
                migrateWarn("jQuery.fn.attr('value') no longer gets properties");
            }
            return name in elem ?
                elem.value :
                null;
        },
        set: function (elem, value) {
            var nodeName = (elem.nodeName || "").toLowerCase();
            if (nodeName === "button") {
                return valueAttrSet.apply(this, arguments);
            }
            if (nodeName !== "input" && nodeName !== "option") {
                migrateWarn("jQuery.fn.attr('value', val) no longer sets properties");
            }
            // Does not return so that setAttribute is also used
            elem.value = value;
        }
    };


    var matched, browser,
        oldInit = jQuery.fn.init,
        oldParseJSON = jQuery.parseJSON,
        // Note: XSS check is done below after string is trimmed
        rquickExpr = /^([^<]*)(<[\w\W]+>)([^>]*)$/;

    // $(html) "looks like html" rule change
    jQuery.fn.init = function (selector, context, rootjQuery) {
        var match;

        if (selector && typeof selector === "string" && !jQuery.isPlainObject(context) &&
                (match = rquickExpr.exec(jQuery.trim(selector))) && match[0]) {
            // This is an HTML string according to the "old" rules; is it still?
            if (selector.charAt(0) !== "<") {
                migrateWarn("$(html) HTML strings must start with '<' character");
            }
            if (match[3]) {
                migrateWarn("$(html) HTML text after last tag is ignored");
            }
            // Consistently reject any HTML-like string starting with a hash (#9521)
            // Note that this may break jQuery 1.6.x code that otherwise would work.
            if (match[0].charAt(0) === "#") {
                migrateWarn("HTML string cannot start with a '#' character");
                jQuery.error("JQMIGRATE: Invalid selector string (XSS)");
            }
            // Now process using loose rules; let pre-1.8 play too
            if (context && context.context) {
                // jQuery object as context; parseHTML expects a DOM object
                context = context.context;
            }
            if (jQuery.parseHTML) {
                return oldInit.call(this, jQuery.parseHTML(match[2], context, true),
                        context, rootjQuery);
            }
        }
        return oldInit.apply(this, arguments);
    };
    jQuery.fn.init.prototype = jQuery.fn;

    // Let $.parseJSON(falsy_value) return null
    jQuery.parseJSON = function (json) {
        if (!json && json !== null) {
            migrateWarn("jQuery.parseJSON requires a valid JSON string");
            return null;
        }
        return oldParseJSON.apply(this, arguments);
    };

    jQuery.uaMatch = function (ua) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
            /(webkit)[ \/]([\w.]+)/.exec(ua) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
            /(msie) ([\w.]+)/.exec(ua) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
            [];

        return {
            browser: match[1] || "",
            version: match[2] || "0"
        };
    };

    // Don't clobber any existing jQuery.browser in case it's different
    if (!jQuery.browser) {
        matched = jQuery.uaMatch(navigator.userAgent);
        browser = {};

        if (matched.browser) {
            browser[matched.browser] = true;
            browser.version = matched.version;
        }

        // Chrome is Webkit, but Webkit is also Safari.
        if (browser.chrome) {
            browser.webkit = true;
        } else if (browser.webkit) {
            browser.safari = true;
        }

        jQuery.browser = browser;
    }

    // Warn if the code tries to get jQuery.browser
    migrateWarnProp(jQuery, "browser", jQuery.browser, "jQuery.browser is deprecated");

    jQuery.sub = function () {
        function jQuerySub(selector, context) {
            return new jQuerySub.fn.init(selector, context);
        }
        jQuery.extend(true, jQuerySub, this);
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init(selector, context) {
            if (context && context instanceof jQuery && !(context instanceof jQuerySub)) {
                context = jQuerySub(context);
            }

            return jQuery.fn.init.call(this, selector, context, rootjQuerySub);
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        migrateWarn("jQuery.sub() is deprecated");
        return jQuerySub;
    };


    // Ensure that $.ajax gets the new parseJSON defined in core.js
    jQuery.ajaxSetup({
        converters: {
            "text json": jQuery.parseJSON
        }
    });


    var oldFnData = jQuery.fn.data;

    jQuery.fn.data = function (name) {
        var ret, evt,
            elem = this[0];

        // Handles 1.7 which has this behavior and 1.8 which doesn't
        if (elem && name === "events" && arguments.length === 1) {
            ret = jQuery.data(elem, name);
            evt = jQuery._data(elem, name);
            if ((ret === undefined || ret === evt) && evt !== undefined) {
                migrateWarn("Use of jQuery.fn.data('events') is deprecated");
                return evt;
            }
        }
        return oldFnData.apply(this, arguments);
    };


    var rscriptType = /\/(java|ecma)script/i,
        oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack;

    jQuery.fn.andSelf = function () {
        migrateWarn("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()");
        return oldSelf.apply(this, arguments);
    };

    // Since jQuery.clean is used internally on older versions, we only shim if it's missing
    if (!jQuery.clean) {
        jQuery.clean = function (elems, context, fragment, scripts) {
            // Set context per 1.8 logic
            context = context || document;
            context = !context.nodeType && context[0] || context;
            context = context.ownerDocument || context;

            migrateWarn("jQuery.clean() is deprecated");

            var i, elem, handleScript, jsTags,
                ret = [];

            jQuery.merge(ret, jQuery.buildFragment(elems, context).childNodes);

            // Complex logic lifted directly from jQuery 1.8
            if (fragment) {
                // Special handling of each script element
                handleScript = function (elem) {
                    // Check if we consider it executable
                    if (!elem.type || rscriptType.test(elem.type)) {
                        // Detach the script and store it in the scripts array (if provided) or the fragment
                        // Return truthy to indicate that it has been handled
                        return scripts ?
                            scripts.push(elem.parentNode ? elem.parentNode.removeChild(elem) : elem) :
                            fragment.appendChild(elem);
                    }
                };

                for (i = 0; (elem = ret[i]) != null; i++) {
                    // Check if we're done after handling an executable script
                    if (!(jQuery.nodeName(elem, "script") && handleScript(elem))) {
                        // Append to fragment and handle embedded scripts
                        fragment.appendChild(elem);
                        if (typeof elem.getElementsByTagName !== "undefined") {
                            // handleScript alters the DOM, so use jQuery.merge to ensure snapshot iteration
                            jsTags = jQuery.grep(jQuery.merge([], elem.getElementsByTagName("script")), handleScript);

                            // Splice the scripts into ret after their former ancestor and advance our index beyond them
                            ret.splice.apply(ret, [i + 1, 0].concat(jsTags));
                            i += jsTags.length;
                        }
                    }
                }
            }

            return ret;
        };
    }

    var eventAdd = jQuery.event.add,
        eventRemove = jQuery.event.remove,
        eventTrigger = jQuery.event.trigger,
        oldToggle = jQuery.fn.toggle,
        oldLive = jQuery.fn.live,
        oldDie = jQuery.fn.die,
        ajaxEvents = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess",
        rajaxEvent = new RegExp("\\b(?:" + ajaxEvents + ")\\b"),
        rhoverHack = /(?:^|\s)hover(\.\S+|)\b/,
        hoverHack = function (events) {
            if (typeof (events) !== "string" || jQuery.event.special.hover) {
                return events;
            }
            if (rhoverHack.test(events)) {
                migrateWarn("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'");
            }
            return events && events.replace(rhoverHack, "mouseenter$1 mouseleave$1");
        };

    // Event props removed in 1.9, put them back if needed; no practical way to warn them
    if (jQuery.event.props && jQuery.event.props[0] !== "attrChange") {
        jQuery.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement");
    }

    // Undocumented jQuery.event.handle was "deprecated" in jQuery 1.7
    if (jQuery.event.dispatch) {
        migrateWarnProp(jQuery.event, "handle", jQuery.event.dispatch, "jQuery.event.handle is undocumented and deprecated");
    }

    // Support for 'hover' pseudo-event and ajax event warnings
    jQuery.event.add = function (elem, types, handler, data, selector) {
        if (elem !== document && rajaxEvent.test(types)) {
            migrateWarn("AJAX events should be attached to document: " + types);
        }
        eventAdd.call(this, elem, hoverHack(types || ""), handler, data, selector);
    };
    jQuery.event.remove = function (elem, types, handler, selector, mappedTypes) {
        eventRemove.call(this, elem, hoverHack(types) || "", handler, selector, mappedTypes);
    };

    jQuery.fn.error = function () {
        var args = Array.prototype.slice.call(arguments, 0);
        migrateWarn("jQuery.fn.error() is deprecated");
        args.splice(0, 0, "error");
        if (arguments.length) {
            return this.bind.apply(this, args);
        }
        // error event should not bubble to window, although it does pre-1.7
        this.triggerHandler.apply(this, args);
        return this;
    };

    jQuery.fn.toggle = function (fn, fn2) {

        // Don't mess with animation or css toggles
        if (!jQuery.isFunction(fn) || !jQuery.isFunction(fn2)) {
            return oldToggle.apply(this, arguments);
        }
        migrateWarn("jQuery.fn.toggle(handler, handler...) is deprecated");

        // Save reference to arguments for access in closure
        var args = arguments,
            guid = fn.guid || jQuery.guid++,
            i = 0,
            toggler = function (event) {
                // Figure out which function to execute
                var lastToggle = (jQuery._data(this, "lastToggle" + fn.guid) || 0) % i;
                jQuery._data(this, "lastToggle" + fn.guid, lastToggle + 1);

                // Make sure that clicks stop
                event.preventDefault();

                // and execute the function
                return args[lastToggle].apply(this, arguments) || false;
            };

        // link all the functions, so any of them can unbind this click handler
        toggler.guid = guid;
        while (i < args.length) {
            args[i++].guid = guid;
        }

        return this.click(toggler);
    };

    jQuery.fn.live = function (types, data, fn) {
        migrateWarn("jQuery.fn.live() is deprecated");
        if (oldLive) {
            return oldLive.apply(this, arguments);
        }
        jQuery(this.context).on(types, this.selector, data, fn);
        return this;
    };

    jQuery.fn.die = function (types, fn) {
        migrateWarn("jQuery.fn.die() is deprecated");
        if (oldDie) {
            return oldDie.apply(this, arguments);
        }
        jQuery(this.context).off(types, this.selector || "**", fn);
        return this;
    };

    // Turn global events into document-triggered events
    jQuery.event.trigger = function (event, data, elem, onlyHandlers) {
        if (!elem && !rajaxEvent.test(event)) {
            migrateWarn("Global events are undocumented and deprecated");
        }
        return eventTrigger.call(this, event, data, elem || document, onlyHandlers);
    };
    jQuery.each(ajaxEvents.split("|"),
        function (_, name) {
            jQuery.event.special[name] = {
                setup: function () {
                    var elem = this;

                    // The document needs no shimming; must be !== for oldIE
                    if (elem !== document) {
                        jQuery.event.add(document, name + "." + jQuery.guid, function () {
                            jQuery.event.trigger(name, null, elem, true);
                        });
                        jQuery._data(this, name, jQuery.guid++);
                    }
                    return false;
                },
                teardown: function () {
                    if (this !== document) {
                        jQuery.event.remove(document, name + "." + jQuery._data(this, name));
                    }
                    return false;
                }
            };
        }
    );
};

!$.browser && $jqm(jQuery, window);

//End Migrate

if (!window.PSWeb) window.PSWeb = {};
Date.prototype.format = function (mask) {
    var d = this;
    var zeroize = function (value, length) {
        if (!length) length = 2;
        value = String(value);
        for (var i = 0, zeros = ''; i < (length - value.length) ; i++) {
            zeros += '0';
        }
        return zeros + value;
    };
    return mask.replace(/\"[^\"]*\"|'[^']*'|\b(?:d{1,4}|m{1,4}|yy(?:yy)?|([hHMstT])\1?|[lLZ])\b/g, function ($0) {
        switch ($0) {
            case 'd': return d.getDate();
            case 'dd': return zeroize(d.getDate());
            case 'ddd': return ['Sun', 'Mon', 'Tue', 'Wed', 'Thr', 'Fri', 'Sat'][d.getDay()];
            case 'dddd': return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][d.getDay()];
            case 'M': return d.getMonth() + 1;
            case 'MM': return zeroize(d.getMonth() + 1);
            case 'MMM': return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][d.getMonth()];
            case 'MMMM': return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'][d.getMonth()];
            case 'yy': return String(d.getFullYear()).substr(2);
            case 'yyyy': return d.getFullYear();
            case 'h': return d.getHours() % 12 || 12;
            case 'hh': return zeroize(d.getHours() % 12 || 12);
            case 'H': return d.getHours();
            case 'HH': return zeroize(d.getHours());
            case 'm': return d.getMinutes();
            case 'mm': return zeroize(d.getMinutes());
            case 's': return d.getSeconds();
            case 'ss': return zeroize(d.getSeconds());
            case 'l': return zeroize(d.getMilliseconds(), 3);
            case 'L': var m = d.getMilliseconds();
                if (m > 99) m = Math.round(m / 10);
                return zeroize(m);
            case 'tt': return d.getHours() < 12 ? 'am' : 'pm';
            case 'TT': return d.getHours() < 12 ? 'AM' : 'PM';
            case 'Z': return d.toUTCString().match(/[A-Z]+$/);
            default: return $0.substr(1, $0.length - 2);
        }
    });
};
Number.prototype.format = function (mask, local, group) {
    var nm = this, reg = /([0]+)(\.[0]+)*/g;
    var fs = reg.exec(mask);
    var dsl = fs[0].length - fs[0].indexOf(".") - 1;
    nm = nm.toFixed(dsl);
    var ds = (local && HOYI.numFMT) ? (HOYI.numFMT.numDS || ".") : ".";
    var gs = (HOYI.numFMT ? HOYI.numFMT.numGS || "," : ",");
    var ns = nm.toString();
    if (group) {
        var ag = ns, nn = "", dci = ns.indexOf(".");
        if (dci > 0) {
            ag = ns.substr(0, dci);
            nn = ns.substring(dci + 1);
        }
        var re = new RegExp("(\\d+)(\\d{3})");
        while (re.test(ag)) {
            ag = ag.replace(re, "$1" + gs + "$2");
        }
        ns = ag + ds + nn;
    }
    return mask.replace(fs[0], ns);
};
Array.prototype.copy = function () {
    var cpArray = [];
    for (var a = 0; a < this.length; a++) {
        if ($.type(this[a]) == "object")
            cpArray.push($.extend({}, this[a]));
        else
            cpArray.push(this[a])
    }
    return cpArray;
};
Array.prototype.add = function (data) {
    if (data) {
        if ($.type(data) == "array") {
            for (var n = 0; n < data.length; n++) {
                this.push(data[n]);
            }
        }
        else
            this.push(data);
    }
    return this;
};
Array.prototype.remove = function (itm, pp) {
    if (!itm) return;
    for (var n = 0; n < this.length; n++) {
        var ni = this[n];
        if (ni == itm || (pp && ni && ni[pp] == itm[pp])) {
            this.splice(n, 1);
            break;
        }
    }
};
Array.prototype.clear = function () {
    return this.splice(0, this.length);
};

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (a) {
        for (var i = 0, al = this.length; i < al; i++) {
            if (this[i] == a)
                return i;
        }
        return -1;
    }
};
Array.prototype.concatAsHtmlText = function (cnc) {
    var txt = "";
    for (var a = 0; a < this.length; a++) {
        if (txt.length > 0)
            txt += cnc;
        txt += (this[a] || "").toString().encodeHtml();
    }
    return txt;
};
if (!String.prototype.trim) {
    String.prototype.trim = function () {
        var reExtraSpace = /^\s*(.*?)\s+$/;
        return this.replace(reExtraSpace, "$1");
    };
}
String.prototype.encodeHtml = function (noAnd) {
    return Util.escapeXmlChars(this, noAnd);
};
if (!Function.prototype.bind) {//For IE No bind method
    Function.prototype.bind = function (t) {
        var bFn = function (fn, owner) {
            var _fn = fn;
            var _owner = owner;
            return function () { return _fn.apply(_owner, arguments); };
        };
        return bFn(this, t);
    };
};
String.prototype.fbScript = function () {
    var sr = /((<)script.*?(>))|((<)\/script(>))/ig;
    return this.replace(sr, function ($0) {
        return ($0 || "").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    });
};
(function ($) {
    $.toJSON = function (o) {
        if (typeof (JSON) == 'object' && JSON.stringify)
            return JSON.stringify(o);
        var type = typeof (o);
        if (o === null) return "null";
        if (type == "undefined") return undefined;
        if (type == "number" || type == "boolean") return o + "";
        if (type == "string") return $.quoteString(o);
        if (type == 'object') {
            if (typeof o.toJSON == "function")
                return $.toJSON(o.toJSON()); if (o.constructor === Date) {
                    var month = o.getUTCMonth() + 1; if (month < 10) month = '0' + month; var day = o.getUTCDate(); if (day < 10) day = '0' + day; var year = o.getUTCFullYear(); var hours = o.getUTCHours(); if (hours < 10) hours = '0' + hours; var minutes = o.getUTCMinutes(); if (minutes < 10) minutes = '0' + minutes; var seconds = o.getUTCSeconds(); if (seconds < 10) seconds = '0' + seconds; var milli = o.getUTCMilliseconds(); if (milli < 100) milli = '0' + milli; if (milli < 10) milli = '0' + milli; return '"' + year + '-' + month + '-' + day + 'T' +
                    hours + ':' + minutes + ':' + seconds + '.' + milli + 'Z"';
                }
            if (o.constructor === Array) {
                var ret = []; for (var i = 0; i < o.length; i++)
                    ret.push($.toJSON(o[i]) || "null"); return "[" + ret.join(",") + "]";
            }
            var pairs = []; for (var k in o) {
                var name;
                type = typeof k;
                if (type == "number")
                    name = '"' + k + '"'; else if (type == "string")
                        name = $.quoteString(k); else
                        continue; if (typeof o[k] == "function")
                            continue; var val = $.toJSON(o[k]); pairs.push(name + ":" + val);
            }
            return "{" + pairs.join(", ") + "}";
        }
    };
    $.evalJSON = function (src) {
        try {
            if (typeof (JSON) == 'object' && JSON.parse)
                return JSON.parse(src);
            return eval("(" + src + ")");
        }
        catch (ex) {
            console && console.log && console.log(ex);
            return src;
        }
    };
    $.secureEvalJSON = function (src) {
        if (typeof (JSON) == 'object' && JSON.parse)
            return JSON.parse(src); var filtered = src; filtered = filtered.replace(/\\["\\\/bfnrtu]/g, '@'); filtered = filtered.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']'); filtered = filtered.replace(/(?:^|:|,)(?:\s*\[)+/g, ''); if (/^[\],:{}\s]*$/.test(filtered))
                return eval("(" + src + ")"); else
                throw new SyntaxError("Error parsing JSON, source is not valid.");
    };
    $.quoteString = function (string) {
        if (string.match(_escapeable)) {
            return '"' + string.replace(_escapeable, function (a)
            { var c = _meta[a]; if (typeof c === 'string') return c; c = a.charCodeAt(); return '\\u00' + Math.floor(c / 16).toString(16) + (c % 16).toString(16); }) + '"';
        }
        return '"' + string + '"';
    };
    $.isLogTimeOut = function (xhr) {
        var rpCntType = xhr.getResponseHeader("content-Type") || "";
        if (rpCntType.indexOf("text/html") >= 0) {
            var repText = xhr.responseText;
            return (repText.indexOf("login_cookie.js") > 0 || repText.indexOf("AwsTimeout") > 0)
        }
        return false;
    };
    var _escapeable = /["\\\x00-\x1f\x7f-\x9f]/g; var _meta = { '\b': '\\b', '\t': '\\t', '\n': '\\n', '\f': '\\f', '\r': '\\r', '"': '\\"', '\\': '\\\\' };


    $.extend({
        highlight: function (node, re, nodeName, className) {
            if (node.nodeType === 3) {
                var match = node.data.match(re);
                if (match) {
                    var highlight = document.createElement(nodeName || 'span');
                    highlight.className = className || 'highlight';
                    var wordNode = node.splitText(match.index);
                    wordNode.splitText(match[0].length);
                    var wordClone = wordNode.cloneNode(true);
                    highlight.appendChild(wordClone);
                    wordNode.parentNode.replaceChild(highlight, wordNode);
                    return 1; //skip added node in parent
                }
            } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
                for (var i = 0; i < node.childNodes.length; i++) {
                    i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
                }
            }
            return 0;
        }
    });

    $.fn.unhighlight = function (options) {
        var settings = { className: 'highlight', element: 'span' };
        jQuery.extend(settings, options);

        return this.find(settings.element + "." + settings.className).each(function () {
            var parent = this.parentNode;
            parent.replaceChild(this.firstChild, this);
            parent.normalize();
        }).end();
    };

    $.fn.highlight = function (words, options) {
        var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
        jQuery.extend(settings, options);

        if (words.constructor === String) {
            words = [words];
        }
        words = jQuery.grep(words, function (word, i) {
            return word != '';
        });
        words = jQuery.map(words, function (word, i) {
            return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
        });
        if (words.length == 0) { return this; };

        var flag = settings.caseSensitive ? "" : "i";
        var pattern = "(" + words.join("|") + ")";
        if (settings.wordsOnly) {
            pattern = "\\b" + pattern + "\\b";
        }
        var re = new RegExp(pattern, flag);

        return this.each(function () {
            jQuery.highlight(this, re, settings.element, settings.className);
        });
    };

})(jQuery);

(function () {
    var tuchMv = false;
    var tuhBgTm = 0, startY = 0;
    //HOYI.isTouchDev = true;//for debug toch function in pc
    window.HOYI.touch = {
        support: function () {
            if (HOYI.isTouchDev == undefined) {
                var isThDev = function () {
                    try {
                        document.createEvent("TouchEvent");
                        return true;
                    } catch (e) {
                        return false;
                    }
                };
                HOYI.isTouchDev = isThDev();
            }
            return HOYI.isTouchDev;
        },
        bind: function (elObj, evtName, handler) {
            if (!HOYI.touch.support()) return;
            var jel = elObj.ajaxSuccess ? elObj : $("#" + elObj);
            if (jel.length == 0) return;
            var el = jel[0];
            el.addEventListener(evtName, handler, false);
        },
        ubund: function (elObj, evtName, handler) {
            if (!HOYI.touch.support()) return;
            var jel = elObj.ajaxSuccess ? elObj : $("#" + elObj);
            if (jel.length == 0) return;
            var el = jel[0];
            el.removeEventListener(evtName, handler, false);
        },
        touchStart: function (elObj, handler) {
            if (!HOYI.touch.support()) return;
            HOYI.touch.bind(elObj, "touchstart", function (e) {
                startY = event.touches[0].pageY;
                var nw = (new Date()).getTime();
                var isDbl = (nw - tuhBgTm) <= 500;
                tuhBgTm = nw;
                handler.call(this, e, isDbl);
            });
        },
        touchMove: function (elObj, handler) {
            if (!HOYI.touch.support()) return;
            HOYI.touch.bind(elObj, "touchmove", function (e) {
                var endY = event.touches[0].pageY;
                var isMv = Math.abs(startY - endY) >= 3;
                if (isMv) tuchMv = true;
                if (handler && isMv)
                    handler.call(this, e);
            });
        },
        touchEnd: function (elObj, handler) {
            if (!HOYI.touch.support()) return;
            HOYI.touch.bind(elObj, "touchend", function (e) { handler.call(this, e); tuchMv = false; });
        },
        touchEnd2Click: function (elObj) {
            if (!HOYI.touch.support()) return;
            HOYI.touch.touchEnd(elObj, function (e) {
                if (!tuchMv) {
                    var isTxtNd = (e.target["nodeName"] == "#text"), tg = e.target;
                    $(isTxtNd ? tg.parentNode : tg).trigger("click");
                }
            });
        },
        touch2DblClick: function (elObj, end2clk) {
            if (!HOYI.touch.support()) return;
            HOYI.touch.touchStart(elObj, function (e, isDbl) {
                if (isDbl) {
                    var isTxtNd = (e.target["nodeName"] == "#text"), tg = e.target;
                    $(isTxtNd ? tg.parentNode : tg).trigger("dblclick");
                }
            });
            if (end2clk) HOYI.touch.touchEnd2Click(elObj);
            HOYI.touch.touchMove(elObj);
        }
    };
})();
(function ($) {
    $.cookie = function (name, value, options) {
        if (!HOYI.cookies) { HOYI.cookies = {} }
        if (typeof value != 'undefined') {
            if ($.type(value) == "array") {
                var ckv = value.join("||");
                $.cookie(name, "", options);
                $.cookie.append(name, ckv, options);
                return;
            }
            options = options || {};
            if (value === null) {
                value = '';
                options.expires = -1;
            }
            var expires = '';
            if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                var date;
                if (typeof options.expires == 'number') {
                    date = new Date();
                    date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                } else {
                    date = options.expires;
                }
                expires = '; expires=' + date.toUTCString();
            }
            var path = options.path ? '; path=' + options.path : '';
            var domain = options.domain ? '; domain=' + options.domain : '';
            var secure = options.secure ? '; secure' : '';
            document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
            HOYI.cookies[name] = value;
        } else {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
    };
    $.cookie.append = function (name, value, options) {
        var eck = $.cookie(name);
        if (eck && eck.length > 0)
            eck += "||" + value;
        else
            eck = value;
        $.cookie(name, eck, options);
    };
    $.cookie.gtc = function (name, value, options) {
        var eck = $.cookie(name);
        if (eck)
            return eck.split(/\|\|/g);
        else
            return [];
    };

})(jQuery);

HOYI.triggerReloadPage = function () {
    var cl = window.location;
    var nw = cl.protocol + "//" + cl.host + cl.pathname + cl.search + "&_trld=1";
    window.location.href = nw;
};

(function () {
    HOYI.com = { fstShtType: 878 };
    HOYI.hisHandlers = { "hisEvt": false, "hasInit": false };
    if (!HOYI.msgs) HOYI.msgs = {};
    HOYI.bindHds = {};
    HOYI.msgs.getMsg = function (msgNm, def) { return HOYI.msgs[msgNm] || def || msgNm };
    window.$msg = HOYI.msgs.getMsg;
    var au = "";
    HOYI.initBtn = function (con) {
        con.find(".innerDiv").each(function () {
            $(this).mouseover(function () { $(this).addClass("hoverDiv"); })
			                                            .mouseout(function () { $(this).removeClass("hoverDiv"); })
        })
        HOYI.initInputBtn(con);
    };
    HOYI.initInputBtn = function (con, fdlg) {
        var isIE8 = HOYI.isIEx(8);
        var ie7 = HOYI.isIEx(7, true);
        var cp = (!fdlg && con && con.length) ? con[0].parentNode.parentNode : null;
        var rds = cp && isIE8 && cp.className && cp.className.indexOf("buttondiv") >= 0;
        if (rds) $(cp).css({ position: "relative", zIndex: 0 });
        $("input:button", con).each(function () {
            var jme = $(this);
            var valLen = jme.val().length;
            if (jme.hasClass("schbtn")) return;
            if (jme.hasClass("stdbtn")) {
                jme.mousedown(function () {
                    $(this).addClass("stdbtnclick");
                });
                jme.mouseup(function () {
                    $(this).removeClass("stdbtnclick");
                });
                jme.mouseout(function () {
                    $(this).removeClass("stdbtnclick");
                });
                (ie7 && jme.width() < 60 && jme.attr("autoAdj") !== "0") && jme.css("width", valLen <= 10 ? 80 : valLen * 9);
                rds && HOYI.ie8Radius(this);
            }
            else if (ie7 && valLen > 6) {
                jme.css({ "paddingLeft": "4px", "paddingRight": "4px" });
                jme.css("width", valLen <= 10 ? 100 : valLen * 9);
            }
        });
    };
    HOYI.ajaxHis = {
        hisIndex: [],
        hisData: {},
        init: function () {
            if (!HOYI.hisHandlers.hasInit && window.dhtmlHistory) {
                HOYI.hisHandlers.hasInit = true;
                window.dhtmlHistory.create({ debugMode: false });
                dhtmlHistory.initialize();
                var historyChange = function (an, d) {
                    if (!d || $.isEmptyObject(d)) d = HOYI.ajaxHis.hisData[an];
                    HOYI.hisHandlers.hisEvt = true;
                    var link = HOYI.hisHandlers["_handlerLink"];
                    link && link.call(null, arguments);
                    try { HOYI.hisHandlers[an] && HOYI.hisHandlers[an](an, d); } catch (x) { }
                    HOYI.hisHandlers.hisEvt = false;
                }
                dhtmlHistory.addListener(historyChange);
            }
        },
        hisHandler: function (hd) {
            var link = HOYI.hisHandlers["_handlerLink"];
            if (!link) link = HOYI.hisHandlers["_handlerLink"] = new PS.funLinked();
            link.reg(hd, this);
        },
        regHandler: function (n, f) {
            this.init();
            HOYI.hisHandlers[n] = f;
        },
        preRemember: function (hd) {
            var link = HOYI.hisHandlers["_preRememberLink"];
            if (!link) link = HOYI.hisHandlers["_preRememberLink"] = new PS.funLinked();
            link.reg(hd, this);
        },
        remember: function (n, d, unHash) {
            if (HOYI.hisHandlers.hasInit && !HOYI.hisHandlers.hisEvt) {
                if (HOYI.ajaxHis.hisIndex.length >= 1000)
                    this.reset();
                var link = HOYI.hisHandlers["_preRememberLink"];
                link && link.call(null, arguments);
                this.hisData[n] = d;
                if (!unHash) dhtmlHistory.add(n, d);
                this.hisIndex.push(n);
            }
        },
        reset: function () {
            this.hisIndex = [];
            this.hisData = {};
            if (HOYI.hisHandlers.hasInit) window.historyStorage.reset();
        },
        isHis: function () { return HOYI.hisHandlers.hisEvt; },
        fireHis: function (an) {
            var ixs = HOYI.ajaxHis.hisIndex;
            var anep = new RegExp(an);
            var h = ixs.length - 1;
            for (; h >= 0; h--) {
                if (anep.test(ixs[h])) {
                    dhtmlHistory.fireHistoryEvent(ixs[h]);
                    return true;
                }
            }
            return false;
        },
        getHisData: function (n) {
            if (historyStorage)
                return window.historyStorage.get(n) || this.hisData[n];
            else
                return null;
        }
    };
    function txtareaML() {
        $("textarea[maxlength!='']").keydown(
            function (e) {
                if (HOYI.noEventKeyCode(e.keyCode))
                    return true;
                var jt = $(e.target);
                var maxL = parseInt(jt.attr("maxlength"));
                if (jt.val().length >= maxL) {
                    e.stopPropagation();
                    return false;
                }
                return true;
            }
        );
    };
    function getCurrentEmail() {
        return $(".topzone .userinfo span:first").text() || "";
    };

    HOYI.initImgs = function (brwPth) {
        try {
            var imgUrls = ["img/topbg.gif", "img/topbtn1bg.gif",
                    "img/btndiv_bg.gif", "img/ico_sprite.gif", "img/btn_bg.gif", "img/bg_grid_header.gif",
                    "img/bg_light_scroll_h.gif", "img/bg_light_scroll_v.gif", "img/bottom_bg.gif", "img/msredlogo.gif",
                    "img/close_gray.gif", "img/ui-anim_basic_16x16.gif", "img/fm_sprite.png", "img/mglass.png"];
            for (var i = 0; i < imgUrls.length; i++) {
                var img = new Image();
                img.src = brwPth + imgUrls[i];
            }
        }
        catch (exp) {
        }
    };
    var zoomMonitor = {
        init: function () {
            if (this.scr) return;
            this.scr = { w: window.screen.width, h: window.screen.height };
            this.layouRSL = $mo({}, this.scr);
            this.rsz = $mo({}, this.scr);
            if (HOYI.isIEx(7, true))
                window.setInterval(function () { zoomMonitor.handle(); }, 500);
            else
                $(window).resize(function () { zoomMonitor.handle(); });
        },
        reload: function () {
            this.rldTm = 0;
            var hf = document.location.href;
            document.location.href = new String(hf);
            HOYI.curResolution = this.rsz;
        },
        handle: function () {
            var rsz = { w: window.screen.width, h: window.screen.height };
            var osz = this.rsz;
            if (rsz.w != osz.w || rsz.h != osz.h) {
                this.rsz = rsz;
                var lysz = this.layouRSL;
                this.onZoom && this.onZoom(rsz, osz);
                if (rsz.w > lysz.w || rsz.h > lysz.h) {
                    document.body.style.display = "none";
                    var rld = this.reload.bind(this);
                    if (this.rldTm != 0) {
                        window.clearTimeout(this.rldTm);
                        this.rldTm = 0;
                    }
                    this.rldTm = window.setTimeout(rld, 50);
                }
            }
        },
        onZoom: function (cze, osz) { return false; }
    };
    function initMainHeight() {
        var jbd = null;
        var jctn = $(".content:first");

        if (!HOYI.totalWidth) {
            jbd = $(document.body);
            var tw = jbd.outerWidth();
            var psr = (PS.forPSBatch || HOYI.touch.support() || HOYI.isMobileStyle() || HOYI.pdc == "PSRPTADMIN") ? 1 : 0.9;
            tw = Math.min(1600, Math.max(parseInt(tw * psr), PS.forPSBatch ? 600 : 1024)) - 2;
            jbd.width(tw);
            HOYI.pageWidth = tw;
            HOYI.totalWidth = tw - (jctn.outerWidth() - jctn.width());
            if (!HOYI.isMobileStyle()) zoomMonitor.init();
        }
        var mnLy = $(".main:first");
        var jbtm = $("#pswebbtm");

        if (jbtm && jbtm.length) {
            if (jbtm[0].parentNode.className == "main") {
                jbtm.appendTo($(mnLy[0].parentNode));
            }
            var de = document.documentElement;
            var docH = (de && de.clientHeight) ? de.clientHeight : $(document).height();
            var cnOfs = jctn.offset();
            var btmOH = jbtm.outerHeight(true);
            var mpH = jctn.outerHeight() - jctn.height();
            var cnHght = Math.max(HOYI.isMobileStyle() ? 750 : 725, docH - cnOfs.top - btmOH - mpH - 1);
            HOYI.contentHeight = cnHght;
            jctn.height(cnHght);
        }
        jbd && jbd.addClass("lfrgborder");
        return jctn;
    };
    HOYI.resetMnHgt = initMainHeight;
    window.HOYI.getDataJsonObj = function (container) {
        var objCon = $(container);
        var inputs = $(":input", objCon);
        var jsonObj = {};
        for (var i = 0; i < inputs.length; i++) {
            var jInpt = $(inputs[i]);
            if (jInpt.attr("disabled"))
                continue;
            var dn = jInpt.attr("dataname") || jInpt.attr("name");
            if (!dn)
                continue;
            var v = null;
            if (inputs[i].tagName == "INPUT" || inputs[i].tagName == "input") {
                var it = inputs[i].type.toLowerCase();
                if (it == "button")
                    continue;
                if ((it == "radio" || it == "checkbox") && !inputs[i].checked)
                    continue;
                v = jInpt.attr("factVal") || jInpt.val();
                var regExp = jInpt.attr("reg");
                if (regExp) {
                    var reg = new RegExp(regExp);
                    if (!reg.test(v)) {
                        var msgKey = jInpt.attr("msg") || "cantbeempty";
                        var msg = $msg(msgKey).replace("{field}", $msg("requiredfield"))
                        if (HOYI.showTipMsg(msg))
                            jInpt.focus();
                        else
                            alertMsg(msg, jInpt, false, true);
                        return null;
                    }
                }
            }
            else if (inputs[i].tagName == "SELECT" || inputs[i].tagName == "select") {
                var jsel = jInpt;
                var jselOps = jsel.find("option");
                var a = new Array();
                for (var op = 0; op < jselOps.length; op++) {
                    a.push({ "value": jselOps[op].value, "selected": jselOps[op].selected });
                }
                v = a;
            }
            else {
                v = jInpt.val();
            };
            jsonObj[dn] = v;
        }
        return jsonObj;
    };
    window.HOYI.initDataContainer = function (jsonObj, container) {
        var objCon = $(container);
        var inputs = $(":input", objCon);
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].type && inputs[i].type == "button")
                continue;
            var jInpt = $(inputs[i]);
            if (jInpt.attr("noupdate")) { jsonObj ? jInpt.attr("readonly", "readonly") : jInpt.removeAttr("readonly") };
            var t = jsonObj ? jsonObj[jInpt.attr("dataname") || jInpt.attr("name")] : "";
            jInpt.val(t);
            //if (jInpt[0].ddlObj) jInpt[0].ddlObj.select(jInpt.val());
        };
        return $.toJSON(jsonObj);
    };
    /*function ensureAU() {
        if (au.length == 0)
            au = "data/batchservice.aspx?ProductCode=" + HOYI.pdc + "&action=";
    };
    window.HOYI.post = function (action, data, callback, trigger, errHdl, noProg) {
        ensureAU();
        var url = au + action;
        window.HOYI.request(url, data, callback, trigger, errHdl, noProg);
    };
    window.HOYI.request = function (url, data, callback, trigger, errHd, noProg) {
        var pbMsg = $msg("loading"), tg = null;
        var func = function () {
            if (!noProg) {
                HOYI.progressBar.setText(pbMsg)
                HOYI.progressBar.show();
            }
            var datatype = "json";
            $("#maskLayer").css("display", "block");
            if (typeof (data) == "object")
                data = $.toJSON(data);
            $.post(url, data, function (d) {
                try {
                    if (d) {
                        if (d.error) {
                            HOYI.progressBar.close();
                            alertMsg(d.message, tg ? tg : null);
                            errHd && errHd(d);
                        }
                        else {
                            callback && callback(d, data);
                        }
                    }
                    else {
                        var cnl = arguments[2].getResponseHeader("Content-Length");
                        if (parseInt(cnl) == 0) {
                            window.location.href = "/Logout/awsLogout.aspx?ProductCode=" + HOYI.pdc;
                        }
                    }
                }
                catch (exp) {
                    alertMsg(exp.toString(), null, null, true);
                }
                if (!noProg) {
                    HOYI.progressBar.close();
                }
            }, datatype);
        };
        var cfmKey = null;
        if (trigger) {
            tg = $(trigger.target || trigger);
            var mgKey = trigger.stmsg || tg.attr("stmsg") || tg.attr("name");
            pbMsg = $msg(mgKey);
            pbMsg = (pbMsg == mgKey) ? $msg("loading") : pbMsg;
            cfmKey = tg[0].cfmkey || (tg[0].tagName ? tg.attr("cfmkey") : null);
        }
        if (cfmKey)
            HOYI.confirm(cfmKey, func, null);
        else
            func();
    };*/
    var messgers = [];
    function alertMsg(msg, org, delay, notKey) {
        var msgr = null;
        if (messgers.length > 0)
            msgr = messgers.pop();
        else
            msgr = new HOYI.messager();
        msgr.onClosed = function () {
            if (messgers.indexOf(msgr) < 0) messgers.push(msgr);
            if (org) {
                if (org.select)
                    org.select();
                else
                    $(org).focus();
            }
        };
        if (delay)
            window.setTimeout(function () { alertMsg(msg, org, false, notKey); }, delay);
        else
            msgr.showMsg(notKey ? msg : $msg(msg));
        return msgr;
    };
    var confirms = [];
    HOYI.confirm2 = function (msg, yesFun, noFun, cancelFn, notKey) {
        var cfmer = null;
        if (confirms.length > 0)
            cfmer = confirms.pop();
        else
            cfmer = new HOYI.messager({});
        var opt = {};
        if (yesFun || noFun || cancelFn) {
            opt.YF = yesFun;
            opt.NF = noFun;
            opt.CF = cancelFn;
            opt.onClosed = null;
        }
        cfmer.onClosed = function () { confirms.push(cfmer); };
        cfmer.showMsg(notKey ? msg : $msg(msg), opt);
        return cfmer;
    };
    HOYI.confirm = function (msg, yesFun, noFun, notKey) {
        HOYI.confirm2(msg, yesFun, noFun, null, notKey);
    };
    function listData(url, args, dtgd, pager, callback, completed, tg, preReq) {
        if (pager && pager.setPageParam) {
            url += "?" + pager.setPageParam();
        }
        var cb = function (data) {
            if (!data.d)
                return false;
            dtgd.gridData = null;
            if (callback)
                callback(data, args);
            updateGridData(dtgd, data.d.List);
            if (pager)
                pager.setPageLabel(data.d, dtgd.listData || HOYI.listGrid, "");
            if (completed)
                completed(dtgd, data);
        };
        args =  args || {};
        var param = { pager: { "Order": dtgd.lastSortOrder || args.Order || "ASC", "OrderName": dtgd.lastSortCellKey || args.OrderName || dtgd.cfg.column[dtgd.cfg.checkBoxButton ? 1 : 0].dataIndex } };
        $mo(param, args);
        delete param.Order;
        delete param.OrderName;
        preReq && preReq(url, param, pager ? pager.nextPage || 1 : 1);
        HOYI.callService(url, param, cb);
    };
    function mergeObj(dest, obj, deep) {
        var darray = null;
        if ($.type(dest) == "array")
            darray = dest;
        else
            darray = [dest];
        for (var n = 0; n < darray.length; n++) {
            var destObj = darray[n];
            if (!destObj)
                continue;
            for (var pn in obj) {
                var opnv = obj[pn];
                var tpn = $.type(opnv);
                if (tpn == "string" || tpn == "number" || tpn == "array" || tpn == "boolean") {
                    var ov = obj[pn];
                    destObj[pn] = (ov && ov.copy) ? ov.copy() : ov;
                }
                else if (deep && tpn == "object") {
                    destObj[pn] = mergeObj({}, obj[pn], deep);
                }
            }
        }
        return dest;
    };
    function mergeObj2(dest, obj) {
        return $.extend(dest || {}, obj);
    };
    function updateGridData(dtgd, data, isupd) {
        if (!data)
            return false;
        if (dtgd && dtgd.reBindData) {
            var rk = dtgd.cfg.indexColKey;
            var isUpd = false, m = 0;
            if (dtgd.gridData) {
                for (m = 0; m < data.length; m++) {
                    for (var n = 0; n < dtgd.gridData.length; n++) {
                        if (dtgd.gridData[n][rk] == data[m][rk])
                            mergeObj(dtgd.gridData[n], data[m]);
                    }
                }
                dtgd.reBindData(dtgd.gridData);
            }
            else {
                dtgd.reBindData(data);
                dtgd.gridData = data;
            }
            if (dtgd.setDGCellKeyAndOrder) dtgd.setDGCellKeyAndOrder();
            if (isupd && dtgd.selectRow) {
                for (m = 0; m < data.length; m++) {
                    dtgd.selectRow(data[m][rk], null);
                }
            }
        }
    };
    function insertGridData(dtgd, data) {
        if (!dtgd.gridData) {
            updateGridData(dtgd, data);
        }
        else {
            var ln = data.length;
            for (var n = 0; n < dtgd.gridData.length; n++) {
                data.push(dtgd.gridData[n]);
            }
            dtgd.gridData = null;
            updateGridData(dtgd, data);
            for (var l = 0; l < ln; l++) {
                var rk = dtgd.cfg.indexColKey;
                dtgd.selectRow(data[l][rk], null);
            }
        }
    };
    function deleteGridRow(dtgd, data) {
        var rk = dtgd.cfg.indexColKey;
        var dels = [], gda = dtgd.gridData;
        var n = 0, m = 0, dl = 0, ln = 0;
        for (; n < data.length; n++) {
            var k = data[n][rk];
            dtgd.removeRow(k);
            for (m = 0, dl = gda.length; m < dl; m++) {
                if (data[n] == gda[m] || data[n][rk] == gda[m][rk]) {
                    dels.push(m);
                    break;
                }
            }
        }
        for (n = 0, ln = dels.length; n < ln; n++) {
            delete gda[dels[n]]
        }
        var nw = [];
        for (d = 0, ln = gda.length; d < ln; d++) {
            if (gda[d]) nw.push(gda[d]);
        }
        dtgd.reBindData(nw);
        dtgd.gridData = nw;
    };
    function getGridSelectedData(grid) {
        var lst = new Array();
        for (var prop in grid.selectedRows) {
            if (prop) {
                var d = grid.rows[prop].data;
                if (d) lst.push(d);
            }
        }
        return lst;
    };
    function getGridDataById(grid, id) {
        if (grid.gridData) {
            var rk = grid.cfg.indexColKey;
            return getDataById(grid.gridData, id, rk);
        }
        return null;
    };
    function getDataById(data, id, keyName) {
        if (data) {
            for (var n = 0; n < data.length; n++) {
                if (data[n] && ((keyName ? data[n][keyName] : data[n]) == id)) {
                    return data[n];
                    break;
                }
            }
        }
        return null;
    }
    function getGridOne(grid) {
        var data = getGridSelectedData(grid);
        if (data.length > 1) {
            alertMsg("selectedmorethanone");
            return null;
        }
        else if (data.length == 0) {
            alertMsg("pleaseselectarow");
            return null;
        }
        return data[0];
    };
    function setTab(evt) {
        var tg = $(evt.target);
        var tbc = tg.parents("div .pstab", "*");
        if (tbc.length > 0) {
            var jtbc = $(tbc[0]);
            var lis = tbc.find("li");
            var cntPf = jtbc.attr("tbcontainer");
            for (var l = 0; l < lis.length; l++) {
                var jl = $(lis[l]);
                jl.removeClass("hover");
                if (cntPf) {
                    $("#" + cntPf + (l + 1)).hide();
                }
            }
            if (tg[0].tagName == "A")
                tg.parent().addClass("hover")
            else
                tg.addClass("hover");

            if (evt.data && evt.data.length) {
                var tb = evt.data[0];
                var bc = $("#" + cntPf + tb);
                bc.css("display", "inline-block");
                /*if ($.browser.msie) {
                bc.css("zoom", "0");
                bc.css("zoom", "1");
                }*/
            }
        }
        return tg;
    };
    function switchTab(tbCtn, tb) {
        var tbc = $(tbCtn);
        var tbs = tbc.find(".pstab").find("li");
        var hd = null;
        for (var ti = 0; ti < tbs.length; ti++) {
            var tbi = tbs[ti], jtb = $(tbi);
            if (!tbi["pstbt"]) {
                if (!hd) hd = function (a, d) { $swt(tbCtn, d); };
                var anch = jtb.find("a"), ckHd = null;
                if (anch.length > 0 && anch.attr("href").indexOf("#") == 0) {
                    var aor = anch.attr("href").substring(1);
                    anch.attr("href", "javascript:");
                    ckHd = function (ev) {
                        HOYI.ajaxHis.remember(ev.data[1], ev.data[0] - 1);
                        $st(ev);
                    };
                    HOYI.ajaxHis.regHandler(aor, hd);
                }
                else
                    ckHd = $st;
                jtb.click([ti + 1, aor], ckHd);
                tbi["pstbt"] = true;
            }
            if (ti == tb)
                jtb.trigger("click");
        }
    };
    function selectData(data, keyName, key) {
        for (var d = 0; d < data.length; d++) {
            if (data[d][keyName] == key)
                return data[d];
        }
        return null;
    }
    function initDgTablelayout(tab, data, hd, rowId, hideHD, dataSetted, defVal) {
        //hd:[{"caption","fieldName" }]
        //temp:[{type:"label"},{type:"radio",captions["","",""],values["","",""]}]
        var jtab = $(tab);
        if (!jtab.hasClass("dgtablyt"))
            jtab.addClass("");
        $(tab).find("tr").remove();
        if (!hideHD) {
            $(tab).append("<tr></tr>");
            var jhd = $("tr:last", jtab).addClass("hd");
            for (var n = 0; n < hd.length; n++) {
                var jtd = $("td:last", jhd.append("<td></td>"));
                if (n == 0)
                    jtd.css("border-left-style", "none");
                else if (n == (hd.length - 1))
                    jtd.css("border-right-style", "none");
                jtd.text(Util.escapeXmlChars($msg(hd[n].caption)));
            }
        }
        var createTd = function (jdtr, t, d) {
            var jdtd = $("td:last", jdtr.append("<td></td>"));
            if (hd[t].style)
                jdtd.css(hd[t].style);
            if (t == 0) {
                jdtd.css("border-left-style", "none");
                $("<input type='hidden'/>").attr("name", rowId).appendTo(jdtd).val(data[d][rowId]);
            }
            else if (t == (hd.length - 1))
                jdtd.css("border-right-style", "none");
            return jdtd;
        };
        for (var d = 0; d < data.length; d++) {
            var jdtr = $("tr:last", jtab.append("<tr></tr>")).attr("valign", "middle");
            for (var t = 0; t < hd.length; t++) {
                var tp = hd[t].Template;
                var jdtd = (tp.type != "hidden") ? createTd(jdtr, t, d) : $("td:last", jdtr);
                var val = data[d][hd[t].fieldName];
                switch (tp.type) {
                    case "label":
                        jdtd.append(Util.escapeXmlChars(val));
                        break;
                    case "hidden":
                        jdtd.append($("<input type='hidden' dataname='{0}' name='{0}{1}'/>".replace(/\{0\}/g, hd[t].fieldName).replace(/\{1\}/g, d)).val(val));
                        break;
                    case "radio":
                        var rwState = {};
                        for (var c = 0; c < tp.captions.length; c++) {
                            var nm = hd[t].caption.replace(/\x20/g, "_");
                            var tmpId = nm + d + c;
                            var jrd = $("<input type='radio' name='" + nm + d + "' class='radst'/>").attr("id", tmpId);
                            jrd.attr("dataname", hd[t].fieldName).val(tp.values[c])
                            var jrdlb = $("<label />").attr("for", tmpId);
                            if (c != 0)
                                jrd.css("margin-left", "24px");
                            jrdlb.append(tp.captions[c]);
                            jrd.appendTo(jdtd);
                            jrdlb.appendTo(jdtd);
                            if (dataSetted) {
                                if (rowId) {
                                    var sd = selectData(dataSetted, rowId, data[d][rowId]);
                                    if (sd && sd[hd[t].fieldName] == tp.values[c]) {
                                        rwState[rowId] = tp.values[c];
                                        jrd.attr("checked", "checked");
                                    } else if (rwState[rowId] == undefined)
                                        jrd.attr("checked", "checked");
                                }
                            }
                            else if (defVal != undefined) {
                                if (defVal == tp.values[c])
                                    jrd.attr("checked", "checked");
                            }
                        }
                        break;
                }
            }
        }

        //HOYI.touchScrollBar($(tab[0].parentNode), true)();
    };
    function initSelectList(sel, data, cfg) {
        var jsel = $(sel);
        jsel.find("option").remove();
        for (var d = 0; d < data.length; d++) {
            var op = $("<option></option>");
            op.attr("value", data[d][cfg.valueField]);
            op.text(data[d][cfg.textField])
            op.attr("title", op.text());
            op.appendTo(jsel);
        };
    };
    function select2select(selfrom, selTo, isAll, isHide) {
        if (selfrom.getDataItems && selTo.clickHandler) {
            if (isAll) selfrom.selectAll();
            selTo.bind(selfrom.getDataItems(isHide), selfrom.mapping, null, true);
        }
        else {
            var jf = $(selfrom);
            var jt = $(selTo);
            var fops = jf.find("option");
            var tops = jt.find("option");
            var findMO = function (sel, val) {
                if (!sel.options)
                    return null;
                for (var n = 0; n < sel.options.length; n++) {
                    if (sel.options[n].value == val)
                        return sel.options[n];
                }
                return null;
            };
            for (var s = 0; s < fops.length; s++) {
                if (fops[s].selected || isAll) {
                    var jop = $(fops[s]);
                    var op = findMO(jt[0], jop.attr("value"));
                    if (!op) {
                        op = $("<option></option>");
                        op.attr("value", jop.attr("value"));
                        op.text(jop.text());
                        op.attr("title", jop.text());
                        op.appendTo(selTo);
                    }
                    else {
                        $(op).css("display", "");
                    }
                    if (isHide && !$.browser.msie)
                        jop.css("display", "none");
                    else
                        jop.remove();
                };
            }
        }
    };
    function getTableData(tab) {
        var jtab = $(tab);
        var dn = jtab.attr("dataname") || jtab.attr("name");
        if (!dn)
            return null;
        var data = new Array();
        jtab.find("tr").each(function (t) {
            var jtr = $(this);
            data.push($condata(jtr));
        });
        var res = {};
        res[dn] = data;
        return res;
    };
    function loadFileTypeMaps() {
        HOYI.FTMaps = [{ "Name": "Workbook", "TypeId": 798 }, { "Name": "Factsheet", "TypeId": 878 }, { "Name": "List", "TypeId": 60 }, { "Name": "Search Criteria", "TypeId": 61 }, { "Name": "Workbook", "TypeId": 924, "forBatch": true }, { "Name": "Factsheet", "TypeId": 925, "forBatch": true }, { "Name": "SavedFiles", "TypeId": 926, "forBatch": true }, { "Name": "Model Porfolio", "TypeId": 983 }, { "Name": "Blend", "TypeId": 982 }];
        $post("gfltps", {}, function (d) { HOYI.FTMaps = d.UIData }, { "stmsg": "initpage" });
    };
    function getFileTypeName(tpId) {
        if (HOYI.FTMaps) {
            var mps = HOYI.FTMaps;
            for (var f = 0; f < mps.length; f++) {
                if (mps[f]["TypeId"] == tpId) {
                    return mps[f]["Name"];
                }
            }
        }
        return tpId;
    };
    function setObjsFileTypeName(objs) {
        var ftId = 0;
        for (var t = 0; t < objs.length; t++) {
            ftId = objs[t].FileType;
            objs[t].FileTypeName = $ftn(ftId);
        }
        return objs;
    };
    function editGridCell(row, ckey, actNm, callback, check) {
        var ecell = row.cells[ckey];
        var cell = $(ecell.innerDom);
        if (cell.find("input").length > 0)
            return;
        var btext = cell.text();
        cell.text("");
        var input = $("<input type='text' maxlength='1000' value=\"\" stmsg='btnSave' style='width:100%;border:none;font-size:11px;font-family: tahoma,arial,verdana,sans-serif,\"Bitstream Vera Sans\";'>").appendTo(cell);
        input.height(cell.height() - 3);
        input.val(btext);
        input.focus();
        input[0].aclick = true;
        var editFun = function () {
            var ival = input.val();
            if (ival == btext) {
                cell.text(btext);
                input.remove();
                return;
            }
            input[0].aclick = false;
            input.unbind("blur", editFun);
            var ptd = { _psSource: "gcell" };
            mergeObj(ptd, row.data);
            ptd[ckey] = ival;
            var cbFn = function (d) {
                var er = (d && d.Result != undefined) ? d.Result : true;
                if (er) {
                    input.remove();
                    cell.text(ptd[ckey]);
                    cell.attr("title", ptd[ckey]);
                    row.data[ckey] = ptd[ckey];
                    callback && callback(ptd[ckey]);
                    row.esco = null;
                }
                else
                    input.focus();
            };
            var pas = check ? check(ival) : true;
            if (pas) {
                if (actNm) {
                    $post(actNm, ptd, cbFn, input[0], function (d) {
                        input.blur(blurFun);
                        if (!$.browser.msie) input[0].aclick = true;
                    });
                }
                else
                    cbFn();
            }
            else {
                input.focus();
                HOYI.selectText(input);
            }
        };
        var blurFun = function () { if (this.aclick) editFun(); };
        input.click(function () {
            this.aclick = true;
        });
        var esco = row.esco = {
            r: row, c: cell, otxt: btext, i: input, escEdit: function () {
                this.c.text(this.otxt);
                this.i.unbind("blur", editFun);
                this.i.remove();
                this.r.esco = null;
            }
        };
        input.blur(blurFun);
        input.keydown(function (e) {
            if (e.keyCode == 27)
                esco.escEdit();
            else if (e.keyCode == 13)
                editFun();
        });
        HOYI.selectText(input);
    };
    function gdDefHeaderDblClick(cellkey) {
        if (cellkey == "checkbox") return;
        var gd = this;
        var cols = gd.cfg.column;
        var col = $byId(cols, cellkey, "dataIndex");
        if (col.noSort)
            return;
        if (cellkey !== gd.lastSortCellKey) {
            gd._clearSort();
        }
        gd.setSortByCell(cellkey);
        if (gd.listData)
            gd.listData("", 1);
        else
            HOYI.listGrid("", 1);
    };
    HOYI.processUniv = function (data) {
        var q = HOYI.cq.queue, fun;
        if (data.ResultSet) {
            fun = q[data.ResultSet.jsid];
            if (fun) fun(data.ResultSet.result);
        }
        else {
            fun = q[data.m[0].j];
            if (fun) fun(data.m[0].r);
        }
    };
    HOYI.isIEx = function (x, lowTrue) {
        if ($.browser.msie) {
            var bs = $.browser;
            var v = bs.iver = (bs.iver ? bs.iver : parseInt($.browser.version));
            if (lowTrue) {
                return x >= v;
            }
            else {
                if (x <= 6)
                    return v <= x;
                else
                    return v == x;
            }
        }
        return false;
    };
    HOYI.isIE6 = function () {
        return HOYI.isIEx(6);
    };

    function singleSelectItem(sel) {
        var jsel = $(sel);
        jsel[0].opts = jsel.find("option:selected");
        jsel.mousedown(function (e) {
            jsel[0].mdown = true;
        });

        jsel.change(function (e) {
            jsel[0].opts.removeAttr("selected");
            if (jsel[0].lastOpt)
                $(jsel[0].lastOpt).attr("selected", true);
            jsel[0].opts = jsel.find("option:selected");
        });

        jsel.mousemove(function (e) {
            if (jsel[0].mdown) {
                jsel.find("option:selected").removeAttr("selected");
                e.stopPropagation();
                return false;
            }
        });
        jsel.keydown(function (e) {
            var opts = jsel.find("option:selected");
            if (opts.length > 1)
                jsel.find("option:selected").removeAttr("selected");
        });
        jsel.mouseup(function (e) {
            jsel[0].mdown = false;
        });
        jsel.mouseover(function (e) { if (e.target.tagName.toLowerCase() == "option") jsel[0].lastOpt = e.target; });
    };
    HOYI.down = function (nm, url, isReady) {
        var date = new Date();
        url = url + "&tzoffset=" + (date.getTimezoneOffset() * 60).toString();
        var dlIfm = $("#" + nm);
        if (!dlIfm || dlIfm.length == 0) dlIfm = $("<iframe id='" + nm + "' /></iframe>").css("display", "none").appendTo(document.body);
        var dlBody = dlIfm[0].contentWindow.document.body;
        if (dlBody) dlBody.innerHTML = "";
        if (!isReady) {
            HOYI.progressBar.setText($msg("Downloading"))
            HOYI.progressBar.show();
        }
        var fun = function (e) {
            if (!isReady) HOYI.progressBar.close();
            var idocument = dlIfm[0].contentWindow.document;
            var inHml = idocument.body ? idocument.body.innerHTML : "";
            if (inHml !== "") {
                if (inHml == "feedback")
                    HOYI.confirm("cfmdownrptsmsg", function () { HOYI.down(nm, url + "&feedback=true"); }, null);
                else if (inHml.indexOf("FILEREADY_") == 0 && !isReady) {
                    var dy = inHml.substring(inHml.indexOf("_") + 1);
                    HOYI.down(nm, "reportview.aspx?ProductCode=" + HOYI.pdc + "&vmode=ready&file=" + encodeURIComponent(dy), true);
                }
                else if (inHml == "interactivefail")
                    HOYI.down(nm, url.replace("interactive", "attch"), true);
                else
                    $aMsg(inHml);
            }
        };
        var hde = $.browser.msie ? "readystatechange" : "load";
        var hd = $.browser.msie ? function (e) {
            var rs = e.target.readyState;
            if (rs == "complete" || rs == "interactive")
                fun(e);
        } : fun;
        dlIfm.unbind(hde);
        dlIfm.bind(hde, hd);
        dlIfm.attr("src", url);
    };
    function dowloadRpt(rptId, dg, keyName) {
        var ids = [], rpts = [];
        if (dg) {
            for (var prop in dg.selectedRows) {
                ids.push(prop.length == 36 ? prop : rptId = dg.rows[prop].data[keyName || dg.cfg.indexColKey]);
                rpts.push({ rptId: ids[ids.length - 1] });
            }
            rptId = ids.join(",");
        }
        else
            rpts.push(rptId);

        if (!rptId || rptId.length == 0)
            return;
        $post("recRptAccess", rpts, null, null, null, true);
        var dwm = $.browser.msie ? "attch" : "interactive";
        HOYI.down("rptdwframe", "reportview.aspx?ProductCode=" + HOYI.pdc + "&vmode=" + dwm + "&rptid=" + rptId);
    };
    HOYI.dtSperator = /[\.\x20\:\-\/]/;
    function emptyDate() {
        return new Date(1900, 0, 1);
    };
    function parseDate(dtv, fmt) {
        var dt = null, msec = null;
        if (!dtv) return emptyDate();
        if (dtv.indexOf("/Date(") == 0)
            dtv = parseInt(/[\-0-9]+/ig.exec(dtv)[0]);

        if ($.type(dtv) == "number")
            dt = new Date(dtv);
        else {
            var dtSp = HOYI.dtSperator;
            var dtvs = dtv.split(dtSp), y, M, d, h = 0, m = 0, ss = 0, f = 0;
            if (dtv.length && dtvs.length > 0) {
                if (!fmt) {
                    if (dtvs[0].length == 2 || dtvs[0].length == 1)
                        fmt = "MM/dd/yyyy HH:mm:ss fff";
                    else
                        fmt = "yyyy-MM-dd HH:mm:ss fff";
                }
                var dtSeg = fmt.split(dtSp);
                for (var s = 0; s < dtSeg.length; s++) {
                    if (s >= dtvs.length) break;
                    switch (dtSeg[s]) {
                        case "yyyy":
                        case "YYYY":
                            y = parseInt(dtvs[s]);
                            break;
                        case "YY":
                        case "yy":
                            y = 2000 + parseInt(dtvs[s], 10);
                            break;
                        case "M":
                        case "MM":
                            M = parseInt(dtvs[s], 10);
                            break;
                        case "d":
                        case "dd":
                            d = parseInt(dtvs[s], 10);
                            break;
                        case "H":
                        case "hh":
                        case "h":
                        case "HH":
                            h = parseInt(dtvs[s], 10);
                            if (dtv.toLowerCase().indexOf("pm") > 0) h = h + 12;
                            break;
                        case "m":
                        case "mm":
                            m = parseInt(dtvs[s], 10);
                            break;
                        case "s":
                        case "ss":
                            ss = parseInt(dtvs[s], 10);
                            break;
                        case "fff":
                        case "ff":
                        case "f":
                            f = parseInt(dtvs[s], 10);
                            break;
                    }
                }
                dt = emptyDate();
                dt.setFullYear(y);
                dt.setMonth(M - 1);
                dt.setDate(d);
                dt.setHours(h);
                dt.setMinutes(m);
                dt.setSeconds(ss);
                dt.setMilliseconds(f);
            }
        }
        if (dt.getFullYear() > 2999)
            return emptyDate();
        else
            return dt;
    };
    function dt2str(dt, isUTC, format) {
        if (isUTC) {
            var msec = dt.getTime();
            var tmOfs = dt.getTimezoneOffset() * 60 * 1000;
            msec -= tmOfs;
            dt.setTime(msec)
        }
        if (dt.getFullYear() <= 1900)
            return "";
        else if (HOYI.DateFmtStr || format)
            return dt.format(format || HOYI.DateFmtStr);
        else
            return dt.toLocaleString();
    };
    HOYI.gdDefHeaderDblClick = gdDefHeaderDblClick;
    HOYI.gdDefDbClick = function (e, row, handler) {
        var gd = row.gd;
        var fstCl = gd.cfg.column[gd.cfg.checkBoxButton ? 1 : 0].dataIndex;
        if (row.cells[fstCl].innerDom == e.target) {

            var rwky = row.id;
            for (var prop in gd.selectedRows) {
                gd.selectRow(prop);
            };
            gd.selectRow(rwky);
            if (handler && $.type(handler) == "function")
                handler(e, row);
        }
    };
    HOYI.mkFlLink = function (id, type, title) {
        // convert enum value to string
        if (type && /^[0-9]$/.test(type.toString())) {
            var to = $byId(HOYI.config.fileFormat, type.toString(), "value");
            type = (to ? to.text : type);
        }
        if (type == 0) {
            return null;
        }
        else if (type && type.length > 0) {
            var vurl = "";
            if (PS.forPSBatch === true) {
                vurl = "javascript:void(0);"
            }
            else {

                if (HOYI.isTouchDevice()) {
                    if (type.toLowerCase() == "pdf")
                        vurl = "reportview.aspx?ProductCode=" + HOYI.pdc + "&rptid=" + id;
                    else
                        vurl = "reportview.aspx?ProductCode=" + HOYI.pdc + "&vMode=attch&rptid=" + id;
                }
                else {
                    if (type.toLowerCase() == "pdf")
                        vurl = HOYI.brwPath + "viewdoc.aspx?ProductCode=" + HOYI.pdc + "&rptid=" + id + "&title=" + encodeURIComponent(title || "Report Portal");
                    else
                        vurl = "reportview.aspx?ProductCode=" + HOYI.pdc + "&vMode=attch&rptid=" + id;
                }
            }
            var imga = $("<a href='" + vurl + "' target='_blank'></a>").addClass("icon" + (type || "file").toLowerCase());
            if (PS.forPSBatch === true) {
                imga.click(id, function (e) { $rpt(e.data); });
                imga.removeAttr("target");
            }
            return imga;
        }
        return null;
    };
    HOYI.noEventKeyCode = function (key) {
        if (key == 229)
            return true;
        if ((key >= 16 && key <= 20) || key == 91 || key == 144 || key == 145)
            return true;
        else if (key >= 33 && key <= 40) {
            return true;
        }
        else if (key >= 44 && key < 46) {
            return true;
        }
        else if (key >= 112 && key <= 123) {
            return true;
        }
        return false;
    };
    HOYI.isTouchDevice = function () {
        return HOYI.touch.support();
    };
    HOYI.isMobileStyle = function () {
        return (/\/PAD\/$/ig.test(HOYI.resPath));
    };
    HOYI.touchScroll = function (elObj) {
        if (!elObj) return;
        if (HOYI.isTouchDevice()) {
            var scrollStartPos = 0;
            var stPY = 0;
            HOYI.touch.touchStart(elObj, function (event, isDbl) {
                stPY = event.touches[0].pageY;
                scrollStartPos = this.scrollTop + stPY;
                if (!isDbl) event.preventDefault();
            });
            HOYI.touch.touchEnd2Click(elObj);
            HOYI.touch.touchMove(elObj, function (event) {
                var stPMV = event.touches[0].pageY;
                this.scrollTop = scrollStartPos - stPMV;
                event.preventDefault();
            });
        }
    };

    HOYI.searchSecs = function (sk, callback, univ, pmo, currency, country, n, condition) {
        HOYI.searchSecs.sk = $.trim(sk);
        var cbf = function (d) {
            try {
                var jo = d;
                if (jo.result && jo.result.code == 0) {
                    if (jo.m[0].k == HOYI.searchSecs.sk || jo.m[0].k.toLowerCase() == HOYI.searchSecs.sk.toLowerCase()) {
                        callback(HOYI.parseACData(jo.m[0].r, sk, univ));
                    }
                }
                else {
                    $aMsg((jo.result && jo.result.msg) || jo.message);
                    callback([], sk);
                }
            }
            catch (exp) {
                $aMsg(exp);
                callback([], sk);
            }
        };
        var surl = "searchsecs.aspx?ProductCode=" + HOYI.pdc + "&key=" + encodeURIComponent(sk) + (univ ? "&univ=" + univ : "")
                        + "&currency=" + escape(currency || "")
                        + "&country=" + escape(country || "")
                        + "&n=" + (n || 1000)
                        + (condition ? "&condition=" + encodeURIComponent(condition) : "");
        $.get(surl, null, cbf, "json");
    };
    HOYI.pgmenu = function () { return [{ "caption": $msg("Actions"), "items": [] }]; };
    HOYI.emptyObj = {};
    HOYI.selectText = function (el, s, e) {
        el = el.attr ? el[0] : el;
        el.focus();
        var tl = e || (el.value || el.innerText || "").length, sl = s || 0;
        if ($.browser.msie) {
            var range = el.createTextRange();
            range.collapse(true);
            range.moveStart("character", sl);
            range.moveEnd("character", tl);
            range.select();
        }
        else if (el.setSelectionRange) {
            el.setSelectionRange(sl, tl);
        }
        else {
            el.selectionStart = sl;
            el.selectionEnd = tl;
        }
    };
    window.HOYI.alertMsg = alertMsg;
    window.$aMsg = alertMsg;
    window.$post = window.HOYI.post;
    window.listData = listData;
    window.$gridData = getGridSelectedData;
    window.$gridOne = getGridOne;
    window.$ug = updateGridData;
    window.$ig = insertGridData;
    window.$dg = deleteGridRow;
    window.$mo = mergeObj;
    window.$mo2 = mergeObj2;
    window.$st = setTab;
    window.$swt = switchTab;
    window.$tblayout = initDgTablelayout;
    window.$select = initSelectList;
    window.$sel2sel = select2select;
    window.$condata = HOYI.getDataJsonObj;
    window.$tabledata = getTableData;
    window.$FMps = loadFileTypeMaps;
    window.$ftn = getFileTypeName;
    window.$objsftn = setObjsFileTypeName;
    window.$cell = editGridCell;
    window.$gdById = getGridDataById;
    window.psavoid = function () { };
    window.$byId = getDataById;
    window.$sglSel = singleSelectItem;
    window.$rpt = dowloadRpt;
    window.aurl = au;
    window.$dt = parseDate;
    window.dt2str = dt2str;
    window.$fsfn = function () { return false; };
    window.$cevent = function (e) {
        if (e.stopPropagation) e.stopPropagation();
        return false;
    };
})();
HOYI.cq = {
    queue: {},
    clear: function (cqtype) {
        var q = HOYI.cq.queue;
        var reg = new RegExp("^" + cqtype);
        for (var pn in q) {
            if (reg.test(pn)) {
                delete q[pn];
            }
        }
    }
};
HOYI.parseACData = function (data, value, univ, isFid) {
    var lst = [], sttab = HOYI.config.sttb;
    for (var i = 0; i < data.length; i++) {
        var d = data[i];
        if (d.SecId && d.SecId.length) {
            var caunv = "";
            if (d.OS010 == "CA") {
                if (!d.CA678 || d.CA678 == "")
                    continue;
                caunv = d.OS010 + "]" + d.CA678;
                if (univ && caunv != univ)
                    continue;
            }

            var si = {
                value: isFid ? d.SecId : (d.SecId + ";" + (caunv && caunv != "" ? caunv : d.OS010)),
                text: d.OS01W,
                currency: d.ST460 + " " + $msg("ST460_SUFFIX"),
                region: (d.XI018 || d.OS020) + " " + $msg("XI018_SUFFIX"),
                isin: (d.OS05J || "").trim(),
                inceptiondate: d.OS00F ? $dt(d.OS00F, "yyyy-MM-dd HH:mm:ss").format(HOYI.sDtFmt) + " " + $msg("OS00F_SUFFIX") : "",
                cauniv: caunv, ticker: (d.OS001 || d.OS385)
            };
            si.stDesc = sttab[d.OS010] || d.OS010;
            si.perf = (d.OS66Z == "1" ? $msg("OS66Z_TEXT") : (d.OS66Z == "0" ? "" : d.OS66Z));
            lst.push(si);
        }
    }
    return lst;
};
HOYI.touchScrollBar = function (wrapObj, isEmbed) {
    if (HOYI.isTouchDevice()) {
        if (wrapObj[0].tuchSlBarActived) return wrapObj[0].tuchSlBarActived;
        var resetScrollStatus = function () {
            if (!wrapObj.lstCtn) return;
            var st = wrapObj.lstCtn.scrollTop();
            if (st == 0)
                wrapObj.slUpBa.find("span").removeClass("on").addClass("off");
            else
                wrapObj.slUpBa.find("span").removeClass("off").addClass("on");
            var maxsh = wrapObj.lstCtn[0].scrollHeight - wrapObj.lstCtn.height();
            if (st >= maxsh)
                wrapObj.slDwBa.find("span").removeClass("on").addClass("off");
            else
                wrapObj.slDwBa.find("span").removeClass("off").addClass("on");
        };
        wrapObj.slUpBa = $("<div class=\"upba\"><span class='on'></span></div>");
        wrapObj.slDwBa = $("<div class=\"dwba\"><span class='on'></span></div>");
        if (isEmbed) {
            wrapObj.lstCtn = wrapObj;
            wrapObj.slUpBa.insertBefore(wrapObj);
            wrapObj.slDwBa.insertAfter(wrapObj);
        }
        else {
            wrapObj.lstCtn = $("<div></div>").css({ "overflow": "auto", "overflow-x": "hidden", "padding": "0", "margin": "0", "display": "block" });
            wrapObj.append(wrapObj.slUpBa);
            wrapObj.append(wrapObj.lstCtn);
            wrapObj.append(wrapObj.slDwBa);
        }
        var sclFn = function (e) {
            var n = wrapObj.lstCtn.scrollTop() + (e.data == 1 ? 10 : -10);
            wrapObj.lstCtn.scrollTop(n);
            if (e.stopPropagation) e.stopPropagation();
            return false;
        };
        var slTm = null;
        var slGt = 0;
        var msr = function (e) { if (slTm) { window.clearInterval(slTm); slTm = null; slGt = 0; } };
        var msd = function (e) {
            msr();
            if (slTm == null) {
                slTm = window.setInterval(function () {
                    if (slGt < 6) { slGt++; return; }
                    sclFn({ data: e.data });
                }, 50);
            }
        };
        wrapObj.slUpBa.click(0, sclFn);
        wrapObj.slDwBa.click(1, sclFn);
        wrapObj.slUpBa.mousedown(0, msd);
        wrapObj.slDwBa.mousedown(1, msd);
        /*wrapObj.slUpBa[0].addEventListener("touchstart", function(event) {
        msd({ "data": 0 });
        }, false);
        wrapObj.slDwBa[0].addEventListener("touchstart", function(event) {
        msd({ "data": 1 });
        }, false);
        wrapObj.slUpBa[0].addEventListener("touchend", function(event) {
        msr();
        }, false);
        wrapObj.slDwBa[0].addEventListener("touchend", function(event) {
        msr();
        }, false);*/
        wrapObj.slUpBa.mouseup(0, msr);
        wrapObj.slDwBa.mouseup(1, msr);
        wrapObj.slDwBa.mouseout(1, msr);
        wrapObj.slUpBa.mouseout(1, msr);
        wrapObj.lstCtn.scroll(resetScrollStatus);
        HOYI.touchScroll(wrapObj.lstCtn);
        var initScroll = function () {
            if (!wrapObj.slUpBa || !wrapObj.lstCtn.is(":visible")) return;
            var sh = wrapObj.lstCtn[0].scrollHeight;
            var h = wrapObj.height();
            if (sh > h) {
                var bh = wrapObj.slUpBa.height();
                var w = wrapObj.width();
                var ofs = wrapObj.offset();
                if (!isEmbed) wrapObj.lstCtn.css("height", h - bh - bh + "px");
                wrapObj.slUpBa.show();
                wrapObj.slDwBa.show();
            }
            else {
                wrapObj.slUpBa.hide();
                wrapObj.slDwBa.hide();
                if (!isEmbed) wrapObj.lstCtn.css("height", "auto");
            }
            resetScrollStatus();
        };
        wrapObj[0].tuchSlBarActived = initScroll;
        return wrapObj[0].tuchSlBarActived;
    }
    return $fsfn
};
function dropdown(ddl, iopts, style, attrs, showpt) {
    var dla = null;
    var dli = null;
    var ddlc = null;
    var lstmc = null;
    var lstul = null;
    var keyName = null;
    var isList = iopts.isList;
    var msDwn = false;
    var noItmPan = null;
    var auClrTm = null;
    var disabled = false;
    var me = this, innerItems = null, binding = null, ic = 0;
    function init() {
        ddlc = $(ddl);
        ddlc.css(style);
        ddlc.attr(attrs);
        ddlc.click(clickFun);
        if (isList)
            initElement();
        else {
            ddlc.addClass("fm_dd");
            if (iopts.className)
                ddlc.addClass(iopts.className);
            var dlae = document.createElement("A");
            dlae.setAttribute("href", "javascript:void(0);");
            dlae.className = "fm_dd_arrow";
            var lbl = document.createElement("label");
            var sp = document.createElement("span");
            var dlie = document.createElement("input");
            dlie.setAttribute("type", "hidden");
            dli = $(dlie);
            dlae.appendChild(lbl);
            dlae.appendChild(sp);
            dla = $(dlae);
            ddlc._label = $(lbl);
            ddlc._hidden = dli;
            dli[0].ddlObj = me;
            if (iopts.attrs)
                dli.attr(iopts.attrs);
            dli.appendTo(dla);
            dla.appendTo(ddlc);
        }
    };
    function initElement() {
        if (lstul) return;
        me.lstmc = lstmc = $("<div class=\"lstc\"></div>");
        if (iopts.style) lstmc.css(iopts.style);
        lstmc.attr("psddl", "1.0");
        lstmc.tuchSl = HOYI.touchScrollBar(lstmc);
        if (isList)
            lstmc.css({ "display": "block", "position": "relative", "margin": "0" });
        lstul = $("<ul style=\"width: 100%;\"></ul>");
        lstmc.lstCtn ? lstul.appendTo(lstmc.lstCtn) : lstul.appendTo(lstmc);
        lstmc.appendTo(isList ? ddlc : $(document.body));
        noItmPan = $("<span style='color:#BBB;display:block;font-size:1.2em;position:relative;text-align:center;top:30%;display:none'></span>");
        noItmPan.appendTo(lstmc);
        if (isList) {
            lstmc.mousedown(function (e) {
                msDwn = !e.ctrlKey;
                setValue({});
                if (!e.ctrlKey && !e.shiftKey && e.target != lstmc[0])
                    clearSelection(e.target);
            });
            lstmc.mouseup(function () { msDwn = false; });
            lstmc.mouseout(function (e) { if (e.target == lstmc[0]) { msDwn = false; } });
            if ($.browser.msie) {
                lstmc.bind("dragstart", function (e) { e.stopPropagation(); return false; });
            }
            lstmc.keypress(function (e) {
                if (e.which == 0 || e.which == 13)
                    return;
                var st = lstmc.data("sswt"), s;
                if (st) {
                    window.clearTimeout(st);
                    st = 0;
                    s = lstmc.data("ssw");
                }
                if (!s) s = "";
                s += String.fromCharCode(e.which);
                lstmc.data("ssw", s);
                if (!st) {
                    st = window.setTimeout(clearSearch, 300);
                    lstmc.data("sswt", st);
                }
                searchStartWith(s);
            });
            lstmc.dblclick(function (e) {
                me.dbClick && me.dbClick(e);
            });
        }
    };
    function clearSearch() {
        lstmc.data("ssw", "");
        lstmc.data("sswt", 0);
    };
    function initScroll() {
        if (lstmc.tuchSl) lstmc.tuchSl();
    };
    function clickFun(e) {
        initElement();
        if (disabled) {
            if (e.data == "dc")
                showDropdown(lstmc, e);
            return;
        }
        if (!me.clickHdl || me.clickHdl(e)) {
            if (isList)
                _select(e.target, isList, e);
            else
                showDropdown(lstmc, e);
            e.stopPropagation();
        }
        return false;
    };
    function clear() {
        clearSelection();
        if (lstul) lstul.find("li").remove();
        this.data = null;
        this.boundData = null;
        innerItems = {};
    };
    function clearSelection(igi) {
        setValue({});
        unSelectedItems(igi);
    };
    function unSelectedItems(igi) {
        if (lstul) {
            var sels = lstul.find(".selected").each(function () {
                if (igi == this) return;
                unSelectedItem(this);
            });
        }
    };
    function unSelectedItem(item) {
        var ji = $(item);
        ji.removeClass("selected");
        if (me.mapping && me.mapping.additional) {
            $(ji[0].parentNode).find("span").removeClass("selected");
        }
    };
    function showNoItemMsg(msg) {
        if (msg && msg.length > 0) {
            initElement();
            if (auClrTm) window.clearTimeout(auClrTm);
            noItmPan.css("display", "block");
            noItmPan.text(msg);
            lstul.css("display", "none");
            auClrTm = window.setTimeout(function () { auClrTm = null; showNoItemMsg(""); }, 6000);
        }
        else {
            noItmPan.css("display", "none");
            noItmPan.text("");
            lstul.css("display", "");
        }
    };
    function bind(data, mapping, defVal, append, noItmMsg) {
        if (isList) showNoItemMsg((!data || data.length == 0) && noItmMsg ? noItmMsg : "");
        if (!data) return;
        if (!mapping) mapping = { "textName": "text", "valueName": "value" };
        this.mapping = mapping;
        if (!append || !innerItems) innerItems = {};
        keyName = mapping.valueName;
        if (append && binding) {
            this.boundData = (this.boundData || []).add(data);
            if (isList) {
                binding.data = data;
                binding.append = append;
            }
            else if (binding.done) {
                binding.data = data;
                binding.append = true;
                _bind.call(me);
                binding.data = this.boundData;
                binding.append = false;
            }
            else {
                binding.data = this.boundData;
            }
        }
        else {
            this.clear();
            this.boundData = data;
            binding = { data: data, mapping: mapping, append: append };
        }
        if (isList) {
            var nscl = (append && this.data && this.data.length) ? true : false;
            _bind.call(me);
            if (nscl) scrollToBottom();
        }
        else if (arguments.length >= 3 && defVal != undefined)
            this.select(defVal);
        return this;
    };
    function scrollToBottom() {
        var sch = lstmc[0].scrollHeight;
        scrollTo(sch);
    };
    function scrollTo(pos) {
        lstmc[0].scrollTop = pos;
    };
    function _bind() {
        if (!binding) return;
        initElement();
        var data = binding.data, mapping = binding.mapping, seletedVal = this.item(), append = binding.append;
        binding.done = true;
        var ct = function (da) {
            for (var d = 0; d < da.length; d++) {
                var it = createItem(da[d], mapping, lstul);
            }
        };
        if (!append || !this.data) {
            this.data = [];
            ic = 0;
            ct.call(this, data);
            lstmc.scrollTop(0);
        }
        else {
            var nda = [];
            for (var n = 0; n < data.length; n++) {
                var exItm = this.getHtmlItem(data[n][keyName]);
                if (exItm && exItm[0] && exItm[0].parentNode) {
                    if (exItm[0].style.display == "none") {
                        this.data.push(data[n]);
                        exItm[0].style.display = "";
                    }
                }
                else {
                    nda.push(data[n]);
                }
            }
            ct.call(this, nda);
        }
        if (append) clearSelection();
        if (seletedVal) this.select(seletedVal.v, false);
        if (mapping.additional) {
            if ($.browser.msie && lstmc[0].scrollHeight > lstmc.height()) {
                var ads = lstul.find("span.itemad");
                for (var v = 0; v < ads.length; v++) {
                    ads[v].style.right = "17px";
                }
            }
        }
        initScroll();
        lstmc.focus();
        return this;
    };
    function createItem(data, mapping, pu) {
        var tmpStr = "<li><a href=\"javascript:void(0);\" onclick=\"javascript:return $fsfn();\" onmousedown=\"javascript:return $fsfn();\"></a></li>";
        //var tmp2 = "<li><span style=\"cursor:pointer\" class=\"adico\"></span></li>";
        var tmpSep = "<li><hr style=\"margin: 2px 0;\"/></li>";
        var tn = mapping.textName;
        var vn = mapping.valueName;
        var ad = mapping.additional;
        var exts = mapping.exts;
        var value = data[vn];
        var text = data[tn];
        var itm = $(tmpStr);

        if (value == "seplineindpdwn" || value == "---") {
            itm = $(tmpSep);
            innerItems[value] = { elt: itm, data: data, index: ic++ };
            pu.append(itm);
            return itm;
        }

        var jai = itm.find("a");
        jai.attr("value", value);
        if (text) jai.text(text).attr("title", text);
        if (isList) itm.height(21);
        if (ad && ad.length) {
            var adLn = ad.length;
            if ($.browser.webkit) jai.css("float", "left");
            for (var a = adLn - 1; a >= 0; a--) {
                if (data[ad[a]]) {
                    var adsp = $("<span></span>").addClass("itemad");
                    iopts && iopts.style && iopts.style.backgroundColor && adsp.addClass("whitecolor");
                    adsp.text(data[ad[a]]);
                    itm.append(adsp);
                }
            }
        }
        if (exts && exts.length) {
            var extc = $("<div></div>").appendTo(itm);
            for (var ex = 0, es = exts.length; ex < es; ex++) {
                if (data[exts[ex]]) {
                    var exsp = $("<span></span>").addClass("itemad").css("float", "none");
                    exsp.text(data[exts[ex]])
                    extc.append(exsp);
                }
            }
        }
        pu.append(itm);
        if (isList) {
            var she = { shiftKey: true };
            itm.mousemove(function (e) {
                if (msDwn) {
                    if (e.target.tagName == "A")
                        _select(e.target, true, she);
                    else
                        _select(e.target.children[0], true, she);
                }
            });
            jai.hover(function () {
                itm.find("span").removeClass("selected");
                itm.find("span").addClass("itemadhover");
            },
                function (e) {
                    var sps = itm.find("span");
                    if ($(e.target).hasClass("selected")) {
                        sps.addClass("selected");
                    }
                    sps.removeClass("itemadhover")
                }
            );
        }
        if (data.children) {
            itm.find("a").addClass("adico");
            var cisf = $.type(data.children) == "function";
            if (!cisf) itm[0].ddlChld = createChildItems(data.children, mapping, itm);
            itm.click({ ifn: cisf, d: data, mp: mapping }, function (e) {
                var jme = $(e.target);
                if (jme.hasClass("adico")) {
                    jme.removeClass("adico");
                    jme.addClass("deico");
                    var ed = e.data;
                    if (ed.ifn && !itm[0].ddlChld) {
                        var chdata = ed.d.children();
                        if ($.type(chdata) == "function") {
                            var ced = $.extend({ cit: itm }, ed);
                            chdata(function (chs) {
                                ced.cit[0].ddlChld = createChildItems(chs, ced.mp, ced.cit);
                                ced.cit[0].ddlChld.css("display", "list-item");
                            });
                        }
                        else {
                            itm[0].ddlChld = createChildItems(chdata, ed.mp, itm);
                            itm[0].ddlChld && itm[0].ddlChld.css("display", "list-item");
                        }
                    }
                    else {
                        if (!itm[0].ddlChld)
                            itm[0].ddlChld = createChildItems(ed.d.children, ed.mp, itm);
                        itm[0].ddlChld.css("display", "list-item");
                    }
                }
                else {
                    jme.removeClass("deico");
                    jme.addClass("adico");
                    itm[0].ddlChld.css("display", "none");
                }
                initScroll();
                e.stopPropagation();
                return false;
            });
        }
        else {
            innerItems[value] = { elt: itm, data: data, index: ic++ };
        }
        me.data.push(data);

        return itm;
    };
    function createChildItems(data, mapping, pnt) {
        var citmc = $("<li style=\"display:none\"><div class=\"ddlchdc\"><ul></ul></div></li>");
        citmc[0].ddlPnt = pnt;
        var jul = citmc.find("ul");
        for (var n = 0; n < data.length; n++) {
            createItem(data[n], mapping, jul);
        };
        citmc.insertAfter(pnt);
        return citmc;
    };
    function showDropdown(evts, e) {
        var el = evts;
        if (binding && !binding.done) _bind.call(me);
        var display = el.css("display");
        if (display == "none" || display == "") {
            var da = $("a", ddlc), pos = da.offset();
            el.css({ "left": pos.left + "px", "top": pos.top + da.outerHeight() + "px" });
            el.css("width", ddlc.width());
            el.show();
            if (el.setCapture)
                el.setCapture(false);
            else {
                if (e) e.stopPropagation();
                var dc = $(document).trigger("click");
                dc.click("dc", clickFun);
            }
            var cua = $("A.selected", el);
            if (cua.length == 0)
                return;
            goToItem(cua);
            initScroll();
        }
        else {
            el.hide();
            $(document).unbind("click", clickFun);
            if (el.setCapture)
                el.releaseCapture();
            _select(e.target);
        }
    };
    function goToItem(item) {
        var cua = item;
        expandParent(cua);
        var cuaofp = lstmc.lstCtn || lstmc;
        var atop = cua.offset().top;
        var aptop = cuaofp.offset().top;
        var top = atop - aptop;
        var ph = cuaofp.height();
        if (top > ph || top < 0)
            cuaofp.scrollTop(top + cuaofp.scrollTop());
    }
    function _select(ae, mul, e) {
        if (!ae) return;
        var jse = $(ae);
        if (ae.tagName == "A" && jse.attr("value") != undefined) {
            var sli = { "t": jse.text(), "v": jse.attr("value") };
            if (showpt) {
                var pts = jse.parents("li");
                var ic = pts.length > 1 ? pts[1] : null;
                if (ic && ic.ddlPnt) {
                    var pa = ic.ddlPnt.find("a");
                    if (pa.length > 0) {
                        if (sli.v != pa.attr("value"))
                            sli.t = pa.text() + "(" + sli.t + ")";
                    }
                }
            }
            if (mul && me.multiple) {
                if (e) {
                    if (e.ctrlKey) {
                        if (jse.hasClass("selected"))
                            unSelectedItem(jse[0]);
                        else
                            selectItem(jse);
                    }
                    else if (e.shiftKey) {
                        selectItem(jse);
                        var sel = 0;
                        lstmc.find("a[value]").each(function () {
                            var jthe = $(this);
                            if (jthe.hasClass("selected")) {
                                if (sel == 0)
                                    sel = 1;
                                else if (sel == 2)
                                    unSelectedItem(jthe[0]);
                            }
                            else {
                                if (sel == 1) selectItem(jthe);
                            }
                            if (this == jse[0]) sel = 2;
                        });
                    }
                    else
                        selectItem(jse);
                }
                else {
                    selectItem(jse);
                };
            }
            else {
                unSelectedItems(jse[0]);
                selectItem(jse);
            };
            setValue(sli);
            expandParent(jse);
            jse.focus();
            return jse;
        }
        return null;
    };
    function selectItem(jse) {
        if (!jse.hasClass("selected")) jse.addClass("selected");
        if (me.mapping.additional) {
            var adSps = $(jse[0].parentNode).find("span");
            if (!adSps.hasClass("selected")) adSps.addClass("selected");
        }
    };
    function expandParent(jse) {
        var pnt = jse.parents(".ddlchdc");
        if (pnt.length > 0) {
            var liNd = pnt[0].parentNode;
            var pntItm = liNd.ddlPnt;
            if (pntItm) {
                var aItm = pntItm.find("a");
                if (aItm.hasClass("adico"))
                    aItm.trigger("click");
            }
        }
    };
    function setValue(vo) {
        if (!isList) {
            var jtxt = ddlc._label, jval = ddlc._hidden, txt = vo.t || "";
            jtxt.text(txt).attr("title", txt);
            jval.val(vo.v || "");
        }
        var ovo = ddlc.selected;
        ddlc.selected = vo;
        if (!ovo && (!vo || (!vo.t && !vo.v)))
            return;
        if ((!ovo && vo) || (ovo && !vo) || ovo.t != vo.t || ovo.v != vo.v)
            !disabled && me.itmChg && me.itmChg(vo, ovo);
    };
    function searchStartWith(c) {
        var fp = binding.mapping.textName, lc = c.toLowerCase();
        for (var i in innerItems) {
            var it = innerItems[i];
            if (!it || !it.data) continue;
            var sc = it.data[fp].substr(0, c.length).toLowerCase();
            if (it.elt && sc == lc) {
                me.select(it.data[binding.mapping.valueName], false);
                goToItem(it.elt);
                break;
            }
        }
        return;
    };
    this.bind = bind;
    this.clear = clear;
    this.select = function (s, m) {
        var data = this.boundData, ct = 0;
        if (!data) return null;
        if (!binding.done) {
            var sd = null, mapping = binding.mapping, vn = mapping.valueName, ci = 0, cv, found = false;
            for (var d = 0, c = data.length; d < c; d++) {
                sd = data[d], cv = sd[vn];
                if (sd.children && $.type(sd.children) != "function") {
                    ci++;
                    for (var ch = 0, cl = sd.children.length; ch < cl; ch++) {
                        var csd = sd.children[ch];
                        var cdv = csd[vn];
                        if (ci === s || cdv == s) {
                            found = true;
                            sd = csd;
                            break;
                        }
                        ci++;
                    }
                    if (found) break;
                }
                else {
                    if (ci === s || cv === s) {
                        found = true;
                        break;
                    }
                    ci++;
                }
            }
            if (found && sd) {
                var tn = mapping.textName, innerv = { v: sd[mapping.valueName], t: sd[tn] };
                if (showpt && sd && sd.parent && sd.parent[tn]) innerv.t = sd.parent[tn] + "(" + innerv.t + ")";
                innerItems[innerv.v] = { data: sd };
                setValue(innerv);
            }
            return found ? sd : null;
        }
        else {
            var itm = this.getHtmlItem(s);
            if (itm) {
                return _select(itm[1], m);
            }
            else
                return null;
        }
    };
    this.insert = function (itm) {
        var val = itm[this.mapping.valueName];
        if (val && !this.select(val)) {
            var da = (this.data || binding.data || []);
            da.splice(0, 0, itm);
            this.bind(da, this.mapping, 0);
        }
    };
    this.allowDdl = function (bl) {
        var btn = ddlc.find("span");
        btn.css("display", bl ? "" : "none");
        if (bl && ddlc.attr("aldl")) {
            ddlc.click(clickFun);
            ddlc.removeAttr("aldl");
        }
        else if (!bl) {
            ddlc.unbind("click", clickFun);
            ddlc.attr("aldl", "n");
        }
    };
    this.append = function (items, defval) {
        var def = defval || (this.item() ? this.item().v : 0);
        this.bind(items, this.mapping, def, true);
    };
    this.getHtmlItem = function (s) {
        if (binding && !binding.done) _bind.call(me);
        if (!this.data) return null;
        var elt = null;
        if ($.type(s) == "number" && s >= 0) {
            for (var ip in innerItems) {
                if (ip != undefined) {
                    if (innerItems[ip].index == s) {
                        elt = innerItems[ip];
                        break;
                    }
                }
            }
        }
        else
            elt = innerItems[s];
        if (elt && elt.elt) {
            var li = elt.elt;
            var ai = $("a", li)[0];
            return [li[0], ai];
        }
        return null;
    };
    this.item = function () {
        return ddlc.selected;
    };
    this.showList = function () {
        ddlc.trigger("click");
    };
    this.getItem = function (si) {
        if (!si) return null;
        var i = innerItems[si.v];
        return i ? i.data : null;
    };
    this.dataItem = function () {
        var si = this.item();
        return this.getItem(si);
    };
    this.clickHandler = function (fn) {
        this.clickHdl = fn;
    };
    this.itemChanged = function (fn) {
        this.itmChg = fn;
    };
    init();
    this.getItems = function () {
        var items = [];
        lstmc.find("a.selected").each(function () {
            var jse = $(this);
            items.push({ "t": jse.text(), "v": jse.attr("value") });
        });
        return items;
    };
    this.getListItems = function () {
        var itms = [];
        for (var ik in innerItems) {
            if (ik && innerItems[ik])
                itms.push(innerItems[ik]);
        }
        return itms;
    };
    this.removeItem = function (key, noHide) {
        var hi = (binding && binding.done) ? this.getHtmlItem(key) : null;
        if (hi) {
            unSelectedItem(hi[1]);
            if (noHide) {
                delete innerItems[key];
                hi[0].parentNode.removeChild(hi[0]);
            }
            else
                $(hi[0]).hide();
        }
        var rmedData = null;
        for (var d = 0; d < this.boundData.length; d++) {
            if (this.boundData[d][keyName] == key) {
                rmedData = this.boundData.splice(d, 1)[0];
            }
        }
        this.select(0);
        return rmedData;
    };
    this.getDataItems = function (hide) {
        var di = [];
        var items = this.getItems();
        if (items.length == 0) return;
        for (var i = 0; i < items.length; i++) {
            if (hide)
                di.push(this.removeItem(items[i].v) || this.getItem(items[i]));
            else
                di.push(this.getItem(items[i]));
        }
        if (hide) { clearSelection(); initScroll(); }
        return di;
    };
    this.selectAll = function () {
        if (!this.boundData) return;
        for (var i = 0; i < this.boundData.length; i++) {
            this.select(this.boundData[i][keyName], true);
        }
    };
    this.disable = function (arg1, fn, kpSel) {
        if (arguments.length > 0) {
            disabled = arg1;
            if ($.browser.msie)
                ddlc.find("*").attr("disabled", disabled);
            else
                ddlc.find("*").css("color", disabled ? "gray" : "");
            if (disabled && !kpSel) clearSelection();
            if (fn) fn(this, arg1);
            return disabled;
        }
        else
            return disabled;
    };
    this.changeSelectedText = function (text) {
        var ci = this.item();
        if (!ci) return;
        var dllHi = this.getHtmlItem(ci.v);
        ci.t = text;
        if (dllHi) $(dllHi[1]).text(ci.t).attr("title", ci.text);
        $("label", ddlc).text(ci.t).attr("title", ci.t);
        var di = this.dataItem();
        if (di) di[this.mapping.textName] = text;
    };
    this.dbClick = function () { };
    this.focus = function () { lstmc.focus(); }
    this.clearSelection = clearSelection;
    this.resetTouchScroll = initScroll;
    this.scrollToTop = function () { scrollTo(0); };
    this.multiple = true;
};

function fileuploader(cont, fltypes, name) {
    var ct = null, maxUploadSize = 50, totalBytes = maxUploadSize * 1024 * 1024, start = 0, tmHd = 0, upHd, filea;
    var tgfm = null, pstfm = null, files = [], me = this, upName = name;
    function init() {
        ct = $(cont);
        if (!upName) upName = "uptgfm_" + (new Date()).getTime();
        tgfm = $("<iframe name=\"{0}\" id=\"{0}\" src=\"about:blank\"/>".replace(/\{0\}/ig, upName));
        pstfm = $("<form method=\"post\" action=\"uploadfile.ashx\" target=\"{1}\" enctype=\"multipart/form-data\" name=\"{1}_fileForm\" id=\"{1}_fileForm\"></form>".replace(/\{0\}/ig, HOYI.pdc).replace(/\{1\}/ig, upName));
        tgfm.appendTo(ct);
        pstfm.appendTo(ct);
        tgfm.bind("load", uploadBack);
    };
    function uploadBack(e) {
        var upbkfm = e.target, upo = null, upOK = false;
        clearTmHd();
        try {
            var idoc = upbkfm.contentWindow.document;
            if (!idoc.body && idoc.documentElement.nodeName == "awd") {
                alert(idoc.documentElement.children[0].attributes[1].value);
                return;
            }
            var itxt = idoc.body.innerHTML;
            if (idoc.location.href.length > 11) {
                upOK = /\{.+\}/.test(itxt);
                if (upOK) {
                    upo = $.evalJSON(/\{.+\}/.exec(itxt)[0]);
                    upOK = (upo.status == "OK");
                    if (upOK) clear();
                }
                if (upHd) upHd(upo || itxt, upOK);
                upbkfm.src = "about:blank";
            };
        }
        catch (error) {
            if (upHd) upHd($msg("uploadfilesizeexceed").replace("{0}", maxUploadSize), false);
            upbkfm.src = "about:blank";
        }
        pstfm.find("input").remove();
    };
    this.completed = function (fn) {
        upHd = fn;
    };
    function clear() {
        files.splice(0, files.length);
        clearTmHd();
    };
    function clearTmHd() {
        if (tmHd) {
            window.clearInterval(tmHd);
            tmHd = 0;
        }
    };
    function checkTimeout() {
        var nw = (new Date()).getTime() - start;
        if (nw > 480000) {
            clearTmHd();
            ct[0].removeChild(pstfm[0]);
            ct[0].removeChild(tgfm[0]);
            init();
            if (upHd) upHd($msg("uploadfilestimeout"), false);
        }
    };
    function filter(fl) {
        if (filter && $.type(fltypes) == "array") {
            var fn = $(fl).val();
            var fns = fn.split(/\./);
            var xt = fns[fns.length - 1].toLowerCase();
            var found = false;
            for (var f = 0; f < fltypes.length; f++) {
                if (fltypes[f] == xt) {
                    found = true;
                    break;
                }
            }
            return found;
        }
        return true;
    };
    this.addFile = function (fl, fileId) {
        if (filter(fl)) {
            if (fl.files && fl.files.length) {
                var sz = 0;
                for (var f = 0; f < files.length; f++) {
                    sz += (files[f].files[0].fileSize || files[f].files[0].size);
                }
                if ((sz + (fl.files[0].fileSize || fl.files[0].size)) > totalBytes) {
                    throw $msg("uploadfilesizeexceed").replace("{0}", maxUploadSize);
                }
            }
            if (fileId && $.type(fileId) == "string") fl.setAttribute("id", fileId);
            files.push(fl);
            return true;
        }
        return false;
    };
    this.removeFile = function (id) {
        if ($.type(id) == "number") {
            files.splice(id, 1);
        }
        else {
            for (var i = 0; i < files.length; i++) {
                if (files[i].getAttribute("id") == id) {
                    files.splice(i, 1);
                }
            }
        }
    };
    this.upload = function () {
        pstfm.find("input").remove();
        for (var f = 0; f < files.length; f++) {
            pstfm.append(files[f]);
        }
        start = (new Date()).getTime();
        clearTmHd();
        tmHd = window.setInterval(checkTimeout, 20000);
        pstfm[0].submit();
    };
    this.hasFile = function () {
        return files.length > 0;
    };
    this.fileCount = function () {
        return files.length;
    };
    this.clear = clear;
    function fileSelected(e) {
        var f = $(e.target);
        if (me.onSelected) {
            if (me.onSelected(f))
                attachFile(f);
        }
        else {
            attachFile(f);
        }
    };
    function initFile(fa, isSingle) {
        if (fa) filea = fa;
        if (!filea) return;
        var flc = filea;
        var ipts = $("input", flc);
        ipts.unbind("change", fileSelected);
        ipts.remove();
        var fl = $("<input type=\"file\" class=\"file\" name=\"file\" />");
        fl.appendTo(flc);
        if (!isSingle) fl.bind("change", fileSelected);
        me.onFileCreated && me.onFileCreated(fl);
    };
    function attachFile(fl, isSingle) {
        var fileId = fl.attr("fid");
        try {
            if (me.addFile(fl[0], fileId)) {
                me.onFileAdded && me.onFileAdded(fl, true);
            }
            else
                me.onFileAdded && me.onFileAdded(fl, false);
        }
        catch (jer) {
            window.HOYI.alertMsg(jer.toString ? jer.toString() : jer);
        }
        initFile(null, isSingle);
    };
    this.initFile = initFile;
    this.attachFile = attachFile;
    init();
};
HOYI.backAction = new (function () {
    result = {}, exing = {}, errors = {};
    function __exec(act) {
        var ak = function (aa) {
            return aa.a + (aa.sk ? (":" + aa.sk) : "")
        };
        var cb = function (d) {
            var cak = ak(act);
            delete exing[cak];
            result[cak] = d;
            errors[cak] = null;
            if (act.c) act.c(d);
        }
        var eak = ak(act);
        if (exing[eak])
            return;
        else
            exing[eak] = true;
        $post(act.a, act.d || {}, cb, null, function (d) {
            var k = ak(act);
            errors[k] = true;
            delete exing[k];
        }, true);
    };
    function exec(actionName, data, callback, subKey) {
        if (!actionName) return;
        if (getResult(actionName, subKey)) return;
        __exec({ a: actionName, c: callback, d: data, sk: subKey });
    };
    function getKey(actionName, subKey) {
        return subKey ? actionName + ":" + subKey : actionName;
    };
    function getResult(actionName, subKey) {
        return result[getKey(actionName, subKey)];
    };
    function wait(acts, cb, chkErr) {
        var wObj = { acts: $.type(acts) == "array" ? acts : [acts], cb: cb, i: 0 };
        wObj.fn = function (iswaitting) {
            var allcomp = false;
            for (var a = 0; a < this.acts.length; a++) {
                allcomp = getResult(this.acts[a]) ? true : false;
                if (!allcomp) {
                    if (chkErr && errors[this.acts[a]])
                        allcomp = true;
                    else
                        break;
                }
            }
            if (allcomp) {
                if (this.i != 0) {
                    window.clearInterval(this.i);
                    this.i = 0;
                }
            }
            if (cb) cb(allcomp);
            return allcomp;
        };
        if (!wObj.fn()) wObj.i = window.setInterval(function () { wObj.fn(); }, 500);
    };
    this.getResult = getResult;
    this.clear = function (actionName) {
        if (actionName) {
            delete result[actionName];
            for (var dk in result) {
                if (dk.indexOf(actionName + ":") == 0)
                    delete result[dk];
            }
        }
    };
    this.isError = function (actionName, subKey) {
        return !!errors[getKey(actionName, subKey)]
    };
    this.exec = exec;
    this.wait = wait;
})();
HOYI.getMinDate = function () {
    var nd = new Date();
    var ndtimes = nd.getTime();
    var dayst = 60000 * 60 * 24;
    var tmtimes = ndtimes + dayst;
    nd.setTime(tmtimes);
    return nd.format("yyyy-MM-dd");
};
HOYI.sort = function (pp, nguc) {
    var ppn = pp;
    this.compare = function (a, b, ineq) {
        var av = (ppn) ? a[ppn] : a;
        var bv = (ppn) ? b[ppn] : b;
        if ($.type(av) == "string") {
            av = (av || "").toLowerCase();
            bv = (bv || "").toLowerCase();
        }
        return ineq ? av <= bv : av < bv;
    };
    this.sortArray = function (array) { };
};
HOYI.disableElement = function (el, disabled) {
    var jb = el.attr ? el : $(el);
    if (disabled) {
        jb.attr("disabled", "disabled");
        if (!HOYI.isIEx(8, true))
            jb.css("color", "#bbb");
    }
    else {
        jb.removeAttr("disabled");
        if (!HOYI.isIEx(8, true))
            jb.css("color", "inherit");
    }
    return jb;
};
HOYI.quickSort = function (pp) {
    HOYI.sort.call(this, pp);
    this.sortArray = function (array) {
        if (array.length > 1) {
            var k = array[0];
            var x = [];
            var y = [];
            for (var i = 1, len = array.length; i < len; i++) {
                if (this.compare(array[i], k, true)) {
                    x.push(array[i]);
                } else {
                    y.push(array[i]);
                }
            }
            x = this.sortArray(x);
            y = this.sortArray(y);
            return x.concat(k, y);
        } else {
            return array;
        }
    };
};
HOYI.bullSort = function (pp, nguc) {
    HOYI.sort.call(this, pp);
    this.sortArray = function (array) {
        var temp;
        for (var i = 0; i < array.length; i++) {
            for (var j = array.length - 1; j > i; j--) {
                if (this.compare(array[j], array[j - 1], false)) {
                    temp = array[j];
                    array[j] = array[j - 1];
                    array[j - 1] = temp;
                }
            }
        }
        return array;
    }
};
/* jquery iMask*/
(function ($) {
    var $chk = function (obj) {
        return !!(obj || obj === 0);
    };

    /**
    * Masks an input with a pattern
    * @class
    * @requires jQuery
    * @name jQuery.iMask
    * @param {Object}    options
    * @param {String}    options.type (number|fixed)
    * @param {String}    options.mask Mask using 9,a,x notation
    * @param {String}   [options.maskEmptyChr=' ']
    * @param {String}   [options.validNumbers='1234567890']
    * @param {String}   [options.validAlphas='abcdefghijklmnopqrstuvwxyz']
    * @param {String}   [options.validAlphaNums='abcdefghijklmnopqrstuvwxyz1234567890']
    * @param {Number}   [options.groupDigits=3]
    * @param {Number}   [options.decDigits=2]
    * @param {String}   [options.currencySymbol]
    * @param {String}   [options.groupSymbol=',']
    * @param {String}   [options.decSymbol='.']
    * @param {Boolean}  [options.showMask=true]
    * @param {Boolean}  [options.stripMask=false]
    * @param {Function} [options.sanity]
    * @param {Object}   [options.number] Override options for when validating numbers only
    * @param {Boolean}  [options.number.stripMask=false]
    * @param {Boolean}  [options.number.showMask=false]
    */
    var iMask = function () {
        this.initialize.apply(this, arguments);
    };

    iMask.prototype = {
        options: {
            maskEmptyChr: ' ',

            validNumbers: "1234567890",
            validAlphas: "abcdefghijklmnopqrstuvwxyz",
            validAlphaNums: "abcdefghijklmnopqrstuvwxyz1234567890",

            groupDigits: 3,
            decDigits: 2,
            currencySymbol: '',
            groupSymbol: ',',
            decSymbol: '.',
            showMask: true,
            stripMask: false,

            lastFocus: 0,

            number: {
                stripMask: false,
                showMask: false
            }
        },

        initialize: function (node, options) {
            this.node = node;
            this.domNode = node[0];
            this.options = $.extend({}, this.options, this.options[options.type] || {}, options);
            var self = this;

            if (options.type == "number") this.node.css({ "imeMode": "disabled" });

            this.node
				.bind("mousedown click", function (ev) { ev.stopPropagation(); ev.preventDefault(); })

				.bind("mouseup", function () { self.onMouseUp.apply(self, arguments); })
				.bind("keydown", function () { self.onKeyDown.apply(self, arguments); })
				.bind("keypress", function () { self.onKeyPress.apply(self, arguments); })
				.bind("focus", function () { self.onFocus.apply(self, arguments); })
				.bind("blur", function () { self.onBlur.apply(self, arguments); });
        },

        isFixed: function () { return this.options.type == 'fixed'; },
        isNumber: function () { return this.options.type == 'number'; },

        onMouseUp: function (ev) {
            ev.stopPropagation();
            ev.preventDefault();

            if (this.isFixed()) {
                var p = this.getSelectionStart();
                this.setSelection(p, (p + 1));
            } else if (this.isNumber()) {
                this.setEnd();
            }
        },

        onKeyDown: function (ev) {
            var chr = null, range = null, val = "";
            if (ev.ctrlKey || ev.altKey || ev.metaKey) {
                return;

            } else if (ev.which == 13 && this.options.autoSubimt) { // enter
                this.node.blur();

                this.submitForm(this.node);

            } else if (!(ev.which == 9)) { // tab
                if (this.options.type == "fixed") {
                    ev.preventDefault();

                    var p = this.getSelectionStart();
                    switch (ev.which) {
                        case 8: // Backspace
                            this.updateSelection(this.options.maskEmptyChr);
                            this.selectPrevious();
                            break;
                        case 36: // Home
                            this.selectFirst();
                            break;
                        case 35: // End
                            this.selectLast();
                            break;
                        case 37: // Left
                        case 38: // Up
                            this.selectPrevious();
                            break;
                        case 39: // Right
                        case 40: // Down
                            this.selectNext();
                            break;
                        case 46: // Delete
                            this.updateSelection(this.options.maskEmptyChr);
                            this.selectNext();
                            break;
                        default:
                            chr = this.chrFromEv(ev);
                            if (this.isViableInput(p, chr)) {
                                this.updateSelection(ev.shiftKey ? chr.toUpperCase() : chr);
                                this.node.trigger("valid", ev, this.node);
                                this.selectNext();
                            } else {
                                this.node.trigger("invalid", ev, this.node);
                            }
                            break;
                    }
                } else if (this.options.type == "number") {
                    switch (ev.which) {
                        case 35: // END
                        case 36: // HOME
                        case 37: // LEFT
                        case 38: // UP
                        case 39: // RIGHT
                        case 40: // DOWN
                            break;
                        case 8:  // backspace
                        case 46: // delete
                            var self = this;
                            setTimeout(function () {
                                self.formatNumber();
                            }, 1);
                            break;
                        case 189:
                        case 109: // -
                            ev.preventDefault();
                            range = (new Range(this)).valueOf();
                            val = this.domNode.value;
                            if (val.length == 0 || (range[0] == 0 && val.charAt(range[1]) != "-")) {
                                this.updateSelection("-");
                                this.formatNumber();
                            }
                            else if (val.length == range[0] && val.length == range[1] && val.charAt(0) != "-") {
                                this.domNode.value = "-" + val;
                            }
                            break;
                        default:
                            ev.preventDefault();

                            chr = this.chrFromEv(ev);
                            if (this.isViableInput(p, chr)) {
                                range = new Range(this);
                                val = this.sanityTest(range.replaceWith(chr));
                                if (val !== false) {
                                    this.updateSelection(chr);
                                    this.formatNumber();
                                }
                                this.node.trigger("valid", ev, this.node);
                            } else {
                                this.node.trigger("invalid", ev, this.node);
                            }
                            break;
                    }
                }
            }
        },

        allowKeys: {
            8: 1 // backspace
			, 9: 1 // tab
			, 13: 1 // enter
			, 35: 1 // end
			, 36: 1 // home
			, 37: 1 // left
			, 38: 1 // up
			, 39: 1 // right
			, 40: 1 // down
			, 46: 1 // delete
        },

        onKeyPress: function (ev) {
            var key = ev.which || ev.keyCode;

            if (
				!(this.allowKeys[key])
				&& !(ev.ctrlKey || ev.altKey || ev.metaKey)
			) {
                ev.preventDefault();
                ev.stopPropagation();
            }
        },

        onFocus: function (ev) {
            ev.stopPropagation();
            ev.preventDefault();

            this.options.showMask && (this.domNode.value = this.wearMask(this.domNode.value));
            this.sanityTest(this.domNode.value);

            var self = this;

            setTimeout(function () {
                self[self.options.type === "fixed" ? 'selectFirst' : 'setEnd']();
            }, 1);
        },

        onBlur: function (ev) {
            ev.stopPropagation();
            ev.preventDefault();

            if (this.options.stripMask)
                this.domNode.value = this.stripMask();
        },

        selectAll: function () {
            this.setSelection(0, this.domNode.value.length);
        },

        selectFirst: function () {
            for (var i = 0, len = this.options.mask.length; i < len; i++) {
                if (this.isInputPosition(i)) {
                    this.setSelection(i, (i + 1));
                    return;
                }
            }
        },

        selectLast: function () {
            for (var i = (this.options.mask.length - 1) ; i >= 0; i--) {
                if (this.isInputPosition(i)) {
                    this.setSelection(i, (i + 1));
                    return;
                }
            }
        },

        selectPrevious: function (p) {
            if (!$chk(p)) { p = this.getSelectionStart(); }

            if (p <= 0) {
                this.selectFirst();
            } else {
                if (this.isInputPosition(p - 1)) {
                    this.setSelection(p - 1, p);
                } else {
                    this.selectPrevious(p - 1);
                }
            }
        },

        selectNext: function (p) {
            if (!$chk(p)) { p = this.getSelectionEnd(); }

            if (this.isNumber()) {
                this.setSelection(p + 1, p + 1);
                return;
            }

            if (p >= this.options.mask.length) {
                this.selectLast();
            } else {
                if (this.isInputPosition(p)) {
                    this.setSelection(p, (p + 1));
                } else {
                    this.selectNext(p + 1);
                }
            }
        },

        setSelection: function (a, b) {
            a = a.valueOf();
            if (!b && a.splice) {
                b = a[1];
                a = a[0];
            }

            if (this.domNode.setSelectionRange) {
                this.domNode.focus();
                this.domNode.setSelectionRange(a, b);
            } else if (this.domNode.createTextRange) {
                var r = this.domNode.createTextRange();
                r.collapse();
                r.moveStart("character", a);
                r.moveEnd("character", (b - a));
                r.select();
            }
        },

        updateSelection: function (chr) {
            var value = this.domNode.value
			 , range = new Range(this)
			 , output = range.replaceWith(chr);

            this.domNode.value = output;
            if (range[0] === range[1]) {
                this.setSelection(range[0] + 1, range[0] + 1);
            } else {
                this.setSelection(range);
            }
        },

        setEnd: function () {
            var len = this.domNode.value.length;
            this.setSelection(len, len);
        },

        getSelectionRange: function () {
            return [this.getSelectionStart(), this.getSelectionEnd()];
        },

        getSelectionStart: function () {
            var p = 0,
			    n = this.domNode.selectionStart;

            if (n) {
                if (typeof (n) == "number") {
                    p = n;
                }
            } else if (document.selection) {
                var r = document.selection.createRange().duplicate();
                r.moveEnd("character", this.domNode.value.length);
                p = this.domNode.value.lastIndexOf(r.text);
                if (r.text == "") {
                    p = this.domNode.value.length;
                }
            }
            return p;
        },

        getSelectionEnd: function () {
            var p = 0,
			    n = this.domNode.selectionEnd;

            if (n) {
                if (typeof (n) == "number") {
                    p = n;
                }
            } else if (document.selection) {
                var r = document.selection.createRange().duplicate();
                r.moveStart("character", -this.domNode.value.length);
                p = r.text.length;
            }
            return p;
        },

        isInputPosition: function (p) {
            var mask = this.options.mask.toLowerCase();
            var chr = mask.charAt(p);
            return !! ~"9ax".indexOf(chr);
        },

        sanityTest: function (str, p) {
            var sanity = this.options.sanity;

            if (sanity instanceof RegExp) {
                return sanity.test(str);
            } else if ($.isFunction(sanity)) {
                var ret = sanity(str, p);
                if (typeof (ret) == 'boolean') {
                    return ret;
                } else if (typeof (ret) != 'undefined') {
                    if (this.isFixed()) {
                        p = this.getSelectionStart();
                        this.domNode.value = this.wearMask(ret);
                        this.setSelection(p, p + 1);
                        this.selectNext();
                    } else if (this.isNumber()) {
                        var range = new Range(this);
                        this.domNode.value = ret;
                        this.setSelection(range);
                        this.formatNumber();
                    }
                    return false;
                }
            }
        },

        isViableInput: function () {
            return this[this.isFixed() ? 'isViableFixedInput' : 'isViableNumericInput'].apply(this, arguments);
        },

        isViableFixedInput: function (p, chr) {
            var mask = this.options.mask.toLowerCase();
            var chMask = mask.charAt(p);

            var val = this.domNode.value.split('');
            val.splice(p, 1, chr);
            val = val.join('');

            var ret = this.sanityTest(val, p);
            if (typeof (ret) == 'boolean') { return ret; }
            var maskObj = {
                '9': this.options.validNumbers,
                'a': this.options.validAlphas,
                'x': this.options.validAlphaNums
            };
            if ((maskObj[chMask] || '').indexOf(chr) >= 0) {
                return true;
            }
            return false;
        },

        isViableNumericInput: function (p, chr) {
            return !! ~this.options.validNumbers.indexOf(chr);
        },

        wearMask: function (str) {
            var mask = this.options.mask.toLowerCase()
			 , output = ""
			 , chrSets = {
			     '9': 'validNumbers'
				, 'a': 'validAlphas'
				, 'x': 'validAlphaNums'
			 };

            for (var i = 0, u = 0, len = mask.length; i < len; i++) {
                switch (mask.charAt(i)) {
                    case '9':
                    case 'a':
                    case 'x':
                        output +=
							((this.options[chrSets[mask.charAt(i)]].indexOf(str.charAt(u).toLowerCase()) >= 0) && (str.charAt(u) != ""))
								? str.charAt(u++)
								: this.options.maskEmptyChr;
                        break;

                    default:
                        output += mask.charAt(i);
                        if (str.charAt(u) == mask.charAt(i)) {
                            u++;
                        }

                        break;
                }
            }
            return output;
        },

        stripMask: function () {
            var value = this.domNode.value;
            if ("" == value) return "";
            var output = "", i = 0, len = value.length;
            if (this.isFixed()) {
                for (i = 0; i < len; i++) {
                    if ((value.charAt(i) != this.options.maskEmptyChr) && (this.isInputPosition(i)))
                    { output += value.charAt(i); }
                }
            } else if (this.isNumber()) {
                for (i = 0; i < len; i++) {
                    if (this.options.validNumbers.indexOf(value.charAt(i)) >= 0 || (value.charAt(i) == "-" && i == 0)) {
                        output += value.charAt(i);
                    }
                }
            }
            return output;
        },

        chrFromEv: function (ev) {
            var chr = '', key = ev.which;

            if (key >= 96 && key <= 105) { key -= 48; }     // shift number-pad numbers to corresponding character codes
            chr = String.fromCharCode(key).toLowerCase(); // key pressed as a lowercase string
            return chr;
        },

        formatNumber: function () {
            // stripLeadingZeros
            var dnv = this.domNode.value || ""
            , hasSub = dnv.charAt(0) == "-"
            , olen = dnv.length
            , str2 = this.stripMask()
			, str1 = str2.replace(/^[\-]*0+/, '')
			, range = new Range(this);

            // wearLeadingZeros

            str2 = str1;
            str1 = "";
            for (var len = str2.length, i = this.options.decDigits; len <= i; len++) {
                str1 += "0";
            }
            str1 += str2;

            // decimalSymbol
            str2 = str1.substr(str1.length - this.options.decDigits)
            str1 = str1.substring(0, (str1.length - this.options.decDigits))

            // groupSymbols
            var re = new RegExp("(\\d+)(\\d{" + this.options.groupDigits + "})");
            while (re.test(str1)) {
                str1 = str1.replace(re, "$1" + this.options.groupSymbol + "$2");
            }
            if (hasSub && str1.charAt(0) != "-")
                str1 = "-" + str1;
            this.domNode.value = this.options.currencySymbol + str1 + this.options.decSymbol + str2;
            this.setSelection(range);
        },

        getObjForm: function () {
            return this.node.parents('form');
        },

        submitForm: function () {
            var form = this.getObjForm();
            form.trigger('submit');
        }
    };

    function Range(obj) {
        this.range = obj.getSelectionRange();
        this.len = obj.domNode.value.length
        this.obj = obj;

        this['0'] = this.range[0];
        this['1'] = this.range[1];
    }
    Range.prototype = {
        valueOf: function () {
            var len = this.len - this.obj.domNode.value.length;
            return [this.range[0] - len, this.range[1] - len];
        },
        replaceWith: function (str) {
            var val = this.obj.domNode.value
			 , range = this.valueOf();

            return val.substr(0, range[0]) + str + val.substr(range[1]);
        }
    };

    $.fn.iMask = function (options) {
        this.each(function () {
            new iMask($(this), options);
        });
    };
})(jQuery);

HOYI.log = function (txt) {
    if (!this.lgSpan)
        this.lgSpan = $("#pswebbtm span");
    this.lgSpan.text(txt);
};

HOYI.ie8Radius = (function () {
    function findPos(obj) {
        var curleft = curtop = 0;

        if (obj.offsetParent) {
            do {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
        }

        return ({
            'x': curleft,
            'y': curtop
        });
    };

    function wrapBorder() {



        var arcSize = Math.min(parseInt(this.currentStyle['-moz-border-radius'] ||
                                        this.currentStyle['-webkit-border-radius'] ||
                                        this.currentStyle['border-radius'] ||
                                        this.currentStyle['-khtml-border-radius']) /
                               Math.min(this.offsetWidth, this.offsetHeight), 1);

        var fillColor = this.currentStyle.backgroundColor;
        var fillSrc = this.currentStyle.backgroundImage.replace(/^url\("(.+)"\)$/, '$1');
        var strokeColor = this.currentStyle.borderColor;
        var strokeWeight = parseInt(this.currentStyle.borderWidth);
        var stroked = 'true';
        if (isNaN(strokeWeight)) {
            strokeWeight = 0;
            strokeColor = fillColor;
            stroked = 'false';
        }


        // Find which element provides position:relative for the target element (default to BODY)
        var el = this;

        var limit = 100, i = 0;
        while ((typeof (el) != 'unknown') && (el.currentStyle.position != 'relative') && (el.tagName != 'BODY')) {
            el = el.parentElement;
            i++;
            if (i >= limit) { return (false); }
        }
        if (!el.document || !el.document.createStyleSheet) return;
        var el_zindex = parseInt(el.currentStyle.zIndex);

        if (isNaN(el_zindex)) { el_zindex = 0; }

        //alert('got tag '+ el.tagName +' with pos '+ el.currentStyle.position);
        var jt = $(this);
        var rect_size = {
            'width': jt.outerWidth() - strokeWeight,
            'height': jt.outerHeight() - strokeWeight
        };
        if (rect_size.width <= 0 || rect_size.height <= 0 || isNaN(rect_size.width) || isNaN(rect_size.height))
            return;
        this.style.background = 'transparent';
        this.style.borderColor = 'transparent';
        var el_pos = findPos(el);
        var this_pos = findPos(this);
        this_pos.y = this_pos.y + (0.5 * strokeWeight) - el_pos.y;
        this_pos.x = this_pos.x + (0.5 * strokeWeight) - el_pos.x;

        var rect = document.createElement('v:roundrect');
        rect.arcsize = arcSize + 'px';
        rect.strokecolor = strokeColor;
        rect.strokeWeight = strokeWeight + 'px';
        rect.stroked = stroked;
        rect.style.display = 'block';
        rect.style.position = 'absolute';
        rect.style.top = this_pos.y + 'px';
        rect.style.left = this_pos.x + 'px';
        rect.style.width = rect_size.width + 'px';
        rect.style.height = rect_size.height + 'px';
        rect.style.antialias = true;
        rect.style.zIndex = el_zindex - 1;
        var fill = document.createElement('v:fill');
        fill.color = fillColor;
        fill.src = fillSrc;
        fill.type = 'tile';

        rect.appendChild(fill);
        el.appendChild(rect);

        var css = el.document.createStyleSheet();
        css.addRule("v\\:roundrect", "behavior: url(#default#VML)");
        css.addRule("v\\:fill", "behavior: url(#default#VML)");

        isIE6 = /msie|MSIE 6/.test(navigator.userAgent);
        // IE6 doesn't support transparent borders, use padding to offset original element
        if (isIE6 && (strokeWeight > 0)) {
            this.style.borderStyle = 'none';
            this.style.paddingTop = parseInt(this.currentStyle.paddingTop || 0) + strokeWeight;
            this.style.paddingBottom = parseInt(this.currentStyle.paddingBottom || 0) + strokeWeight;
        }

        if (typeof (window.rounded_elements) == 'undefined') {
            window.rounded_elements = new Array();

            if (typeof (window.onresize) == 'function') { window.previous_onresize = window.onresize; }
            window.onresize = window_resize;
        }
        this.vml = rect;
        window.rounded_elements.push(this);
    };

    function window_resize() {
        if (typeof (window.rounded_elements) == 'undefined') { return (false); }

        for (var i in window.rounded_elements) {
            var el = window.rounded_elements[i];
            if (!el || !el.vml) continue;

            var strokeWeight = el.currentStyle ? parseInt(el.currentStyle.borderWidth) : 0;
            if (isNaN(strokeWeight)) { strokeWeight = 0; }

            var parent_pos = findPos(el.vml.parentNode);
            var pos = findPos(el);
            pos.y = pos.y + (0.5 * strokeWeight) - parent_pos.y;
            pos.x = pos.x + (0.5 * strokeWeight) - parent_pos.x;

            el.vml.style.top = pos.y + 'px';
            el.vml.style.left = pos.x + 'px';
        }

        if (typeof (window.previous_onresize) == 'function') { window.previous_onresize(); }
    }
    return function (el) {
        wrapBorder.call(el);
    };
})();

HOYI.storager = {
    ensure: function () {
        if (this.isChecked) return;
        this.isChecked = true;
        var strg = window.localStorage || (window.globalStorage ? window.globalStorage["www.morningstar.com"] : null);
        if (strg != null) {
            try {
                strg.setItem("~check", "pass");
                strg.removeItem("~check");
                this.storage = strg;
            }
            catch (e) { }
        }
    },
    getKey: function (ikey) {
        return HOYI.clientKey ? ikey + "_" + HOYI.clientKey : ikeye;
    },
    save: function (ikey, value, opts) {
        var key = this.getKey(ikey);
        this.ensure();
        if (this.storage)
            this.storage.setItem(key, value);
        else
            $.cookie(key, value, opts || { expires: 10000 });

    },
    get: function (ikey) {
        this.ensure();
        var key = this.getKey(ikey);
        if (this.storage)
            return this.storage[key];
        else
            return $.cookie(key);
    },
    del: function (ikey) {
        this.ensure();
        var key = this.getKey(ikey);
        if (this.storage)
            this.storage.removeItem(key);
        else
            $.cookie(key, "", { expires: -1 });
    }
};