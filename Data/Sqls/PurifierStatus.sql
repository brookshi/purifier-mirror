USE [Purifier]
GO

/****** Object:  Table [dbo].[PurifierStatus]    Script Date: 03/17/2014 17:47:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PurifierStatus](
	[ID] [bigint] NOT NULL,
	[IP] [varchar](20) NULL,
	[MAC] [varchar](20) NULL,
	[Tear] [int] NULL,
	[OnOffKey] [int] NULL,
	[LockKey] [int] NULL,
	[SleepKey] [int] NULL,
	[AutoKey] [int] NULL,
	[AnionKey] [int] NULL,
	[UVKey] [int] NULL,
	[IsFliter] [int] NULL,
	[TimerHour] [int] NULL,
	[TimerMin] [int] NULL,
	[CreatedTime] [datetime] NULL,
	[HasChanged] [bit] NULL,
	[StatusFlag] [int] NULL,
	[TimeFlag] [int] NULL,
 CONSTRAINT [PK_PURIFIESTATUS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'净化器ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'档位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'Tear'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开关状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'OnOffKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'睡眠' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'SleepKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自动' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'AutoKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'负离子' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'AnionKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'UV键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'UVKey'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'更换滤网' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus', @level2type=N'COLUMN',@level2name=N'IsFliter'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'净化器信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurifierStatus'
GO

