﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace HOYI.Common
{
    /// <summary>
    /// A common comparer
    /// </summary>
    /// <typeparam name="T">A type compared</typeparam>
    public class CommonComparer<T> : IComparer, IComparer<T>, IEqualityComparer, IEqualityComparer<T>
    {
        /// <summary>
        /// A member name of type compared
        /// </summary>
        public string CompareMemberName { get; private set; }
        /// <summary>
        /// If order is asc
        /// </summary>
        public bool IsAscOrder { get; set; }
        /// <summary>
        /// Get/Set IComparer to compare member value of T.
        /// </summary>
        public IComparer ExternalComparer { get; set; }
        MemberInfo sortedMmbInfo;
        Type sortedType;

        /// <summary>
        /// Construtor CommonComparer
        /// </summary>
        /// <param name="compareMemberName">A member name for get value from T instance </param>
        /// <param name="isAscOrder">is asc order?</param>
        public CommonComparer(string compareMemberName, bool isAscOrder)
        {
            if (string.IsNullOrEmpty(compareMemberName))
                throw new ArgumentNullException("compareMemberName");
            CompareMemberName = compareMemberName;
            IsAscOrder = isAscOrder;
            sortedMmbInfo = typeof(T).GetProperty(CompareMemberName, BindingFlags.Instance | BindingFlags.Public);
            if (sortedMmbInfo == null)
                sortedMmbInfo = typeof(T).GetType().GetField(CompareMemberName, BindingFlags.Instance | BindingFlags.Public);
            ExternalComparer = null;
        }

        object GetValue(object obj)
        {
            if (sortedMmbInfo is PropertyInfo)
            {
                var pi = (sortedMmbInfo as PropertyInfo);
                if (sortedType == null)
                    sortedType = pi.PropertyType;
                return (sortedMmbInfo as PropertyInfo).GetValue(obj, null);
            }
            else if (sortedMmbInfo is FieldInfo)
            {
                var fi = sortedMmbInfo as FieldInfo;
                if (sortedType == null)
                    sortedType = fi.FieldType;
                return fi.GetValue(obj);
            }
            else
                return null;
        }

        int CompareObj(object x, object y)
        {
            if (sortedMmbInfo == null && (x != null || y != null))
            {
                sortedMmbInfo = (x != null ? x : y).GetType().GetProperty(CompareMemberName, BindingFlags.Instance | BindingFlags.Public);
                if (sortedMmbInfo == null)
                    sortedMmbInfo = (x != null ? x : y).GetType().GetField(CompareMemberName, BindingFlags.Instance | BindingFlags.Public);
            }
            if (sortedMmbInfo == null)
                return 0;
            object v1 = GetValue(x);
            object v2 = GetValue(y);
            if (ExternalComparer != null)
                return ExternalComparer.Compare(IsAscOrder ? v1 : v2, IsAscOrder ? v2 : v1);
            else
            {
                if (v1 != null && v2 != null && typeof(IComparable).IsAssignableFrom(v1.GetType()))
                {
                    if (IsAscOrder)
                        return ((IComparable)v1).CompareTo(v2);
                    else
                        return ((IComparable)v2).CompareTo(v1);
                }
                else
                {
                    string sv1 = v1 == null ? string.Empty : v1.ToString();
                    string sv2 = v2 == null ? string.Empty : v2.ToString();

                    if (IsAscOrder)
                        return sv1.CompareTo(sv2);
                    else
                        return sv2.CompareTo(sv1);
                }
            }
        }

        #region IComparer Members
        /// <summary>
        /// Compare two T object
        /// </summary>
        /// <param name="x">x T object</param>
        /// <param name="y">y T object</param>
        /// <returns></returns>
        public int Compare(object x, object y)
        {
            return CompareObj(x, y);
        }

        #endregion

        #region IComparer<string> Members
        /// <summary>
        /// Compare two T object
        /// </summary>
        /// <param name="x">x T object</param>
        /// <param name="y">y T object</param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            return CompareObj(x, y);
        }

        #endregion

        #region IEqualityComparer Members
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <param name="obj">Hashed obj</param>
        /// <returns> A hash code for the current System.Object.</returns>
        public int GetHashCode(object obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            object v = GetValue(obj);
            if (v != null)
                return v.GetHashCode();
            else
                return int.MinValue;
        }

        bool IEqualityComparer.Equals(object x, object y)
        {
            return Compare(x, y) == 0;
        }

        #endregion

        #region IEqualityComparer<T> Members
        /// <summary>
        /// Determines whether two specified T objects have the  same value of member name.
        /// </summary>
        /// <param name="x">A T or null.</param>
        /// <param name="y">A T or null.</param>
        /// <returns></returns>
        public bool Equals(T x, T y)
        {
            return Compare(x, y) == 0;
        }
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <param name="obj">Hashed obj</param>
        /// <returns> A hash code for the current T.</returns>
        public int GetHashCode(T obj)
        {
            return GetHashCode((object)obj);
        }

        #endregion
    }

    /// <summary>
    /// Sorter List&lt;T&gt;
    /// </summary>
    public class ListSorter
    {
        /// <summary>
        /// Get/Set a name as ordering gist, the name must be a member name of the list item.
        /// </summary>
        public string OrderName { get; private set; }

        /// <summary>
        /// If order is asc.
        /// </summary>
        public bool IsAsc { get; private set; }

        /// <summary>
        /// Initializes a new instance of the ListSorter class.
        /// </summary>
        /// <param name="orderName">set to property 'OrderName'</param>
        /// <param name="isAsc">set to property 'IsAsc'</param>
        public ListSorter(string orderName, bool isAsc)
        {
            OrderName = orderName;
            IsAsc = isAsc;
        }

        /// <summary>
        /// Sort a list
        /// </summary>
        /// <typeparam name="T">Item type of the list</typeparam>
        /// <param name="list">A list sorted.</param>
        /// <returns>A CommonComparer<T> instance</returns>
        public CommonComparer<T> Sort<T>(List<T> list)
        {
            var cp = new CommonComparer<T>(OrderName, IsAsc);
            list.Sort(cp);
            return cp;
        }

        /// <summary>
        /// Paging a list
        /// </summary>
        /// <typeparam name="T">Item type of the list</typeparam>
        /// <param name="list">A list paged</param>
        /// <param name="pageCount">every page count</param>
        /// <param name="pageIndex">What page</param>
        /// <returns></returns>
        public List<T> Paging<T>(List<T> list, int pageCount, int pageIndex)
        {
            Sort(list);
            int begin = pageIndex * pageCount;
            int end = begin + pageCount - 1;
            var ta = new T[Math.Min(pageCount, list.Count - begin)];
            if (list.Count > begin)
                Array.Copy(list.ToArray(), begin, ta, 0, ta.Length);
            return new List<T>(ta);
        }
    }
}
