﻿using System;
using System.Collections.Generic;
using System.Reflection;
using HOYI.Common.Data.Collections;

namespace HOYI.Common.Data
{
    public class DataObjInfo
    {
        public string TableName { get; private set; }
        public string PrimaryKeyName { get; private set; }
        public PropertyInfo PrimaryKeyInfo { get; private set; }
        public string DBTemplate { get; private set; }
        public string DBName { get; private set; }
        public string SqlServerDBName { get; private set; }
        public DataFieldInfoList Fileds { get; private set; }
        public bool EnableMultiDb { get; private set; }
        public int DbCount { get; private set; }
        public IIdGenerator IdGenerator { get; private set; }
        public DataMapSPsAttribute SPs { get; private set; }
 
        protected internal virtual List<string> GetWriteFields()
        {
            var wfds = new List<string>();
            foreach (var dfi in Fileds)
            {
                if (dfi.CanWriteDb)
                    wfds.Add(dfi.FieldName);
            }
            return wfds;
        }

        protected internal virtual List<string> GetUpdateFields()
        {
            var wfds = new List<string>();
            foreach (var dfi in Fileds)
            {
                if (dfi.CanWriteDb && !dfi.IsPrimaryKey)
                    wfds.Add(dfi.FieldName);
            }
            return wfds;
        }

        protected internal virtual string GetPrimaryKeyDbName()
        {
            foreach (var dfi in Fileds)
            {
                if (dfi.IsPrimaryKey)
                    return dfi.FieldName;
            }
            return PrimaryKeyName;
        }

        static readonly Dictionary<Type, DataObjInfo> infoDic = new Dictionary<Type, DataObjInfo>();

        internal static DataObjInfo GetDataObjInfo(Type dataObjType)
        {
            lock (infoDic)
            {
                if (infoDic.ContainsKey(dataObjType))
                    return infoDic[dataObjType];
                Type idGenType = null;
                object[] doas = dataObjType.GetCustomAttributes(typeof(DataObjAttribute), true);
                var doInfo = new DataObjInfo();
                doInfo.TableName = dataObjType.Name;
                doInfo.DBTemplate = dataObjType.Name;
                doInfo.PrimaryKeyName = "DataId";
                doInfo.DBName = dataObjType.Namespace + "Db";
                doInfo.EnableMultiDb = false;
                doInfo.DbCount = 1;
                if (doas.Length > 0)
                {
                    var hasSetteds = new List<int>();
                    foreach (DataObjAttribute da in doas)
                    {
                        if (!string.IsNullOrEmpty(da.TableName) && !hasSetteds.Contains(1))
                        {
                            doInfo.TableName = da.TableName;
                            hasSetteds.Add(1);
                        }
                        if (!string.IsNullOrEmpty(da.DBTemplate) && !hasSetteds.Contains(2))
                        {
                            doInfo.DBTemplate = da.DBTemplate;
                            hasSetteds.Add(2);
                        }
                        if (!string.IsNullOrEmpty(da.PrimaryKeyName) && !hasSetteds.Contains(3))
                        {
                            doInfo.PrimaryKeyName = da.PrimaryKeyName;
                            hasSetteds.Add(3);
                        }
                        if (!string.IsNullOrEmpty(da.DBName) && !hasSetteds.Contains(4))
                        {
                            doInfo.DBName = da.DBName;
                            hasSetteds.Contains(4);
                        }
                        if (!string.IsNullOrEmpty(da.SqlServerDBName) && !hasSetteds.Contains(5))
                        {
                            doInfo.SqlServerDBName = da.SqlServerDBName;
                            hasSetteds.Add(5);
                        }
                        idGenType = da.IdGeneratorType;
                    }
                    doInfo.EnableMultiDb = ((DataObjAttribute)doas[0]).EnableMultiDb;
                    doInfo.DbCount = ((DataObjAttribute)doas[0]).DbCount;
                }
                doInfo.PrimaryKeyInfo = dataObjType.GetProperty(doInfo.PrimaryKeyName, BaseDataObjPersister.ConstPubInsFlag | BindingFlags.NonPublic);
                doInfo.Fileds = DataFieldInfo.GetDataFieldInfo(dataObjType);
                foreach (var dfi in doInfo.Fileds)
                {
                    if (dfi.FieldInfo.Name == doInfo.PrimaryKeyName)
                    {
                        dfi.IsPrimaryKey = true;
                        break;
                    }
                }
                if (doInfo.PrimaryKeyInfo == null)
                {
                    throw new InvalidOperationException(dataObjType.FullName + " hasn't primery key or can't map to other field.");
                }
                if (idGenType != null)
                {
                    if (!typeof(IIdGenerator).IsAssignableFrom(idGenType))
                        throw new InvalidOperationException(string.Format("The id generator of the '{0}' doesn't inherit from 'IIdGenerator'", dataObjType.FullName));

                    doInfo.IdGenerator = (IIdGenerator)Activator.CreateInstance(idGenType);
                }
                
                var dmpas = dataObjType.GetCustomAttributes(typeof(DataMapSPsAttribute), true);
                if (dmpas.Length > 0) doInfo.SPs = (DataMapSPsAttribute)dmpas[0];

                infoDic[dataObjType] = doInfo;
                return doInfo;
            }
        }
    }
}
