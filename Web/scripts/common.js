﻿///<reference path="jquery-vsdoc.js" />
CM = {
    com: {
        grid: function (colKey, rwDbClickHd, dgc, pagingc) {
            var cols = [], gridc = dgc || "grid", pgcpt, gdcpt, the = this, onClDbClkEvents = null, rowBoundHds = new CM.funLinked();
            var settings = $mo({}, Util.DataGrid.defaultSetting), evtHds = { onHeaderDblClick: HOYI.gdDefHeaderDblClick };
            settings.indexColKey = colKey || "Id";
            settings.column = cols;
            evtHds.hd = rwDbClickHd;
            evtHds.onRowDoubleClick = function (e, row) {
                if (evtHds.hd) evtHds.hd(e, row);
                if (onClDbClkEvents) onClDbClkEvents.call(row, [e, row]);
            };
            this.disableDataCache = true;
            this.addCol = function (header, dataIndex, width, isFixed, noSort, additional, clRender) {
                var clSetting = { "header": header || $msg(dataIndex), "dataIndex": dataIndex, "width": width, "isfixed": isFixed || false, "noSort": noSort || false, "additional": additional || {} };
                if (clRender) clSetting.renderer = clRender;
                cols.push(clSetting);
                return clSetting;
            };
            function getGridContainer() {
                return $.type(gridc) == "string" ? $('#' + gridc) : $(gridc);
            };
            function initGridWidth() {
                settings.width = getGridContainer().width();
                var gdw = settings.width;
                if (!gdw) {
                    var jbd = $(document.body);
                    var tw = Math.max(jbd.width(), 1024);
                    settings.width = gdw = tw - 2;
                }
                return settings.width;
            }
            this.calcColPercent = function () {
                var gdw = initGridWidth();
                var gdSetting = $.extend({}, settings);
                var colus = gdSetting.column = cols.copy();
                settings.calcCols = colus;
                var colTW = gdw - (this.simpleTable ? 0 : (colus.length * 2));
                if (gdSetting["checkBoxButton"] !== false) {
                    if (gdSetting["checkBoxCellWidth"]) {
                        if (this.simpleTable) {
                            var cbs = gdSetting.checkBoxButton.css;
                            colTW = colTW - (cbs && cbs.width ? parseInt(cbs.width) : 20);
                        }
                        else
                            colTW = colTW - parseInt(gdSetting["checkBoxCellWidth"]) - 1;
                    }
                    else
                        colTW = colTW - 21;
                }
                else
                    colTW++;
                var preCols = [];
                for (var c = 0; c < colus.length; c++) {
                    var fxw = colus[c];
                    if ($.type(fxw.width) == "string") {
                        if (fxw.width.lastIndexOf("%") == (fxw.width.length - 1)) {
                            preCols.push(fxw);
                        }
                        else if (fxw.width != "auto")
                            colTW = colTW - parseInt(fxw.width, 10);
                    }
                    else if (fxw.width) {
                        colTW = colTW - fxw.width;
                    }
                }
                if (colTW <= 0)
                    gdSetting.width = gdSetting.width + 0;
                else {
                    var otw = colTW;
                    for (var prc = 0; prc < preCols.length; prc++) {
                        var prcl = preCols[prc];
                        prcl.width = parseInt((colTW * parseInt(prcl.width) / 100).toString(), 10);
                        otw = otw - prcl.width;
                    }
                    if (otw != colTW && (!this.simpleTable || !preCols[0].width)) {
                        preCols[0].width += otw;
                    }
                }
                return gdSetting;
            };
            this.clearHeader = function () {
                this.cols.clear();
            };
            this.onCellResize = function (onresize) {
                evtHds.afterCellResize = onresize;
            };
            this.onRowSelected = function (onrowselected) {
                evtHds.onRowSelected = onrowselected;
            };
            this.onCellDoubleClick = function (cellIndex, onDbClick) {
                if (!onClDbClkEvents)
                    onClDbClkEvents = new CM.funLinked();
                var dbHd = function (e, row) {
                    if (row.cells[cellIndex].innerDom == e.target) {
                        onDbClick.call(this, e, row);
                    }
                }
                onClDbClkEvents.reg(dbHd);
            };
            this.adjustCellsSize = function () {
                var gdcn = getGridContainer();
                if (gdcn.is(":hidden")) return;
                var nowWidth = settings.width;
                var cfgs = this.calcColPercent();
                if (cfgs.width != nowWidth) {
                    var cs = cfgs.column;
                    for (var c = 0, ln = cs.length; c < ln; c++) {
                        this.resizeCell(cs[c].dataIndex, cs[c].width, cs[c].height);
                    }
                }
            };
            var onWindowResize = this.adjustCellsSize.bind(this);
            function monitorWindowResize() {
                $(window).unbind("resize", onWindowResize).bind("resize", {}, onWindowResize);
            };
            this.stopResize = function () {
                $(window).unbind("resize", onWindowResize);
            };
            this.create = function () {
                if (pgcpt) pgcpt.clear();
                var pgc = null;
                if (!pagingc || $.type(pagingc) == "string")
                    pgc = $(pagingc || "#pageLabel");
                else
                    pgc = $(pagingc);
                if (pgc && pgc.length > 0)
                    pgcpt = this.pgcmpt = HOYI.pageConpoment = new Util.PageLayer(pgc);
                var jgdc = getGridContainer();
                this.removeRows();
                $("*", jgdc).remove();
                var gdSetting = this.calcColPercent();
                if (gdSetting.height == "auto") {
                    gdSetting.height = jgdc.height();
                }
                HOYI.grid = gdcpt = this.gridObj = this.simpleTable ? new CM.table(jgdc[0], gdSetting) : new Util.DataGrid(jgdc[0], {}, gdSetting, evtHds);
                monitorWindowResize();
                return HOYI.grid;
            };
            this.resizeCell = function (cellName, size, inde) {
                if (!gdcpt) return;
                var rows = [gdcpt.header.row], dfcw = 0;
                var ccfg = $byId(settings.calcCols, cellName, "dataIndex");
                if (ccfg) {
                    if (inde) {
                        settings.width += size;
                        ccfg.width += size;
                    }
                    else {
                        var df = size - ccfg.width;
                        settings.width = settings.width + (df || 0);
                        ccfg.width = size;
                    }
                    gdcpt.resizeCell(cellName, ccfg.width);
                    gdcpt.resize && gdcpt.resize(settings.width);
                }
            };
            this.completed = function () {
                for (var prop in gdcpt.rows) {
                    var rw = gdcpt.rows[prop];
                    if (rw.data.isGDBlankData) continue;
                    rowBound(rw, gdcpt);
                    formatRowCell(this, rw);
                }
                if (this.doneHandler) this.doneHandler(gdcpt);
            };
            this.preBind = function (d, pd, action) {
                if (this.hisHandler) {
                    if (!HOYI.ajaxHis.isHis()) {
                        var pg = pgcpt.getCurrentPage();
                        var an = this.getHistoryKey(action, pd), me = this;
                        HOYI.ajaxHis.regHandler(an, function (n, d) { me.recoverHis(n, d); });
                        d.postData = pd;
                        d.pageIndex = pg;
                        if (!$.isEmptyObject(HOYI.cookies))
                            d.cookies = $mo2({}, HOYI.cookies);
                        HOYI.ajaxHis.remember(an, d);
                    }
                }
                if (this.befBindHandler) this.befBindHandler(d);
                formatDate(d);
            };
            this.getHistoryKey = function (action, actParam) {
                var pg = pgcpt.getCurrentPage();
                return action + pg;
            };
            this.bind = function (data) {
                if (gdcpt.scrollPanel) {
                    var spl = gdcpt.scrollPanel;
                    if (spl.scrollbarY) spl.scrollbarY.scrollTo(0);
                    if (spl.scrollbarX) spl.scrollbarX.scrollTo(0);
                }
                formatDate(data);
                gdcpt.gridData = null;
                $ug(gdcpt, data);
                this.completed();
            };
            this.listData = function (action, data, pgIndex, befBind, done, tg) {
                var ld = function (npg) {
                    if (pgcpt && npg)
                        pgcpt.nextPage = npg;
                    var psgd = the;
                    var comp = function () {
                        psgd.doneHandler = done;
                        psgd.completed();
                    };
                    var pre = function (d, pd) {
                        psgd.befBindHandler = befBind;
                        psgd.preBind(d, pd, action);
                    };
                    var arg = data || {};
                    arg.disableCache = psgd.disableDataCache;
                    if (psgd.disableDataCache)
                        psgd.disableDataCache = false;
                    var onPreReq = function (ac, ag, p) {
                        psgd.beforListHandler && psgd.beforListHandler(ac, ag, p);
                    };
                    listData(action, arg, gdcpt, pgcpt, pre, comp, tg, onPreReq);
                };
                this.listGrid = ld;
                gdcpt.listData = ld;
                if (this.pgcmpt) HOYI.listGrid = ld;
                ld(pgIndex);
            };
            this.changeSettings = function (nw) {
                $.extend(settings, nw);
            };
            this.enableCellLink = function (colName, clk) {
                if (colName && clk)
                    this.cellLink = { "colName": colName, handler: clk };
                else
                    this.cellLink = null;
            };
            this.enableFileLink = function (colName, typeIndex, nameIndex, idIndex) {
                if (colName)
                    this.fileLink = { "colName": colName, "typeIndex": typeIndex, "nameIndex": nameIndex || colName, "idIndex": idIndex || settings.indexColKey };
                else
                    this.fileLink = null;
            };
            this.addRowBoundHandler = function (hd) {
                rowBoundHds.reg(hd, this);
            };
            this.cellBound = null; //cell, [cols[s], rw, psgd, s]
            this.enableHistory = function (hisH) {
                if (hisH)
                    this.hisHandler = { "h": hisH };
                else
                    this.hisHandler = null;
            };
            this.recoverHis = function (hisName, hisData) {
                this.create();
                this.preBind(hisData, hisData.postData);
                $ug(gdcpt, hisData.UIData);
                pgcpt.setCurrentPage(hisData.pageIndex);
                HOYI.listGrid = this.listGrid;
                pgcpt.setPageLabel(hisData, this.listGrid, "");
                this.completed();
                if (this.hisHandler.h) this.hisHandler.h(hisName, hisData);
            };
            this.cols = cols;
            this.simpleTable = false;
            this.getData = function (all) {
                if (!all) {
                    if (gdcpt.cfg.multiSelect)
                        return $gridData(gdcpt);
                    else
                        return gdcpt.selectedRow && gdcpt.selectedRow.data ? [gdcpt.selectedRow.data] : null;
                }
                else {
                    var data = [];
                    for (var rp in gdcpt.rows) {
                        if (rp && gdcpt.rows[rp] && gdcpt.rows[rp].data && !gdcpt.rows[rp].data.isGDBlankData)
                            data.push(gdcpt.rows[rp].data);
                    }
                    return data;
                }
            };
            this.getSelected = function (noTip, tipc, msg) {
                var data = this.getData(false);
                if (data && data.length)
                    return data;
                else {
                    if (!noTip) CM.com.showTip(msg || "请至少选择一条数据。", tipc, 2000);
                }
                return null;
            };
            this.getUnSelectedData = function () {
                var data = [];
                var allRows = gdcpt.rows, selRows = gdcpt.selectedRows;
                if (gdcpt.selectedRow) selRows[gdcpt.selectedRow.id] = gdcpt.selectedRow;
                for (var rp in allRows) {
                    if (rp && !selRows[rp] && !allRows[rp].disable && !allRows[rp].data.isGDBlankData) data.push(allRows[rp].data);
                }
                return data;
            };
            this.getDisabledData = function () {
                var data = [];
                var allRows = gdcpt.rows;
                for (var rp in allRows) {
                    if (rp && allRows[rp] && allRows[rp].disable) data.push(allRows[rp].data);
                }
                return data;
            };
            this.getRow = function (rowId) {
                return gdcpt.rows[rowId];
            };
            this.getRowData = function (rwId) {
                return this.getRow(rwId).data;
            };
            this.addRow = function (data, index) {
                if (!data) {
                    data = {};
                    for (var s = 0; s < cols.length; s++) {
                        if (!cols[s].dataIndex) continue;
                        data[cols[s].dataIndex] = "";
                    }
                }
                if (!data[settings.indexColKey]) data[settings.indexColKey] = (new Date()).getTime();
                if (gdcpt.rows[data[settings.indexColKey]])
                    return false;
                else {
                    if (!data.isGDBlankData) {
                        var blankRow = null;
                        for (var rwp in gdcpt.rows) {
                            var brw = gdcpt.rows[rwp];
                            if (brw.isBlank) { blankRow = brw; break; }
                        }
                        if (blankRow) gdcpt.removeRow(blankRow.id);
                        formatDate([data]);
                    }
                    if (gdcpt._appendRowTo) {
                        gdcpt.addRow(data);
                        var rw = gdcpt.rows[data[settings.indexColKey]];
                        if (arguments.length >= 2)
                            gdcpt._appendRowTo(rw, index);
                    }
                    else
                        gdcpt.addRow(data, index);

                    rowBound(rw, gdcpt);
                    if (!data.isGDBlankData) formatRowCell(this, rw);
                    return true;
                }
            };
            function rowBound(row, gd) {
                rowBoundHds.call(this, arguments);
            };
            this.reFormatRow = function (rowId) {
                var row = gdcpt.rows[rowId];
                if (row && !row.isBlank) formatRowCell(this, row);
            };
            this.addBlankRow = function () {
                var d = { isGDBlankData: true };
                this.addRow(d);
            };
            this.selectAll = function () {
                if (gdcpt) {
                    for (var grwp in gdcpt.rows) {
                        var rw = gdcpt.rows[grwp];
                        if (gdcpt.selectedRows[rw.id] || rw.isBlank) continue;
                        gdcpt.selectRow(grwp);
                    }
                }
            };
            this.clearSelection = function () {
                if (gdcpt) {
                    for (var grwp in gdcpt.rows) {
                        var rw = gdcpt.rows[grwp];
                        if (gdcpt.selectedRows[rw.id] && !rw.isBlank)
                            gdcpt.selectRow(grwp);
                    }
                }
            };
            this.updateRow = function (data) {
                if (!data) return false;
                formatDate([data]);
                var rowKey = data[settings.indexColKey];
                if (!rowKey) return false;
                var row = gdcpt.rows[rowKey];
                var rwd = row.data;
                var rcells = row.cells;
                for (var cp in rwd) {
                    rcells[cp] && $(rcells[cp].innerDom).text(data[cp] || "");
                    rwd[cp] = data[cp];
                }
                rowBound(row, gdcpt);
                formatRowCell(this, row);
                return true;
            };
            this.ApplyRowToNewData = function (row, newData) {
                if (!newData || !row) return;
                var nRowId = newData[settings.indexColKey];
                if (!nRowId) {
                    nRowId = (new Date()).getTime();
                    data[settings.indexColKey] = nRowId;
                }
                var oRowId = row.data[settings.indexColKey];

                if (gdcpt.selectedRows[oRowId]) {
                    gdcpt.selectRow(oRowId);
                }
                delete gdcpt.rows[oRowId];
                gdcpt.rows[nRowId] = row;
                row.data = newData;
                row.id = nRowId;
                for (var cp in row.cells) {
                    row.cells[cp].rowid = nRowId;
                }
                this.updateRow(newData);
            };
            this.addOrUpdateRow = function (data) {
                if (!data) return false;
                var rowKey = data[settings.indexColKey];
                if (!rowKey) return this.addRow(data);
                var row = gdcpt.rows[rowKey];
                if (row) {
                    this.updateRow(data);
                    return false
                }
                else
                    return this.addRow(data, 0);

            };
            this.getRowCount = function () {
                var count = 0;
                for (var rp in gdcpt.rows) {
                    if (rp) {
                        var r = gdcpt.rows[rp];
                        if (r && r.data && !r.data.isGDBlankData) count++;
                    }
                }
                return count;
            };
            this.removeRows = function (rws, keyNm, fillBlank) {
                if (!gdcpt) return [];
                var rmRws = [];
                if (rws) {
                    for (var r = 0; r < rws.length; r++) {
                        var dgrw = keyNm ? rws[r][keyNm] : rws[r];
                        if (dgrw) {
                            gdcpt.removeRow(dgrw);
                            if (fillBlank) this.addBlankRow();
                        }
                    }
                    rmRws.add(rws);
                }
                else {
                    for (var rp in gdcpt.selectedRows) {
                        if (rp) {
                            gdcpt.removeRow(rp);
                            rmRws.push(rp);
                        }
                    }
                }
                return rmRws;
            };
            this.clearRows = function (fillBlank) {
                if (!gdcpt) return;
                var allRows = [];
                if (arguments.length == 0) fillBlank = true;
                for (var rp in gdcpt.rows) {
                    if (!rp || gdcpt.rows[rp].isBlank)
                        continue;
                    allRows.push(rp);
                }
                var rms = this.removeRows(allRows, null, fillBlank);
                if (this.pgcmpt) this.pgcmpt.clear();
                return rms;
            };
            this.getOneRow = function (noTip, cnt) {
                var data = this.getData(false);
                if (data && data.length > 1) {
                    if (!noTip) CM.com.showTip("此操作只能选择一行数据", cnt, 2000);
                    return null;
                }
                if (!data || data.length == 0) {
                    if (!noTip) CM.com.showTip("请选择一行数据", cnt, 2000);
                    return null;
                }
                return data[0];
            };
            function formatDate(d) {
                var ud = d.d || d;
                if (ud.List) ud = ud.List;
                if (!ud || !ud.length) return;
                var hasDate = false;
                for (var s = 0; s < cols.length; s++) {
                    if (cols[s].additional && cols[s].additional.isDate) {
                        hasDate = true;
                        break;
                    }
                }
                if (hasDate) {
                    for (var n = 0; n < ud.length; n++) {
                        for (var o = 0; o < cols.length; o++) {
                            var c = cols[o], ad = c.additional, cld = ud[n][c.dataIndex];
                            if (!c.dataIndex) continue;
                            if (ad && ad.isDate) {
                                var _cld = ud[n]["_" + c.dataIndex];
                                if (!_cld) {
                                    ud[n]["_" + c.dataIndex] = cld;
                                    ud[n][c.dataIndex] = dt2str($dt(cld, HOYI.DateFmtStr));
                                }
                                else {
                                    ud[n][c.dataIndex] = dt2str($dt(_cld, HOYI.DateFmtStr));
                                }
                            }
                        }
                    }
                }
            };
            function formatRowCell(psgd, rw) {
                if (psgd.cellBound || psgd.cellLink || psgd.fileLink) {
                    for (var s = 0; s < cols.length; s++) {
                        if (!cols[s].dataIndex) continue;
                        var cell = rw.cells[cols[s].dataIndex];
                        if (psgd.cellBound) psgd.cellBound(cell, [cols[s], rw, psgd, s]);
                        if (psgd.cellLink && psgd.cellLink.handler) {
                            if (cols[s].dataIndex == psgd.cellLink.colName) {
                                var jCLDom = $(cell.innerDom).css({ "color": "#4479AB", "textDecoration": "underline", "cursor": "pointer" });
                                jCLDom.click(rw.data, psgd.cellLink.handler);
                            }
                        }
                        var recRptAcsFn = function (e) { !CM.forPSBatch && CM.com.doAction("recRptAccess", [{ rptId: e.data }], null, null, null, true); };
                        if (psgd.fileLink) {
                            if (cols[s].dataIndex == psgd.fileLink.colName) {
                                var rd = rw.data, g = psgd.fileLink;
                                var rptId = rd[g.idIndex], rptName = rd[g.nameIndex], rptType = rd[g.typeIndex];
                                var isAvaliable = (rptId && rptId.length > 0);
                                if (isAvaliable) {
                                    var jdom = $(cell.innerDom)
                                    var icon = HOYI.mkFlLink(rptId, rptType, rptName);
                                    if (icon) {
                                        jdom.text(""); //.css("text-align", "center");
                                        icon.appendTo(jdom);
                                        icon.click(rptId, recRptAcsFn);
                                    }
                                }
                            }
                        }
                    }
                }
            };
        },
        demandShowGrid: function (colKey, rwDbClickHd, dgc, pagingc) {
            CM.com.grid.call(this, colKey, rwDbClickHd, dgc, pagingc);
            var gd = this, _create = this.create, grid, splInfo, dataItems = [], gridBody, innerData = {}, _clearSelection = this.clearSelection;
            function initDemandGrid() {
                gd.addRowBoundHandler(function (row, dg) {
                    if (!row.ddls) row.ddls = {};
                    if (!splInfo.rowHeight)
                        splInfo.rowHeight = row.el.outerHeight();
                    row.cells["checkbox"].checkboxbutton.cb._oc = row.cells["checkbox"].checkboxbutton.cb.onClick;
                    row.cells["checkbox"].checkboxbutton.cb.onClick = function (a, b) {
                        innerData[a] = b.isSelected ? 2 : 1;
                        var srows = gd.gridObj.selectedRows || HOYI.emptyObj;
                        if (!!srows[a] != b.isSelected) {
                            b.cb._oc(a, b);
                            gd.onRowSelected && gd.onRowSelected(a, b, false);
                        }
                    };
                });
            };
            this.onRowSelected = function (a, b, isAll) {

            };
            this.clearSelection = function () {
                _clearSelection.call(this);
                selectAll(false);
            };
            this.create = function () {
                dataItems = [];
                innerData = {};
                grid = _create.apply(this, arguments);
                grid.header.row.cells["checkbox"].checkboxbutton.cb.onClick = function (a, b) {
                    selectAll(b.isSelected);
                    gd.onRowSelected && gd.onRowSelected(a, b, true);
                };
                if (grid.scrollPanel) {
                    var spl = grid.scrollPanel;
                    if (spl.scrollbarY) spl.scrollbarY.scrollTo(0);
                    if (spl.scrollbarX) spl.scrollbarX.scrollTo(0);
                    var hdsbH = grid.header.el.height() + spl.cfg.arrowSize;
                    splInfo = { spl: spl, height: grid.cfg.height - hdsbH, curTop: 0, rowHeight: 0 };
                    spl.scrollbarY.CB.onScrollEnd = function () {
                        var t = splInfo.spl.scrollTop;
                        if (splInfo.curTop == t) return;
                        var ct = splInfo.curTop;
                        var sc = t - ct;
                        splInfo.curTop = t;
                        if (sc != 0) gd.gdData(sc);
                    };
                }
                if (splInfo.rowHeight == 0) {
                    this.addRow(null);
                    this.clearRows(false);
                }
                gridBody = grid.scBodys;
                gridBody.css("position", "relative");
                this.initDataInGd();
                return grid;
            };
            this.initDataInGd = function () {
                if (!gridBody) return;
                var plH = dataItems.length * splInfo.rowHeight;
                if (plH > splInfo.height)
                    plH = plH + splInfo.spl.scrollbarX.bar.height();
                plH = Math.max(plH, splInfo.height);
                splInfo.totalHeight = plH;
                gridBody.css("height", plH);
                var plIsShow = splInfo.spl.scrollbarY.isShow;
                if (plIsShow) {
                    splInfo.spl.scrollbarY.isShow = !plIsShow;
                    splInfo.spl.scrollbarY.reSize(plIsShow);
                }
                this.gdData(0);
                grid._resizeBody();
            };
            this.gdData = function (sh) {
                if (!gridBody) return;
                if (dataItems.length == 0) {
                    gridBody.css("top", 0);
                    return;
                }
                var bnr = this.onScrollBegin ? this.onScrollBegin(sh) : null;
                var rmIds = [], spl = splInfo.spl, isCA = grid.header.row.cells["checkbox"].checkboxbutton.isSelected;
                this.removeRows(rmIds, null, false);
                var rws = parseInt(splInfo.height / splInfo.rowHeight), stp = spl.scrollTop;
                var strw = parseInt(stp / splInfo.rowHeight), itmLen = dataItems.length, inData = [];
                grid.rowNumStart = strw;
                for (var i = 0, n = strw; i <= rws && n < itmLen; n++, i++) {
                    var da = dataItems[n];
                    if (da) {
                        inData.push(da);
                        if (grid.rows[da.Id]) continue;
                        if (sh < 0)
                            this.addRow(da, i);
                        else
                            this.addRow(da);
                    }
                }
                for (var cri in grid.rows) {
                    if ($byId(inData, cri, grid.cfg.indexColKey)) continue;
                    rmIds.push(cri);
                }
                this.removeRows(rmIds, null, false);
                var nrwh = (stp % splInfo.rowHeight);
                var gbh = Math.max(splInfo.totalHeight - (stp - nrwh), splInfo.height);
                gridBody.css({ "top": stp - nrwh, "height": gbh });
                window.setTimeout(isCA ? selectGridAll : selectGridItem, 0);
                this.onScrollEnd && this.onScrollEnd(sh, bnr);
                grid.setDGCellKeyAndOrder && grid.setDGCellKeyAndOrder();
            };
            this.addOrUpdateRow = function (data) {
                if (!data) return false;

                var rowKey = data[grid.cfg.indexColKey], isNew = true;
                var idx = 0, existData;

                for (; idx < dataItems.length; idx++) {
                    var dId = dataItems[idx][grid.cfg.indexColKey];
                    if (rowKey == dId) {
                        isNew = false;
                        existData = dataItems[idx];
                        break;
                    }
                }

                if (isNew) {
                    dataItems.push(data);
                    innerData[rowKey] = 1;
                    return this.addRow(data, 0);
                }
                else {
                    $mo(existData, data);
                    this.updateRow(data);
                    return false;
                }
            };
            this.removeItems = function (all) {
                var isCA = all ? true : grid.header.row.cells["checkbox"].checkboxbutton.isSelected;
                var rms = all ? this.clearRows(false) : this.removeRows();
                if (isCA) {
                    dataItems = [];
                    innerData = {};
                }
                else {
                    for (var r = 0; r < rms.length; r++) {
                        innerData[rms[r]] = 2;
                    }
                    var mCurItems = dataItems, mInnerData = innerData, idx = 0;
                    for (var ind in mInnerData) {
                        if (ind && mInnerData[ind] == 2) {
                            for (; idx < mCurItems.length; idx++) {
                                var dId = mCurItems[idx][grid.cfg.indexColKey];
                                if (mInnerData[dId] == 2) {
                                    mInnerData[dId] = -1;
                                    mCurItems.splice(idx, 1);
                                    if (ind == dId)
                                        break;
                                    else
                                        idx--;
                                }
                            }
                        }
                    }
                    for (var ind2 in mInnerData) {
                        if (ind2 && mInnerData[ind2] == -1)
                            delete mInnerData[ind2];
                    }
                }
                this.initDataInGd();
            };
            this.pushItems = function (items) {
                for (var d = 0; d < items.length; d++) {
                    var rwd = items[d], rwdId = rwd ? rwd[grid.cfg.indexColKey] : null;
                    if (rwdId && !innerData[rwdId]) {
                        dataItems.push(rwd);
                        innerData[rwdId] = 1;
                    }
                }
                if (items.length) {
                    this.initDataInGd();
                    return true;
                }
                else
                    return false;

            };
            this.getItems = function () {
                return dataItems || [];
            };
            this.getSelectedKeys = function (includeView) {
                return this.getKeys(includeView, true);
            };
            this.getKeys = function (includeView, isSelected) {
                var keys = {}, tl = 0, nos = !isSelected;
                for (var did in innerData) {
                    if (!includeView && grid.rows[did]) continue;
                    if (nos || innerData[did] == 2) {
                        keys[did] = did;
                        tl++;
                    }
                }
                return { keys: keys, count: tl };
            };
            this.hasItem = function (item) {
                return grid && innerData[item[grid.cfg.indexColKey]] ? true : false;
            };
            this.getData = function (all) {
                if (all) {
                    return [].add(dataItems);
                }
                else {
                    var selKeys = this.getSelectedKeys(true);
                    var sels = [];
                    for (var k in selKeys.keys) {
                        sels.push($byId(dataItems, k, grid.cfg.indexColKey));
                    }
                    return sels;
                }
            };
            this.getRowData = function (rwId) {
                return $byId(dataItems, rwId, grid.cfg.indexColKey);
            };
            function selectGridAll() {
                grid.header.row.cells["checkbox"].checkboxbutton.select();
                gd.selectAll();
            };
            function selectGridItem() {
                for (var grwp in grid.rows) {
                    var rw = grid.rows[grwp];
                    if (rw && innerData[rw.id] == 2 && !grid.selectedRows[rw.id])
                        grid.selectRow(grwp);
                }
            };
            function selectAll(isSel) {
                for (var pp in innerData) {
                    if (pp && innerData[pp])
                        innerData[pp] = isSel ? 2 : 1;
                }
            };
            initDemandGrid();
        },
        ready: function (f) {
            if (!this.jWin) this.jWin = $(window);
            this.jWin.ready(f);
        },
        actionMenu: function () {
            var actMnu = HOYI.pgmenu();
            this.addItem = function (caption, handler) {
                var mi = actMnu[0].items[actMnu[0].items.push({ "caption": caption }) - 1];
                mi.click = handler;
            };
            this.create = function (c) {
                var mnc = null;
                if (!c)
                    mnc = $("#menu");
                else if ($.type(c) == "string")
                    mnc = $("#" + c);
                else
                    mnc = c;
                return $.menu(mnc, actMnu);
            };
            this.prePop = function (handler) {
                actMnu.onPrePop = handler;
            };
        },
        anchorUrl: function (anchor) {
            var u = window.location.href;
            var ix = u.indexOf("#");
            if (ix >= 0)
                u = u.substring(0, ix + 1);
            else
                u = u + "#";
            window.location.href = u + anchor;
        },
        getAnchor: function () {
            var u = window.location.href;
            var ix = u.indexOf("#");
            if (ix > 0)
                return u.substring(ix + 1);
            else
                return null;
        },
        makeNavAddrs: function (navs, navc) {
            var jnav = $(navc), anavs = [];
            $("*", jnav).remove();
            jnav.text("");
            for (var n = 0; n < navs.length; n++) {
                if (!navs[n]) continue;
                var ntxt = navs[n].text;
                if (ntxt && ntxt.length > 0) {
                    if (ntxt.length > 50) ntxt = ntxt.substr(0, 50) + "...";
                    var na = $("<a href='" + navs[n].url + "' class='anav'></a>");
                    na.text(ntxt);
                    anavs.push(na);
                    for (var pn in navs[n]) {
                        if (pn == "text") continue;
                        if (pn.indexOf("on") == 0)
                            na.bind(pn.substr(2), navs[n][pn]);
                        else
                            na.attr(pn, navs[n][pn]);
                    }
                }
            }
            for (var a = 0; a < anavs.length; a++) {
                var ja = anavs[a];
                ja.appendTo(jnav);
                if (a != (anavs.length - 1))
                    jnav.append("&nbsp;&gt;&nbsp;");
                else
                    ja.css({ "textDecoration": "none", "cursor": "auto" });
            }
        },
        resetHis: function () {
            HOYI.ajaxHis.reset();
        },
        goHis: function (an) {
            var hr = HOYI.ajaxHis.fireHis(an + "[0-9]*");
            CM.com.anchorUrl(an);
            return hr;
        },
        initSearchBox: function (tip, txt, btn, schFn) {
            var schFun = function (e) {
                e.stopPropagation();
                if (HOYI.noEventKeyCode(e.keyCode))
                    return false;
                var schText = txt.val().trim();
                if (schText.length > 0 && (!e.keyCode || e.keyCode == 13)) {
                    btn.focus();
                    if (schFn) schFn(schText, e);
                }
                return false;
            };
            window.setTimeout(function () { txt.val(""); }, 10);
            txt.attr("maxLength", "60");
            txt.click(function () { tip.hide(); });
            txt.blur(function (e) { var t = e.target.value.trim(); if (t.length == 0) { e.target.value = ""; tip.show(); } })
            txt.keyup(schFun);
            btn.click(schFun);
            tip.click(function (e) { tip.hide(); txt.focus(); });
            tip.width(txt.width() - 20);
        },
        doAction: function (action, data, callback, prgmsg, error, hideprg) {
            var t = null;
            if (prgmsg) {
                if ($.type(prgmsg) == "string")
                    t = { "stmsg": prgmsg };
                else
                    t = prgmsg;
            }
            $post(action, data, callback, t, error, hideprg);
        },
        showTip: function (tipMsg, tipc, tms) {
            $tMsg(tipMsg.indexOf(" ") > 0 ? tipMsg : $msg(tipMsg), tms || 10000, tipc);
        },
        alert: function (msg, notKey, org, cb) {
            var mgr = window.HOYI.alertMsg(msg, org, null, notKey);
            if (cb)
                mgr.OPT = { YF: cb };
            else if (mgr.OPT)
                mgr.OPT.YF = null;
        },
        addMessages: function (msgs) {
            for (var m = 0; m < msgs.length; m++) {
                HOYI.msgs[msgs[m].name] = msgs[m].msg;
            }
        },
        getZoneData: function (zone) {
            return $condata(zone);
        },
        isNum: function (t) {
            return /^(-?\d+)([\.]*\d+)?$/.test(t);
        }
    },
    table: function (tabc, settings) {
        this.cfg = settings;
        this.tabElt = null;
        this.selectedRows = {};
        this.rows = {};
        var innerRows = [], rowBoundHds = new CM.funLinked(), tb = this, sbw = 0;
        function init() {
            var jmtabc = $(tabc);
            $("*", jmtabc).remove();
            var jhdc = $(document.createElement("div")).css({ padding: 0, margin: 0 }).appendTo(jmtabc);
            var jtab = this.hdtTab = $(document.createElement("table")).appendTo(jhdc);
            jtab.addClass("dgtablyt");
            var cols = settings.column;
            this.header = $(document.createElement("tr")).css("fontWeight", "bold");
            this.header.attr("valign", "middle");
            this.header.appendTo(jtab);
            this.header.addClass("head");
            jhdc.addClass("headdiv");
            createCells.call(this, this.header, function (c) { return c.header; });
            var bdcss = { "overflowX": "hidden", "overflowY": "auto", padding: 0, margin: 0 };

            if (this.cfg.height == "auto")
                bdcss.height = "auto";
            else if($.type(this.cfg.height)=="string")
                bdcss.height = parseInt(this.cfg.height) - jhdc.outerHeight();
            else
                bdcss.height = this.cfg.height - jhdc.outerHeight();

            var bodyc = $(document.createElement("div")).appendTo(jmtabc);

            bodyc.css(bdcss);
            this.tabElt = $(document.createElement("table")).appendTo(bodyc);
            this.tabElt.addClass("dgtablyt");
            if (this.cfg.hdCss) this.header.css(this.cfg.hdCss);
            //HOYI.touchScrollBar(bodyc, true)();
        };
        this.resize = function (sz) {
            this.totalSize = sz;
            this.tabElt.width(sz - sbw - 1);
            this.hdtTab.width(sz - sbw);
            adjustHeaderWidth.call(this);
        };
        this.resizeCell = function (cellKey, w, h) {
            var cols = this.cfg.column, cst = null, ix = -1;
            for (var c = 0, ln = cols.length; c < ln; c++) {
                var cs = cols[c];
                var cn = cs.dataIndex;
                if (cellKey == cn) {
                    cst = cs;
                    cs.height = h;
                    cs.width = w;
                    ix = c + (settings.checkBoxButton ? 1 : 0);
                    break;
                }
            }
            if (ix >= 0) {
                var arws = $mo2({}, this.rows);
                arws.hd = this.header;
                for (var pn in arws) {
                    var row = arws[pn];
                    var cl = row[0].cells[ix];
                    var cdv = cl.getElementsByTagName("DIV");
                    var ws = (typeof (w) == "number") ? w + "px" : "auto";
                    cdv && cdv.length && (cdv[0].style.width = ws);
                    cl.style.width = ws;
                    h && (cl.style.height = h + "px");
                }
                
            }
        };
        var cbClickHandlers = {
            itemCB: function cbClick(e) {
                var rw = e.data.r, ctb = e.data.t;
                if (this.checked)
                    ctb.selectedRows[rw.id] = rw;
                else
                    delete ctb.selectedRows[rw.id];
                ctb.allCbx.attr("checked", ctb.tabElt.find("input[type=checkbox]:checked").length == innerRows.length);
            }, allCB: function onSACBClick(e) {
                var ctb = e.data;
                if (this.checked)
                    ctb.selectedRows = $.extend({}, ctb.rows);
                else
                    ctb.selectedRows = {};
                ctb.tabElt.find("input[type=checkbox]").attr("checked", this.checked);
            }
        };
        function createCells(rw, gv) {
            rw.cells = {};
            var cols = settings.column;
            if (settings.checkBoxButton) {
                var cbx = $(document.createElement("input")).attr("type", "checkbox");
                var cbtd = $(document.createElement("td"));
                cbtd.addClass("checkbox");
                if (settings.checkBoxButton.css)
                    cbtd.css(settings.checkBoxButton.css);
                else
                    cbtd.width(20);
                cbx.appendTo(cbtd);
                cbtd.appendTo(rw);
                if (rw == this.header) {
                    settings.multiSelect = true;
                    cbx.click(tb, cbClickHandlers.allCB);
                    tb.allCbx = cbx;
                }
                else {
                    cbx.click({ r: rw, t: tb }, cbClickHandlers.itemCB);
                }
            }
            for (var c = 0; c < cols.length; c++) {
                var cell = $(document.createElement("td"));
                var clSetting = cols[c];
                var nhwd = !clSetting.width;
                rw.cells[clSetting.dataIndex] = cell;
                var inner = $(document.createElement("div")).css({ "width": clSetting.width }).addClass("ellipsis").attr("cix", c);
                if (nhwd) inner.css("whiteSpace", "normal");
                inner.text(Util.escapeXmlChars(gv(clSetting), true));
                inner.appendTo(cell);
                cell.innerDom = inner;
                cell.appendTo(rw);
                cell.row = rw;
                if (clSetting.width && (rw == this.header || innerRows[0] == rw)) cell.css("width", clSetting.width);
                if (clSetting.additional && clSetting.additional.css) cell.css(clSetting.additional.css);
                if (c == 0) cell.css("borderLeftStyle", "none");
                if (c == (cols.length - 1)) cell.css("borderRightStyle", "none");
                if (rw != this.header && this.cellBound) this.cellBound(cl, c);
                if (rw == this.header && !clSetting.noSort) {
                    cell.css("cursor", "pointer");
                    cell.dblclick([cols[c], this], function (e) {
                        var cl = e.data[0];
                        var t = e.data[1];
                        if (t.lastSortCellKey == cl.dataIndex) {
                            var lso = t.lastSortOrder;
                            t.lastSortOrder = lso == "DESC" ? "ASC" : "DESC";
                        }
                        else
                            t.lastSortOrder = "ASC";
                        t.lastSortCellKey = cl.dataIndex;
                        if (t.listData) t.listData(1);
                    });
                }
            }
        };
        this.bindData = function (data) {
            for (var r = 0; r < innerRows.length; r++) innerRows[r].remove();
            this.rows = {};
            this.selectedRows = {};
            innerRows = [];
            for (var d = 0; d < data.length; d++) {
                this.addRow(data[d]);
            }
            adjustHeaderWidth.call(this);
            ellipsisCells.call(this);
        };
        function adjustHeaderWidth() {
            var cols = this.cfg.column;
            var bgc = this.cfg.checkBoxButton ? 1 : 0;
            var hd = this.header[0];
            var tb = this.tabElt[0];
            var tbc = this.tabElt.parent();
            if (sbw == 0) sbw = Math.max(0, (tbc.width() - this.tabElt.width()));
            if (sbw > 0) this.hdtTab.parent().css("paddingRight", sbw);
        };
        function ellipsisCells() {
            var ells = this.tabElt.find("div[cix]");
            var cols = this.cfg.column;
            ells.each(function (i) {
                var j = $(this);
                var heOut = j.outerHeight() > 25;
                var cwd = cols[parseInt(j.attr("cix"))].width;
                var sizeOut = heOut || j[0].scrollWidth > (cwd || 10000);
                if (sizeOut || !cwd) {
                    if (cwd)
                        j.width(cwd);
                    else
                        j.width(j.width() - 5);
                    j.attr("title", j.text());
                    j.css("whiteSpace", "nowrap");
                }
            });
        };
        this.addRow = function (rwd, pos) {
            var rw = $(document.createElement("tr")).addClass("gdtabtr");
            rw.addClass((innerRows.length % 2) == 0 ? "evenrow" : "oddrow");
            innerRows.push(rw);
            rw.data = rwd;
            rw.attr("valign", "middle");
            if (this.cfg.cnCss) rw.css(this.cfg.cnCss);
            rw.id = rwd[this.cfg.indexColKey];
            this.rows[rw.id] = rw;
            if (pos == undefined)
                rw.appendTo(this.tabElt);
            else {
                if (this.tabElt.has("tr").length == 0)
                    rw.appendTo(this.tabElt);
                else
                    rw.insertBefore(this.tabElt.find("tr:eq(" + pos + ")"));
            }
            createCells.call(this, rw, function (c) { return rwd[c.dataIndex]; });
            rowBound(rw, rwd);
            rw.grid = this;
            if (this.allCbx && this.allCbx[0].checked) this.allCbx[0].checked = false;
        };
        function rowBound(row, gd) {
            rowBoundHds.call(this, arguments);
        };
        this.removeRow = function (rwId) {
            var rw = this.rows[rwId];
            if (rw) {
                delete this.rows[rwId];
                delete this.selectedRows[rwId];
                rw[0].parentNode.removeChild(rw[0]);
                innerRows.remove(rw, tb.cfg.indexColKey);
            }
            if (innerRows.length == 0 && this.allCbx && this.allCbx[0].checked) this.allCbx[0].checked = false;
            for (var r = 0, s = innerRows.length; r < s; r++) {
                rw = innerRows[r];
                if ((r % 2) == 0) {
                    if (rw.hasClass("oddrow")) {
                        rw.removeClass("oddrow");
                    }
                    rw.addClass("evenrow");
                }
                else {
                    if (rw.hasClass("evenrow")) {
                        rw.removeClass("evenrow");
                    }
                    rw.addClass("oddrow");
                }
            }
        };
        this.reBindData = this.bindData;
        this.cellBound = null;
        this.addRowBoundHandler = function (hd) {
            rowBoundHds.reg(hd, this);
        };
        this.empty = function () {
            for (var rwi in this.rows) {
                this.removeRow(rwi);
            }
        };
        init.call(this);
    },
    forEach: function (eachFn, fnThis) {
        var doeach = function (a, args) {
            if (!a) return;
            if ($.type(a) == "array") {
                for (var n = 0, ln = a.length; n < ln; n++) {
                    if (fnThis)
                        eachFn.call(fnThis, a[n], n, args);
                    else
                        eachFn(a[n], n, args);
                }
            }
            else {
                for (var pp in a) {
                    if (fnThis)
                        eachFn.call(fnThis, a[pp], pp, args);
                    else
                        eachFn(a[pp], pp, args);
                }
            }
        };
        return doeach;
    },
    pageSize: function () { return Util.DataGrid.defaultSetting.pageSize },
    getStandardDate: function (dateTxt) {
        return $dt(dateTxt, HOYI.sDtFmt).format("yyyy-MM-dd");
    },
    funLinked: function () {
        var fnList = [];
        this.reg = function (fun, This, args) {
            fnList.push({ f: fun, t: This, a: args });
        };
        function icall(o, n, ags) {
            if (o.f) {
                var fun = o.f, cts = o.t || this;
                try { fun.apply(cts, o.args || ags || []); } catch (ex) { alert(ex); }
            }
        };
        this.call = function (cThis, args) {
            CM.forEach(icall, cThis)(fnList, args);
        };
    },
    gridTextReander: function (text) {
        return Util.escapeXmlChars(text);
    },
    eo: {},
    convertTo: function (data, ppmps) {
        var objs = [];
        for (var d = 0; d < data.length; d++) {
            var nobj = {};
            for (var spp in ppmps) {
                nobj[ppmps[spp]] = data[d][spp];
            }
            objs.push(nobj);
        }
        return objs;
    },
    getArray: function (objs, pp) {
        var aa = [], isFun = $.type(pp) == "function";
        this.forEach(function (o) {
            if (!isFun || pp(o))
                aa.push(isFun ? o : o[pp]);
        })(objs);
        return aa;
    },
    isGuid: function (guid) {
        var reg = /^[0-9a-fA-F]{8}(-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}$/g;
        return reg.test(guid);
    },
    isEmptyGuid: function (uid) {
        return /[0\-]{36}/g.test(uid);
    },
    getObjPropsCount: function (obj) {
        var n = 0;
        for (var p in obj)
            n++;
        return n;
    },
    resize: function (rw, rh, isEx) {
        if (isEx) {
            var cw = window.innerWidth || window.document.documentElement.clientWidth;
            var ch = window.innerHeight || window.document.documentElement.clientHeight;
            if (cw > rw && ch > rh)
                return;
            if (cw > rw)
                rw = cw;
            if (ch > rh)
                rh = ch;
        }
        if (window.resizeTo) {
            window.resizeTo(rw, rh);
        }
        else {
            window.innerHeight = rh;
            window.innerWidth = rw;
        }
    },
    cityPicker: function (cc) {
        var cities = [
    { "n": "直辖市", "c": ["北京", "上海", "天津", "重庆"] },
    { "n": "北京市", "c": ["东城区", "西城区", "崇文区", "宣武区", "朝阳区", "丰台区", "石景山区", "海淀区", "门头沟区", "房山区", "通州区", "顺义区", "昌平区", "大兴区", "怀柔区", "平谷区", "密云县", "延庆县"] },
    { "n": "上海市", "c": ["黄浦区", "卢湾区", "徐汇区", "长宁区", "静安区", "普陀区", "闸北区", "虹口区", "杨浦区", "闵行区", "宝山区", "嘉定区", "浦东新区", "金山区", "松江区", "青浦区", "南汇区", "奉贤区", "崇明县"] },
    { "n": "天津市", "c": ["和平区", "河东区", "河西区", "南开区", "河北区", "红桥区", "塘沽区", "汉沽区", "大港区", "东丽区", "西青区", "津南区", "北辰区", "武清区", "宝坻区", "宁河县", "静海县", "蓟县"] },
    { "n": "重庆市", "c": ["万州区", "涪陵区", "渝中区", "大渡口区", "江北区", "沙坪坝区", "九龙坡区", "南岸区", "北碚区", "万盛区", "双桥区", "渝北区", "巴南区", "黔江区", "长寿区", "綦江县", "潼南县", "铜梁县", "大足县", "荣昌县", "璧山县", "梁平县", "城口县", "丰都县", "垫江县", "武隆县", "忠县", "开县", "云阳县", "奉节县", "巫山县", "巫溪县", "石柱土家族自治县", "秀山土家族苗族自治县", "酉阳土家族苗族自治县", "彭水苗族土家族自治县", "江津市", "合川市", "永川市", "南川市"] },
    { "n": "河北省", "c": ["石家庄市", "唐山市", "秦皇岛市", "邯郸市", "邢台市", "保定市", "张家口市", "承德市", "沧州市", "廊坊市", "衡水市"] },
    { "n": "山西省", "c": ["太原市", "大同市", "阳泉市", "长治市", "晋城市", "朔州市", "晋中市", "运城市", "忻州市", "临汾市", "吕梁市"] },
    { "n": "内蒙古自治区", "c": ["呼和浩特市", "包头市", "乌海市", "赤峰市", "通辽市", "鄂尔多斯市", "呼伦贝尔市", "巴彦淖尔市", "乌兰察布市", "兴安盟", "锡林郭勒盟", "阿拉善盟"] },
    { "n": "辽宁省", "c": ["沈阳市", "大连市", "鞍山市", "抚顺市", "本溪市", "丹东市", "锦州市", "营口市", "阜新市", "辽阳市", "盘锦市", "铁岭市", "朝阳市", "葫芦岛市"] },
    { "n": "吉林省", "c": ["长春市", "吉林市", "四平市", "辽源市", "通化市", "白山市", "松原市", "白城市", "延边朝鲜族自治州"] },
    { "n": "黑龙江省", "c": ["哈尔滨市", "齐齐哈尔市", "鸡西市", "鹤岗市", "双鸭山市", "大庆市", "伊春市", "佳木斯市", "七台河市", "牡丹江市", "黑河市", "绥化市", "大兴安岭地区"] },
    { "n": "江苏省", "c": ["南京市", "无锡市", "徐州市", "常州市", "苏州市", "南通市", "连云港市", "淮安市", "盐城市", "扬州市", "镇江市", "泰州市", "宿迁市"] },
    { "n": "浙江省", "c": ["杭州市", "宁波市", "温州市", "嘉兴市", "湖州市", "绍兴市", "金华市", "衢州市", "舟山市", "台州市", "丽水市"] },
    { "n": "安徽省", "c": ["合肥市", "芜湖市", "蚌埠市", "淮南市", "马鞍山市", "淮北市", "铜陵市", "安庆市", "黄山市", "滁州市", "阜阳市", "宿州市", "巢湖市", "六安市", "亳州市", "池州市", "宣城市"] },
    { "n": "福建省", "c": ["福州市", "厦门市", "莆田市", "三明市", "泉州市", "漳州市", "南平市", "龙岩市", "宁德市"] },
    { "n": "江西省", "c": ["南昌市", "景德镇市", "萍乡市", "九江市", "新余市", "鹰潭市", "赣州市", "吉安市", "宜春市", "抚州市", "上饶市"] },
    { "n": "山东省", "c": ["济南市", "青岛市", "淄博市", "枣庄市", "东营市", "烟台市", "潍坊市", "济宁市", "泰安市", "威海市", "日照市", "莱芜市", "临沂市", "德州市", "聊城市", "滨州市", "荷泽市"] },
    { "n": "河南省", "c": ["郑州市", "开封市", "洛阳市", "平顶山市", "安阳市", "鹤壁市", "新乡市", "焦作市", "濮阳市", "许昌市", "漯河市", "三门峡市", "南阳市", "商丘市", "信阳市", "周口市", "驻马店市"] },
    { "n": "湖北省", "c": ["武汉市", "黄石市", "十堰市", "宜昌市", "襄樊市", "鄂州市", "荆门市", "孝感市", "荆州市", "黄冈市", "咸宁市", "随州市", "恩施土家族苗族自治州", "仙桃市", "潜江市", "天门市", "神农架林区"] },
    { "n": "湖南省", "c": ["长沙市", "株洲市", "湘潭市", "衡阳市", "邵阳市", "岳阳市", "常德市", "张家界市", "益阳市", "郴州市", "永州市", "怀化市", "娄底市", "湘西土家族苗族自治州"] },
    { "n": "广东省", "c": ["广州市", "韶关市", "深圳市", "珠海市", "汕头市", "佛山市", "江门市", "湛江市", "茂名市", "肇庆市", "惠州市", "梅州市", "汕尾市", "河源市", "阳江市", "清远市", "东莞市", "中山市", "潮州市", "揭阳市", "云浮市"] },
    { "n": "广西壮族自治区", "c": ["南宁市", "柳州市", "桂林市", "梧州市", "北海市", "防城港市", "钦州市", "贵港市", "玉林市", "百色市", "贺州市", "河池市", "来宾市", "崇左市"] },
    { "n": "海南省", "c": ["海口市", "三亚市", "五指山市", "琼海市", "儋州市", "文昌市", "万宁市", "东方市", "定安县", "屯昌县", "澄迈县", "临高县", "白沙黎族自治县", "昌江黎族自治县", "乐东黎族自治县", "陵水黎族自治县", "保亭黎族苗族自治县", "琼中黎族苗族自治县", "西沙群岛", "南沙群岛", "中沙群岛的岛礁及其海域"] },
    { "n": "四川省", "c": ["成都市", "自贡市", "攀枝花市", "泸州市", "德阳市", "绵阳市", "广元市", "遂宁市", "内江市", "乐山市", "南充市", "眉山市", "宜宾市", "广安市", "达州市", "雅安市", "巴中市", "资阳市", "阿坝藏族羌族自治州", "甘孜藏族自治州", "凉山彝族自治州"] },
    { "n": "贵州省", "c": ["贵阳市", "六盘水市", "遵义市", "安顺市", "铜仁地区", "黔西南布依族苗族自治州", "毕节地区", "黔东南苗族侗族自治州", "黔南布依族苗族自治州"] },
    { "n": "云南省", "c": ["昆明市", "曲靖市", "玉溪市", "保山市", "昭通市", "丽江市", "思茅市", "临沧市", "楚雄彝族自治州", "红河哈尼族彝族自治州", "文山壮族苗族自治州", "西双版纳傣族自治州", "大理白族自治州", "德宏傣族景颇族自治州", "怒江傈僳族自治州", "迪庆藏族自治州"] },
    { "n": "西藏自治区", "c": ["拉萨市", "昌都地区", "山南地区", "日喀则地区", "那曲地区", "阿里地区", "林芝地区"] },
    { "n": "陕西省", "c": ["西安市", "铜川市", "宝鸡市", "咸阳市", "渭南市", "延安市", "汉中市", "榆林市", "安康市", "商洛市"] },
    { "n": "甘肃省", "c": ["兰州市", "嘉峪关市", "金昌市", "白银市", "天水市", "武威市", "张掖市", "平凉市", "酒泉市", "庆阳市", "定西市", "陇南市", "临夏回族自治州", "甘南藏族自治州"] },
    { "n": "青海省", "c": ["西宁市", "海东地区", "海北藏族自治州", "黄南藏族自治州", "海南藏族自治州", "果洛藏族自治州", "玉树藏族自治州", "海西蒙古族藏族自治州"] },
    { "n": "宁夏回族自治区", "c": ["银川市", "石嘴山市", "吴忠市", "固原市", "中卫市"] },
    { "n": "新疆维吾尔自治区", "c": ["乌鲁木齐市", "克拉玛依市", "吐鲁番地区", "哈密地区", "昌吉回族自治州", "博尔塔拉蒙古自治州", "巴音郭楞蒙古自治州", "阿克苏地区", "克孜勒苏柯尔克孜自治州", "喀什地区", "和田地区", "伊犁哈萨克自治州", "塔城地区", "阿勒泰地区", "石河子市", "阿拉尔市", "图木舒克市", "五家渠市"] }], pk = this;

        function init() {
            var jcc = $(cc);
            var html = '';
            for (var i = 0; i < cities.length; i++) {
                var city = cities[i];
                html += '<dl>';
                html += '<dt>';
                html += city.n;
                html += '</dt>';
                html += '<dd>';
                for (var j = 0; j < city.c.length; j++) {
                    html += '<a href="javascript:void(0);">' + city.c[j] + '</a>';
                }
                html += '</dd>';
                html += '</dl>';
            }
            jcc.html(html);
            HOYI.uiDialog.call(this, jcc, { width: 460, caption: "选择城市", height: 660 });
            this.findInnerObj("a").click(function (e) { returnCity(e.target); });
        };
        function returnCity(city) {
            var shen = $(city).parent().parent().find("dt").html();
            var shi = $(city).html();
            pk.onPicked && pk.onPicked(shen, shi);
        };
        this.onPicked = function (shen, shi) { };
        init.call(this);
    },
    converters: {
        toInt: function (s) {
            if (!s || !s.length) return 0;
            return parseInt(s);
        }
    }
};

