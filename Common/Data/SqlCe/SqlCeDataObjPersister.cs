﻿using System;
using System.Data.Common;
using System.Data.SqlServerCe;
using Morningstar.PSReporting.Common;

namespace HOYI.Common.Data.SqlCe
{
    class SqlCeDataObjPersister : BaseDataObjPersister
    {
        protected override DbProviderFactory DbFactory { get { return SqlCeProviderFactory.Instance; } }

        protected override T ExecuteMethod<T>(Func<T> method)
        {
            using (var sw = new IdentitySwitcher(IdentitySwitcher.OsIdentity))
            {
                return base.ExecuteMethod<T>(method);
            }
        }

        protected override void ExecuteMethod(Action method)
        {
            using (var sw = new IdentitySwitcher(IdentitySwitcher.OsIdentity))
            {
                base.ExecuteMethod(method);
            }
        }

        protected override int HandleInsertException(Exception ex, BaseDataObj dataObj)
        {
            if (ex is SqlCeException)
                return base.HandleInsertException(ex, dataObj);
            else
                return 0;
        }
    }
}
