﻿using System;

namespace HOYI.Common
{
    /// <summary>
    /// Provides a basic utility generic class that is used to store three related objects.
    /// </summary>
    /// <typeparam name="X">The firt type</typeparam>
    /// <typeparam name="Y">The second type</typeparam>
    /// <typeparam name="Z">The third type</typeparam>
    [Serializable]
    public class Triplet<X, Y, Z>
    {
        /// <summary>
        /// Gets or sets the first object of the object pair.
        /// </summary>
        public X First { get; set; }
        
        /// <summary>
        /// Gets or sets the second object of the object pair.
        /// </summary>
        public Y Second { get; set; }
        
        /// <summary>
        /// Gets or sets the third object of the object pair.
        /// </summary>
        public Z Third { get; set; }

        public Triplet(X x, Y y, Z z)
        {
            First = x;
            Second = y;
            Third = z;
        }
    }
}
