/*
 * File: app/model/StationModel.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('purifier.model.StationModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Field'
    ],

    fields: [
        {
            name: 'Id'
        },
        {
            name: 'SNCode'
        },
        {
            name: 'Name'
        },
        {
            name: 'Aqi'
        },
        {
            name: 'Pm25'
        },
        {
            name: 'Temperature'
        },
        {
            name: 'Humidity'
        },
        {
            dateFormat: 'MS',
            name: 'CreateDate',
            type: 'date'
        },
        {
            name: 'Ispm25in'
        }
    ]
});