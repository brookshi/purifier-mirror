﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Data;
using HOYI.Web.Common;

namespace HOYI.Web.Services
{
    [WebService(Namespace = "http://hoyi.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class AirQuality : WebServiceBase
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetQM25INCityList(int ver)
        {
            List<quality_city_qm25in> citys = quality_city_qm25in.LoadAll(); 
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = citys.Count,
                message = "",
                data = citys.ToList<object>(),
                success = true
            };
            return modelList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetAQICNCityList(int ver)
        {
            List<quality_city_aqicn> citys = quality_city_aqicn.LoadAll();
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = citys.Count,
                message = "",
                data = citys.ToList<object>(),
                success = true
            };
            return modelList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetQualityForCitys(List<string> qm25incitynames, List<string> aqicncitynames)
        {
            List<quality_record> records = new List<quality_record>();
            if (qm25incitynames != null && qm25incitynames.Count > 0)
                records.AddRange(quality_record_qm25in.GetRecordeListByCityNames(qm25incitynames).Cast<quality_record>());

            if (aqicncitynames != null && aqicncitynames.Count > 0)
                records.AddRange(quality_record_aqicn.GetRecordeListByCityNames(aqicncitynames).Cast<quality_record>());
            records.OrderBy(o => o.cityname);
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = records.Count,
                message = "",
                data = records.ToList<object>(),
                success = true
            };
            return modelList;
        }

        public List<PurifierInformation> GetInfo()
        {
            List<PurifierInformation> infos = new List<PurifierInformation>();
            if (CurrentUser.IsInRole(Role.AdminRole))
            {
                infos.AddRange(PurifierInformation.LoadAll());
            }
            else
            {
                infos.AddRange(PurifierInformation.LoadUserPurifiers(CurrentUser.Id));
            }
            return infos;
        }

        public List<object> GetPurifierRecord(int page, int rows, ref int total)
        {
            List<PurifierInformation> infoList = GetInfo();
            Dictionary<long, PurifierInformation> infoDict = infoList.ToDictionary<PurifierInformation, long>(o => o.Id);
            if (infoList.Count == 0)
                return null;
            total = infoList.Count;
            List<PurifierRecord> recordList = null;
            if (CurrentUser.IsInRole(Role.AdminRole))
                recordList = PurifierRecord.GetNewestRecordByPage(rows, page - 1, "CreateDate", OrderDirection.DESC);
            else
                recordList = PurifierRecord.GetNewestRecordByPageForUser(infoList.Select(o => o.Id.ToString()).ToArray(), rows, page - 1, "CreateDate", OrderDirection.DESC);

            List<object> rstList = new List<object>();

            Func<long,bool> IsFilter= pid =>{
                PurifierStatus status = PurifierStatus.GetByID(pid);
                if(status!=null)
                    return status.IsFliter==1;
                return false;
            };

            recordList.ForEach(o => rstList.Add(new
            {
                Id = o.Id,
                SNCode = infoDict.ContainsKey(o.PurifierID) ? infoDict[o.PurifierID].SNCode : "",
                Name = infoDict.ContainsKey(o.PurifierID) ? infoDict[o.PurifierID].Name : "",
                PurifierID = o.PurifierID,
                Aqi = o.Pm25,
                Pm25 = o.Pm25,
                Temperature = o.Temperature,
                Humidity = o.Humidity,
                CreateDate = o.CreateDate,
                IsPurifier = true,
                NeedChangeFilter = IsFilter(o.PurifierID),
            }));

            return rstList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetQualityAndPurifier(List<string> qm25incitynames, List<string> aqicncitynames, int page, int rows)
        {
            List<quality_record> records = new List<quality_record>();
            if (qm25incitynames != null && qm25incitynames.Count > 0)
                records.AddRange(quality_record_qm25in.GetRecordeListByCityNames(qm25incitynames).Cast<quality_record>());

            if (aqicncitynames != null && aqicncitynames.Count > 0)
                records.AddRange(quality_record_aqicn.GetRecordeListByCityNames(aqicncitynames).Cast<quality_record>());
            records.OrderBy(o => o.cityname);

            int total = 0;
            List<object> purifierList = GetPurifierRecord(page, rows, ref total);

            List<object> rstList = new List<object>();
            records.ForEach(o => rstList.Add(new
            {
                Id = o.Id,
                SNCode = "",
                Name = o.cityname,
                PurifierID = o.cityid,
                Aqi = o.aqi,
                Pm25 = o.pm25,
                Temperature = o.pm10,
                Humidity = o.o3,
                CreateDate = o.updatetime,
                IsPurifier = false,
                NeedChangeFilter = false,
            }));

            rstList.AddRange(purifierList);

            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = total,
                message = "",
                data = rstList,
                success = true
            };
            return modelList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cityname"></param>
        /// <param name="type">0 is qm25in, 1 is aqicn</param>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ModelListEx<object> GetStationQuality(string cityname, int type)
        {
            List<quality_record> records = new List<quality_record>();
            if (type == 0)
            {
                var stationRecords = quality_record_qm25in.GetStationRecordForCity(cityname);
                if (stationRecords != null)
                    records.AddRange(stationRecords.Cast<quality_record>());
            }
            else if (type == 1)
            {
                var stationRecords = quality_record_aqicn.GetStationRecordForCity(cityname);
                if (stationRecords != null)
                    records.AddRange(stationRecords.Cast<quality_record>());
            }
            
            ModelListEx<object> modelList = new ModelListEx<object>()
            {
                total = records.Count,
                message = "",
                data = records.ToList<object>(),
                success = true
            };
            return modelList;
        }
    }
}
