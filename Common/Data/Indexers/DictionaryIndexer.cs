﻿using System;
using System.Collections;

namespace HOYI.Common.Data.Indexers
{
    public class DictionaryIndexer : DataIndexer
    {
        IDictionary dic;

        internal DictionaryIndexer(IDictionary dic)
        {
            if (dic == null)
                throw new ArgumentNullException("dic");
            this.dic = dic;
        }

        protected override object GetValue(string name)
        {
            if (dic != null && dic.Contains(name))
                return dic[name];
            else
                return null;
        }

        public override bool Contains(string name)
        {
            return dic.Contains(name);
        }
    }
}
