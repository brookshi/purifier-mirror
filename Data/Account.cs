﻿using HOYI.Common;
using HOYI.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HOYI.Data
{

    public class Role
    {
        public Guid AccountId
        {
            get;
            private set;
        }

        public string RoleName
        {
            get;
            private set;
        }

        public Role(Guid accountId, string roleName)
        {
            this.AccountId = accountId;
            this.RoleName = roleName;
        }

        public const string UserRole = "User";
        public const string AdminRole = "Admin";
    }

    [DataObj(DBName = "HOYI", PrimaryKeyName = "Id", TableName = "[User]")]
    public class Account : BaseDataObj
    {
        protected Account()
        {
            CreatedDate = PersistenceHelper.MinDateTime;
            UpdatedDate = PersistenceHelper.MinDateTime;
            LoginDate = PersistenceHelper.MinDateTime;
        }

        public Guid Id
        {
            get;
            private set;
        }

        public virtual string Email
        {
            get;
            protected set;
        }

        public virtual string UserName
        {
            get;
            protected set;
        }

        public virtual Guid PwdHash
        {
            get;
            protected set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual string Phone
        {
            get;
            set;
        }

        public virtual string City
        {
            get;
            set;
        }

        public virtual string Province
        { 
            get; 
            set;
        }

        public virtual string Address
        {
            get;
            set;
        }

        public virtual DateTime LoginDate
        {
            get;
            set;
        }

        public DateTime CreatedDate
        {
            get;
            protected set;
        }

        public virtual DateTime UpdatedDate
        {
            get;
            protected set;
        }

        public virtual bool IsAdmin
        {
            get;
            protected set;
        }

        public void SetPassword(string password)
        {
            if ((password ?? "").Trim().Length == 0)
                throw new ArgumentNullException("password");
            this.PwdHash = GetPwdHash(password);
        }


        public void Save(bool isLogin)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                throw new InvalidOperationException("UserName can not be null.");
            }
            if (!Tools.IsValid(this.CreatedDate))
                this.CreatedDate = DateTime.Now;
            if(!isLogin)
                this.UpdatedDate = DateTime.Now;
            PersistenceHelper.Default.Save(this);
        }

        [DataObjField(IgnoreReadDb = true, IgnoreWriteDb = true)]
        public List<Role> Roles
        {
            get
            {
                List<Role> roles = new List<Role>();
                roles.Add(new Role(this.Id, Role.UserRole));
                if (this.IsAdmin)
                    roles.Add(new Role(this.Id, Role.AdminRole));
                return roles;
            }
        }

        public static Account Read(Guid accountId)
        {
            return PersistenceHelper.Default.Read<Account>(accountId);
        }

        public static Account Create(string userName,string email)
        {
            return Create(userName,email, false);
        }

        public static void DelUser(string userName)
        {
            if(IsExist(userName))
            {
                 WhereCriterion wc = new WhereCriterion("UserName", OperatorFlag.Equal, userName);
                 PersistenceHelper.Default.Delete<Account>(wc);
            }
        }

        public static void DelUser(Guid id)
        {
            PersistenceHelper.Default.DeleteObj<Account>(id);
        }

        public static Account Create(string userName, string email, bool isAdmin)
        {
            if(string.IsNullOrEmpty((userName??"").Trim()))
                throw new ArgumentNullException("userName");

            var usr = BaseDataObj.CreateDataObj<Account>();
            usr.UserName = userName;
            usr.IsAdmin = isAdmin;
            usr.Email = email;
            return usr;
        }

        public static Account Create(string userName, string password, string email, string phone, string name,
            string province, string city, string address, bool isAdmin)
        {
            Account user = Create(userName, email, isAdmin);
            user.SetPassword(password);
            user.Phone = phone;
            user.Name = name;
            user.Province = province;
            user.City = city;
            user.Address = address;
            return user;
        }

        public static void EditUser(string userName, string password, string email, string phone, string name,
            string province, string city, string address, bool isAdmin)
        {
            var user = Account.SearchUser(userName);
            if (user == null)
                user = Account.Create(userName, email);
            user.PwdHash = IsGuidByReg(password) ? new Guid(password) : GetPwdHash(password);
            user.Email = email;
            user.Phone = phone;
            user.Name = name;
            user.Province = province;
            user.City = city;
            user.Address = address;
            user.IsAdmin = isAdmin;
            user.Save(false);
        }

        public static bool EditUser(string userName, string email, string phone, string name,
            string province, string city, string address, bool isAdmin)
        {
            var user = Account.SearchUser(userName);
            if (user == null)
                return false;
            user.Email = email;
            user.Phone = phone;
            user.Name = name;
            user.Province = province;
            user.City = city;
            user.Address = address;
            user.IsAdmin = isAdmin;
            user.Save(false);
            return true;
        }

        static bool IsGuidByReg(string strSrc)
        {
            try
            {
                Guid guid = new Guid(strSrc);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsExist(string userName)
        {
            WhereCriterion wc = new WhereCriterion("UserName", OperatorFlag.Equal, userName);
            return PersistenceHelper.Default.Count<Account>(wc) > 0;
        }

        public static bool IsEmailUsed(string mail)
        {
            WhereCriterion wc = new WhereCriterion("Email", OperatorFlag.Equal, mail);
            return PersistenceHelper.Default.Count<Account>(wc) > 0;
        }

        private static Guid GetPwdHash(string password)
        {
            var md5 = MD5.Create();
            return new Guid(md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(password)));
        }

        public bool CheckOldPwd(string oldpwd)
        {
            return this.PwdHash.Equals(GetPwdHash(oldpwd));
        }

        public static Account GetUser(string userName, string pwdHash)
        {
            return GetUser(userName, GetPwdHash(pwdHash));
        }

        public static Account SearchUser(string userName)
        {
            WhereCriterion wc = new WhereCriterion("UserName", OperatorFlag.Equal, userName);
            return PersistenceHelper.Default.TopOne<Account>("UserName", OrderDirection.ASC, wc);
        }

        public static List<Account> GetUsersByPage(int pageIndex, int pageCount, string orderName, OrderDirection order)
        {
            WhereCriterion wc = new WhereCriterion();
            return PersistenceHelper.Default.PageObjs<Account>(pageIndex, pageCount, orderName, order, wc);
        }

        public static Account GetUser(string userName, Guid pwdHash)
        {
            WhereCriterion wc = new WhereCriterion("PwdHash", OperatorFlag.Equal, pwdHash);
            wc.AddCriteria(WhereCriterion.AndLink).AddCriteria(WhereCriterion.LeftBracket);
            wc.AddCriteria(new WhereCriterion("UserName", OperatorFlag.Equal, userName));
            wc.AddCriteria(WhereCriterion.OrLink).AddCriteria(new WhereCriterion("Email", OperatorFlag.Equal, userName));
            wc.AddCriteria(WhereCriterion.RightBracket);
           
            return PersistenceHelper.Default.TopOne<Account>("UserName", OrderDirection.ASC, wc);
        }

        public static List<Account> GetUserByPurifierId(string purifierid)
        {
            //select * from Account where userid in (select userid in up where purifierid in viewlist)
            SelectSentence subWC = new SelectSentence("UserId", typeof(UserPurifier), new WhereCriterion("PurifierId", OperatorFlag.Equal, purifierid));
            WhereCriterion wc = new WhereCriterion();
            wc.IN("Id", subWC);
            var pfs = PersistenceHelper.Default.Query<Account>(wc);
            return pfs;
        }

        public static int GetUserCount()
        {
            WhereCriterion wc = new WhereCriterion();
            return PersistenceHelper.Default.Count<Account>(wc);
        }
    }
}
