﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HOYI.Common.Data;
using HOYI.Data;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Command;
using SuperSocket.SocketBase.Protocol;
using SuperSocket.Common;

namespace PurifierLogic
{
    public class UpdateRecordHandler : PurifierLogic.CommandAdditional.BinaryCommandBase<PurifierSession>////StringCommandBase<PurifierSession>//
    {
        public override string Name
        {
            get { return "Z"; }
        }

        static readonly byte uploadFunctionByte = 0xcc;
        static readonly byte searchFunctionByte = 0xaa;
        static readonly byte backFunctionByte = 0xbb;
        static readonly byte beginByte = 0x5a;

        public override void ExecuteCommand(PurifierSession session,  BinaryRequestInfo requestInfo)//StringRequestInfo requestInfo)
        {
            //#PurifierId#MacAddr#Ip#RunTime#FilterMediumTime#Pm25#Temperature#Humidity#Flag#
            byte[] body = requestInfo.Body;
            string mac = GetMAC(body);
            if (IsUpload(body[0]))
            {
                session.Close(CloseReason.ServerClosing);
                string ip = GetIP(body);
                byte functionByte = body[17];
                DataHandler.GetInstance().ReceiveData(mac, ip, GetTear(body), GetOnOffKey(functionByte), GetLockKey(functionByte), GetAutoKey(functionByte),
                    GetSleepKey(functionByte), GetAnionKey(functionByte), GetUVKey(functionByte), GetFliterKey(functionByte), GetPM25(body),
                    GetTempuration(body), GetHumidity(body), GetTimerHour(body), GetTimerMin(body));
            }
            else if(IsSearch(body[0]))
            {
                PurifierStatus status = DataHandler.GetInstance().GetStatus(mac);
                DataInterval dateInver = DataHandler.GetInstance().GetDateInverval();
                byte[] array = new byte[11];
                array[0] = beginByte;
                array[1] = backFunctionByte;
                array[2] = Convert.ToByte(dateInver.SearchInterval);
                array[3] = Convert.ToByte(dateInver.UploadInterval);

                if (status != null)
                {
                    array[4] = Convert.ToByte(status.OnOffKey * 1 + status.LockKey * 2 + status.SleepKey * 4 +
                        status.AutoKey * 8 + status.AnionKey * 16 + status.UVKey * 32 + status.IsFliter * 64);

                    array[5] = Convert.ToByte(status.TimerHour);
                    array[6] = Convert.ToByte(status.TimerMin);
                    array[7] = Convert.ToByte(status.Tear);
                    array[8] = Convert.ToByte(status.TimeFlag);
                    array[9] = Convert.ToByte(status.StatusFlag);
                    array[10] = (byte)array.Sum(o => o);
                }
                else
                {
                    array[4] = Convert.ToByte(0);

                    array[5] = Convert.ToByte(0);
                    array[6] = Convert.ToByte(0);
                    array[7] = Convert.ToByte(0);
                    array[8] = Convert.ToByte(0);
                    array[9] = Convert.ToByte(0);
                    array[10] = (byte)array.Sum(o => o);
                }

                session.TrySend(array, 0, array.Length);

                session.Close();
            }
        }

        public string ConvertByteTo16(byte b, bool isAdd0)
        {
            string rst = Convert.ToString(b, 16);
            if (rst.Length == 1 && isAdd0)
                rst = "0" + rst;
            return rst;
        }

        public string ConvertByteAsciiToStr(byte b)
        {
            string rst = Encoding.ASCII.GetString(new byte[] { b });
            return rst;
        }

        public string GetIP(byte[] bArr)
        {
            return string.Format("{0}.{1}.{2}.{3}", bArr[1].ToString(), bArr[2].ToString(), bArr[3].ToString(), bArr[4].ToString());
        }

        public string GetMAC(byte[] bArr)
        {
            return string.Format("{0}{1}-{2}{3}-{4}{5}-{6}{7}-{8}{9}-{10}{11}",
                ConvertByteAsciiToStr(bArr[5]), ConvertByteAsciiToStr(bArr[6]), ConvertByteAsciiToStr(bArr[7]),
                ConvertByteAsciiToStr(bArr[8]), ConvertByteAsciiToStr(bArr[9]), ConvertByteAsciiToStr(bArr[10]),
                ConvertByteAsciiToStr(bArr[11]), ConvertByteAsciiToStr(bArr[12]), ConvertByteAsciiToStr(bArr[13]),
                ConvertByteAsciiToStr(bArr[14]), ConvertByteAsciiToStr(bArr[15]), ConvertByteAsciiToStr(bArr[16]));
        }

        public bool IsUpload(byte b)
        {
            return byte.Equals(b, uploadFunctionByte);
        }

        public bool IsSearch(byte b)
        {
            return byte.Equals(b, searchFunctionByte);
        }

        public int GetOnOffKey(byte b)
        {
            return BitAssist.GetTargetBit(0, b);
        }

        public int GetLockKey(byte b)
        {
            return BitAssist.GetTargetBit(1, b);
        }

        public int GetSleepKey(byte b)
        {
            return BitAssist.GetTargetBit(2, b);
        }

        public int GetAutoKey(byte b)
        {
            return BitAssist.GetTargetBit(3, b);
        }

        public int GetAnionKey(byte b)
        {
            return BitAssist.GetTargetBit(4, b);
        }

        public int GetUVKey(byte b)
        {
            return BitAssist.GetTargetBit(5, b);
        }

        public int GetFliterKey(byte b)
        {
            return BitAssist.GetTargetBit(6, b);
        }

        public int GetPM25(byte[] bArr)
        {
            string rst = ConvertByteTo16(bArr[18], false) + ConvertByteTo16(bArr[19], false);
            return Convert.ToInt32(rst, 16);
        }

        public int GetTempuration(byte[] bArr)
        {
            int temp = GetIntInBytes(bArr, 20);
            if (temp > 128)
                return 128 - temp;
            return temp;
        }

        public int GetHumidity(byte[] bArr)
        {
            return GetIntInBytes(bArr, 21);
        }

        public int GetTimerHour(byte[] bArr)
        {
            return GetIntInBytes(bArr, 22);
        }

        public int GetTimerMin(byte[] bArr)
        {
            return GetIntInBytes(bArr, 23);
        }

        public int GetTear(byte[] bArr)
        {
            return GetIntInBytes(bArr, 24); 
        }

        int GetIntInBytes(byte[] bArr, int index)
        {
            return Convert.ToInt32(bArr[index].ToString(), 10); 
        }
    }
}
