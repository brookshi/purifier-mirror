﻿using HOYI.Common;
using HOYI.Common.Data;
using HOYI.Data;
using HOYI.Web.Common;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace HOYI.Web.Services
{
    [WebService(Namespace = "http://hoyi.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class UserService : WebServiceBase
    {
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool Login(string userName, string password)
        {
            HttpContext context = HttpContext.Current;
            context.User = AuthenticationManager.Instance.Login(userName, password);
            return true;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object LoginWithUserInfo(string userName, string password, string client)
        {
            HttpContext context = HttpContext.Current;
            try
            {
                context.User = AuthenticationManager.Instance.Login(userName, password);
            }
            catch { return null; }
            UserLog.Save(client, userName);
            return GetUserInfo();
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool CheckOldPwd(string oldpwd)
        {
            HttpContext context = HttpContext.Current;
            if (context.User != null && (context.User as User).GetAccount().CheckOldPwd(oldpwd))
                return true;
            return false;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool ChangePwd(string oldpwd, string newpwd)
        {
            HttpContext context = HttpContext.Current;
            if (context.User != null)
            {
                Account account = (context.User as User).GetAccount();
                account.SetPassword(newpwd);
                account.Save(false);
                return true;
            }
            return false;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool VerifyEmail(string email)
        {
            return UserRegister.VerifyEmail(email);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool VerifyUserName(string username)
        {
            return UserRegister.VerifyUserName(username);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public Guid RegisterUser(UserRegister user)
        {
            return user.Register();
        }

        public object GetUserInfo()
        {
            var cu = HOYI.Web.Common.User.Current;

            return new { User = cu.Name, isAdmin = cu.IsInRole(Role.AdminRole) };
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public object LoadUserInfo()
        {
            var cu = HOYI.Web.Common.User.Current;
            var menus = (from m in Menu.Menus where m.IsAuthen() select m).ToList();
            
            return new { User = cu.Name, Menus = menus, isAdmin = cu.IsInRole(Role.AdminRole) };
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool Logout()
        {
            AuthenticationManager.Instance.Logout();
            return true;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Account> LoadUsers()
        {
            var isAdmin = CurrentUser.IsInRole(Role.AdminRole);
            if (!isAdmin) return null;
            var users = PersistenceHelper.Default.Query<Account>(new WhereCriterion());
            users.ForEach(u => u.SetPassword("NULL"));
            ListSorter sorter = new ListSorter("UserName", true);
            sorter.Sort(users);
            return users;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<Guid> DeleteUsers(List<Guid> userIds)
        {
            List<Guid> dels = new List<Guid>();
            userIds.ForEach(id => {
                Tools.ExecAction(() =>
                {
                    Account.DelUser(id);
                    dels.Add(id);
                });
            });
            return dels;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public PageResult<Account> LoadPagedUsers(UIPager pager)
        {
            PageResult<Account> result = new PageResult<Account>(pager);
            result.LoadData(new WhereCriterion());
            return result;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool UpdateUser(UserRegister user, string userId)
        {
            user.UpdateUser(Parser.Parse<Guid>(userId));
            return true;
        }
    }
}
