﻿hoYi.register = {
    init: function () {
        var k = $(".suc_kuang");
        k.find("input").keydown(function (e) {
            $(e.target.parentNode).find("span").css("display", "none");
        }).blur((function (et) {
            var tg = et.target;
            var tgn = et.target.name;
            if (this.editData && (tgn == "Email" || tgn == "UserName")) return;
            this.chkObj[tgn] && this.chkObj[tgn]($(tg), true);
        }).bind(this));
        k.find(".regbtn").click(this.regUser.bind(this));
        k.find("input[name='City']").click(k, function (e) {
            hy.cityPk.show(this);
            hy.cityPk.onPicked = (function (shen, shi) {
                this.target.value = (shen != "直辖市" && shen.lastIndexOf("市") == (shen.length - 1)) ? shen + shi : shi;
                this.data.find("input[name='Address']").val((shen + shi).replace("直辖市", ""));
                hy.cityPk.close();
            }).bind(e);
        });
    },
    setData: function (d) {
        this.editData = d;
        $(".suc_kuang .check_tips").hide();
        $(".suc_kuang").find("input").each(function (t) {
            if (this.type == "checkbox")
                this.checked = d ? (d[this.name] == "true" || d[this.name] == true) : false;
            else if (this.type == "password") {
                $(this.parentNode).prev().add(this.parentNode).css("display", d ? "none" : "");
            }
            else {
                this.value = d ? d[this.name] : "";
                if (this.name == "Email" || this.name == "UserName")
                    d ? $(this).attr("readonly", "readonly") : $(this).removeAttr("readonly");
            }
        });
    },
    chkObj: {
        checkName: function (im, act, msg, ck) {
            var mv = im.val();
            var pt = im.parent();
            pt.find("span").css("display", "none");
            if (!mv || $.trim(mv || "").length == 0) {
                pt.find("span:eq(1)").css("display", "inline");
                return false;
            }
            var rul = im.attr("rule");
            var rgx = new RegExp(rul, "ig");
            if (!rgx.test(mv)) {
                pt.find("span:eq(0)").css("display", "inline");
                return false;
            }
            var data = {};
            data[im[0].name.toLowerCase()] = mv;
            if (ck) {
                hoYi.callService("services/userservice.asmx/" + act, data, function (s) {
                    if (!s) return;
                    if (s.d)
                        pt.find("span:eq(1)").css("display", "inline").text(msg);
                    else
                        pt.find("span:eq(2)").css("display", "inline");
                });
            }
            return true;
        },
        Email: function (im, ck) {
            return this.checkName(im, "VerifyEmail", "邮箱已被注册，请更换邮箱", ck);
        },
        UserName: function (im, ck) {
            return this.checkName(im, "VerifyUserName", "用户名已被使用，请更改", ck);
        },
        Password: function (im) {
            var mv = im.val();
            var pt = im.parent();
            pt.find("span").css("display", "none");
            if (!mv || $.trim(mv || "").length == 0) {
                pt.find("span.empty_tip").css("display", "inline");
                return false;
            }
            if (mv.length < 6 || mv.length > 16) {
                pt.find("span.tips_1").css("display", "inline");
                return false;
            }
            if (/^[A-Za-z]+$/ig.test(mv)) {
                pt.find("span.tips_2").css("display", "inline");
                return false;
            }
            if (/^[0-9]+$/ig.test(mv)) {
                pt.find("span.tips_3").css("display", "inline");
                return false;
            }
            var mad = ($("input[name='Email']").val() || "").trim();;
            if (mad.length > 0 && mv.toLowerCase() == mad.toLowerCase()) {
                pt.find("span.tips_4").css("display", "inline");
                return false;
            }
            var mun = ($("input[name='UserName']").val() || "").trim();;
            if (mun.length > 0 && mv.toLowerCase() == mun.toLowerCase()) {
                pt.find("span.tips_5").css("display", "inline");
                return false;
            }
            if (!(/^(?!\D+$)(?![^a-zA-Z]+$)\S+$/ig.test(mv))) {
                pt.find("span.tips_6").css("display", "inline");
                return false;
            }
            return true;
        },
        Repassword: function (im) {
            var mv = im.val();
            var pt = im.parent();
            pt.find("span").css("display", "none");
            var ipwd = $("input[name='Password']").val();
            if (mv != ipwd) {
                pt.find("span.repeat_tip").css("display", "inline");
                return false;
            }
            return true;
        },
        Phone: function (im) {
            if ((im.val() || "").length == 0) return true;
            var tip = $(im).next();
            tip.hide();
            if (!(/[0-9]{11}/).test(im.val())) {
                tip.css("display", "inline");
                return false
            }
            return true;
        },
        Name: function () { return true; },
        Address: function () { return true; },
        City: function () { return true; }
    },
    onRegistered: function () { alert("注册成功") },
    regUser: function (e) {
        var usr = {}, isPassed = true, me = this;
        if (this.editData) $mo(usr, this.editData);
        $(".suc_kuang").find("input").each(function (i) {
            var cv = (this.type.toLowerCase() == "checkbox" ? (this.checked ? "true" : "false") : this.value);
            if (me.editData)
                usr[this.name] = cv;
            else if (isPassed) {
                isPassed = isPassed && (!me.chkObj[this.name] || me.chkObj[this.name]($(this), false));
                usr[this.name] = cv;
            }
        });
        if (this.editData) {
            delete usr.Password;
        }
        if (isPassed) {
            var jb = $(e.target).addClass("reging").attr("disabled", true);
            var onReg = this.onRegistered ? this.onRegistered.bind(this) : null;
            var pmo = { user: usr };
            if (this.editData) pmo.userId = this.editData.Id;
            hoYi.callService("services/userservice.asmx/" + (this.editData ? "UpdateUser" : "RegisterUser"), pmo, function (s) {
                if (CM.isGuid(s.d)) usr.Id = s.d;
                jb.attr("disabled", false);
                jb.removeClass("reging");
                if (s && s.d && onReg) onReg(usr);
            }, function (x, e, o) {
                jb.attr("disabled", false);
                jb.removeClass("reging");
                o && alert(o.Message);
                return !!o;
            });
        }
    }
};
