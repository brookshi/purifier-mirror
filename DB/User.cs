﻿// File:    User.cs
// Author:  iwxiaot
// Created: 2013年12月14日 23:48:15
// Purpose: Definition of Class User

using System;

/// 用户信息表
public class User
{
   public string Id
   {
      get
      {
         return id;
      }
      set
      {
         this.id = value;
      }
   }
   
   public string UserName
   {
      get
      {
         return userName;
      }
      set
      {
         this.userName = value;
      }
   }
   
   public string Password
   {
      get
      {
         return password;
      }
      set
      {
         this.password = value;
      }
   }
   
   public string Name
   {
      get
      {
         return name;
      }
      set
      {
         this.name = value;
      }
   }
   
   public string Address
   {
      get
      {
         return address;
      }
      set
      {
         this.address = value;
      }
   }
   
   public string Contacts
   {
      get
      {
         return contacts;
      }
      set
      {
         this.contacts = value;
      }
   }
   
   public string Company
   {
      get
      {
         return company;
      }
      set
      {
         this.company = value;
      }
   }
   
   public string Department
   {
      get
      {
         return department;
      }
      set
      {
         this.department = value;
      }
   }
   
   public string PurifierIDs
   {
      get
      {
         return purifierIDs;
      }
      set
      {
         this.purifierIDs = value;
      }
   }
   
   public string Permission
   {
      get
      {
         return permission;
      }
      set
      {
         this.permission = value;
      }
   }
   
   /// 用户ID
   public string id;
   /// 用户名
   public string userName;
   /// 密码
   public string password;
   /// 名字
   public string name;
   /// 地址
   public string address;
   /// 联系方式
   public string contacts;
   /// 公司
   public string company;
   /// 部门
   public string department;
   /// 可控制净化器ID集合
   public string purifierIDs;
   /// 权限标记
   public string permission;

}