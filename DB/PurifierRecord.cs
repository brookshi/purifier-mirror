﻿// File:    PurifierRecord.cs
// Author:  iwxiaot
// Created: 2013年12月14日 23:48:15
// Purpose: Definition of Class PurifierRecord

using System;

/// 净化器上传数据记录表
public class PurifierRecord
{
   public string Id
   {
      get
      {
         return id;
      }
      set
      {
         this.id = value;
      }
   }
   
   public string PurifierID
   {
      get
      {
         return purifierID;
      }
      set
      {
         this.purifierID = value;
      }
   }
   
   public string MacAddr
   {
      get
      {
         return macAddr;
      }
      set
      {
         this.macAddr = value;
      }
   }
   
   public string Ip
   {
      get
      {
         return ip;
      }
      set
      {
         this.ip = value;
      }
   }
   
   public string RunTime
   {
      get
      {
         return runTime;
      }
      set
      {
         this.runTime = value;
      }
   }
   
   public string FilterMediumTime
   {
      get
      {
         return filterMediumTime;
      }
      set
      {
         this.filterMediumTime = value;
      }
   }
   
   public float Pm25
   {
      get
      {
         return pm25;
      }
      set
      {
         this.pm25 = value;
      }
   }
   
   public float Temperature
   {
      get
      {
         return temperature;
      }
      set
      {
         this.temperature = value;
      }
   }
   
   public float Humidity
   {
      get
      {
         return humidity;
      }
      set
      {
         this.humidity = value;
      }
   }
   
   public DateTime LastUpdateTime
   {
      get
      {
         return lastUpdateTime;
      }
      set
      {
         this.lastUpdateTime = value;
      }
   }
   
   /// 记录ID
   public string id;
   /// 净化器ID
   public string purifierID;
   /// 净化器网卡Mac地址
   public string macAddr;
   /// 净化器IP
   public string ip;
   /// 运行时间
   public string runTime;
   /// 滤材时间
   public string filterMediumTime;
   /// PM2.5值
   public float pm25;
   /// 温度
   public float temperature;
   /// 湿度
   public float humidity;
   /// 上次更新时间
   public DateTime lastUpdateTime;

}