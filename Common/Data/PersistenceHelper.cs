﻿using System;
using System.Collections.Generic;
using System.Linq;
using HOYI.Common.Data.SqlServer;
using HOYI.Common.Data.Oracle;


namespace HOYI.Common.Data
{
    public abstract class PersistenceHelper : BasePersister
    {

        internal protected DBSetting Setting { get; private set; }


        protected abstract IDataObjPersister CreateDataObjPersister(string dbName);

        protected abstract string GetDbName(Type dataObjType, object objId);

        protected virtual bool SupportEnableMultiDb { get { return true; } }


        protected PersistenceHelper(DBSetting setting)
        {
            Setting = setting;
        }

        protected virtual Dictionary<string, List<T>> GroupDataObjByDb<T>(IEnumerable<T> dataObjs) where T : BaseDataObj
        {
            var dic = new Dictionary<string, List<T>>(StringComparer.OrdinalIgnoreCase);
            foreach (var tObj in dataObjs)
            {
                object objId = GetDataObjIdValue(tObj);
                Type objType = tObj.GetType();
                DataObjInfo doi = DataObjInfo.GetDataObjInfo(objType);
                if (SupportEnableMultiDb && doi.EnableMultiDb && doi.DbCount > 1)
                {
                    if (objId == null || objId.GetHashCode() == 0)
                    {
                        AssignDataObjId(tObj);
                        objId = GetDataObjIdValue(tObj);
                    }
                }
                string dbNm = GetDbName(objType, objId);
                if (!dic.ContainsKey(dbNm))
                {
                    dic[dbNm] = new List<T>();
                }
                dic[dbNm].Add(tObj);
            }
            return dic;
        }

        [ThreadStatic] protected static List<IDataObjPersister> currentPersisters;

        Dictionary<string, IDataObjPersister> InitPersisters<T>(Dictionary<string, List<T>> groupDb) where T : BaseDataObj
        {
            var pers = new Dictionary<string, IDataObjPersister>();
            foreach (var db in groupDb.Keys)
            {
                IDataObjPersister per = CreateDataObjPersister(db);
                pers[db] = per;
            }

            currentPersisters = pers.Values.ToList();
            return pers;
        }

        public int Save<T>(T dataObj) where T : BaseDataObj
        {
            var dic = SaveObjs(new List<T> { dataObj });
            if (dic.ContainsKey(dataObj))
                return dic[dataObj];
            else
                return -1;
        }

        public Dictionary<T, int> Save<T>(IEnumerable<T> dataObjs) where T : BaseDataObj
        {
            return SaveObjs(dataObjs);
        }

        public T Read<T>(object objID) where T : BaseDataObj
        {
            return ReadDataObjByID<T>(objID);
        }

        public List<T> Query<T>(WhereCriterion critical) where T : BaseDataObj
        {
            return QueryDataObjs<T>(critical);
        }

        List<IEnumerator<T>> GetEnumerators<T>(WhereCriterion critical) where T : BaseDataObj
        {
            var enumers = new List<IEnumerator<T>>();
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            int dbCount = (SupportEnableMultiDb && doi.EnableMultiDb) ? doi.DbCount : 1;
            for (int d = 0; d < dbCount; d++)
            {
                string dbName = GetDbName(typeof(T), d);
                var result = CreateDataObjPersister(dbName).EnumerateData<T>(critical);
                enumers.Add(result);
            }
            return enumers;
        }

        public IEnumerator<T> GetEnumerator<T>(WhereCriterion critical) where T : BaseDataObj
        {
            return EnumerateData<T>(critical);
        }

        public int Delete<T>(WhereCriterion critical) where T : BaseDataObj
        {
            return DeleteObjs<T>(critical);
        }

        public int Upate<T>(Dictionary<string, object> values, WhereCriterion critical) where T : BaseDataObj
        {
            return ForeUpate<T>(values, critical);
        }

        private int Delete<T>(T obj, IDataObjPersister per) where T : BaseDataObj
        {
            var r = per.Delete<T>(GetDataObjIdValue(obj));
            if (obj.IsExisted)
                obj.IsExisted = (r == 0);
            return r;
        }

        public Dictionary<T, int> DeleteObjs<T>(IEnumerable<T> dataObjs) where T : BaseDataObj
        {
            var objs = (from tObj in dataObjs where tObj != null select tObj).ToList();
            var groupDb = GroupDataObjByDb(objs);
            Dictionary<string, IDataObjPersister> pers = InitPersisters<T>(groupDb);
            try
            {
                BeginTransaction();
                var resultDic = new Dictionary<T, int>();
                foreach (var db in groupDb.Keys)
                {
                    IDataObjPersister per = pers[db];
                    groupDb[db].ForEach(o => { resultDic[o] = Delete(o, per); });
                }
                Commit();
                objs.ForEach(d => d.ClearChangeds());
                return resultDic;
            }
            catch
            {
                Rollback();
                throw;
            }
        }

        protected abstract string GetConnectionString(IDBSetting setting);
        
       
        public int DeleteObj<T>(object objId) where T : BaseDataObj
        {
            string dbNm = GetDbName(typeof(T), objId);
            currentPersisters = new List<IDataObjPersister>() { CreateDataObjPersister(dbNm) };
            try
            {
                BeginTransaction();
                int r = Delete<T>(objId);
                Commit();
                return r;
            }
            catch
            {
                Rollback();
                throw;
            }
        }

        public List<T> Top<T>(int count, string topOrderName, OrderDirection direct, WhereCriterion critical) where T : BaseDataObj
        {
            return TopObjs<T>(count, topOrderName, direct, critical);
        }

        public List<T> PageObjs<T>(int pageIndex, int pageCount, string orderBy, OrderDirection direct, WhereCriterion critical) where T : BaseDataObj
        {
            int readCount = (pageIndex + 1) * pageCount;
            int begin = pageIndex * pageCount;
            int end = begin + pageCount - 1;
            if (pageIndex > 0)
            {
                var summeries = LoadDataSummery<T>(readCount, orderBy, direct, critical);

                List<DataSummery> pagingSummeries = summeries.FindAll(p =>
                {
                    var i = summeries.IndexOf(p);
                    return i >= begin && i <= end;
                });

                if (pagingSummeries != null && pagingSummeries.Count > 0)
                {
                    var persisters = new List<IDataObjPersister>();
                    foreach (var summ in pagingSummeries)
                    {
                        if (!persisters.Contains(summ.Persister))
                        {
                            persisters.Add(summ.Persister);
                        }
                    }
                    var list = new List<T>();
                    var info = DataObjInfo.GetDataObjInfo(typeof(T));
                    foreach (var p in persisters)
                    {
                        var ids = new List<object>();
                        foreach (var summ in pagingSummeries)
                        {
                            if (summ.Persister == p)
                                ids.Add(summ.ObjId);
                        }
                        list.AddRange(p.QueryDataObjs<T>(new WhereCriterion().IN(info.PrimaryKeyName, ids.ToArray())));
                    }
                    if (list != null && list.Count > 0)
                    {
                        var sorter = new ListSorter(orderBy, direct == OrderDirection.ASC);
                        sorter.Sort(list);
                    }
                    return list;
                }
                else
                    return new List<T>();
            }
            else
            {
                return Top<T>(readCount, orderBy, direct, critical);
            }
        }

        public int Count<T>(WhereCriterion critcal) where T : BaseDataObj
        {
            return CountObjs<T>(critcal);
        }

        public T TopOne<T>(string topOrderName, OrderDirection direct, WhereCriterion critical) where T : BaseDataObj
        {
            var objs = Top<T>(1, topOrderName, direct, critical);
            if (objs != null && objs.Count > 0)
                return objs[0];
            else
                return null;
        }

        protected override int ForeUpate<T>(Dictionary<string, object> values, WhereCriterion critical)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            if (SupportEnableMultiDb && doi.EnableMultiDb)
            {
                int count = 0;
                for (int d = 0; d < doi.DbCount; d++)
                {
                    string dbName = GetDbName(typeof(T), d);
                    count += CreateDataObjPersister(dbName).ForeUpate<T>(values, critical);
                }
                return count;
            }
            else
            {
                string dbName = GetDbName(typeof(T), 0);
                return CreateDataObjPersister(dbName).ForeUpate<T>(values, critical);
            }
        }

        #region IDataObjPersister Members

        protected override Dictionary<T, int> SaveObjs<T>(IEnumerable<T> dataObjs)
        {
            var objs = (from tObj in dataObjs where tObj != null select tObj).ToList();
            var groupDb = GroupDataObjByDb(objs);
            Dictionary<string, IDataObjPersister> pers = InitPersisters<T>(groupDb);
            try
            {
                BeginTransaction();
                var resultDic = new Dictionary<T, int>();
                foreach (var db in groupDb.Keys)
                {
                    IDataObjPersister per = pers[db];
                    var gpRes = per.SaveObjs(groupDb[db]);
                    foreach (var k in gpRes.Keys)
                    {
                        resultDic.Add(k, gpRes[k]);
                    }
                }
                Commit();
                objs.ForEach(d => d.ClearChangeds());
                return resultDic;
            }
            catch
            {
                Rollback();
                throw;
            }
        }

        protected override T ReadDataObjByID<T>(object objID)
        {
            string dbName = GetDbName(typeof(T), objID);
            return CreateDataObjPersister(dbName).ReadDataObjByID<T>(objID);
        }

        protected override List<T> TopObjs<T>(int count, string orderByPropertyName, OrderDirection direction, WhereCriterion critical)
        {
            if (count <= 0) count = 1;
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            var objs = new List<T>();
            if (SupportEnableMultiDb && doi.EnableMultiDb)
            {
                for (int d = 0; d < doi.DbCount; d++)
                {
                    string dbName = GetDbName(typeof(T), d);
                    var result = CreateDataObjPersister(dbName).TopObjs<T>(count, orderByPropertyName, direction, critical);
                    objs.AddRange(result);
                }
            }
            else
            {
                string dbName = GetDbName(typeof(T), 0);
                objs = CreateDataObjPersister(dbName).TopObjs<T>(count, orderByPropertyName, direction, critical);
            }
            if (objs != null && objs.Count > 0)
            {
                var sorter = new ListSorter(orderByPropertyName, direction == OrderDirection.ASC);
                sorter.Sort(objs);
                return objs.Take(count).ToList();
            }
            else
                return objs;
        }

        protected override List<DataSummery> LoadDataSummery<T>(int count, string summeryName, OrderDirection direction, WhereCriterion critical)
        {
            if (count <= 0) count = 1;
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            var objs = new List<DataSummery>();
            if (SupportEnableMultiDb && doi.EnableMultiDb)
            {
                for (int d = 0; d < doi.DbCount; d++)
                {
                    string dbName = GetDbName(typeof(T), d);
                    var result = CreateDataObjPersister(dbName).LoadDataSummery<T>(count, summeryName, direction, critical);
                    objs.AddRange(result);
                }
            }
            else
            {
                string dbName = GetDbName(typeof(T), 0);
                objs = CreateDataObjPersister(dbName).LoadDataSummery<T>(count, summeryName, direction, critical);
            }
            if (objs != null && objs.Count > 0)
            {
                var sorter = new ListSorter("Summery", direction == OrderDirection.ASC);
                sorter.Sort(objs);
                return objs.Take(count).ToList();
            }
            else
                return objs;
        }

        protected override List<T> QueryDataObjs<T>(WhereCriterion critical)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            if (SupportEnableMultiDb && doi.EnableMultiDb)
            {
                var objs = new List<T>();
                for (int d = 0; d < doi.DbCount; d++)
                {
                    string dbName = GetDbName(typeof(T), d);
                    var result = CreateDataObjPersister(dbName).QueryDataObjs<T>(critical);
                    objs.AddRange(result);
                }
                return objs;
            }
            else
            {
                string dbName = GetDbName(typeof(T), 0);
                return CreateDataObjPersister(dbName).QueryDataObjs<T>(critical);
            }
        }

        protected override int CountObjs<T>(WhereCriterion critical)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            int count = 0;
            if (SupportEnableMultiDb && doi.EnableMultiDb)
            {
                var objs = new List<T>();

                for (int d = 0; d < doi.DbCount; d++)
                {
                    string dbName = GetDbName(typeof(T), d);
                    count += CreateDataObjPersister(dbName).CountObjs<T>(critical);
                }
            }
            else
            {
                string dbName = GetDbName(typeof(T), 0);
                count = CreateDataObjPersister(dbName).CountObjs<T>(critical);
            }
            return count;
        }

        protected override int DeleteObjs<T>(WhereCriterion critical)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(typeof(T));
            if (SupportEnableMultiDb && doi.EnableMultiDb)
            {
                int res = 0;
                for (int d = 0; d < doi.DbCount; d++)
                {
                    string dbName = GetDbName(typeof(T), d);
                    int result = CreateDataObjPersister(dbName).DeleteObjs<T>(critical);
                    res += result;
                }
                return res;
            }
            else
            {
                string dbName = GetDbName(typeof(T), 0);
                return CreateDataObjPersister(dbName).DeleteObjs<T>(critical);
            }
        }

        protected override int Delete<T>(object objId)
        {
            if (currentPersisters != null)
            {
                int i = 0;
                currentPersisters.ForEach(dop => i = dop.Delete<T>(objId));
                return i;
            }
            return -1;
        }

        protected override void BeginTransaction()
        {
            if (currentPersisters != null)
            {
                currentPersisters.ForEach(dop => dop.BeginTransaction());
            }
        }

        protected override void Commit()
        {
            if (currentPersisters != null)
            {
                currentPersisters.ForEach(dop => dop.Commit());
                currentPersisters = null;
            }
        }

        protected override void Rollback()
        {
            if (currentPersisters != null)
            {
                currentPersisters.ForEach(dop => dop.Rollback());
            }
        }

        protected override IEnumerator<T> EnumerateData<T>(WhereCriterion critical)
        {
            var bEnus = GetEnumerators<T>(critical);
            try
            {
                foreach (var be in bEnus)
                {
                    while (be.MoveNext())
                    {
                        yield return be.Current;
                    }
                }
            }
            finally
            {
                bEnus.ForEach(e => e.Dispose());
            }
        }

        #endregion


        readonly static Dictionary<string, PersistenceHelper> s_Helpers = new Dictionary<string, PersistenceHelper>(StringComparer.OrdinalIgnoreCase);

        public static void InitDefault(DBSetting setting)
        {
            if (setting == null)
                throw new ArgumentNullException("setting");
            InitHelper("Default", setting);
        }

        public static void InitHelper(string name,DBSetting setting)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentNullException("name");
            if (setting == null)
                throw new ArgumentNullException("setting");
            lock (s_Helpers)
            {
                if (s_Helpers.ContainsKey(name)) return;
                s_Helpers[name] = Create(setting);
            }
        }

        [ThreadStatic]
        internal static PersistenceHelper Current;

        public static PersistenceHelper Get(string name)
        {
            lock (s_Helpers)
            {
                if (!s_Helpers.ContainsKey(name))
                    throw new ArgumentException("Do not init helper '" + name + "'");
                var hlp = s_Helpers[name];
                Current = hlp;
                return hlp;
            }
        }

        public static PersistenceHelper Default { get { return Get("Default"); } }

        public static DateTime MinDateTime { get { return DateTime.SpecifyKind(DateTime.Parse("1900-01-01"), DateTimeKind.Local); } }

        static PersistenceHelper Create(DBSetting setting)
        {
            return (setting.DbType == DbProvider.Oracle) ? (PersistenceHelper)new OraclePersistenceHelper(setting) : new SqlServerPersistenceHelper(setting);
        }

        public static string GetKeyDbName(Type dataObjType)
        {
            DataObjInfo doi = DataObjInfo.GetDataObjInfo(dataObjType);
            return doi.GetPrimaryKeyDbName();
        }
    }
}
